### SET FOLDER TO WATCH + FILES TO WATCH + SUBFOLDERS YES/NO
   $watcher = New-Object System.IO.FileSystemWatcher
   $watcher.Path = $PSScriptRoot + "\source"
   $watcher.Filter = "?*.*?"
   $watcher.IncludeSubdirectories = $true
   $watcher.EnableRaisingEvents = $true  

### DEFINE ACTIONS AFTER A EVENT IS DETECTED
   $action = { 
                Write-Host "Build started.."
                #cmd /c "make.bat" clean
                cmd /c "make.bat" html
				Write-Host "Build done." 
             }    
### DECIDE WHICH EVENTS SHOULD BE WATCHED + SET CHECK FREQUENCY  
   $created = Register-ObjectEvent $watcher "Created" -Action $action
   $changed = Register-ObjectEvent $watcher "Changed" -Action $action
   $deleted = Register-ObjectEvent $watcher "Deleted" -Action $action
   $renamed = Register-ObjectEvent $watcher "Renamed" -Action $action
   while ($true) {sleep 1}