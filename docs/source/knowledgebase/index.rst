##############
Knowledge Base
##############

This is the Knowledge Base. Here you can find articles how to solve certain errors, questions and
other solutions.

================================== ====
KB     
================================== ====
`100000 <articles/kb100000.html>`_ The type 'Object' is defined in an assembly that is not referenced. You must add a reference to assembly 'netstandard, Version=2.0.0.0, Culture=neutral, PublicKeyToken=cc7b13ffcd2ddd51'.
`100001 <articles/kb100001.html>`_ NuGet package content files which are added through Paket, can be added as resource type in Visual Studio in stead of content type.
================================== ====