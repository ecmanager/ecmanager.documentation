####################
Securing your plugin
####################

If you create a plugin, you want this plugin to be just as secure as any other CMS page. 
There are a few methods to secure your page, which are described on this page.

Securing your plugin page
=========================
In order to allow access to a new plugin page, you should let your controller inherit from 
AuthenticatedBaseController. Any controller which inherits from AuthenticatedBaseController 
requires an authorized user in order to access it.

.. code-block:: C#
	:linenos:

	public class CustomController : AuthenticatedBaseController
	{
	    // Your controller methods
	}

This method only checks if there is currently an authorized user, not if this user has any 
specific permissions.

Securing your plugin methods
============================
You can choose to secure a specific method in order to require certain permissions to call this
method. This can be done using the EcManagerPrincipalPermission attribute. The following example shows how you could secure your 
controller methods:

.. code-block:: C#
	:linenos:

	public class CustomController : AuthenticatedBaseController
	{
	    [EcManagerPrincipalPermission(SecurityAction.Demand, Operation = ResourceOperations.Read, Resource = "CustomResource")]
	    public ActionResult Get(int id)
	    {
	        // Your logic
	    }
	}

.. note:: You can add multiple EcManagerPrincipalPermission attributes to a method.

Security checks in your logic
=============================
You might want to perform a security check in your logic to see if the current user has permission to 
manipulate a certain resource. You can do this as follows:

.. code-block:: C#
	:linenos:

	try 
	{
	    AuthorizationHelper.CheckPermission("CustomResource", ResourceOperations.Delete);
	}
	catch (SecurityException)
	{
	    // User doesn't have the right permission, do your error handling
	}

This line of code checks if the current user has the permission to delete a CustomResource. If this is 
not the case, a SecurityException is thrown.