#########
Extension
#########

.. toctree::
    :maxdepth: 1

    adding-a-custom-page-to-the-ecManager-menu
    creating-CMS-plugin-pages
    creating-CMS-plugin-section
    securing-your-plugin
