##########################################
Adding a custom page to the ecManager menu
##########################################

When you create a custom plugin page for ecManager, you need to integrate these pages with the menu.
The first step is creating your own section in the menu. We will create one called 'My custom pages'. Below these pages we will create three pages as shown in the image below.

.. image:: /_static/img/how-to/extend-the-ecManager-CMS/extend-the-ecManager-CMS.png
	:scale: 90%
	:align: center

The first step is to open the database, and navigate to the table cms.Pages.
First find the ID of the root of the navigation. You can locate this entry by querying for the item where the field ParentId is NULL and the URL is '~/home/'.
We will refer to this id as RootNavigationId in the upcoming examples. 

Then we will start editing the table by adding a set of rows.

+-------------------+------------------+-------------------------+-------------------------------------------------------------------------------------------------------+-------------------------------------------+
| Id names          | ParentId         | Url                     | Resources                                                                                             | Remarks                                   |
+===================+==================+=========================+=======================================================================================================+===========================================+
| SectionPageId     | RootNavigationId | #                       | http://schemas.ecmanager.nl/ecmanager/2012-1/cms/web/controllers/my-unique-section-resource           | This is the page respresenting the entity |
+-------------------+------------------+-------------------------+-------------------------------------------------------------------------------------------------------+-------------------------------------------+
| CustomPageId      | SectionPageId    | ~/CustomPageOne/Index   | http://schemas.ecmanager.nl/ecmanager/2012-1/cms/web/controllers/my-unique-custom-page-one-resource   | This is Custom Page One                   |
+-------------------+------------------+-------------------------+-------------------------------------------------------------------------------------------------------+-------------------------------------------+
| CustomPageTwoId   | SectionPageId    | ~/CustomPageTwo/Index   | http://schemas.ecmanager.nl/ecmanager/2012-1/cms/web/controllers/my-unique-custom-page-two-resource   | This is Custom Page Two                   |
+-------------------+------------------+-------------------------+-------------------------------------------------------------------------------------------------------+-------------------------------------------+
| CustomPageThreeId | SectionPageId    | ~/CustomPageThree/Index | http://schemas.ecmanager.nl/ecmanager/2012-1/cms/web/controllers/my-unique-custom-page-three-resource | This is Custom Page Three                 |
+-------------------+------------------+-------------------------+-------------------------------------------------------------------------------------------------------+-------------------------------------------+

After creating these pages, you need to define the texts for the pages. You can do this by adding the texts to the dbo.PageTexts table. Add a text for every language availiable in the CMS.
Use the ID's SectionPageId, CustomPageOneId, CustomPageTwoId and CustomPageThreeId for adding the texts.

Adding resources
================

After adding these pages to the CMS, we need to add the pages to the permission system in ecManager. 
Navigate to the cms.Resources table and open it in editing mode.

Add the following rows:

+---------------------------+----------+-------------------+-------------------------------------------------------------------------------------------------------+
| Id                        | Category | Name              | Resource                                                                                              |
+===========================+==========+===================+=======================================================================================================+
| CustomSectionResourceId   | 20       | My custom section | http://schemas.ecmanager.nl/ecmanager/2012-1/cms/web/controllers/my-unique-section-resource           |
+---------------------------+----------+-------------------+-------------------------------------------------------------------------------------------------------+
| CustomPageOneResourceId   | 20       | Custom page one   | http://schemas.ecmanager.nl/ecmanager/2012-1/cms/web/controllers/my-unique-custom-page-one-resource   |
+---------------------------+----------+-------------------+-------------------------------------------------------------------------------------------------------+
| CustomPageTwoResourceId   | 20       | Custom page two   | http://schemas.ecmanager.nl/ecmanager/2012-1/cms/web/controllers/my-unique-custom-page-two-resource   |
+---------------------------+----------+-------------------+-------------------------------------------------------------------------------------------------------+
| CustomPageThreeResourceId | 20       | Custom page three | http://schemas.ecmanager.nl/ecmanager/2012-1/cms/web/controllers/my-unique-custom-page-three-resource |
+---------------------------+----------+-------------------+-------------------------------------------------------------------------------------------------------+

.. note:: Make sure the resources in the Resources table match the resources in the Pages table.

The permission system will automatically map the resource to the page using the Resource while building the menu.
You currently have no permission to view the pages, so you can't see the menu's right now.

Making the resources maintainable in the CMS
============================================

After creating the resources in the database, we need to make them visible on the Role detail page in the CMS.
You can do this by creating resource relations. Open the database and navigate to the cms.ResourceRelations table. Open this table, and add the following rows. 

+-------------------------+---------------------------+------+
| ParentResourceId        | ChildRecourceId           | Rank |
+=========================+===========================+======+
| 1                       | CustomSectionResourceId   | 100  |
+-------------------------+---------------------------+------+
| CustomSectionResourceId | CustomPageOneResourceId   | 10   |
+-------------------------+---------------------------+------+
| CustomSectionResourceId | CustomPageTwoResourceId   | 20   |
+-------------------------+---------------------------+------+
| CustomSectionResourceId | CustomPageThreeResourceId | 30   |
+-------------------------+---------------------------+------+

ParentResourceId 1 is the parent of the resource for the Application (Resource: http://schemas.ecmanager.nl/2012-1/identity/resources/application). In case this has a different ID in your cms.Resources table you should modify this value.
After adding these entries, you should be able to view this in the Role detail screen.

.. note:: **Testing the permissions** After modifying the permissions for your role, you need to log out and log in again.

