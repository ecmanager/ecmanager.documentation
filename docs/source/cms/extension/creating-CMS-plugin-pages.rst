#########################
Creating CMS plugin pages
#########################

Introduction
============
In order to manage your custom business logic from the CMS you can create a set of custom pages in 
the CMS. On this page we will describe how you can create your own page that will be working in the 
CMS.

Concept
=======
The concept of your custom page is very simple. You create your own set of pages, and compile them 
into a single DLL file. Make sure the CMS references the DLL, and your pages will be available. 
We use  WebActivatorEx and Autofac to resolve your controllers and inject the registered modules in 
the constructor of the controller.

When you create a new  ASP.NET MVC 5 Web Application, you build your controllers and views. You need
to make sure there is a reference from the CMS to your application.

Prerequisites
=============
- Visual Studio 2013 or higher
- MVC 5
- ecManager.Cms.Plugins.Template

Getting started
===============
In the following sections we will explain how you can create your own pages in detail. To help you 
create CMS plugin pages faster we created a template. This template will give you a Lister and a
Detail page with a sample model.

The first step is to clone the template repository. At this moment only partners are allowed to 
clone the template repository and receive read access to the repositories. If you are a partner
and want access, please contact your partner mananager.

.. code-block:: text
	:linenos:
	
	git clone https://bitbucket.org/ecmanager/ecmanager.cms.plugins.template.git .
 
Page layout
===========
To help you compose your custom page there is a screenshot with a basic CMS page and all the
components highlighted. This will give you more insight in the way the CMS works and how you can use
the default functionalities.

.. figure:: ../../_static/img/how-to/creating-CMS-plugin-pages/creating-CMS-plugin-pages-layout.png
	:alt: Page layout

	Page layout
  
Structure
=========
The structure of the template project is the same as any MVC project. With a couple of extra files 
and settings. 

.. figure:: ../../_static/img/how-to/creating-CMS-plugin-pages/creating-CMS-plugin-pages-template-structure.png
	:alt: Template Structure
	:align: right
	:scale: 90%

	CMS Plugin template structure

**AssemblyInfo.cs**

In the AssemblyInfo.cs is the following line added to allow the CMS to scan for only DLLs with
Autofac modules. 

.. code-block:: C#
	:linenos:
	
	[assembly: AutofacActivator]


**RazorGeneratorMvcStart**

In the RazorGeneratorMvcStart.cs file the views are compiled in the DLL. We use WebActivatorEx to
trigger the *Start()* method. In the start method we use RazorGenerator.MVC to compile the views
into the DLL.


**Controllers**

The template has a sample controller with the name RewriteThisController. You can name this to
whatever you wish. The controller is a default MVC controller with the exception it does not extend 
from *Controller* but from ``ecManager.Cms.UI.Controllers.AuthenticatedBaseController``. This will
give you access to basic CMS functionalities and authentication. It can be needed to force
additional permissions on actions of the controller, how to secure your custom pages can be read on
`Securing your plugin <securing-your-plugin.html>`_.

When creating a controller you may need you own logic or services. The services and logics are 
injected using constructor injection. You can register you logics in the AutofacModule.cs using the
*ContainerBuilder*.

**Models**

There are two Models included in the template. *RewriteThisListViewModel* is the sample model for
the lister. It implements the IFilterableListViewModel to let the model integrate with default 
lister functionalities for example the Grid of MvcContrib. The second model
*RewriteThisDetailViewModel* should extend the DetailViewModel or MultilingualDetailViewModel to
integrate with the default detail functionalities.

**Views**

The views are basic MVC views with MvcContrib as addition for Grids. The template has three sample 
views. One for the lister (Index.cshtml), on for the detail (Detail.cshmtl) and finally one for the 
delete confirmation (Delete.cshtml). Every pagina can be devided into sections to help users find
there data faster. These sections are shown in the side menu. Use the BeginSection HTML helper to 
start sections.