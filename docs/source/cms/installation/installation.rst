############
Installation
############

The following page describes how you should install and update the CMS.

Installing the CMS
==================

When installing the CMS we are starting off with a blank solution. 
Then use the following steps to install the CMS.

1. Open the wizard to add a new project
	
2. Select a C# ASP.NET Web Application. Use the template called 'Empty'.
	
	.. image:: /_static/img/how-to/installing-the-cms/project-type.gif
		:scale: 100%
		:align: center
		
3. Remove the following files:

	* Web.Debug.config
	* Web.Release.config
		
4. Run the following command in the Nuget Package Manager Console.

	.. code-block:: Powershell
		
		Install-Package ecManager.CMS.UI.Web
		
	.. note::
	
		The Install-Package instruction takes a few minutes due to the sheer amount of content that
		needs to be installed.
		
	During the installation a conflict will occur. You can resolve this by inserting 'A' or 'Y'.
	
	.. image:: /_static/img/how-to/installing-the-cms/file-conflict.png
		:scale: 100%

Installing modules
------------------

The next step would be installing and configuring the required modules. In previous releases the CMS
was delivered with a default configuration for the modules. As explained in the section called 
modules `Configuration files`_ this is no longer the case. This section will list which modules need 
to be installed and describes the default module as configured in Modules.example.config. You can 
add the custom modules instead of the items listed below where needed. Please make sure you update 
the Modules.config with the new module details. Start off by creating a copy of the 
Modules.example.config file, and name it Modules.config.

1. Install an auditing module.

	.. code-block:: Powershell
		
		Install-Package ecManager.Modules.AuditLoggingProvider.LLBL
		
2. Install a logging module.

	.. code-block:: Powershell
		
		Install-Package ecManager.Modules.LoggingProvider.LLBL
		
3. Install a Geolocator module.

	.. code-block:: Powershell
		
		Install-Package ecManager.Modules.GeoLocator.Google
		
4. Install a Navigation module.

	.. code-block:: Powershell
		
		Install-Package ecManager.Modules.Navigation.DefaultNavigationProvider

5. Install an EntityState module 

	.. code-block:: Powershell
		
		Install-Package ecManager.Modules.EntityState.DefaultStateLogic
		
6. Install a SEO module.

	.. code-block:: Powershell
		
		Install-Package ecManager.Modules.Seo.DefaultSeoModule
		
7. Install a UrlFormatting module.

	.. code-block:: Powershell
		
		Install-Package ecmanager.Modules.UrlFormatting.DefaultUrlLogic
		
8. Install an ImportExport module.

	.. code-block:: Powershell
		
		Install-Package ecManager.Modules.ImportExport.ImportExportModule
		
9. Install an Basket module.

	.. code-block:: Powershell
		
		Install-Package ecManager.Modules.Basket.DefaultBasketLogic

9a. Install an additional Basket modules.
	
	* Basket calculation module
	* Basket check module 
	* Convert Basket-to-Order module
	* Basket Voucher Logic
	* Basket Promotion Logic
	
	.. note:: 
	
		These modules are included in the DefaultBasketLogic package installed in the previous step.
		No separate installation is required for this module.
		
10. Install an ImageResizer module.

	.. code-block:: Powershell
		
		Install-Package ecManager.Modules.ImageResizer.DefaultImageResizer
		
11. Install a VideoSnapshot module.

	.. code-block:: Powershell
		
		Install-Package ecManager.Modules.VideoSnapshot.DefaultVideoSnapshotCreator
		
12. Install a StorageProvider module.
	
	.. code-block:: Powershell
		
		Install-Package ecManager.Modules.StorageProvider.FileSystem

13. Install a PredicateExpressionProvider module.

	.. code-block:: Powershell

		Install-Package ecManager.Modules.PredicateExpressionProvider.DefaultPredicateExpressionProvider

14. Install a caching module.

	.. code-block:: Powershell

		Install-Package ecManager.Modules.Cache.MemoryCache


Nuget package contents
======================
The CMS is distributed in a Nuget packages. In addition to the assemblies a lot of 'content'-files 
are installed, for example Javascript files and the Global.asax file. This content should be added 
to your source control.

.. note:: 

	In previous releases a lot of '.compiled' were provided as content. These files ended up in your
	bin folder. The views are now fully integrated into the assembly and will no longer be distributed.

Configuration files
===================
In previous releases the CMS was deployed with a set of 'format' configs like web.format.config. 
These files were written so they could be processed using NAnt and we realize that might not be the
tool you prefer. There are also scenario's where you need to modify these configs for custom modules 
or other features. During a Nuget update these changes would be overwritten.

In order to solve these problems the 'format' files have been renamed to 'example' files. You can find
a table describing the changes below.  When using a tool like NAnt to proccess configs, it is 
recommended to duplicate the 'example' file(web.example.config) and call that duplicate 'format'
(web.format.config). You can modify this file to your needs. When you aren't using a processing tool 
like NAnt you can just name the config web.config. Remove the NAnt tags and modify the file to your 
needs. 

.. note::

	We strongly suggest that you add the example files to your source control system so it becomes 
	easier to keep track of changes.

+----------------------------------+-----------------------------------+
| Old filename                     | New file name                     |
+==================================+===================================+
| web.format.config                | web.example.config                |
+----------------------------------+-----------------------------------+
| FeedDataDefinition.format.config | FeedDataDefinition.example.config |
+----------------------------------+-----------------------------------+
| Modules.config                   | Modules.example.config            |
+----------------------------------+-----------------------------------+
| DynamicDataSource.config         | DynamicDataSource.example.config  |
+----------------------------------+-----------------------------------+

Updating the ecManager CMS
==========================
When updating the CMS to a new version you can use the folling command. The *-Reinstall* option is necessary to update the content from the packages. 

.. code-block:: Powershell
	
	Update-Package -Reinstall

Troubleshooting
===============
Runtime error on the navigation lister page
-------------------------------------------
When you see a runtime error on the navigation page related to Ext.NET, you should make sure that 
the nuget.org feed is the last one in your list of package sources. Clear the NuGet cache and delete
the restored packages, then perform a NuGet restore again. This time you should see no errors.