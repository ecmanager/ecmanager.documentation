########
Overview
########

Introduction
============
The CMS has support for dashboards and widgets. You can add widgets to display useful information
on the homepage of the CMS. Every user of the CMS has its own personal dashboard. This section
will describe how to get started with widgets in the CMS.

There are a few widgets that are installed in the CMS by default. These are located in the 
``ecManager.Cms.Widgets`` assembly. You can check out the source code for inspiration to `create your
own widgets <creating-widgets.html>`_.

Widget settings
===============
Each type of widget has its own collection of settings. These are stored as JSON in the database.
This allows widgets to be very flexible.

Disabling widget types
======================
If you don't want a specific type of widget to be available to your customers, simply don't import
it in your final JavaScript bundle. This will prevent the widget type to be selected by users of
the CMS.

To disable a widget type, open the file ``Scripts/dashboard.ts`` and, for example, remove these lines:

.. code-block:: javascript
	:linenos:

	dashboard.registerWidget(
	    "WIDGET_RSSFEED",
	    RssFeedWidgetComponent,
	    RssFeedWidgetSettingsComponent);

Don't forget to remove the unused imports.

Google Analytics
================
Because we think Google Analytics is an interesting source of information for a widget, there is
configuration for it shared across widgets. There is a default widget which displays the current
visitors for a view in GA which uses this configuration.

You can add the following configuration to add websites for GA:

.. code-block :: XML

	<ecManager.Cms.Widgets>
	  <googleAnalytics>
	    <websites>
	      <website name="example" viewId="12345678" keyPath="App_Data/example.json" />
	    </websites>
	  </googleAnalytics>
	</ecManager.Cms.Widgets>

A website element contains a name which will be displayed in the widget, a view id retrieved from GA,
and a path to a service account key, relative from the application root. To retrieve the key, follow
these steps:

* Go to the `Google developer console <https://console.developers.google.com/>`_ 
* Click on 'Select project' select the project you want to use.
* In the API library, search for the Google Analytics API and enable it.
* On the credentials screen, click 'Create credentials' and select 'Service account key'.
* Select JSON as the key and then save.

After these steps, you will be able to download the JSON service account key.

.. note:: The service account will need the correct permissions in order to retrieve data.