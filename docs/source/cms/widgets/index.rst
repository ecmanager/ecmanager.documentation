######################
Dashboards and widgets
######################

.. toctree::
    :maxdepth: 1
   
    overview
    creating-widgets
