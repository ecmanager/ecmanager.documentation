################
Creating widgets
################

Introduction
============
The dashboard system in the ecManager CMS was built with the intention to let developers create 
their own widgets. ecManager offers utilities to let you easily create beautiful widgets which can
display charts, graphs or any other information of your choice. To get started, install the 
following NuGet package in the project in which you want to create a widget:

.. code-block:: PowerShell

	PM> Install-Package ecManager.Cms.Widgets.Core

In this section, we will describe how you can create a widget from scratch using the existing 
``RssFeed`` widget as an example.

.. note:: The front-end examples are written in TypeScript, because we encourage you to use this. You can also write JavaScript if you prefer.

Requirements
============
* NPM
* Knowledge of React
* Knowledge of JavaScript or TypeScript

Settings
========
Each instance of a widget has some settings which should be configurable in the CMS by the user. The
``WidgetSettings`` class will contain these settings.

.. code-block:: C#
	:linenos:

	public class RssFeedWidgetSettings : WidgetSettings
	{
	    [JsonProperty("title")]
	    public string Title { get; set; }

	    [JsonProperty("rssFeedLink")]
	    public string RssFeedLink { get; set; }
	}

Widget
======
The widget class contains some metadata about the type of widget, like the name and size. The 
``Type`` property should be a unique value. We use .resx files for the name and description, so they
are  shown in the right language for the user.

.. code-block:: C#
	:linenos:

	public class RssFeedWidget : Widget<RssFeedWidgetSettings>
	{
	    public const string Identifier = "WIDGET_RSSFEED";

	    public override string Type => Identifier;
	    public override string Name => Resources.Resources.Name;
	    public override string Description => Resources.Resources.Description;
	    public override int Width => 2;
	    public override int Height => 2;
	}

The dashboard has a total of 6 columns. You can use this to determine the width and height. Widgets
are not resizable.

Controller
==========
A widget requires a controller to retrieve its data when it is rendered. It should return some JSON
which the JavaScript can then handle. It is also a good idea to think of some error handling
scenarios. For example, if null is returned by the controller the user will see a message saying
the widget needs to be configured.

.. code-block:: C#
	:linenos:

	[Authorize]
	public class RssFeedWidgetController : Controller
	{
	    public ActionResult Content(RssFeedWidgetSettings settings)
	    {
	        if (settings == null || string.IsNullOrWhiteSpace(settings.RssFeedLink))
	        {
	            return new JsonNetResult(null);
	        }
	        // Build a response.
	        return new JsonNetResult(response);
	    }
	}

.. note:: Remember to add the ``Authorize`` attribute, otherwise anonymous users will be able to access the controller.

Widget React component
======================
A widget component is what will be rendered after the CMS user adds a new widget to the dashboard.
As seen in the following example, the ``props`` of the component will always be of type 
``WidgetComponentProps``. ``this.props.settings`` contains the settings defined by the user.

To build your widget, you should install the following NPM packages:

.. code-block:: batch

	npm install @ecmanager/ecmanager-cms-dashboard @ecmanager/ecmanager-cms-widgets --save

.. note:: For the complete example, check out the source code. There you will also find how we render our widgets and how we deal with translations.

For simplicity's sake, we only included the necessary code in the following example:

.. code-block:: javascript
	:linenos:

	import { WidgetComponentProps } from '@ecmanager/ecmanager-cms-dashboard';
	import { WidgetMessage, WidgetTitle } from '@ecmanager/ecmanager-cms-widgets';
	import { isEqual } from 'lodash-es';
	import * as React from 'react';
	
	export interface RssFeedWidgetComponentState {
	    isLoading: boolean;
	    isEmpty: boolean;
	    isError: boolean;
	    title: string;
	    imageLink: string;
	    items: RssFeedWidgetComponentItem[];
	}
	
	export interface RssFeedWidgetComponentItem {
	    title: string;
	    link: string;
	    publicationDate: Date;
	    author: string;
	}
	
	export class RssFeedWidgetComponent extends React.Component<WidgetComponentProps, RssFeedWidgetComponentState> {
	
	    constructor(props: WidgetComponentProps) {
	        super(props);

	        this.state = {
	            isLoading: true,
	            isEmpty: true,
	            isError: false,
	            title: '',
	            imageLink: this.props.settings && this.props.settings.imageLink ? this.props.settings.imageLink : '',
	            items: []
	        };
	    }
	
	    public componentDidMount() {
	        if (this.props && this.props.settings && this.props.settings.rssFeedLink) {
	            this.getData(this.props);
	        } else {
	            this.setState({ ...this.state, isLoading: false, isEmpty: true });
	        }
	    }
	
	    public shouldComponentUpdate(nextProps: WidgetComponentProps, nextState: RssFeedWidgetComponentState): boolean {
	        return !isEqual(this.props, nextProps) || !isEqual(this.state, nextState);
	    }

	    public componentDidUpdate(prevProps: WidgetComponentProps, prevState: RssFeedWidgetComponentState) {
	        if (this.props && this.props.settings && this.props.settings.rssFeedLink) {
	            if (isEqual(this.state, prevState)) {
	                this.getData(this.props);
	            }
	        } else {
	            this.setState({ ...this.state, isLoading: false, isEmpty: true });
	        }
	    }
	
	    public render() {
	        return ({/* HTML */});
	    }
	
	    private getData(props: WidgetComponentProps) {
	        const queryString = new URLSearchParams();
	        if (props.settings) {
	            Object.keys(props.settings).forEach(key => queryString.append(key, props.settings[key]));
	        }

	        fetch(`/rssfeedwidget/content/?${queryString}`, { credentials: 'same-origin' })
	            .then(response => {
	                if (response.ok) {
	                    return response.json();
	                }
	                throw new Error();
	            })
	            .then(response => this.setState({ ...this.state, isLoading: false, isEmpty: false, items: response.items, title: response.title }))
	            .catch(err => this.setState({ ...this.state, isLoading: false, isError: true }));
	    }
	}

This is a fairly straightforward React component which uses the React lifecycle methods like ``componentDidMount`` 
and ``render``. When the component is being initalized, it probably doesn't have all the required
data to render the final widget.

The ``getData`` function retrieves data from the controller that was created earlier. It passes the
settings to the controller as query string parameters. It then renders the component based on the
result of the AJAX call.

It is a good idea to implement the ``componentDidMount``, ``shouldComponentUpdate`` and 
``componentDidUpdate`` functions as shown in the example. This ensures that widgets only load data
when it is mounted initially and when its props change (e.g. after saving settings for the widget).

Widget settings React component
===============================
A widget should also have a setting component, where a user can customize the widget. In the example,
a user can configure the URL of an RSS feed and a title.

.. code-block:: javascript
	:linenos:

	export interface RssFeedWidgetSettingsComponentState {
	    isLoading: boolean;
	    isError: boolean;
	    title: string;
	    rssFeedLink: string;
	}
	
	export class RssFeedWidgetSettingsComponent extends React.Component<WidgetSettingsComponentProps, RssFeedWidgetSettingsComponentState> {

	    constructor(props: WidgetSettingsComponentProps) {
	        super(props);

	        this.state = {
	            isLoading: true,
	            isError: false,
	            title: props.settings && props.settings.title ? props.settings.title : '',
	            rssFeedLink: props.settings && props.settings.rssFeedLink ? props.settings.rssFeedLink : ''
	        };

	        this.handleTitleChange = this.handleTitleChange.bind(this);
	        this.handleRssFeedLinkChange = this.handleRssFeedLinkChange.bind(this);
	        this.handleSave = this.handleSave.bind(this);
	    }

	    public componentDidMount() {
	        this.getData();
	    }
	
	    public render() {
	        return ({/* HTML form */});
	    }

	    private handleTitleChange(event: React.ChangeEvent<HTMLInputElement>) {
	        this.setState({ ...this.state, title: event.target.value });
	    }

	    private handleRssFeedLinkChange(event: React.ChangeEvent<HTMLInputElement>) {
	        this.setState({ ...this.state, rssFeedLink: event.target.value });
	    }

	    private handleSave(event: React.MouseEvent<HTMLInputElement>) {
	        event.preventDefault();

	        const newSettings = {
	        	...this.props.settings,
	        	title: this.state.title || null,
	        	rssFeedLink: this.state.rssFeedLink || null
	        };

	        this.props.onSave(this.props.widgetId, newSettings);
	    }

	    private getData() {
	        fetch('/rssfeedwidget/settings/', { credentials: 'same-origin' })
	            .then(response => {
	                if (response.ok) {
	                    return response.json();
	                }

	                throw Error();
	            })
	            .then(response => {
	                this.setState({ ...this.state, isLoading: false });
	            })
	            .catch((err) => this.setState({ ...this.state, isLoading: false, isError: true }));
	    }
	}

In the settings component example you can see that this is a simple React component with a few
event handlers. When saving settings, you must call the ``this.props.onSave`` function. This will
cause the settings to be persisted, the settings to be closed and the widget to be reloaded.

Registering your widget
=======================
Open the ``Scripts/dashboard.ts`` file in your CMS project and add your registration in the same 
fashion as the others. You will have to add an import for your components. Make sure the 
registration is added before the call to ``dashboard.initialize();``.

.. note:: The identifier specified in the ``registerWidget`` method should match the ``Type`` property of the C# widget class!

Bundling
========
Because you added new TypeScript or JavaScript, it should be added to the bundle which is created by
webpack. Run ``npm run build`` which should trigger webpack. You should now be able to use your 
newly created widget.

You can tweak the build settings by editing ``.babelrc``, ``tsconfig.json`` and 
``webpack.config.js``. We recommend trying to use the default build settings, because this will make
future migrations easier.
