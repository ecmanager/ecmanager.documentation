#############
Contributions
#############

This page explains the steps needed to make a contribution to Fundatie or an ecManager Module. At
this moment only known partners are allowed to make contributions and receive read access to the 
repositories. If you are a partner and want access please contact 
your partner mananager.

---------------------------
Choose the right repository
---------------------------

The first step in making a contribution is choosing in which repository or repositories to code is 
you want to change. This can by done be search in the repositories to find the code you think
needs to be improved. When you need help with finding the right repository you can ask this by 
creating a issue with the type question.

-------------------
Fork the repository
-------------------

You can make changes to our repositories by forking the repositories and creating a pull request.
This `Forking a Repository <https://confluence.atlassian.com/bitbucket/forking-a-repository-221449527.html>`_
will help you with forking a repository.

To have a bigger chance of us accepting your pull request, please make sure your code is clean and 
unit tests still succeed.


-------------
Support issue
-------------

We would like you to create a support issue with a link to the pull request so we can keep you 
up-to-date with the status of the pull request.