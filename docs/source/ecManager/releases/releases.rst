========
Releases
========

New features and fixes are continuously added as updates to ecManager.
This is a listing of ecManager updates, including patches to earlier product versions. 
Refer to the documentation below for more information about ecManager releases.

.. toctree::
	:maxdepth: 1
	:glob:
	:reversed:

	older/releases
	updates/2019/releases
	updates/2020/releases