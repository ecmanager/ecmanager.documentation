====
2020
====

.. toctree::
	:maxdepth: 1
	:glob:
	:reversed:
	:hidden:

	*
	
.. |update2020.020| raw:: html

	<strong>
		<a href="2020.020.html">ecManager - update 2020.020</a>
	</strong>

| |update2020.020|
| *16-12-2020*
| Packages:

- ecManager.Services 4.3.22
- ecManager.Modules.EntityState.Default 4.3.4
- ecManager.Modules.ImportExport.Default 4.3.1
- ecManager.Modules.BasketLogic.Default 4.3.4
- ecManager.Search.Core 4.3.13
- ecmanager.Search.AzureSearch 4.3.13
- ecManager.Cms.UI.Web 4.3.19
- ecManager.Cms.Widgets.Core 4.3.19
- ecManager.Search.LuceneNET.MvcPages 4.3.4

| Tooling:

- ecManager.Search.Tools 4.3.9
- ecManager.Tools.EntityStateApplication 4.3.5

| Repositories:

- ecManager.SqlMigration tag: 4.3.11

Added several new features and performance improvements in the Azure Search, Promotions, Export and EntityState functionality.

.. |update2020.018| raw:: html

	<strong>
		<a href="2020.018.html">ecManager - update 2020.018</a>
	</strong>

| |update2020.018|
| *21-10-2020*
| Packages:

- ecManager.Api.GraphQL 0.5.4.3
- ecManager.Cms 4.3.18

Added loader image to export popup in the CMS. Quantities of combination products in the basket are
now correct in de GraphQL Api.

.. |update2020.017| raw:: html

	<strong>
		<a href="2020.017.html">ecManager - update 2020.017</a>
	</strong>

| |update2020.017|
| *06-10-2020*
| Packages:

- ecManager.Api.GraphQL 0.5.3.2
- ecManager.Cms 4.3.17
- ecManager.Search.AzureSearch 4.3.12
- ecManager.Search.Core 4.3.12
- ecManager.Services 4.3.21

| Tooling:

- ecManager.Search.Tools 4.3.8

Fixed some bugs in the CMS and the GraphQL API. Updated Search tooling with new configuration for
searchable fields.

.. |update2020.016| raw:: html

	<strong>
		<a href="2020.016.html">ecManager - update 2020.016</a>
	</strong>

| |update2020.016|
| *09-09-2020*
| Packages:

- ecManager.Api.GraphQL 0.5.2.1

Prevent update by reference if MemoryCache is used.

.. |update2020.015| raw:: html

	<strong>
		<a href="2020.015.html">ecManager - update 2020.015</a>
	</strong>

| |update2020.015|
| *03-09-2020*
| Packages:

- ecManager.Api.GraphQL 0.5.1.1

| Tooling:

- ecManager.Tools.GenerateAllFeeds 4.3.1
- ecManager.Tools.GenerateAllFeedsAzure 4.3.1

Added type properties to Voucher and Promotions entities in the GraphQL API. Updated the feed tools
to deal with special characters in texts.

.. |update2020.014| raw:: html

	<strong>
		<a href="2020.014.html">ecManager - update 2020.014</a>
	</strong>

| |update2020.014|
| *14-08-2020*
| Packages:

- ecManager.Cms 4.3.16

Added duplicate functionality on navigation blocks in the CMS.

.. |update2020.013| raw:: html

	<strong>
		<a href="2020.013.html">ecManager - update 2020.013</a>
	</strong>

| |update2020.013|
| *24-07-2020*
| Packages:

- ecManager.Cms 4.3.15
- ecManager.Search.AzureSearch 4.3.11
- ecManager.Search.Core 4.3.11
- ecManager.Search.LuceneNET.MvcPages 4.3.3

| Tooling:

- ecManager.Search.Tools 4.3.7

Rewritten the AddTerms search modifications for search. ecManager Search LuceneNET is now end of
life. Fixed 2 bugs in the Navigation picker, the tree is shown on the full width of the popup.
Selected Navigation items are correctly shown on the detail view.

.. |update2020.012| raw:: html

	<strong>
		<a href="2020.012.html">ecManager - update 2020.012</a>
	</strong>

| |update2020.012|
| *26-06-2020*
| Packages:

- ecManager.Modules.PredicateExpressionProvider.Default 4.3.11
- ecManager.Search.Lucene 4.3.10
- ecManager.Services 4.3.20

| Tooling:

- ecManager.Search.Lucene.Tools 4.3.6

Removed scheme from cachekeys of entities and searchtasks. Added sorting to lists in searchtasks for 
key uniqueness. Fixed a caching bug in the search tooling.

.. |update2020.011| raw:: html

	<strong>
		<a href="2020.011.html">ecManager - update 2020.011</a>
	</strong>

| |update2020.011|
| *11-06-2020*
| Packages:

- ecManager.Api.GraphQL 0.5.0.4
- ecManager.Cms 4.3.14
- ecManager.Search.Lucene 4.3.9

| Repositories:

- ecManager.SqlMigration tag: 4.3.10

Added new endpoint to the GraphQL API for attributes. Fixed some issues with Combinations and Search
and updated the CKeditor in the CMS.

.. |update2020.010| raw:: html

	<strong>
		<a href="2020.010.html">ecManager - update 2020.010</a>
	</strong>

| |update2020.010|
| *20-05-2020*
| Packages:

- ecManager.Api.GraphQL 0.4.0.10
- ecManager.Modules.PredicateExpressionProvider.Default 4.3.10
- ecManager.Services 4.3.19

Added new endpoints to the GraphQL API for news items and news item comments. Also added the
possibility to get attributes of products in the lister endpoint.

.. |update2020.009| raw:: html

	<strong>
		<a href="2020.009.html">ecManager - update 2020.009</a>
	</strong>

| |update2020.009|
| *06-05-2020*
| Packages:

- ecManager.Api.GraphQL 0.3.2.2

Fixed a bug which prevented the lister and lister search endpoints from sorting the data.

.. |update2020.008| raw:: html

	<strong>
		<a href="2020.008.html">ecManager - update 2020.008</a>
	</strong>

| |update2020.008|
| *29-04-2020*
| Packages:

- ecManager.Api.GraphQL 0.3.1.14
- ecManager.Modules.PredicateExpressionProvider.Default 4.3.9
- ecManager.Services 4.3.18

| Repositories:

- ecManager.SqlMigration tag: 4.3.9

Fixed some bugs and added some improvements in the GraphQL API, rewritten Family RollUp 
functionatility to enable custom Entity State.

.. |update2020.007| raw:: html

	<strong>
		<a href="2020.007.html">ecManager - update 2020.007</a>
	</strong>

| |update2020.007|
| *21-04-2020*
| Packages:

- ecManager.Cms 4.3.13
- ecManager.Modules.PredicateExpressionProvider.Default 4.3.8

Hotfixed a bug which had major impact on lister counts. Updated the CMS with new dependencies.

.. |update2020.006| raw:: html

	<strong>
		<a href="2020.006.html">ecManager - update 2020.006</a>
	</strong>

| |update2020.006|
| *09-04-2020*
| Packages:

- ecManager.Api.GraphQL 0.3.0.3
- ecManager.Cms 4.3.12
- ecManager.Modules.Scheduling.Hangfire 4.3.2
- ecManager.Modules.Scheduling.Hangfire.Server 4.3.2
- ecManager.Modules.Scheduling.Tasks 4.3.3
- ecManager.Search.Lucene 4.3.8
- ecManager.Services 4.3.17

| Repositories:

- ecManager.SqlMigration tag: 4.3.8

Added some improvements for the database and the Hangfire implementation. Fixed some bugs in the
CMS, Azure Search and the GraphQL Api.

.. |update2020.005| raw:: html

	<strong>
		<a href="2020.005.html">ecManager - update 2020.005</a>
	</strong>

| |update2020.005|
| *18-03-2020*
| Packages:

- ecManager.Api.GraphQL 0.2.1.5
- ecManager.Modules.EntityState.Default 4.3.3
- ecManager.Search.Lucene 4.3.7
- ecManager.Services 4.3.16

| Tooling:

- ecManager.Search.Lucene.Tools 4.3.5
- ecManager.Tools.EntityState 4.3.4

| Repositories:

- ecManager.SqlMigration tag: 4.3.7

Released the first stable version of our new GraphQL API. Changed the EntityState logic to more
overridable possibilities. Rewritten ecManager search logic to retrieve correct optional meta data.

.. |update2020.004| raw:: html

	<strong>
		<a href="2020.004.html">ecManager - update 2020.004</a>
	</strong>

| |update2020.004|
| *12-03-2020*
| Packages:

- ecManager.Modules.PredicateExpressionProvider.Default 4.3.7

Updated query expression for a correct search on productgroup numbers.

.. |update2020.003| raw:: html

	<strong>
		<a href="2020.003.html">ecManager - update 2020.003</a>
	</strong>

| |update2020.003|
| *25-02-2020*
| Packages:

- ecManager.Modules.Scheduling.Tasks 4.3.2
- ecManager.Services 4.3.15

Added CommandTimeout to SQL task for scheduling. Fixed some bugs in ecManager services regarding
search like predicates and paging.

.. |update2020.002| raw:: html

	<strong>
		<a href="2020.002.html">ecManager - update 2020.002</a>
	</strong>

| |update2020.002|
| *06-02-2020*
| Packages:

- ecManager.Cms 4.3.11
- ecManager.Modules.PredicateExpressionProvider.Default 4.3.6
- ecManager.Search.LuceneNET.MvcPages 4.3.2
- ecManager.Services 4.3.14

Fixed some bugs in the CMS (locations and search term modifications). Add filter option 
ConfirmationSend to the searchtask of orders.

.. |update2020.001| raw:: html

	<strong>
		<a href="2020.001.html">ecManager - update 2020.001</a>
	</strong>

| |update2020.001|
| *16-01-2020*
| Packages:

- ecManager.Search.Lucene 4.3.6
- ecManager.Services 4.3.13

Improved the feed data query constructors to work with diacritics. Improved the search queries in
Azure Search for better results.