===========================
ecManager - update 2019.012
===========================

Features
--------

| **ECM-6266**
| Packages:

- ecManager.Cms 4.3.9
- ecManager.Modules.BasketLogic.Default 4.3.3
- ecManager.Modules.PredicateExpressionProvider.Default 4.3.5
- ecManager.Modules.PredicateExpressionProvider.Tweakwise 4.3.2
- ecManager.Search.Lucene 4.3.5
- ecManager.Search.Lucene.Tools 4.3.3
- ecManager.Services 4.3.10

| Repositories:

- ecManager.SqlMigration tag: 4.3.6

Fixed some bugs in the CMS with filtering on specific CustomerIds in overviews.

Added sync functionality for updating products and combinations in the basket when going offline,
out-of-stock etc. The functionality is overridable for your own implementation.

Implemented the CustomerIds filter from the SearchTasks in the ecManager platform to filter out
products, combinations etc. specific for the given CustomerIds based on the given configuration.
This has been changed in the PredicateExpressionProviders and the search tooling.

For more information about configuring the catalog, read more `here <../../../../ecManager/how-to/configuring-catalog/index.html>`_.

.. warning ::

	The catalog configuration section is updated with new attribute names. Check your
	implementation.	

.. important ::

	Run SQL script 035_UpdateOptimizeProducts.sql or use DbUp to update the database.