===========================
ecManager - update 2019.001
===========================

Features
--------

| **ECM-6153, ECM-6154**
| Packages:

- ecManager.Services 4.3.1

Created a location predicate expression provider. This replaces the default search implementation in 
the core libraries. You are now able to change the default search predicates.

Relocated the default location search implementation to a location predicate expression provider. 

Added product number filter to Order overview in the CMS.

.. important ::

	Add the predicate expression provider registration to your modules.config file in your implementation and in the CMS.

.. code-block :: xml

  <component
    type="ecManager.Modules.PredicateExpressionProvider.DefaultPredicateExpressionProvider.DefaultLocationPredicateExpressionProvider, ecManager.Modules.PredicateExpressionProvider.DefaultPredicateExpressionProvider"
    service="ecManager.PredicateExpressionProvider.IPredicateProvider`2[[ecManager.Information.Entities.LocationSearchTask, ecManager.Information]‌,[ecManager.Information.Entities.Location, ecManager.Information]],ecManager.PredicateExpressionProvider">
  </component>

| **ECM-6118**
| Packages:

- ecManager.Cms 4.3.1
- ecManager.Modules.PredicateExpressionProvider.Default 4.3.1
- ecManager.Services 4.3.1

Created an order predicate expression provider. This replaces the default search implementation in 
the core libraries. You are now able to change the default search predicates.

Relocated the default order search implementation to an order predicate expression provider.

.. important ::

	Add the predicate expression provider registration to your modules.config file in your implementation and in the CMS.

.. code-block :: xml

  <component
    type="ecManager.Modules.PredicateExpressionProvider.DefaultPredicateExpressionProvider.DefaultOrderPredicateExpressionProvider, ecManager.Modules.PredicateExpressionProvider.DefaultPredicateExpressionProvider"
    service="ecManager.PredicateExpressionProvider.IPredicateProvider`2[[ecManager.Customer.Entities.OrderSearchTask, ecManager.Customer]‌,[ecManager.Customer.Entities.Order, ecManager.Customer]],ecManager.PredicateExpressionProvider">
  </component>

Bugfixes
--------

| **ECM-6152**
| Packages:

- ecManager.Cms 4.3.1

Corrected styling for import popup redirects.

| **ECM-6151**
| Packages:

- ecManager.Cms 4.3.1

Removed duplicate sortable columns DisplayMethod and Obligated from Options overview.

| **ECM-6164**
| Packages:

- ecManager.Cms 4.3.1

Removed the deep-links to the Navigation Items. Added label names to the links.

| **ECM-6141**
| Packages:

- ecManager.Modules.BasketLogic.Default 4.3.1

A promotion is now always valid if there aren't any conditions, regardless of the option 'All 
conditions are required'.