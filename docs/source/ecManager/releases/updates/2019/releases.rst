====
2019
====

.. toctree::
	:maxdepth: 1
	:glob:
	:reversed:
	:hidden:

	*

.. |update2019.015| raw:: html

	<strong>
		<a href="2019.015.html">ecManager - update 2019.015</a>
	</strong>

| |update2019.015|
| *24-12-2019*
| Packages:

- ecManager.Services 4.3.12

Added cache logic to the Combination Service.

.. |update2019.014| raw:: html

	<strong>
		<a href="2019.014.html">ecManager - update 2019.014</a>
	</strong>

| |update2019.014|
| *05-12-2019*
| Packages:

- ecManager.Cms 4.3.10
- ecManager.Modules.Scheduling.Hangfire 4.3.1
- ecManager.Modules.Scheduling.Hangfire.Server 4.3.1
- ecManager.Services 4.3.11

Updated the Hangfire scheduling implementation in the ecManager platform. Fixed some bugs with
incorrect statuses, updated the Hangfire libraries to version 1.7.

.. |update2019.012| raw:: html

	<strong>
		<a href="2019.012.html">ecManager - update 2019.012</a>
	</strong>

| |update2019.012|
| *21-10-2019*
| Packages:

- ecManager.Cms 4.3.9
- ecManager.Modules.BasketLogic.Default 4.3.3
- ecManager.Modules.PredicateExpressionProvider.Default 4.3.5
- ecManager.Modules.PredicateExpressionProvider.Tweakwise 4.3.2
- ecManager.Search.Lucene 4.3.5
- ecManager.Search.Lucene.Tools 4.3.3
- ecManager.Services 4.3.10

Implemented the CustomerIds filter from the SearchTasks in the ecManager platform to filter out
products, combinations etc. specific for the given CustomerIds based on the given configuration.
Fixed some bugs in the CMS with filtering on specific CustomerIds in overviews. Added sync
functionality for updating products and combinations in the basket when going offline, out-of-stock
etc.

.. |update2019.011| raw:: html

	<strong>
		<a href="2019.011.html">ecManager - update 2019.011</a>
	</strong>

| |update2019.011|
| *13-10-2019*
| Packages:

- ecManager.Cms 4.3.8
- ecManager.Services 4.3.9

| Repositories:

- ecManager.SqlMigration tag: 4.3.5

Product entity contains two new properties, MinCount and MaxCount. These properties can be used to
limit the ordered amount of product per customer.

.. |update2019.010| raw:: html

    <strong>
        <a href="2019.010.html">ecManager - update 2019.010</a>
    </strong>

| |update2019.010|
| *11-09-2019*
| Packages:

- ecManager.Cms 4.3.7
- ecManager.Modules.SeoLogic.Default 4.3.1
- ecManager.Modules.UrlFormattting.Default 4.3.3
- ecManager.Services 4.3.8

| Repositories:

- ecManager.SqlMigration tag: 4.3.4

Added SEO possibilities for Locations. Fixed some bugs in the CMS.

.. |update2019.008| raw:: html

    <strong>
        <a href="2019.008.html">ecManager - update 2019.008</a>
    </strong>

| |update2019.008|
| *20-06-2019*
| Packages:

- ecManager.Cms 4.3.6
- ecManager.Modules.BasketLogic.Default 4.3.2
- ecManager.Modules.EntityState.DefaultStateLogic 4.3.2
- ecManager.Modules.PredicateExpressionProvider.Default 4.3.4
- ecManager.Modules.UrlFormattingLogic.Default 4.3.2
- ecManager.Search.Lucene 4.3.4
- ecManager.Search.Lucene.Tools 4.3.2
- ecManager.Services 4.3.7
- ecManager.Tools.EntityState 4.3.3

Added CustomerIds filter to the different SearchTasks in the ecManager platform to filter out
products, combinations etc. specific for the given CustomerIds. Optimized product - customer
relation database performance for large quantities.

.. |update2019.007| raw:: html

    <strong>
        <a href="2019.007.html">ecManager - update 2019.007</a>
    </strong>

| |update2019.007|
| *03-06-2019*
| Packages:

- ecManager.Cms 4.3.5
- ecManager.Modules.PredicateExpressionProvider.Default 4.3.3
- ecManager.Services 4.3.6

Added a new B2B possibility in the ecManager platform, customer specific products are now available
via the ecManager service layer and managable in the CMS.

.. |update2019.006| raw:: html

    <strong>
        <a href="2019.006.html">ecManager - update 2019.006</a>
    </strong>

| |update2019.006|
| *09-05-2019*
| Packages:

- ecManager.Cms 4.3.4
- ecManager.Services 4.3.5
- ecManager.Tools.EntityState 4.3.2

Updated the CMS module landingpages filter by label. Fixed the password recovery function. Fixed a
bug in the interval settings in the Entity state application.

.. |update2019.005| raw:: html

	<strong>
		<a href="2019.005.html">ecManager - update 2019.005</a>
	</strong>

| |update2019.005|
| *17-04-2019*
| Packages:

- ecManager.Cms 4.3.3
- ecManager.Modules.HttpModules.HttpRedirectModule 4.3.1
- ecManager.Search.Lucene 4.3.3
- ecManager.Search.LuceneNET.MvcPages 4.3.1
- ecManager.Services 4.3.4

Updated redirect module for using regex groups. Optimized search statistics for better performance.

.. |update2019.004| raw:: html

	<strong>
		<a href="2019.004.html">ecManager - update 2019.004</a>
	</strong>

| |update2019.004|
| *12-03-2019*
| Packages:

- ecManager.Search.Lucene 4.3.2
- ecManager.Services 4.3.3

Removed SearchTask from ExpressionResult. Optimized SearchTerm logging in combination with cached 
results. This property caused logging errors due to invalid values.

.. |update2019.003| raw:: html

	<strong>
		<a href="2019.003.html">ecManager - update 2019.003</a>
	</strong>

| |update2019.003|
| *07-03-2019*
| Packages:

- ecManager.Cms 4.3.2
- ecManager.Modules.EntityState.DefaultStateLogic 4.3.1
- ecManager.Modules.PredicateExpressionProvider.Default 4.3.2
- ecManager.Modules.PredicateExpressionProvider.Tweakwise 4.3.1
- ecManager.Modules.Scheduling.Tasks 4.3.1
- ecManager.Modules.UrlFormattingLogic.Default 4.3.1
- ecManager.Search.Lucene 4.3.1
- ecManager.Search.Lucene.Tools 4.3.1
- ecManager.Services 4.3.2
- ecManager.Tools.EntityState 4.3.1

Added Entity State to the Vacancy Entity. New scheduling task types added to the scheduling system.
Added possibility to log results of a search action within ecManager.Services.

.. |update2019.002| raw:: html

	<strong>
		<a href="2019.002.html">ecManager - update 2019.002</a>
	</strong>

| |update2019.002|
| *07-02-2019*
| Packages:

- ecManager.Modules.HttpModules.LoginModule 4.3.1

Updated the LoginModule with fixes and enhancements.

.. |update2019.001| raw:: html

	<strong>
		<a href="2019.001.html">ecManager - update 2019.001</a>
	</strong>

| |update2019.001|
| *23-01-2019*
| Packages:

- ecManager.Cms 4.3.1
- ecManager.Modules.BasketLogic.Default 4.3.1
- ecManager.Modules.PredicateExpressionProvider.Default 4.3.1
- ecManager.Services 4.3.1

Created two new PredicateExpressionProviders, updated BasketLogic, and many fixes to the CMS.