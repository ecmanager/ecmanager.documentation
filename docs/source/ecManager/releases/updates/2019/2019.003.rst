===========================
ecManager - update 2019.003
===========================

Features
--------

| **ECM-6081**
| Packages:

- ecManager.Cms 4.3.2
- ecManager.Modules.EntityState.DefaultStateLogic 4.3.1
- ecManager.Modules.UrlFormattingLogic.Default 4.3.1
- ecManager.Services 4.3.2
- ecManager.Tools 4.3.1

Added EntityState context to the VacancyService in ecManager.Services.

Added EntityStateLogic for Vacancies in the DefaultStateLogic module.

Added Vacancies to logic for automating EntityState for Vacancy Entity in the EntityState 
Application.

Added Quickview for EntityState in Vacancy overview in the CMS.

Updated FormattingLogic for use with Vacancies EntityState.

| **ECM-6179**
| Packages:

- ecManager.Cms 4.3.2

Created new quick navigation menu on detail pages.

| **ECM-6174**
| Packages:

- ecManager.Modules.Scheduling.Tasks 4.3.1

Added two new tasktypes. SqlCommand task and HttpRequest task. SqlCommand task can be used to run 
StoredProcedures on your SQL database. HttpRequest task can be used for doing requests (webhook 
style).

| **ECM-6120**
| Packages:

- ecManager.Modules.PredicateExpressionProvider.Default 4.3.2
- ecManager.Modules.PredicateExpressionProvider.Tweakwise 4.3.1
- ecManager.Search.Lucene 4.3.1
- ecManager.Search.Lucene.Tools 4.3.1
- ecManager.Services 4.3.2

Relocated the logging functionality of Lucene/Azure Search in ecManager.Core. If the result is 
cached, the logging is now handled separately.

Implemented the new function LogResult for logging. Moved existing logging (if exists) functionality 
into this new function.

Updated dependencies in the SearchTools.

.. important ::

	The IPredicateProvider interface has new function LogResult. You'll have to implement this function in your own implementation.

| **ECM-5879**
| Packages:

- ecManager.Modules.PredicateExpressionProvider.Default 4.3.2
- ecManager.Services 4.3.2

Added possibility to filter NewsItems based on Product Ids.

Bugfixes
--------

| **ECM-6178**
| Packages:

- ecManager.Cms 4.3.2

Added stylefix. Long menu names have now an ellepsis if the're too long.

Resource changes
----------------

.. csv-table::
    :header: "Filename", "Status", "Key", "Original value", "New value"
    :widths: 2,1,1,3,3

    "enumerations.resx", "Added", "ecManager_Modules_Scheduling_Tasks_HttpRequestTask", "", "HTTP request task"
    "enumerations.resx", "Added", "ecManager_Modules_Scheduling_Tasks_HttpRequestTask_UrlLabelKey", "", "Url"
	"enumerations.resx", "Added", "ecManager_Modules_Scheduling_Tasks_SqlCommandTask", "", "SQL command task"
	"enumerations.resx", "Added", "ecManager_Modules_Scheduling_Tasks_SqlCommandTask_ParameterName1LabelKey", "", "Parameter 1: name"
	"enumerations.resx", "Added", "ecManager_Modules_Scheduling_Tasks_SqlCommandTask_ParameterName2LabelKey", "", "Parameter 2: name"
	"enumerations.resx", "Added", "ecManager_Modules_Scheduling_Tasks_SqlCommandTask_ParameterName3LabelKey", "", "Parameter 3: name"
	"enumerations.resx", "Added", "ecManager_Modules_Scheduling_Tasks_SqlCommandTask_ParameterName4LabelKey", "", "Parameter 4: name"
	"enumerations.resx", "Added", "ecManager_Modules_Scheduling_Tasks_SqlCommandTask_ParameterName5LabelKey", "", "Parameter 5: name"
	"enumerations.resx", "Added", "ecManager_Modules_Scheduling_Tasks_SqlCommandTask_ParameterValue1LabelKey", "", "Parameter 1: value"
	"enumerations.resx", "Added", "ecManager_Modules_Scheduling_Tasks_SqlCommandTask_ParameterValue2LabelKey", "", "Parameter 2: value"
	"enumerations.resx", "Added", "ecManager_Modules_Scheduling_Tasks_SqlCommandTask_ParameterValue3LabelKey", "", "Parameter 3: value"
	"enumerations.resx", "Added", "ecManager_Modules_Scheduling_Tasks_SqlCommandTask_ParameterValue4LabelKey", "", "Parameter 4: value"
	"enumerations.resx", "Added", "ecManager_Modules_Scheduling_Tasks_SqlCommandTask_ParameterValue5LabelKey", "", "Parameter 5: value"
	"enumerations.resx", "Added", "ecManager_Modules_Scheduling_Tasks_SqlCommandTask_StoredProcedureLabelKey", "", "Stored Procedure"
	"enumerations.nl.resx", "Added", "ecManager_Modules_Scheduling_Tasks_HttpRequestTask", "", "HTTP request task"
    "enumerations.nl.resx", "Added", "ecManager_Modules_Scheduling_Tasks_HttpRequestTask_UrlLabelKey", "", "Url"
	"enumerations.nl.resx", "Added", "ecManager_Modules_Scheduling_Tasks_SqlCommandTask", "", "SQL command task"
	"enumerations.nl.resx", "Added", "ecManager_Modules_Scheduling_Tasks_SqlCommandTask_ParameterName1LabelKey", "", "Parameter 1: naam"
	"enumerations.nl.resx", "Added", "ecManager_Modules_Scheduling_Tasks_SqlCommandTask_ParameterName2LabelKey", "", "Parameter 2: naam"
	"enumerations.nl.resx", "Added", "ecManager_Modules_Scheduling_Tasks_SqlCommandTask_ParameterName3LabelKey", "", "Parameter 3: naam"
	"enumerations.nl.resx", "Added", "ecManager_Modules_Scheduling_Tasks_SqlCommandTask_ParameterName4LabelKey", "", "Parameter 4: naam"
	"enumerations.nl.resx", "Added", "ecManager_Modules_Scheduling_Tasks_SqlCommandTask_ParameterName5LabelKey", "", "Parameter 5: naam"
	"enumerations.nl.resx", "Added", "ecManager_Modules_Scheduling_Tasks_SqlCommandTask_ParameterValue1LabelKey", "", "Parameter 1: waarde"
	"enumerations.nl.resx", "Added", "ecManager_Modules_Scheduling_Tasks_SqlCommandTask_ParameterValue2LabelKey", "", "Parameter 2: waarde"
	"enumerations.nl.resx", "Added", "ecManager_Modules_Scheduling_Tasks_SqlCommandTask_ParameterValue3LabelKey", "", "Parameter 3: waarde"
	"enumerations.nl.resx", "Added", "ecManager_Modules_Scheduling_Tasks_SqlCommandTask_ParameterValue4LabelKey", "", "Parameter 4: waarde"
	"enumerations.nl.resx", "Added", "ecManager_Modules_Scheduling_Tasks_SqlCommandTask_ParameterValue5LabelKey", "", "Parameter 5: waarde"
	"enumerations.nl.resx", "Added", "ecManager_Modules_Scheduling_Tasks_SqlCommandTask_StoredProcedureLabelKey", "", "Stored Procedure"