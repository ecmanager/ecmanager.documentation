############
Writing code
############

*New to the ecManager Framework? We'll give you a hand. This document describes how to get started when implementing the framework.*

Naming Conventions
==================

Solution and projects
---------------------
The projects within the ecManager solution use the following naming conventions:

- Project names start with "ecManager" followed by the [domain] and the optional [project contents]
- Samples of projects:
   - ecManager.[domain].[project]
   - ecManager.Common
   - ecManager.Common.Tests
   - ecManager.Catalog
   - ecManager.Catalog.Tests
   - ecManager.Services

Namespaces
----------
The namespaces that are used in the files are implicitly determined using the project and folder structure. In some cases, a different namespace can be used:

- To distinguish to groups of classes:
   - ie. the classes in ecManager.Common.Types are split-up into normal types and "primary key" types. The "Primary key" types do life in a subfolder "PrimaryKeys" but do not have the corresponding Namespace.
- Sample namespaces:
   - Product.cs in ecManager.Catalog\\Entities has namespace ecManager.Catalog.Entities
   - ProductNumber.cs in ecManager.Common\\ValueTypes has namespace ecManager.Common.ValueTypes
   - ProductId.cs in ecManager.Common\\ValueTypes\\PrimaryKeys has namespace ecManager.Common.ValueTypes

Entities
--------
The entities used in ecManager are data containers and also maybe called a POCO or DTO. Every entity has some guidelines applied.

- An entity inherits from Entity
  
.. rubric:: Methods

Services
--------

.. rubric:: Methods

Returned or instanced values
----------------------------

+--------------------------+-----------------------------------------------+-------------------------------+
| Type                     | On instantiation                              | Service Request if not found  |
+==========================+===============================================+===============================+
| Entities                 | New Instance                                  | null                          |
+--------------------------+-----------------------------------------------+-------------------------------+
| Primary Key Value types  | Instance with default value (ie. 0 for ints)  | null                          |
+--------------------------+-----------------------------------------------+-------------------------------+
| Collection               | Collection with 0 items                       | Collection 0 items            |
+--------------------------+-----------------------------------------------+-------------------------------+
| String                   |                                               | null                          |
+--------------------------+-----------------------------------------------+-------------------------------+


Implementing the security model in your website and applications
================================================================

When you create a new web shop based on ecManager, all consumers of the Service Layer will have to 
authenticate. The security system allows you to restrict the access per user. There are three possible 
scenarios for authentication:

- The user is authenticated, and has permission for the requested action.
- The user is authenticated, but does not have permission for the requested action.
- The user is not authenticated.

In the first scenario, the requested action will be executed as requested. In the other scenario's a 'SecurityException' will occur.

Authenticating your website
---------------------------

When your website wants to consume the service layer, you need to authenticate. The easiest way to authenticate is using your application pool identity. You can choose to use the default identity called **'ApplicationPoolIdentity'**, or you can choose to create a custom account More information on specifying the application pool identity can be found here: `technet <http://technet.microsoft.com/nl-nl/library/cc771170(v=ws.10).aspx>`_ Use the following steps to authenticate your website:

1) Open your 'Global.asax'.
2) Import the namespace 'ecManager.Services'
3) Add the following code:

.. code-block:: C#
	:linenos:

	/// <summary>
	/// Occurs when a security module has established the identity of the user.
	/// </summary>
	/// <param name="sender">The sender of the authenticate request.</param>
	/// <param name="e">The event arguments.</param>
	protected void Application_AuthenticateRequest( object sender, EventArgs e )
	{
	    var applicationPermissionLogic = new AuthenticateService(new ServiceRequestContext());
	    var principal = service.Authenticate( WindowsIdentity.GetCurrent(), token);
	 
	    //Also set the user in the context.
	    this.Context.User = principal;
	}
	
4) This will authenticate your application using the identity of your application pool identity. 
Now you only have to make sure you are there is a user with the login name that matches your application 
pool identity name, and that this has sufficient permissions. You can find the exact name of the 
application pool identity using the following line of code:

Command::

    WindowsIdentity.GetCurrent().Name

Authenticating your service application
---------------------------------------

In some cases you will have to authenticate the service in order to process data imports and exports. The easiest way to authenticate a service is by using the 'Log On As' functionality in Windows. After setting the correct identity for the service, you can authenticate as soon as the application starts by running the following code:

.. code-block:: C#
	:linenos:

	/// <summary>
	/// Constructor of the service.
	/// </summary>
	public Service1()
	{
	    this.InitializeComponent();
		var token = "token";
	    var service = new AuthenticateService(new ServiceRequestContext());
	    var principal = service.Authenticate( WindowsIdentity.GetCurrent(), token);
	}

After the authenticate call you should have all the required permissions you have configured in the CMS. The permissions are stored in a place that can be accessed over several threads.

Exceptions
----------

**Exception:** SecurityException

**Message:** "Access is denied. The user is not authenticated, or the principal isn't a ClaimsPrincipal." 

Possible causes:

- The consumer is not authenticated 
   - **Solution:** Authenticate using the 'Authenticate' method in the ecManager.Security.Logic.ApplicationPermissionLogic class.
- The consumer is authenticated, but it isn't a ClaimsPrincipal. 
   - **Solution:** Make sure you have authenticated using the ApplicationPermissionLogic. In case you have authenticated, make sure there isn't a DefaultRoleProvider that overrides the authentication.
  
**Exception:** SecurityException

**Message:** "User does not have permission for this action." 
Cause:

- You are trying to perform an action, but you do not have the right permissions. 
   - **Solution:** Check the InnerException. This exception should state which resources you are trying to access but of which you do not have the right permissions. Make sure you have the right permission.

