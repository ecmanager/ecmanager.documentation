#####################
Configuring your URLs
#####################

In ecManager you are able to configure how your URLs look. In some cases you want to have your shipping country in your URL for SEO purposes. In other cases you would like to have as little data as possible. In this page we will take a look at how you can configure your own URLs so they meet your demands.

Preparing your web.config
=========================

First we need to make sure that the implementation of the UrlLogic (DefaultUrlLogic module) is loaded using Autofac.
Add the following XML to the 'Component'-section of the Autofac configuration.

.. code-block:: C#
   :linenos:
   :name: Loading the DefaultUrlLogic module in Autofac
   :caption: Loading the DefaultUrlLogic module in Autofac

    <component
    	type="ecmanager.Modules.Url.DefaultUrlLogic.DefaultUrlLogic, ecmanager.Modules.Url.DefaultUrlLogic"
    	service="ecManager.UrlFormatter.AUrlLogic, ecManager.UrlFormatter"
   		instance-scope="single-instance">
    </component>

The next step is adding the configuration for the URLs to your web.config.
First define a new section by adding the following line to the 'configuration/ConfigSections'-element.

.. code-block:: C#
   :linenos:
   :name: Defining the urlConfiguration section
   :caption: Defining the urlConfiguration section

   <section name="urlConfiguration" type="ecManager.Common.Configurations.Cms.UrlConfigurationSection, ecManager.Common" />

Then we need to add the section itself. We can do this by adding the following XML.

.. code-block:: C#
   :linenos:
   :name: The URL configuration section
   :caption: The URL configuration section

	<urlConfiguration>
	    <httpsAvailable>true</httpsAvailable>
	    <forceHttps>true</forceHttps>
	    <generateSecureUrls>
	        <selectionCodes></selectionCodes>
	    </generateSecureUrls>
	    <urlPatterns>
	        <urlPattern name="ProductPattern" parsePattern="^((?&lt;protocol&gt;(.+))://)?(?&lt;url&gt;([^/]*))/(.*)/(?&lt;languagecode&gt;(\D{0,2}))/product/(?&lt;identifier&gt;(\d+))/(\?(.*))?$" generatePattern="/{seo:product}/{languagecode}/product/{identifier}/" />
	        <urlPattern name="CombinationPattern" parsePattern="^((?&lt;protocol&gt;(.+))://)?(?&lt;url&gt;([^/]*))/(.*)/(?&lt;languagecode&gt;(\D{0,2}))/combination/(?&lt;identifier&gt;(\d+))/(\?(.*))?$" generatePattern="/{seo:combination}/{languagecode}/combination/{identifier}/" />
	        <urlPattern name="NewsItemPattern" parsePattern="^((?&lt;protocol&gt;(.+))://)?(?&lt;url&gt;([^/]*))/(.*)/(?&lt;languagecode&gt;(\D{0,2}))/news/(?&lt;identifier&gt;(\d+))/(\?(.*))?$" generatePattern="/{seo:news}/{languagecode}/news/{identifier}/" />
	        <urlPattern name="PagePattern" parsePattern="^((?&lt;protocol&gt;(.+))://)?(?&lt;url&gt;([^/]*))/(.*)/(?&lt;languagecode&gt;(\D{0,2}))/page/(?&lt;identifier&gt;(\d+))/(\?(.*))?$" generatePattern="/{seo:page}/{languagecode}/page/{identifier}/">
	            <specificUrlPattern labelId="2" parsePattern="^((?&lt;protocol&gt;(.+))://)?(?&lt;url&gt;([^/]*))/(.*)/page/(?&lt;identifier&gt;(\d+))/(\?(.*))?$" generatePattern="/{seo:page}/page/{identifier}/" />
	        </urlPattern>
	        <urlPattern name="LocationPattern" parsePattern="^((?&lt;protocol&gt;(.+))://)?(?&lt;url&gt;([^/]*))/(.*)/(?&lt;languagecode&gt;(\D{0,2}))/location/(?&lt;identifier&gt;(\d+))/(\?(.*))?$" generatePattern="/{seo:location}/{languagecode}/location/{identifier}/" />
	        <urlPattern name="VacancyPattern" parsePattern="^((?&lt;protocol&gt;(.+))://)?(?&lt;url&gt;([^/]*))/(.*)/(?&lt;languagecode&gt;(\D{0,2}))/vacancy/(?&lt;identifier&gt;(\d+))/(\?(.*))?$" generatePattern="/{seo:vacancy}/{languagecode}/vacancy/{identifier}/" />
	        <urlPattern name="EventPattern" parsePattern="^((?&lt;protocol&gt;(.+))://)?(?&lt;url&gt;([^/]*))/(.*)/(?&lt;languagecode&gt;(\D{0,2}))/event/(?&lt;identifier&gt;(\d+))/(\?(.*))?$" generatePattern="/{seo:event}/{languagecode}/event/{identifier}/" />
	        <urlPattern name="FeedPattern" parsePattern="^((?&lt;protocol&gt;(.+))://)?(?&lt;url&gt;([^/]*))/CmsData/Feeds/(?&lt;filename&gt;(\\d+))/$" generatePattern="/CmsData/Feeds/{filename}" />
	        <urlPattern name="404Pattern" parsePattern="^((?&lt;protocol&gt;(.+))://)?(?&lt;url&gt;([^/]*))/404(\?(.*))?$" generatePattern="/404" />
	        <urlPattern name="HomePagePattern" parsePattern="^((?&lt;protocol&gt;(.+))://)?(?&lt;url&gt;([^/]*))/(\?(.*))?$" generatePattern="/" />
	        <urlPattern name="LandingPagePattern" parsePattern="^((?&lt;protocol&gt;(.+))://)?(?&lt;url&gt;([^/]*))/(?&lt;seo&gt;[^/]*)(\?(.*))?$" generatePattern="/{seo}" />
	    </urlPatterns>
	</urlConfiguration>

You should now be able to fetch URLs using the URL service.

A closer look at the configuration settings
===========================================

In the following table we will take a closer look at the elements in the URL configuration. Then we will take a closer look at the urlPattern element an it's attributes. 

+--------------------+----------------------+------------------------+---------+-----------+---------------------------------------------------------------------------------------------------+
| Element name       | Allowed child        | Value                  | Default | Mandatory | Remarks                                                                                           |
|                    | elements             |                        | value   |           |                                                                                                   |
+====================+======================+========================+=========+===========+===================================================================================================+
| urlConfiguration   | | httpsAvailable     | | No value, only       | x       | Yes       | | This is the section that is read by the configuration section definition.                       |
|                    | | generateSecureUrls | | child elements       |         |           |                                                                                                   |
|                    | | urlPatterns        |                        |         |           |                                                                                                   |
|                    | | forceHttps         |                        |         |           |                                                                                                   |
+--------------------+----------------------+------------------------+---------+-----------+---------------------------------------------------------------------------------------------------+
| httpsAvailable     | | None               | | True or false        | False   | Yes       | | This boolean determines if there is a secure connection available on this environment.          |
|                    |                      |                        |         |           | | If set to true, the URL service will generate.                                                  |
+--------------------+----------------------+------------------------+---------+-----------+---------------------------------------------------------------------------------------------------+
| forceHttps         | | None               | | True or false        | False   | No        | | This boolean determines if all URLs should be generated using HTTPS.                            |
|                    |                      |                        |         |           | | This setting overrides the httpsAvailable setting.                                              |
|                    |                      |                        |         |           | | This means HTTPS URLs will be generated, even if httpsAvailable is set to false.                |
+--------------------+----------------------+------------------------+---------+-----------+---------------------------------------------------------------------------------------------------+
| generateSecureUrls | | selectionCodes     | | No value, only       | x       | No        | | In this section you can configure which pages should be visited using HTTPS.                    |
|                    |                      | | child elements       |         |           | | You can select pages using the child element selectionCodes.                                    |
|                    |                      |                        |         |           | | In future versions other selection methods could be added if we this these will be useful.      |
+--------------------+----------------------+------------------------+---------+-----------+---------------------------------------------------------------------------------------------------+
| selectionCodes     | | None               | | Selection codes,     | x       | No        | | When requesting an URL for a navigation item with the configured selection code,                |
|                    |                      | | comma separated      |         |           | | the URL wil be prepended with HTTPS.                                                            |
+--------------------+----------------------+------------------------+---------+-----------+---------------------------------------------------------------------------------------------------+
| urlPatterns        | | urlPattern         | | No value, only       | x       | Yes       | | This section contains the actual URL patterns                                                   |
|                    |                      | | child elements       |         |           |                                                                                                   |
+--------------------+----------------------+------------------------+---------+-----------+---------------------------------------------------------------------------------------------------+
| urlPattern         | | specificUrlPattern | | Attributes and child | x       | Yes       | | The restriction is not forced by the configuration section definition, but by the URL service.  |
|                    |                      | | elements             |         |           |                                                                                                   |
+--------------------+----------------------+------------------------+---------+-----------+---------------------------------------------------------------------------------------------------+
| specificUrlPattern | | None               | | No value, only       | x       | No        | | This optional element can be used to create patterns for a specific label. If a request is made |
|                    |                      | | child elements       |         |           | | for a urlPattern which contains a specificUrlPattern element with a matching label id,          |
|                    |                      |                        |         |           | | the patterns specified in the specificUrlPattern will be used.                                  |
+--------------------+----------------------+------------------------+---------+-----------+---------------------------------------------------------------------------------------------------+

The urlPattern element has a set of attributes. These are defined in the following table.

+-----------------+----------------------------------------+-----------+-----------------------------------------------------------------------------------------------------------------------------------+
| Attribute name  | Values                                 | Mandatory | Remarks                                                                                                                           |
+=================+========================================+===========+===================================================================================================================================+
| name            | Unique name of the pattern             | Yes       | | This name is used to uniquely identify the patterns                                                                             |
+-----------------+----------------------------------------+-----------+-----------------------------------------------------------------------------------------------------------------------------------+
| parsePattern    | A regular expression for parsing a URL | Yes       | | The regex should contain a matching group for the entity identifier, and the language code.                                     |
|                 |                                        |           | | Characters like < and > should be escaped using &gt; and &lt;                                                                   |
+-----------------+----------------------------------------+-----------+-----------------------------------------------------------------------------------------------------------------------------------+
| generatePattern | Pattern for building a URL             | Yes       | | The pattern should contain keys like {myKey} or {myKey:defaultValue}.                                                           |
|                 |                                        |           | | When you request an url, and pass along a dictionary with key = myKey and a value we will replace {myKey} with the given value. |
|                 |                                        |           | | Keys provided by ecManager are:                                                                                                 |
|                 |                                        |           | | - seo (seo key defined by the entity texts)                                                                                     |
|                 |                                        |           | | - languagecode (language code of the request)                                                                                   |
|                 |                                        |           | | - identifier (entity id)                                                                                                        |
+-----------------+----------------------------------------+-----------+-----------------------------------------------------------------------------------------------------------------------------------+

The specificUrlPattern element has the following set of attributes:

+-----------------+----------------------------------------+-----------+-----------------------------------------------------------------------------------------------------------------------------------+
| Attribute name  | Values                                 | Mandatory | Remarks                                                                                                                           |
+=================+========================================+===========+===================================================================================================================================+
| labelId         | Integer                                | Yes       | | The id of the label for which specific patterns will be defined. This value must be unique within the urlPattern collection.    |
+-----------------+----------------------------------------+-----------+-----------------------------------------------------------------------------------------------------------------------------------+
| parsePattern    | A regular expression for parsing a URL | No        | | The regex should contain a matching group for the entity identifier, and the language code.                                     |
|                 |                                        |           | | Characters like < and > should be escaped using &gt; and &lt;                                                                   |
+-----------------+----------------------------------------+-----------+-----------------------------------------------------------------------------------------------------------------------------------+
| generatePattern | Pattern for building a URL             | No        | | The pattern should contain keys like {myKey} or {myKey:defaultValue}.                                                           |
|                 |                                        |           | | When you request an url, and pass along a dictionary with key = myKey and a value we will replace {myKey} with the given value. |
|                 |                                        |           | | Keys provided by ecManager are:                                                                                                 |
|                 |                                        |           | | - seo (seo key defined by the entity texts)                                                                                     |
|                 |                                        |           | | - languagecode (language code of the request)                                                                                   |
|                 |                                        |           | | - identifier (entity id)                                                                                                        |
+-----------------+----------------------------------------+-----------+-----------------------------------------------------------------------------------------------------------------------------------+

Breaking down a parse pattern
=============================

In this section we will take a look at the parse pattern for products. The pattern in the web config is encoded so the web config is does not get XML parsing errors. 

This is the encoded version::

    ^((?&lt;protocol&gt;(.+))://)?(?&lt;url&gt;([^/]*))/(.*)/(?&lt;languagecode&gt;(\D{0,2}))/product/(?&lt;identifier&gt;(\d+))/(\?(.*))?$

If we decode it, we get the following regex::

	^((?<protocol>(.+))://)?(?<url>([^/]*))/(.*)/(?<languagecode>(\D{0,2}))/product/(?<identifier>(\d+))/(\?(.*))?$    

.. warning:: You can use an online XML tool to encode and decode the parsing patterns.

The pattern is evaluated on the URL that is given to the URL Service. The following example is based on the URL structure that was generated by DNZ.EP. In the example we will use the following URL: https://www.example.com/my-awesome-product/en/product/124/?utm_source=twitter 

+------------------------+-------------------------------------------------------------------------------------------------------------+-----------+----------------------------+--------------------+
| Part                   | Description                                                                                                 | Mandatory | Part of the regex          | Example value      |
+========================+=============================================================================================================+===========+============================+====================+
| Protocol               | | The protocol of the request. Expect values as 'http' or 'https'.                                          | No        | ((?<protocol>(.+))://)?    | https              |
+------------------------+-------------------------------------------------------------------------------------------------------------+-----------+----------------------------+--------------------+
| URL                    | | The domain on which the request is taking place.                                                          | Yes       | (?<url>([^/]*))            | www.example.com    |
+------------------------+-------------------------------------------------------------------------------------------------------------+-----------+----------------------------+--------------------+
| Language code          | | The language code of the request                                                                          | Yes       | (?<languagecode>(\D{0,2})) | en                 |
+------------------------+-------------------------------------------------------------------------------------------------------------+-----------+----------------------------+--------------------+
| Entity keyword         | | This is a unique identifier which is used to see this is a product, and not a page.                       | Yes       | /product/                  | /product/          |
|                        | | We don't actually use this part of the URL, but DNZ.EP needed this to identify the URL was for a product. |           |                            |                    |
+------------------------+-------------------------------------------------------------------------------------------------------------+-----------+----------------------------+--------------------+
| Identifier             | | The identifier of the entity, in this example the product id.                                             | Yes       | (?<identifier>(\d+))/      | 123                |
+------------------------+-------------------------------------------------------------------------------------------------------------+-----------+----------------------------+--------------------+
| Querystring parameters | | The query string parameters in the URL. Everything that is placed after ? in the URL.                     | No        | (\?(.*))?                  | utm_source=twitter |
+------------------------+-------------------------------------------------------------------------------------------------------------+-----------+----------------------------+--------------------+
	
Default values in generate patterns
===================================

In some cases you want extra data inside your generate pattern. This information might not always be present when generating a URL. If a URL still contains a key like "{myCustomValue}" after processing the request, the generate action will be aborted. All keys must be resolved. We will take a look at the default pattern for generating URLs for products.

generate patterns::

	/{seo:product}/{languagecode}/product/{identifier}/ 	
		
This pattern only contains a default value for the SEO key. If a product does not contain a valid SEO text, the value 'product' will be filled in instead. The other keys have to be provided with actual data, otherwise the URL would get unexpected results.

.. warning:: The key van the default value must be separated by a ":". 

.. warning:: Default values are not available in parse patterns.

Creating label specific patterns
================================

There are cases where it might be useful to create patterns specific for one label. For example, one label might be multilingual and another one not. For this example you can create patterns for the non-multilingual label which do not include the language key. This can be done in the configuration as follows:

.. code-block:: C#
   :linenos:

   <urlPatterns>
   		<urlPattern name="ProductPattern" parsePattern="^((?&lt;protocol&gt;(.+))://)?(?&lt;url&gt;([a-z0-9\.\-]+))/([a-z0-9\-]*)/(?&lt;languagecode&gt;([a-z]{0,2}))/product/(?&lt;identifier&gt;(\d+))/(\?(.*))?$" generatePattern="/{seo:product}/{languagecode}/product/{identifier}/">
    	<specificUrlPattern labelId="2" parsePattern="^((?&lt;protocol&gt;(.+))://)?(?&lt;url&gt;([a-z0-9\.\-]+))/([a-z0-9\-]*)/product/(?&lt;identifier&gt;(\d+))/(\?(.*))?$" generatePattern="/{seo:product}/product/{identifier}/" />
  		</urlPattern>
  		// Other urlPattern elements
   </urlPatterns>

In the example the specificUrlPattern is created for the non-multilingual label (with label id 2) and values of the patterns no longer use languages when the ProductPattern is used. URLs for any other label other than 2 or other patterns without a specificUrlPattern element will still use the patterns defined in the urlPattern element.

More flexibility
================

In case the the default URL logic doesn't meet your demands, you can create your own implementation of the ecManager.UrlFormatter.AUrlLogic. The implementation can be loaded into ecManager using dependency injection. This should give you the freedom you need.


