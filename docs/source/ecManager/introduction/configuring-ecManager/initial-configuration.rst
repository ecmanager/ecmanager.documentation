.. include:: /common/stub-topic.rst

############################
|icon| Initial configuration
############################

.. warning:: This page is still under construction

This page describes the choices you most likely will make at the beginning of your implementation.

Web.config
==========

This section will describe all the important configuration sections:

- `Configuring the framework <../configuring-ecManager/configuring-the-framework.html>`_ 
- `Configuring your URLs <../configuring-ecManager/configuring-urls.html>`_ 
- `Configuring the ecManager CMS <../configuring-ecManager/configuring-the-ecManager-CMS.html>`_ 

Configuration inheritance
-------------------------

If ecManager is a child application of your website in IIS, all configuration from your website will be inherited by ecManager. This can result in unexpected results. For example, your website and ecManager both need to declare the autofac configuration section. When you declare autofac components in your website which aren't referenced in ecManager, you will run into problems. To prevent this from happening, you can wrap your configuration section in a location element to prevent inheritance as follows:

.. code-block:: C#
	:linenos:

	<location inheritInChildApplications="false">
	    <autofac configSource="Modules.config" />
	    <urlConfiguration>
	        <!-- Child elements that should not be inherited by child applications. -->
	    </urlConfiguration>
	    <!-- Other configsections that should not be inherited by child applications. -->
	</location>

It is considered a best practice to wrap all your sections like this to prevent conflicts, unless you explicitly wish your configuration to be inherited. 

.. warning:: The location element cannot be declared everywhere in your web.config. For example, it cannot be used within the <configSections> collection. The <location> element should be declared in your <configuration> element.

Caching
-------

To configure the default caching implementation see: :ref:`quick-guide` 

Modules.config
==============

Things about your modules.config.
