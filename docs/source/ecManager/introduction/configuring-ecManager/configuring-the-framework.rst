#########################
Configuring the framework
#########################

ecManager configuration section
===============================

The ecManager configuration section contains all settings for the ecManager services.

.. code-block :: xml
	:linenos:
	:caption: ecManager configuration section

	<configSections>
	 <section name="ecManagerConfig" type="ecManager.Common.Configurations.ecManagerConfigurationSection, ecManager.Common" />
	</configSections>
	<ecManagerConfig>
	 <urlConfiguration></urlConfiguration>
	 <security></security>
	 <seoSettings seoSpecificFileName="NewSEOSpecifics.xml" seoDefaultsFileName="NewSEOConfig.xml" />
	 <paths relativepath="/CmsData" absolutepath="D:\Projects\ecManager\master\Samples\Shirtify\Webshop\Shirtify.Webshop.Web\App_Data\CmsData"></paths>
	 <calculateVat calculatevatfrom="inclusive" value="true"/>
	 <allowedFileTypes></allowedFileTypes>
	 <orderNumber pattern="ORDER{7,4000001}"/>
	 <customerNumber pattern="CUSTOMER{6}"/>
	 <parameterisedPrefetchPathThreshold value="251"/>
	 <catalog>
	  <anonymousCatalog orderable="true" showProducts="true"/>
	  <customerCatalog orderable="true" showProducts="true"/>
	 </catalog>
	</ecManagerConfig>

Url Configuration
-----------------

In ecManager you are able to configure how your URLs look. In some cases you want to have your shipping country in your URL for SEO purposes. In other cases you would like to have as little data as possible. In this page we will take a look at how you can configure your own URLs so they meet your demands.

`Configuring your URLs <../configuring-ecManager/configuring-urls.html>`_ 

Security Configuration
----------------------

The framework security settings

.. code-block :: xml
	:linenos:
	:caption: Security

	<security>
	 <application authenticationSalt="{PlACEHOLDER}" authenticationRetryLimit="5" newPasswordTokenValidity="2"/>
	 <customer authenticationSalt="" authenticationEncryptionMethod="SHA512" />
	</security>

.. rubric:: Application

- authenticationSalt: The salt used for password encryption random set of characters **is required**
- authenticationRetryLimit: The maximum number of retries before locking an account a number with a minimum of 1 **is required** 
- newPasswordTokenValidity: The number of hours a new password token is valid, defaults to 2 hours **is optional** 

.. rubric:: Customer

- authenticationSalt: The salt used for password encryption random set of characters **is required** 
- authenticationEncryptionMethod: The encryption method used for password encryption defaults to SHA512 **is optional**

Seo settings configuration
==========================
Seo settings are stored in separate XML files. In the configuration you specify two paths to the XML
files. First one is for the global seo configuration and the second one contains a list of specify
configuration per entity. The XML files can be managed in the ecManager CMS. Below you can see a
sample of the configuration in the web.config.

.. code-block :: xml
	:linenos:
	:caption: seoSettings

	<seoSettings seoSpecificFileName="NewSEOSpecifics.xml" seoDefaultsFileName="NewSEOConfig.xml" />

Paths configuration
===================

All physical and relative storage of files are managed in the paths configuration

.. code-block :: xml
	:linenos:
	:caption: Paths

	<paths relativepath="/CmsData" absolutepath="D:\Projects\ecManager\CmsData">
	 <upload relativepath="/Uploads" absolutepath="D:\Projects\ecManager"/>
	 <mediabrowser relativepath="/MB" thumbpath="_thumbs" absolutepath="D:\Projects\ecManager">
	  <subpaths>
	   <subpath mediaType="Image" subpath="images"/>
	   <subpath mediaType="Video" subpath="videos"/>
	   <subpath mediaType="File" subpath="files"/>
	  </subpaths> 
	 </mediabrowser>
	 <frameworkconfig relativepath="/Config" absolutepath="D:\Projects\ecManager"/>
	 <resource relativepath="/Resources/Enumerations.resx" absolutepath="D:\Projects\ecManager"/>
	</paths>

The relativepath is the path from which the site and CMS can access the global storage 

The absolutepath is the path from which the framework can access the global storage

- upload: The place where uploads are placed before being moved to the correct location
	- relativepath: is the last part  relative path for uploads in the example above the relative path will be  /CmsData/Uploads **is required**
	- absolutepath: is the optional physical location when it is different from the global in the above example the absolute path will be D:\Projects\ecManager\Uploads **is optional**

- mediabrowser:  The place where the media browser stores its files
	- relativepath: is the last part  relative path for media browser in the example above the relative path will be  /CmsData/MB **is required**
	- thumbpath: is the relative sub path where the media browser thumbs are stored in the example above the relative path will be  /CmsData/MB/_thumbs
	- absolutepath: is the optional physical location when it is different from the global in the above example the absolute path will be D:\\Projects\\ecManager\\MB\\MediaBrowser **is optional**
	- subpath: is the element for storing mediaTypes that are declared in the `Allowed file types configuration <#id2>`_ for each declared media type a subpath must be declared.
	
		- mediaType: the media type string must match the mediaType in the allowed file types.
		- subpath: is the sub path relative to the relative path of the mediabrowser element

- frameworkconfig:  The place where all the separate configurations are placed
	- relativepath: is the last part  relative path for framework in the example above the relative path will be  /CmsData/Config **is required**
	- absolutepath: is the optional physical location when it is different from the global in the above example the absolute path will be D:\\Projects\\ecManager\\Config **is optional**

- resource: The place where the resource service gets the resource files with the translations
	- relativepath: is the last part  relative path for resource file in the example above the relative path will be  /CmsData/Resources/Enumerations.resx **is required**
	- absolutepath: is the optional physical location when it is different from the global in the above example the absolute path will be D:\\Projects\\ecManager\\Resources\\Enumerations.resx **is optional**

Calculate VAT
=============
.. code-block :: xml
	:linenos:
	:caption: CalculateVat

	<calculateVat calculatevatfrom="inclusive" value="true"/>

This settings indicates that the inclusive or exclusive amount should be calculated and which field
to calculate from.

+-------------------+------------+------------------------+--------------------------------------------------------------------------------------------------+
| Name              | Type       | Values                 | Description                                                                                      |
+===================+============+========================+==================================================================================================+
| calculateVat      | element    |                        | the container element for the attributes:calculatevatfrom,value                                  |
+-------------------+------------+------------------------+--------------------------------------------------------------------------------------------------+
| calculatevatfrom  | attribute  | inclusive / exclusive  | indicates from which field the VAT must be calculated                                            |
+-------------------+------------+------------------------+--------------------------------------------------------------------------------------------------+
| value             | attribute  | true / false           | indicated if the VAT must be calculated (for now only works if UsePriceFallBack value = "true")  |
+-------------------+------------+------------------------+--------------------------------------------------------------------------------------------------+

.. warning ::
	
	For the VAT calculation to work price fall back needs to be active. Read more about
	`Data fallback <../concepts-in-short.html#id4>`_.

Allowed file types configuration
================================

The allowed file types is where for each file category the file types (extensions) are allowed.

.. code-block :: xml
	:linenos:
	:caption: allowedFileTypes

	<allowedFileTypes>
	 <AllowedFileTypeCollections>
	  <AllowedFileTypeCollection mediaType="Image">
	   <add fileExtension="png" />
	   <add fileExtension="jpeg" />
	   <add fileExtension="jpg" />
	   <add fileExtension="gif" />
	   <add fileExtension="bmp" />
	  </AllowedFileTypeCollection>
	  <AllowedFileTypeCollection mediaType="Video">
	   <add fileExtension="flv" />
	   <add fileExtension="swf" />
	   <add fileExtension="mp4" />
	   <add fileExtension="wmv" />
	  </AllowedFileTypeCollection>
	  <AllowedFileTypeCollection mediaType="File">
	   <add fileExtension="doc" />
	   <add fileExtension="docx" />
	   <add fileExtension="gzip" />
	   <add fileExtension="pdf" />
	   <add fileExtension="zip" />
	  </AllowedFileTypeCollection>
	 </AllowedFileTypeCollections>
	</allowedFileTypes>

There are three mediaTypes in the AllowedFileTypeCollection:

Image: for all image uploads

Video: for all video uploads

File : for all file uploads

Patterns
========

The order numbers and customer numbers are generated on save with a database trigger. To let the trigger
work you need to insert a pattern. The patterns can be configured in the framework configuration.
Below you can read how the patterns can be build.

The database trigger use the FormatSequenceNumber database function. Below a few examples:

	
* Sample where 5 digit number is ``created dbo.[FormatSequenceNumber]('WEB{5}',1)`` returns WEB00001
* Sample where 5 digit nr. is created and 10000 added ``dbo.[FormatSequenceNumber]('WEB{5,10000}',1)`` returns WEB10001
* Sample with custom ordernummerhandling ``dbo.[FormatSequenceNumber]('WEB10123133',1)`` returns WEB10123133
	
	

Order number pattern
--------------------

.. code-block :: xml
	:linenos:
	:caption: orderNumber

	<orderNumber pattern="order{7,4000001}"/>

The order number pattern is used to set how the order number is increased.

The numbers in the {} are used to  {7 positions, 4000001 start number}.

Before and after the {} it is possible to add pre and post fix numbers and text.

Customer number pattern
-----------------------

.. code-block :: xml
	:linenos:
	:caption: customerNumber

	<customerNumber pattern="customer{6}"/>

The customer number pattern is used to set how the customer number is increased.

The numbers in the {} are used to  {6 positions}.

Before and after the {} it is possible to add pre and post fix numbers and text.

Parameterised prefetch path threshold
=====================================

.. code-block :: xml
	:linenos:
	:caption: parameterisedPrefetchPathThreshold

	<parameterisedPrefetchPathThreshold value="251"/>

The parameterised prefetch path threshold configuration option determines how queries are generated 
when using paging in combination with retrieving related data. It is recommended to set this value 
larger than the page size you want to use. This configuration option is optional. If not set, it 
defaults to a value of 251.

Catalog
=======

.. code-block :: xml
	:linenos:
	:caption: catalog

	<catalog>
	 <anonymousCatalog orderable="true" showProducts="true"/>
	 <customerCatalog orderable="true" showProducts="true"/>
	</catalog>

Both catalog sections have two configurable settings:

.. rubric:: orderable

- Are products orderable? If the product orderable value is ``false``, or this value is ``false``, the product is not orderable.

.. rubric:: showProducts

- Are products visible to the visitor? 

Based on authentication one of the sections is used. Anonynous (not logged-in users) correspondents
to the section anonymousCatalog, logged-in users to the customerCatalog section.