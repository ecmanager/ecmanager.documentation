#############################
Configuring the ecManager CMS
#############################

CMS configuration
=================

The CMS configuration holds all settings for the CMS that are not already set in the ecManager configuration

.. code-block:: C#
   :linenos:

   <cmsConfig>
    <productSettings productRelatedDataEnabled="false" />
    <help helpUrlPattern="https://www.manula.com/manuals/ecmanager/ecmanager/2.7.1/{0}/topic/{1}?noNav=1&amp;key=ZlysOmNQJFFA9hBTllJb4hSDuRy3yCwoNVHs8l9GQE8PLyyXJ9" />
    <instance websiteUrl="http://ecmanager.arjen.dnz" logoUrl="~/CmsData/logo.jpg"/>
    <supplier name="De Nieuwe Zaak" supportEmail="info@denieuwezaak.nl" supportPhone="+31 (0) 38 423 0163"/>
    <mail host="dnz5.dnz" fromAddress="info@denieuwezaak.nl"/>
    <defaultLabelId value="1" />
    <fallbackLabels>
        <fallbackLabel labelId="2" fallbackLabelId="1" />
        <fallbackLabel labelId="1" fallbackLabelId="3 />
    </fallbackLabels>
    <inlineContentSections>
        <!--            
        Adds an iFrame to a selection of pages to add custom UI elements.
         
        Sample section implementation:
        <Page key="Product.Detail">
            <Section key="Actions" title="Actions" height="400" url="~/iframes/myiframe.aspx"></Section>
        </Page>
         
        Properties on Page element:
        - key: unique key of the page that implements the sections [available keys : Product.Detail, Order.Detail]
         
        Properties in Section element:
        - key: unique section key [required]
        - title: Text to display in UI (section title and left menu) [required]
        - height: height of the iFrame in pixels [required]
        - url: url of the page to load [required]
        - useRetroForwarder: set to true if you want to authenticate for old CMS
        -->
    </inlineContentSections> 
   </cmsConfig>

Product settings
----------------

With the productRelatedDataEnable flag you can disable the productRelatedData. The part which is not shown is marked red.

.. image:: /_static/img/introduction/configuring-ecManager-cms.png
	:scale: 90%	

Help
----

The helpUrlPattern is the url to online the manula manual site.

Instance
--------

websiteUrl: The url to the shop site.

logoUrl: the url to the logo image.

Supplier
--------

name: name of the implementation partner

supportEmail: email adress to get support on the site and the cms

supportPhone: phone numer to get support on the site and the cms

Mail
----

host: smtp server to send mail from (only used for CMS forgotten password mails?)

fromAddress: email adress from which the forgotten passwords are send

defaultLabelId
--------------

This is the id of the label which is selected by default when no preference is set in the settings.

inlineContentSections
---------------------
The inlineContentSections give you the ability to add your custom sections to pages in the CMS. This allows you to manage extra data in for example the product detail page.
These pages are loaded using a iframe and gives you great flexibility 


fallbackLabels
--------------
The fallbackLabels contains the labels used for texts fallback based on labels. These labels are 
used when generating SEO for products and combinations. This option sets the FallbackLabelIds in 
the ServiceRequestContext. To learn more about the ServiceRequestContext read 
`ServiceRequestContext <../../introduction/concepts-in-short.html#servicerequestcontext>`_