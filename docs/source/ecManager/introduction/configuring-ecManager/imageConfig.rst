###########
imageConfig
###########

The image configuration is the place where the image size, amount, name is configured and for which entity

.. code-block:: C#
   :linenos:
   :name: ImageConfig
   :caption: ImageConfig

    <imageCollection>
    	<module name="products">
        	<image   name="MainImage" description=": hoofd (ratio: 1:1,3333 - largest 1000x1333)" 
	                max="2" keyindexes="0,1" filenamebase="main" imagedir="Products" imagesubdir="Images" 
	                usekeyassubdir="true" cutWhiteSpace="false">
	            <resize name="ExtraSmall" filenameextra="36x48">
	                <width>36</width>
	                <height>48</height>
	                <quality>100</quality>
	                <watermark></watermark>        
        	</image>
         
        	<image   name="ExtraImages" description=": extra (ratio: 1:1,3333 - largest 1000x1333)" filenamebase="extra" 
                max="4" keyindexes="0,1" imagedir="Products" imagesubdir="Images" 
                usekeyassubdir="true" cutWhiteSpace="false">
            ....           
        	</image> 
    	</module>
 
	    <module name="combinations">
	        ...
	    </module>E   
    </imageCollection>

The module element describes per entity the different images.

The following modules can be configured:

- products
- combinations
- events
- locations
- newsitems
- paymentmethodgroups
- paymentmethods
- informationitems
- navigation
- optionchoices
- attributes
- attributevalues
- productgroups
  
The image element defines the image:

- name: The name by which the image size can be retrieved.
- description: the description used in the CMS
- filenamebase: part of the image file name that will be genereated
- max: the amount of different images that can be uploaded under this name
- keyindexes: will  use part of the entity number as directory structure 0 will use the last char of the entity number as directory, values can be comma separated, make sure the index is smaller then the entity number. 
- usekeyassubdir: when true the keyindex is used as part of the directory where the image is placed
- imagedir: part of the directory structure where the image is placed
- imagesubdir:  part of the directory structure where the image is placed
- cutWhiteSpace: when resizing the white space is cut from the image  

The resize element is there to set the size of the image:

- name: name of the resize to get the image size by.
- filenameextra: post fix for the risized image filename
	- width element: the width in pixels
	- height element : the height in pixels
	- quality element : the quality in percentage (int)
	- watermark: the url to the watermark image
	
.. note:: In the **ecManager CMS**, the **GetDefaultDisplayImage** method is used to retrieve images for the listers. This method retrieves the first Image from the config with at least 151px x 101px. When no size is configured for these dimensions, no image is returned. This can result in a **ecManager CMS** lister with no images.