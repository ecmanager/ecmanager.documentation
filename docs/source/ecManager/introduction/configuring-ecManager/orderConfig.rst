###########
OrderConfig
###########

The order config is the place where you can configure what payment methods can be used to pay an order.

.. code-block:: xml
   :linenos:
   :name: OrderConfig
   :caption: OrderConfig

	<?xml version="1.0" encoding="utf-8" ?>
	<config>
		<!-- configuration about the payment methods. -->
		<betaalwijzen>
			<!-- betaalwijze: The id of the payment method. Maps to the ID of the payment method in the CMS. -->
			<!-- toeslaginclbtw: The additional fee in cents for this payment method including the VAT. -->
			<!-- toeslagexclbtw: The additional fee in cents for this payment method excluding the VAT. -->
			<!-- toeslagpercentage: The additional fee for this payment method in percentage of the Basket.SubTotal (default 0). -->
			<betaalwijze betaalwijze="82" toeslaginclbtw="295" toeslagexclbtw="295" toeslagpercentage="0">
				<!-- Configuration of country specific payment fees -->
				<!-- shippingcountryid: The shipping country id for the specific configuration. When country is not specific default is used. -->
				<landspecifiekebtw shippingcountryid="12" toeslaginclbtw="300" toeslagexclbtw="50" toeslagpercentage="0" />
			</betaalwijze>
		</betaalwijzen>
	</config>
