##################
Concepts in short
##################

*The ecManager framework is based on some high level concepts that illustrate the core functionality. In this section we'll have a closer look at the more important concepts.*

Entities
========

When talking about entities in the context of ecManager, we talk about classes that have a data container purpose. They are a sort of POCO types that consists of properties and sub-classes and do not have logic.

.. note:: Related diagram: `A typical service & entity layout <../introduction/architecture/design-diagram.html#a-typical-service-entity-layout>`_ 

Entity Id property
------------------

Entities stored in the database, in most cases, will have an Id property for identifying this specific record. This Id property is not a default C# type but a specific identifier type. Identfier types are implemented as structs and are defined in the ecManager.Common.ValueTypes namespace. The struct wraps a default C# type like Int or Short. In the code below you will see an InformationItemId type property on the InformationItem entity class.

.. code-block:: C#
	:caption: Entity Id property
	:name: Entity Id property
	:linenos:

	/// <summary>
	///     The ID of the information item.
	/// </summary>
	public InformationItemId Id { get; set; }

Entity resource property
------------------------

For type-identification and security, every entity has a Resource constant. This property contains a unique Uri that is used for identification in the security model:

.. code-block:: C#
	:caption: Sample Resource property
	:name: Sample Resource property
	:linenos:

	/// <summary>
	/// The resource of the informationitem.
	/// </summary>
	public const string Resource = "http://schemas.ecmanager.nl/ecmanager/2012-1/information/entities/informationitem";

Inherited IsNew/IsDirty/IsDeleted properties
--------------------------------------------

Every entity has some generic properties that tell about the state of the entity. The properties are 3 boolean types calles IsNew, IsDirty and IsDeleted. Most important usage is to instruct the service what to do with an entity.

+-----------+--------------------------------------------------------------------------+------------------+------------------+
| Property  | Description                                                              | New() value (1)  | Get() value (2)  |
+===========+==========================================================================+==================+==================+
| IsNew     | When true; Instructs the service to create a new entity in the database  | True             | False            |
+-----------+--------------------------------------------------------------------------+------------------+------------------+
| IsDirty   | When true; Instructs the service to update the entity in the database    | True             | False            |
+-----------+--------------------------------------------------------------------------+------------------+------------------+
| IsDeleted | When true; Instructs the service to delete the entity from database      | False            | False            |
+-----------+--------------------------------------------------------------------------+------------------+------------------+

(1) value when creating a new instance
(2) value when retrieving an instance the service

.. note:: Read more: Data persistence and deletion

Entity hierarchy
----------------

The entities defined in ecManager are not fully independent. Based on the usage, an entity is actually an hierarchy of entities to become a more business wise model.
More specifically: most entities will have a Texts entity collection for localization. The Text entity itself will be a sub-class of the parent entity. In the example below an InformationItem entity is defined with a Text as sub-class.

.. code-block:: C#
	:linenos:

	/// <summary>
	///     An entity representing an information item.
	/// </summary>
	public class InformationItem : Entity
	{
	    /// <summary>
	    ///     The collection of texts related to this information item.
	    /// </summary>
	 
	    public TextsCollection<Text> Texts { get; set; }
	 
	 
	    /// <summary>
	    ///     The texts that are related to this information item.
	    /// </summary>
	    public class Text : TextEntity
	    {
	        #region Public Properties
	 
	 
	        /// <summary>
	        ///     The introduction of an information item.
	        /// </summary>
	        public string Introduction { get; set; }
	 
	 
	        /// <summary>
	        ///     The actual text of an information item.
	        /// </summary>
	        public string Description { get; set; }
	 
	 
	        /// <summary>
	        ///     The title of an information item.
	        /// </summary>
	        public string Title { get; set; }
	 
	 
	        #endregion
	    }
	}

The same sample is also available as class diagram: Typical service & entity layout.
Classes are created an handled based on their type of relation.

.. note:: When a text entity contains the property LabelId, this property contains the value of the label for which the text is retrieved. In previous versions it contained the value for which label the text is specified. This change only has effect on entities with text fall back.

Related entities
----------------

In the entity hierarchy we define three kinds of relations.

- **1:n (one-to-many)** In a one-to-many relation we create a sub-entity containing the data. An example of a one-to-many relation is the product-price relation. A product can have multiple prices, but a price always belongs to a single product. In this case we create a sub-entity called Price which contains the data for the prices.
- **n:m (many-to-many)** In a many-to-many relation we initially use a collection of id's which function as a foreign key. In some cases the relation contains more data. A good example is the relation between a product and a group. In this case we the rank inside the product group is held in the relation. When the relation contains more data, we create sub-entities instead of a collection of id's.
- **1:1 (one-to-one)** In an one-to-one relation we do not need to store id's. We try to integrate the data inside the the main entity.
  
.. image:: ../../_static/img/introduction/related-entities.png
    :align: center
    :alt: related entities

The service Layer
=================

The main interaction from an implementation with the ecManager framework will run through the service layer. This is an application layer which contains services for retrieving and updating entities and their related data. Samples of services are the NavigationService and the ProductService.
It is important to note that the services are meant for data handling(retrieval and storing) and not for logic calls (i.e. calculations). Those types of classes will be part of separate libraries.

.. note:: Related diagram: `A typical service & entity layout <../introduction/architecture/design-diagram.html#a-typical-service-entity-layout>`_ 

Instantiation & Scope
---------------------

When you create an instance of a service, you'll have to supply constructor arguments. The constructor requires a ServiceRequestContext and sometimes a EntityState argument. Both arguments will heavily impact the data you'll get from the Services and are designed to be static within the lifecycle of the instance.

The following code blocks show you how (not) to scope your service.

.. code-block:: C#
	:caption: Implementation not according recommendation
	:name: Implementation not according recommendation
	:linenos:

	public class MyLargeScopeBasePage : System.Web.UI.Page
	{
	    // "Global" instance for ProductService
	    public ProductService LargeScopeProductService { get; set; }
	 
	    // Constructor for Page
	    public MyLargeScopeBasePage()
	    {
	        LargeScopeProductService = new ProductService(ServiceRequestContext, EntityStateContext);
	        service.RelatedDataFlags.Add(ProductRelatedDataFlags.Texts);
	    }
	}
	 
	// .. somewhere in your application ..
	 
	public void MyMethod()
	{
	    var product = base.Page.LargeScopeProductService.GetProduct(12345);
	    // Do something complicated with the product
	}

.. note:: Create instances of services with really small scopes (i.e. a method) to make sure the service ServiceRequestContext, EntityState and DataFlags match your requirements.

.. code-block:: C#
	:caption: Recommended implementation
	:name: Recommended implementation
	:linenos:

	public void MyMethod()
	{
	    var service = new ProductService(ServiceRequestContext, EntityStateContext);
	    service.RelatedDataFlags.Add(ProductRelatedDataFlags.Texts);
	 
	    var product = service.GetProduct(12345);
	    // Do something complicated with the product
	}

ServiceRequestContext
---------------------

When instantiating a service; it will always require a basic set of parameters to make sure you get the correct data (for example to get the correct pricing information for products). This set of data is wrapped in the ServiceRequestContext and contains properties like PriceListsIds, Labels and Languages. Each service uses one or more properties from the ServiceRequestContext to filter the data for you. In the examples below we will show how you can use the ServiceRequestContext to get the data you need. The ServiceRequestContext class is defined in the ecManager.Common.ComplexTypes namespace.

.. rubric:: Labels

A **Label** is a concept implemented in the framework which allows you to have multiple webshops/applications on the same catalog with different texts, prices etc.

.. rubric:: LanguageCode

LanguageCodes in the ServiceRequestContext give you the ability to filter texts and other language specific content. Texts are placed in a Texts collection which per language a text.

.. rubric:: PriceLists

A webshop/application can have multiple price lists. This allows you to set multiple prices for products and combinations, and can be used to have specific prices for customers.

.. rubric:: ShippingCountryIds

When shipping to multiple countries you can use this collection to filter the countries for this request.

.. rubric:: FallbackLabelIds

The labels which are used in the data fallback. Read more about `Data fallback <../introduction/concepts-in-short.html#id4>`_.

.. rubric:: FallbackPriceListIds

The price list used in the data fallback. Read more about `Data fallback <../introduction/concepts-in-short.html#id4>`_.

.. rubric:: Sample service call

The following code will instantiate a service and requests a single InformationItem entity.

.. code-block:: C#
	:linenos:

	// Instantiate new RequestContext
	var serviceRequestContext = new ServiceRequestContext();
	// Set label to retrieve data for
	serviceRequestContext.LabelIds = new List<LabelId>() { 1 };
	// Set language for texts
	serviceRequestContext.LanguageCodes = new List<LanguageCode>() { "nl" };
	// Set PriceLists for pricing
	serviceRequestContext.PriceListsIds = new List<PriceListId>() { 1 };
	// Set shipping country for pricing and VAT
	serviceRequestContext.ShippingCountryIds = new List<ShippingCountryId>() {21};
	// Set shipping country for pricing and VAT
	// Set PriceLists for fallback pricing
	serviceRequestContext.FallbackPriceListsIds = new List<PriceListId>() { 1 };
	// Set label to fallback on when retrieving data
	serviceRequestContext.FallbackLabelIds = new List<LabelId>() { 1 };
	 
	// Instantiate service and supply context request variables
	var service = new InformationService(serviceRequestContext);
	service.RelatedDataFlags.Add(InformationRelatedDataFlags.Texts);
	var informationItem = service.GetInformationItem(id);

In this example, only the language code property is relevant because an information item has no price and is not directly bound to a label. When you fetch the information item with the texts, the service layer will only fetch the texts with language code 'nl'. In case you want the english ánd the dutch text, you can add the "en" language code to the service request context.

.. rubric:: Another sample service call

The following code will instantiate a service and requests a single Product entity.

.. code-block:: C#
	:linenos:

	// Instantiate new RequestContext
	var serviceRequestContext = new ServiceRequestContext();
	// Set label to retrieve data for
	serviceRequestContext.LabelIds = new List<LabelId>() { 1 };
	// Set language for texts
	serviceRequestContext.LanguageCodes = new List<LanguageCode>() { "en" };
	// Set PriceLists for pricing
	serviceRequestContext.PriceListsIds = new List<PriceListId>() { 1 };
	// Set shipping country for pricing and VAT
	serviceRequestContext.ShippingCountryIds = new List<ShippingCountryId>() {21};
	 
	 
	// Instantiate service and supply context request variables
	var service = new ProductService(serviceRequestContext, MyEntityStateContex);
	service.RelatedDataFlags.Add(ProductRelatedDataFlags.Texts);
	service.RelatedDataFlags.Add(ProductRelatedDataFlags.SeoTexts);
	service.RelatedDataFlags.Add(ProductRelatedDataFlags.Prices);
	service.RelatedDataFlags.Add(ProductRelatedDataFlags.ProductTaxes);
	var product = service.GetProduct(id);

This will get you a product with the texts, seo texts, prices and taxes. The texts and seo texts will be filtered by website label 1, and language code 'en'. The prices will be related to price list id 1, and all the taxes that apply for shipping country id 21.

Data retrieval
--------------

Requesting data from the service in general comes with in 2 flavors. The first and most basic flavor is using the Get methods on a Service. Get methods accept a single Identifier or a collection of Identifiers and returns the items matching the Id(s).
This basic behavior only works if you know which entity to get. A more advanced and search like feature is implemented using so called SearchTasks.

.. rubric:: Filtering using SearchTasks

Every service is equipped with a more advanced Search method. Search methods are implemented to get entities matching your criteria. The return type of a Search method is a PagedSearchResult<T>.
The criteria for searching are defined in entity specific SearchTask classes. A SearchTask instructs the service on the criteria to apply when searching. A SearchTask also instructs the service to limit the results to a PageSize and PageNumber.
The number of matching items (=count) and returned items(=items matching page and pagesize) will change on the criteria in the searchtask.

.. warning:: A pagesize of 0 will return all items

.. rubric:: Sample service call:

The following code will instantiate a service and requests a single InformatonItem entity.

.. code-block:: C#
	:linenos:

	// Instantiate searchtask to search for Order entities ordered in the last 24 hours; pagesize 10, page 2
	var searchTask = new OrderSearchTask();
	searchTask.OrderDate = new Range<DateTime>() {From = DateTime.Now.AddDays(-1)};
	searchTask.PageSize = 10;
	searchTask.PageNumber = 2;
	searchTask.SortColumn = OrderSearchTask.SortColumnOptions.OrderAmount;
	 
	 
	// Search and get PagedResultset as return value
	PagedSearchResult<InformationItem> searchResult = service.SearchInformationItems(searchTask);

.. rubric:: High amount of filter values

Due to the parameter limit in SQL server, we do not support search tasks that exceed 2100 filter values. There is no validation on the amount of filter values. When exceeding the limit, an SqlException will be thrown. The message will say something along the lines of "Procedure or function has too many arguments specified". Every filter value in a search task and most of the values in the ServiceRequestContext will result in a SQL exception.

Avoid large amount of filter values. Use as few filter values as possible for optimal performance.  

When there is a need to use large amount of filter values, try to keep it below a thousand filter values.

.. note:: For more information about the parameter limit, please see the MSDN website. On `this MSDN page <https://msdn.microsoft.com/en-us/library/ms143432.aspx>`_ the parameter limit is listed under 'Parameters per user-defined function'.

**DataFlags**

By default the service returns a basic entity. So "GetInformationItem" will, by default, return a single entity without related entities or texts. This won't be enough for displaying the item to a website as it will not contain any texts. For this, we created the so called "DataFlags". This is a flaggable enum which lets you define your dataset.

.. note:: DataFlags will make your dataset larger and will affect performance. Keep that in mind when retrieving data.

.. rubric:: Sample service call:

This call gets an InformationItem. By adding the Texts dataflag, the service is instructed to include texts in the resultset.

.. code-block:: C#
	:linenos:

	// Instantiate service
	InformationService service = new InformationService(ServiceRequestContext);
	// Add dataflag to include Texts
	service.RelatedDataFlags.Add(InformationRelatedDataFlags.Texts);

	// Search and get PagedResultset as return value
	PagedSearchResult<InformationItem> searchResult = service.GetInformationItem(10);

.. rubric:: Services inside services

In some cases we use 'services' inside a service. For example, the ProductImages property on the ProductService is an implementation of the IImageService interface. As the name infers, this service is responsible for handling all images of the products. These services are only relevant in the context of the parent service, meaning that the service in the ProductImages will never return combination images. The nested services are named using the following convention: <EntityName><MediaType>. For example, if I want to use combination videos I'd have to use the CombinationVideos property on the CombinationService.

There are a few scenarios where we use nested services:
 - Images
 - Videos
 - Files / Downloads
   
.. note:: These services are implemented as interfaces so they can be mocked for testing purposes.

.. _data_fallback:

.. rubric:: Data fall back

ecManager provides you with a feature to fall back on data like texts from another label or prices form another price list.
The fall back mechanism uses the coalesce statement from SQL Server which we'll try to explain by an example.

.. note:: COALESCE is a SQL Server feature documented on the `MSDN <https://msdn.microsoft.com/en-us/library/ms190349.aspx>`_ page.

Imagine an implementation with three labels, selling the same products. The first two labels are targeting corporate customers. The third label is targeting consumers. In this scenario you might want your product texts to match on the first two labels, but have a different version on the third label. This would result in the following data in the database.

+------------+---------------------------------------+----------+-------+
| Product ID | Name                                  | Language | Label |
+============+=======================================+==========+=======+
| 1          | A somewhat formal title of my product | en       | 1     |
+------------+---------------------------------------+----------+-------+
| 1          | *NULL*                                | en       | 2     |
+------------+---------------------------------------+----------+-------+
| 1          | This product title is more informal   | en       | 3     |
+------------+---------------------------------------+----------+-------+

During a call to the product service, I have to specify the label I'm currently on. If I'm currently on label 2, I would get the following code. 

.. code-block:: C#
	:linenos:

	// ServiceRequestContext.LabelIds contains 1 entry, LabelId with value 2.
	var service = new ProductService(ServiceRequestContext, EntityStateContext);
	service.RelatedDataFlags.Add(ProductRelatedDataFlags.Texts);
	 
	var product = service.GetProduct(1);

This would not return a valid text, because the database entry is NULL. We can make ecManager fall back to the texts for label 1 by using the same call using a different ServiceRequestContext. We can build our label id's collection in the service request context like this:

.. code-block:: C#
	:linenos:

	ServiceRequestContext.FallbackLabelIds.Clear(); // make sure the labels are empty
	ServiceRequestContext.FallbackLabelIds.Add(new LabelId(2)); // add the specific label
	ServiceRequestContext.FallbackLabelIds.Add(new LabelId(1)); // add the fallback label

.. warning:: Keep in mind that the order in which you add the label id's is important. You should always add them from specific label to the fallback label(s)

After configuring the service request context with a fallback label the call to the product service will now return the text of the first label.

.. code-block:: C#
	:linenos:

	// ServiceRequestContext.FallbackLabelIds now contains 2 entries
	var service = new ProductService(ServiceRequestContext, EntityStateContext);
	service.RelatedDataFlags.Add(ProductRelatedDataFlags.Texts);
	 
	var product = service.GetProduct(1);
	Console.WriteLine(product.Texts.First().Name); // This should output "A somewhat formal title of my product."

.. warning:: The fallback only works when a cell value is NULL.  
	
If you would send label 3 and label 1 to the service you would get "This product title is more informal.". This same concept can be applied to prices.  We check if a product has a price for customers with price list 2, and if it doesn't we fall back to the price of price list 1.		

.. rubric:: Recommendations

Due to the fallback feature in ecManager we recommend providing your 'fallback' label or price list with as much data as you can. We also want to point out we did not create a limit to the amount of coalesce arguments. At some point you might reach a point where performance will suffer due to the amount of coalesce parameters. We recommend that you try to limit the coalesce parameters where you can. 

.. rubric:: Where can I use coalesce

Coalesce is a powerful feature that hasn't been implemented in every part of ecManager. You can use it in the following cases:
 - *Product texts* By providing several label id's, we can coalesce texts across labels. You cannot coalesce across languages.
 - *Product prices* By providing several pricelist id's, we can coalesce prices. 
 - *Combination texts* By providing several label id's, we can coalesce texts.You cannot coalesce across languages.
 - *Combination discounts* By providing several pricelist id's, we can coalesce discounts.  

.. rubric:: Enabling coalesce

If you want to enable coalescing set fill the FallbackLabelIds or FallbackPriceListIds in the ServiceRequestContext.

Data persistence and deletion
-----------------------------

In the previous section we talked about how to retrieve data from a service. That was about reading and did not mention writing or deleting data. In this section we'll talk about writing and deleting data.
Essentially every service comes with 2 Save and 2 Delete methods.

.. rubric:: Saving new or existing entities

Saving entities can be done per entity or per collection. While saving there is no distinction between creating or updating entities. This is determined per entity by interpreting the IsNew and IsDirty value.

+-------+---------+-------------------------------------------------+
| IsNew | IsDirty |                                                 |
+=======+=========+=================================================+
| True  | True    | Creates a new records                           |
+-------+---------+-------------------------------------------------+
| False | True    | Updates an existing record                      |
+-------+---------+-------------------------------------------------+
| False | False   | Will not update the record (nothing happens)    |
+-------+---------+-------------------------------------------------+

 .. rubric:: Sample service call:

The following sample instantiates a new InformationItem entity and persists it using the InformationService.

.. code-block:: C#
	:linenos:

	// Create entity to save
	var itemToSave = new InformationItem(); // Defaults to IsDirty = true, IsNew = true, IsDeleted = false
	itemToSave.CmsDescription = "Test save";
	/* Fill other properties */
	 
	 
	var service = new InformationService( ServiceRequestContext );
	// Save new entity to database
	service.SaveInformationItem(itemToSave);

.. rubric:: Sample service call:

The following sample retrieves a record from the databases, updates a property and persists it.

.. code-block:: C#
	:linenos:

	var service = new InformationService(ServiceRequestContext);
	// Get item to update
	var entityToUpdate = service.GetInformationItem(10); // Defaults entity to IsNew = false, IsDirty = false, IsDeleted = false
	// Get item to update
	entityToUpdate.IsDirty = true;
	entityToUpdate.CmsDescription = "new description";
	/* Update other properties */
	 
	// Save existing item to database
	service.SaveInformationItem(entityToUpdate);

.. warning:: The ServiceRequestContext is not used to fill in missing data in the entities when saving. For example when saving a Review the LanguageCode property is required but is not copied for the ServiceRequestContext.

.. rubric:: Deleting entities

Deletes the item with the supplied id(s).

+-------+-----------+--------------------------------------------------------------------------------+
| IsNew | IsDeleted |                                                                                |
+=======+===========+================================================================================+
| True  | False     | Will not do anything; you request the service to delete a non existent record  |
+-------+-----------+--------------------------------------------------------------------------------+
| False | True      | Will delete the record                                                         |
+-------+-----------+--------------------------------------------------------------------------------+
| False | False     | Will not delete the record (nothing happens)                                   |
+-------+-----------+--------------------------------------------------------------------------------+

SearchTasks
-----------

Most properties of type string on a SearchTask are handled with a LIKE operation, when no LIKE operation is used this will be noted in the in code documentation of the property.

For specific types as SelectionCode and ProductId will be used as a exact match in the search.

All search operations are combined with a AND operator, with the exception of values in a Collection which are combined with a OR operator

Some SearchTasks have nested search tasks those are used to get complex filters in to the current search task.
For instance the AttributeValueSearchTask has a ProductSearchTask on which the attribute values are filtered on the products from the ProductSearchTask.

Predicate expression modules can contain their own search tasks, for instance DefaultProductPredicateExpressionProvider has the ProductSearchTask which is based on the IProductSearchTask.


Search versus Get
-----------------

Use Get when specific items are needed from known id or numbers this has slightly less overhead then a search operation.
For the rest use search


Collections
===========

Depending on the type of usage; ecManager uses different types of collections. This section describes the various usages the types of collections.

.. rubric:: Collections as return types

When executing an API call where multiple items are returned, you will receive an IEnumerable<T>. Internally ecManager will remain using IEnumerable<T> to support deferred execution in future scenarios.

.. code-block:: C#
	:linenos:
	:caption: Collections as parameter

	// sample product service
	var productService = new ProductService(MyRequestContext, my EntityState);
	 
	// Returntype of a collection retrieval method is of type IEnumerable<T>
	IEnumerable<Product> products = productService.Get( {ids} );
	
.. rubric:: Collections as properties

Collections as part of an entity are defined as type ICollection<T> with a public get and a private set. This will avoid issues with using custom collection types. Internally ecManager will handle them als List<T>.

.. code-block:: C#
	:linenos:
	:caption: Sample property

	// Sample ecManager Product entity
	public class Product : Entity
	{
	    public Product()
	    {
	        Prices = new List<Price>();
	    }
	     
	    // Collection as property on an ecManager entity will be of type ICollection<T>.
	    public ICollection<Price> Prices { get; private set; }
	}

.. rubric:: Custom collections

Sometimes ecManager uses its own collection type. For example the "TextsCollection". These types always come with a matching interface for abstraction. This interface implements ICollection and is the type that is used as property in the entity.

.. code-block:: C#
	:linenos:
	:caption: Type of collection

	public class Product
	{
	    public Product()
	    {
	        Texts = new TextsCollection<Text>();
	    }
	    // Property for texts using the interface
	    public ITextsCollection<Text> Texts { get; private set; }
	}

.. code-block:: C#
	:linenos:
	:caption: Sample collection definition

	// Collection that implementt the concrete Collection and the interface
	public class MyCollection<T> : Collection<T>, IMyCollection<T>
	{
	}
	 
	// Interface for abstraction
	public interface IMyCollection<T> : ICollection<T>
	{
	 
	}

.. rubric:: Collections in the SearchTask and PagedSearchResult

A search task is used when searching entities using ecManager. The collection properties in the search task are of type ICollection<T>. The return type is of type PagedSearchResult<T> when searching.

Return values in relation to exception handling
===============================================

When triggering a method on the ecManager framework a response is expected. This involves the actual returned response and also related to the way exceptions are handled. The returned values and exception handling is standardized throughout the framework and in this section we will give you an overview of the common scenarios and the way of handling.

For this we've created an overview with common actions. The following properties are defined per action:

- Return type: this informs you on the signature of the method
- Response: the value (or null) to expect
- Handling of implementation errors: can implementation exceptions be expected or not
- Handling of other errors: how is are exceptions handled (or not)

+--------------------+------------------------+---------------------------------------------------------------+---------------------------------------+---------------------------------+
| Action             | Return type            | Response                                                      | Implementation errors                 | Other errors                    |
+====================+========================+===============================================================+=======================================+=================================+
| Delete(Id)         | bool                   | | Successful Delete returns true otherwise false. The delete  | | Exception possible; documented      | | Exceptions are handled in     |
|                    |                        | | is also successful if the record doesn't exist at all.      | | in code documentation               | | ecManager (catch + log)       |
+--------------------+------------------------+---------------------------------------------------------------+---------------------------------------+---------------------------------+
| Delete(Ids)        | bool                   | | Successful Delete returns true otherwise false. The delete  | | Exception possible; documented      | | Exceptions are handled in     |
|                    |                        | | is also successful if records don't exist at all.           | | in code documentation               | | ecManager (catch + log)       |
+--------------------+------------------------+---------------------------------------------------------------+---------------------------------------+---------------------------------+
| Get(Id)            | Entity                 | Returns existing Entity otherwise null                        |                                       | | Exceptions are not handled in |
|                    |                        |                                                               |                                       | | ecManager (log + rethrow)     |
+--------------------+------------------------+---------------------------------------------------------------+---------------------------------------+---------------------------------+
| Get(Ids)           | IEnumerable<Entity>    | | Returns the existing entities or empty list. You will not   |                                       | | Exceptions are not handled in |
|                    |                        | | be informed if a requested entity does not exist.           |                                       | | ecManager (log + rethrow)     |
+--------------------+------------------------+---------------------------------------------------------------+---------------------------------------+---------------------------------+
| Get(Number)        | Entity                 | Returns existing Entity otherwise null                        |                                       | | Exceptions are not handled in |
|                    |                        |                                                               |                                       | | ecManager (log + rethrow)     |
+--------------------+------------------------+---------------------------------------------------------------+---------------------------------------+---------------------------------+
| | Get(TIdenfifier, | | IDictionary          | Returns an IDictionary with an identifier as key and a value. |                                       | | Exceptions are not handled in |
| | TSomething)      | | <TIdentifier,TValue> |                                                               |                                       | | ecManager (log + rethrow)     |
+--------------------+------------------------+---------------------------------------------------------------+---------------------------------------+---------------------------------+
| Move()             | bool                   | Successful Move returns true otherwise false                  | | Exception possible; documented      | | Exceptions are handled in     |
|                    |                        |                                                               | | in code documentation               | | ecManager (catch + log)       |
+--------------------+------------------------+---------------------------------------------------------------+---------------------------------------+---------------------------------+
| | Save(entity /    | bool                   | Successful Save returns true otherwise false                  | | Exception possible; documented      | | Exceptions are handled in     |
| | entities)        |                        |                                                               | | in code documentation               | | ecManager (catch + log)       |
+--------------------+------------------------+---------------------------------------------------------------+---------------------------------------+---------------------------------+
| Search()           | PagedSearchResult<T>   | Returns a PagedSearchResult<T> with 0 or more Entities        | | Exception possible; documented      | | Exceptions are not handled in |
|                    |                        |                                                               | | in code documentation  (i.e. throws | | ecManager (log + rethrow)     |
|                    |                        |                                                               | | exception when searchtask is null)  |                                 |
+--------------------+------------------------+---------------------------------------------------------------+---------------------------------------+---------------------------------+

EntityState model
=================

Some entities have been enriched with the entity state. The Entity State model allows you to define a state for one or more contexts. By creating your own module, you can decide where and when an entity should be shown on the website. Whether a product should be shown often is decided using a set of rules that differ for each implementation. When you implement this system, the end-user of the CMS will get a better understanding of where and why a product is or isn't show on the website. For this concept we assume that entities with state 0 are offline, and entities with the state 100 are online. All states from 1 to 99 can be used for substates. When you define your EntityStateContext object, you define the minimum required state. If you ask for entities with state 75 in a certain context, you will get the entities with state 75 and above. To explain in detail this concept, we will show an example below.

.. rubric:: Entity state example

We will work out the entity states of a product. The following example is hypothetical, and can differ for your implementation.
There are a few scenario's(/contexts) a product can be displayed:

- The product lister
- The product detail page
- The search results
- The Google product feed 	

A product can have a few states:

1) A product is in stock and can be ordered (online)
2) A product is out of stock, but can be ordered again soon (out of stock) This represents a product that can be ordered again as soon as the new stock has arrived.
3) A product is out of stock, and will be removed from the product catalog soon (end of life)
4) A product is visible on the site, but cannot be ordered yet (upcoming)
5) This could be a product that is part of the new collection which cannot be bought yet, which you want to showcase on your website for promotional purposes. A product is no longer visible on the website (offline)

We can combine theses states and contexts in a matrix.

+------------------------+-----------------+-----------------+-----------------+-----------------+
|                        | Product lister  | Product detail  | Search results  | Product feed    |
+========================+=================+=================+=================+=================+
| State 1 (Online)       | Yes             | Yes             | Yes             | Yes             |
+------------------------+-----------------+-----------------+-----------------+-----------------+
| State 2 (Out of stock) | Yes             | Yes             | Yes             | Yes             |
+------------------------+-----------------+-----------------+-----------------+-----------------+
| State 3 (End of life)  | No              | Yes(1)          | Yes             | No              |
+------------------------+-----------------+-----------------+-----------------+-----------------+
| State 4 (Upcoming)     | No              | Yes             | No              | No              |
+------------------------+-----------------+-----------------+-----------------+-----------------+
| State 5 (Offline)      | No              | No              | No              | No              |
+------------------------+-----------------+-----------------+-----------------+-----------------+

(1) Only accessible via the search results or direct links (f.e. from the Order History).

A product is offline by default. A different state is assigned when a product meets a set of conditions for that state.
Here we have a set of example rules:

A product is online when:

- The product has at least one price, and prices should be shown on the site.
- The product has stock > 0
- The product has the correct tax information
- The product is active (Active from date is a date in the past, active to is a date in the future)
- The release date is empty, or a date in the past
- The product is not marked as sold out
- The product is marked as orderable
- The product is connected to at least one product group that isn't reserved for the new collection.

A product is is out of stock when:

- All rules apply for being online, except the stock <= 0

A product is end of life when:

- All rules apply for being out of stock, except:
	- The product is marked as not orderable
	- The product is marked as sold out

A product is upcoming when:

- All rules apply for being online, except:
	- The product is marked as not orderable
	- The release date is a date in the future.
	- The stock option is not relevant for this state
	  
When we take these rules, we need to assign states per contexts. We number every context and state to get the following matrix:

+------------------------+--------------------------+----------------------+----------------------+----------------------+
|                        | Product lister           | Product detail       | Search results       | Product feed         |
+========================+==========================+======================+======================+======================+
| State 1 (Online)       | **context 1; state 100** | context 1; state 100 | context 1; state 100 | context 1; state 100 |
+------------------------+--------------------------+----------------------+----------------------+----------------------+
| State 2 (Out of stock) | **context 1; state 75**  | context 1; state 75  | context 1; state 75  | context 1; state 75  |
+------------------------+--------------------------+----------------------+----------------------+----------------------+
| State 3 (End of life)  | context 1; state 50      | context 1; state 50  | context 1; state 50  | context 1; state 50  |
+------------------------+--------------------------+----------------------+----------------------+----------------------+
| State 4 (Upcoming)     | context 1; state 25      | context 1; state 25  | context 1; state 25  | context 1; state 25  |
+------------------------+--------------------------+----------------------+----------------------+----------------------+
| State 5 (Offline)      | context 1; state 0       | context 1; state 0   | context 1; state 0   | context 1; state 0   |
+------------------------+--------------------------+----------------------+----------------------+----------------------+	  

If we use this matrix to design our entity state module, you can modify your product detail data get action with the following service request context:

.. code-block:: C#
	:linenos:
	:caption: Example entity state context

	var productDetailStateContext = new EntityStateContext
	{
	    Context = ProductContexts.Detail, // Enumeration containing all contexts defined for products.
	    State = ProductStates.Upcoming // Enumeration containing all possible states for products. This could be one enumeration for all types of entities.
	};
	var service = new ProductService(serviceRequestContext, productDetailStateContext);

When you call the GetProduct method the entity state service will get the product if it has the state 'Upcoming', 'End of life', 'Out of stock' or 'Online' as designed by the matrix. In case of the product lister, you should use the state 'Out of Stock'.

When using ecManager services, entity state is updated automatically when you are manipulating stateful 
entities. Sometimes it is useful to update data straight in the database, for example if you are 
developing a connector that exports data into the ecManager database. In this case you can read more on how to 
manually update entity state on the `entity state <../how-to/Customize%20Entity%20State/index.html#manually-updating-entity-state>`__ page.

Security
========

.. image:: ../../_static/img/introduction/concept-in-shorts-1.png
	:scale: 90%

Service calls will be checked by the security model when accessing them. This model is based on Microsoft's CodeAccessSecurity.
Authentication is done using the Thread.CurrentPrincipal which translates in an ApplicationUser entity. A user can be related to multiple Roles. A Role relates to multiple permissions and a permissions tells about the allowed Action(i.e. Read) on a Resource (i.e. InformationItem Entity)
Permissions checking created the requirement that an application needs to authenticate before making any other calls. The security can be extended to cover custom resources (classes, ..) in custom modules.

.. note:: For more information, read the security section here: `Architecture <../introduction/architecture/index.html>`__ 

Besides code access security; the most basic level of security can be defined on database level. This means you can create an account in SQL Server with restricted permissions. You can imagine restricting saving a product from the website.

Modules
=======

Some parts of the ecManager logic are changeable with modules, Those modules are configurable in the `modules.config`.
Information on how this works can be found on the `modules <../how-to/Configuring%20Modules/index.html>`_ page.

Images
======

ecManager images are saved for a corresponding entity by a image service that is located on the entity service.
For instance the ProductService has a property ProductImages which can retrieve, save and delete images for products.

.. code-block:: C#
	:linenos:
	:caption: ProductService

	/// <summary>
	///     A service to manage the product images.
	/// </summary>
	public IImageService<ProductId, ProductNumber> ProductImages { get; set; }

These all make use of a media service which makes it possible to handle the files for those entities.
The media services are all initialized with the id and the number of the entity.

The IImageService
-----------------

The image service is responsible for the images on a entity and exposes the following method types:

- GetImageSize is for getting the image sizes configured in the ImageConfig.xml
	- Get image size by name
	- Get image size by predicate on width and height
- GetImage is for getting the sized image with a chosen size from imagesizes
	- Get image by sized image
	- Get image by sized image with a image index to get the other images for this size 
- SaveImage is for saving a image
- DeleteImage is for deleting images
- GetDefaultDisplayImage is for getting the image when there is no image for the entity
- GetOriginalImageUri is for getting the original not resized image for the entity

.. rubric:: Getting images for a entity

There are several approaches for getting the correct image(s). the recommended method is by configuring image resize names in the ImageConfig.xml and also configure those in the implementation.
Then the implementation can retrieve a image size by image name and resize name. Using the image size the correct sized image can be retrieved, which contains the URL to the image and the actual size.

Other approaches are getting the image that is closest in size by getting the imageSize by size predicate and then getting the correct image. Or reading all image sizes and choosing the correct size from code.

.. code-block:: C#
	:linenos:
	:caption: Get image
	
	ProductService service = new ProductService(.......);
	var imageSize = service.ProductImages.GetImageSize("MainImage", "Size100x100");
	var sizedImages = service.ProductImages.GetImages(productNumber, productId, imageSize);
	var imageUrl = sizedImages.First(image => image.index == 1).Location;

Configuring the image names and resize names
--------------------------------------------

The image config is described in: `ImageConfig <../introduction/configuring-ecManager/imageConfig.html>`__ 	

File IO
========

ecManager is very flexible in the way that it allows you to use different data sources for different files. 
File IO within ecManager is based on virtual paths. This means that on an application level you do not have any knowledge about where files are stored. 

These virtual path have to follow a few simple rules:
  - All paths should start with the path configured as RelativePath in your ecManager configuration
  - All virtual paths need to be lowercase
  - All virtual path start with a '/'
  - Paths cannot start with '~/'
  
As an example of this let's say we want to store two types of files. The first one is private data in the form of résumés. 
The second one is public data in the form of images which will be used as visuals your website. 
The résumé files should never be on public storage whereas the visuals are required to be on public storage. 
Based on a virtual path such as ``/cmsdata/private/resumes`` you can configure that the resumes should be stored in the database whereas the visuals can 
be stored in ``/cmsdata/public/visuals`` and are stored in a public directory of your webserver. 

Because you cannot use these virtual urls to link to your visual in the public folder on you website you can have the UrlService translate these virtual 
paths to an absolute path. This is done as follows:

.. code-block:: c#
	:linenos:
	
	var virtualPath = new Uri("public/visuals/mynewvisual.jpg");
	UrlService service = new UrlService(...);
	var absolutePath = service.ResolveUrl(virtualPath);

More information on how this works can be found in the `Storage <../services/storage/index.html>`__ documentation.