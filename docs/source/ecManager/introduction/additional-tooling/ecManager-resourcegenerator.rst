###########################
ecManager resourcegenerator
###########################

The ResourceGenerator can be used when there is a need for custom logic to select resource files in an application.

An example for this custom logic are labels. A label is the same website with a different target group. Each target wants to see different texts and the texts are generally saved in resource files. This tool gives you the ability to use a different set of resource files (.resx) for every label. The label is just an example of a criterion which you can use.

In the default resources files for C# project the ResourceManager contains the logic to determine which resources are selected and what criteria are used. The default ResourceManager can handle only one criterion which is the language. The main goal of the ResourceGenerator is to get control over the ResourceManager. 

The ResourceGenerator consist of three parts:

- The custom tool
- A ResourceManager
- A key in the App.config/Web.config
  
Installing the ResourceGenerator custom tool and the Item template
==================================================================

For the installation of the custom tool follow these steps:

1) Go to the "Tool" directory of the ecManager release linked to the implementation (Webshop).
2) Go into the "ResourceGenerator" sub directory.
3) Open the "ecManager ResourceGenerator.vsix"
4) Select the version of Visual Studio for which you want to install the extension.

After these steps there are two additions in your Visual Studio:

- There is a custom tool installed. The name of the custom tool is: "ecManager.ResourceGenerator".
- There is an Item Template installed. The item template can be found by adding an item to a project and select "ecManager Resources".
  
Default behaviour of the Resource.resx
======================================

When adding a resource file in Visual Studio, Visual Studio generates a **resource.Designer.cs** file for you this file contains the logic to access the resources in a type strict way. This is done by adding a property with the name of the resource for every resource (element in a resource file).

.. code-block:: C#
   :linenos:
   :caption: Designer code

    /// <summary>
	///   Looks up a localized string similar to Test.
	/// </summary>
	internal static string ExampleResource {
	    get {
	        return ResourceManager.GetString("ExampleResource", resourceCulture);
	    }
	}

As you can see in "Designer code" the resource will be retrieved from a ResourceManager. The default ResourceManager knows how to handle different cultures and languages. The ResourceManager is for the default designer code is **private static**. This is the reason why we can't implement a custom ResourceManager with the default designer code without a custom ResourceManager. That why it is not possible to apply custom logic for selecting the resource files.

Using the ResourceGenerator
===========================

After installing the tool you can start using the ecManager ResourceGenerator:

1) (Re)Start Visual Studio
2) Open the project/solution where the resources files needs to be
3) Right click on the project and select Add → New Item... (Ctrl + Shift + a)
4) Select "ecManager Resources"

Now you have a resource with custom designer code but with the default ResourceManager. (System.Resources.ResourceManager)

.. rubric:: Configure a custom ResourceManager:

1) Create a ResourceManager ResourceManager.cs in your project:

.. code-block:: C#
   :linenos:
   :caption: ResourceManager.cs

   	using System.Reflection;
	namespace Shirtify.Webshop.Resources
	{
	    public class ResourceManager : System.Resources.ResourceManager
	    {
	        public ResourceManager(string baseName, Assembly assembly ) : base(baseName, assembly)
	        {
	        }
	    }
	}

2) Create a App.config or a Web.config (or use existing). Right click on the project → Add new item... → Application Configuration File.
3) Create or search for the "appSettings" section.
4) Add the following key: 

.. code-block:: xml
   :linenos:
   :name: App.config
   :caption: App.config

   <add key="ResourceManager" value="ResourceManager" />

   Now the ResourceManager will use the resource manager from you in your project. It's possible to define a path to a referenced library.

5) After editing or running the custom tool for the resource file your ResourceManager will be used.
   
When not specifying a ResourceManager in the config the ResourceGenerator will use the default ResourceManager from the .NET framework.   

Creating custom ResourceManager
===============================

For this example there is a custom ResourceManager which uses the label in the resource file name. When the label is equal to the "default label" the label name isn't used.

.. code-block:: C#
   :linenos:

    using System;
	using System.Reflection;
	 
	namespace Shirtify.Webshop.Resources
	{
	    public class ResourceManager : System.Resources.ResourceManager
	    {
	        /// <summary>
	        /// The name of the label for the current request.
	        /// </summary>
	        [ThreadStatic]
	        public static string LabelName;
	        /// <summary>
	        /// The default label name. When the label in LabelName is the same the default resource (without label specified) is used.
	        /// </summary>
	        public static string DefaultLabelName;
	        
	        /// <summary>
	        /// Creation of the ResourceManager. The label will be used for the BaseNameField.
	        /// </summary>
	        /// <param name="baseName"></param>
	        /// <param name="assembly"></param>
	        public ResourceManager(string baseName, Assembly assembly ) : base(baseName, assembly)
	        {
	            if(LabelName != null && LabelName != DefaultLabelName)
	                BaseNameField += "." + LabelName;
	        }
	    }
	}

To use this ResourceManager by setting the Label as follow before using a resource::

	Resources.ResourceManager.LabelName = "DemoRed";

and setting the default label as::
	
	Resources.ResourceManager.DefaultLabelName = "Demo";

When the label is “Demo” the default resource files are used and when the label is “DemoRed” the resource files for that label are used:

Demo:

- "Account.resx"
- "Account.en.resx"
  
DemoRed:

- "Account.DemoRed.resx"
- "Accont.DemoRed.en.resx"

The ResourceManager used in this example contains no fallback mechanism implemented for when a resource file does not exist for a certain label. When this functionality is required it's possible to implement your own ResourceManger.
 
After all these steps you can use you own ResourceManager with you own logic for selecting the reseources. This functionality gives you great flexibility which can be useful in a variety of scenarios.