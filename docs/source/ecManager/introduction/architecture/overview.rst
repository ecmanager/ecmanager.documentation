########
Overview
########

*This document describes some high level architectural aspects of the ecManager framework.*

The following schema shows the global architecture of the ecManager project. The top gray blocks show the ecManager CMS and custom website implementations that are build on top of the ecManager framework.

.. image:: /_static/img/introduction/architecture-overview.png
    :align: center
    :alt: architecture overview


+----------------+-----------------------------------------------------------------------------------------------------------------+
| Component      | Description                                                                                                     |
+================+=================================================================================================================+
| Common         | | The Common library is widely used within (almost) all other libraries and projects.                           |
|                | | It mainly focuses on containing base-types, interfaces and widely used classes.                               |
+----------------+-----------------------------------------------------------------------------------------------------------------+
| Security       | | Next to the common library lives a security library.                                                          |
|                | | This library handles the security within the framework.                                                       |
+----------------+-----------------------------------------------------------------------------------------------------------------+
| Service host   | |                                                                                                               |
+----------------+-----------------------------------------------------------------------------------------------------------------+
| Service layer  | | The service layer is the public interface to communicate with when implementing a website or application.     |
|                | | It supplies services and methods to retrieve and manage data.                                                 |
+----------------+-----------------------------------------------------------------------------------------------------------------+
| ecManager core | | The ecManager.Core is a overall name for a set of domains and libraries implementing the logic that is        |
|                | | triggered from the Service Layer.                                                                             |
|                | | The core is split up into business domains and every domain has several libraries.                            |
|                | | The schema at the right shows the ecManager Core having several vertical and independent business domain.     |
|                | | For example the Catalog Domain (Products, Groups) and the Commerce (Navigation, Promotions) domain.           |
+----------------+-----------------------------------------------------------------------------------------------------------------+

The layout of a business domain within the ecManager Core
=========================================================

When opening up a domain you will get the following libraries. The service layer is added in the schema but does not live in a specific domain.

.. image:: /_static/img/introduction/architecture-overview-1.png
    :align: center
    :alt: business entity  

Entities library
----------------

A library that contains classes, entities and definitions that are used through the domain. Entity libraries that are part of a domain should not be used by other domains. Generic entities are part of the common domain. Communication between entities and domains can be done using interfaces (i.e. Product which is part of Catalog domain that will convert into an Orderline which is part of the Customer domain).
The entity libraries will be used in the service layer and the dependent applications.

Business Logic Library
----------------------

Every service call is forwarded to a Logic library. This is where a call is handled, forwarded to the database and returned. More specific:

 - When retrieving entities; it will parse your dataflags and retrieve the requested dataset
 - When saving entities; it will parse the IsNew/IsDirty/IsDeleted properties and update the database with the requested insert/updates/deletes

Data Access Library
-------------------

In the Business Logic Library there will be determined what data *will be* retrieved. In the Data Access Library the data *is* retrieved. We use `LLBLGen <https://www.llblgen.com/>`_ to generated the access to our database.

Test library
------------

Domain specific tests are created here.

The Common domain
=================

The Common library is widely used within all other libraries and projects. It mainly focusses on containing base-types/interfaces/classes. It is the library which contains elements which are used throughout the framework.


The Security domain
===================

The security domain is a broadly available domain within the ecManager framework. The domain facilitates entities like user, role and permissions. It supplies helpers for encryption and also supplies logic on checking permissions and doing auditing.

.. image:: /_static/img/introduction/architecture-overview-2.png
    :align: center
    :alt: The Security domain   

Authentication
--------------

Within ecManager there are two user stores. One user-store is for saving end customers that login on a webshop for making orders. The other user is for authenticating on the framework / CMS and that is where we focus on in the security domain. 
When calling an action on an ecManager Service; the service will try to check if the current user is allowed to make the requested call (see Authorization). For this, the implementing application needs to have a Authenticated IIdentity. For the CMS this is arranged using the MVC Authenticate logic where the user that logins gets set as user for the framework as well.
For other implementing websites, services or applications; the executing user(current) can be used to authorize. That means the framework can use any identity to authorize with; for example:

- IIS Application pool identity
- WCF Identity
- A windows service identity

The table below addresses three scenarios.

.. image:: /_static/img/introduction/architecture-overview-3.png
    :align: center
    :alt: three scenarios
    :scale: 90%

After authorization; the user will be updated with the allowed roles and related permissions.
As mentioned before; the ecManager Framework will authorize every request. This means that every requesting party (user, website or application) needs to be able to supply its identity. It also means you can narrow down the permissions on for example the website user to avoid deletion of products in the website.

Authorization
-------------

After authorizing; we have a user with roles and permissions. A user can have several roles and every role has a collection of permissions. A permission is a combination of a resource(ie. Entity) and an Action (Read, write, delete, ..).

.. image:: /_static/img/introduction/architecture-overview-4.png
    :align: center
    :alt: three scenarios
    :scale: 90%

With this information every service call can be authorized by checking if user X has permissions Y on resource Z:

- Is "John" allowed to "View" the "Product Page"?
- Is "Henk" allowed to "Delete" a "Product Entity"?
- Is user "Website" allowed to "Write" an "Order Entity"?

Permissions, Resources & Actions
--------------------------------

Within ecManager; authorization is done based on an Identity, a Resource and an Action. The Identity stands for the "user" that calls the action. The Resource can be any unique identifiable object (Entities or Pages) and an Action tells about the action you want to apply on the Resource.
Sample:

- Resource: Product Entity, Action: Read
- Resource: User Page, Action: View
- Resource: User Entity, Action: Write

When calling a method on the service; the requested resource and action are matched with the active identity permissions.

Auditing
--------

A different aspect on security is checking the usage of methods and the possibility to check what made a change to an entity.
Auditing is a service that can log(audit) the actions that are called by a user. With auditing is it possible to track events on entities. Auditing is facilitated in ecManager and can be configured to act on a certain level to avoid audit trashing.

Database Security
-----------------

ecManager will not force any database security. Any application can use any credential to logon to the database. To raise the security level is it wise to separate logins and give or take permissions on database objects.

The CMS domain
==============

The CMS domain is about managing CMS Specific information. It is NOT about the content that is managed through the ecManager CMS. For example; the users that are allowed to access the CMS are managed through this domain. A second sample; products; is about the content that is managed using ecManager is so is not part of this domain.

ecManager.CMS.Web (web project)
-------------------------------

The Cms.Web project is the MVC project that provides the User Interface for managing the websites content. The webproject references most other domain because it will manage content cross-domain.
The project contains:

- Models?
- Controllers?
- Views (partial views)
- Style sheets
- Script files
- Images

ecManager.Cms.UI (control library)
----------------------------------

The ecManager Cms project will partially be founded on a control library. This library contains the base for the ecManager CMS. 
It is separated from the CMS to allow you to create plug-in pages with custom functionalities.
To learn more about the custom pages read `Extend the ecManager CMS <../../../cms/extension/index.html>`_.
