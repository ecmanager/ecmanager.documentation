##############
Design diagram
##############

*The ecManager architectural diagrams are grouped on this page. Per diagram a short checklist/description is included to empower the diagram.*

A typical service & entity layout
=================================

.. image:: /_static/img/introduction/design-diagram-1.png
    :align: center
    :alt: typical layout of a service
    :scale: 90%

This diagram displays a typical layout of an service, the involved entities and base classes.

**A service class:**

- Is located in the ecManager.Services domain
- Inherits from base Service (ecManager.Common domain)
- Instantiates with a ServiceRequestContext
- Has a property with flags for retrieving related data
- Has Get, Search, Save and Delete methods
- Has its logic implemented in a specific domain
- Accepts and returns entities defined in a specific domain
- Accepts and returns IEnumerable<T> collections

**A logic class:**

- Is located in a specific domain (i.e. ecManager.Information domain)
- Inherits from BaseLogic (ecManager.Common domain)
- Implements the Get, Search, Save and Delete methods called by the service
- Has static method for retrieving prefetch data (implements RelatedDataFlags)
- Has static method for handling predicates (implements SearchTask)
- Uses converters to translate LLBLEntities to ecManagerEntities and vice verse

**An entity class:**

- Is located in a specific domain (i.e. ecManager.Information domain)
- Inherits from Entity class (ecManager.Common)
- Is a set of properties and sub-classes that define an entity including its related entity
- Has a Resource property to identify the type (also used for security model)
- Mostly has an Id property (specific Id type)
- Mostly has an Text sub-class for localizing purpose
   

**A SearchTask class:**

- Defines properties you can search entities for
- Implements a SearchTask

  - Defines properties for paging (page size and number)
  - Defines properties for sorting (sort column and direction)

The EntityState overview
========================

.. image:: /_static/img/introduction/design-diagram-2.png
    :align: center
    :alt: the EntityState overview
    :scale: 90%

**When requesting a product:**

- You instantiate a ProductService with the Context (ie. Lister) and required State  (ie. Online)
- This will trigger ecManager to only return Products that are Online when displayed on a Lister
- The in ecManager.Catalog suggested ProductStateContexts and ProductStates enumerations are suggested values
  
**When dispaying the Product state in the CMS:**

- The Cms will request the state for all contexts
- For this; a IStateLogic implementation is required

  - This StateLogic will return Contexts and their Criteria

The Code Access Security overview
=================================

.. image:: /_static/img/introduction/design-diagram-3.png
    :align: center
    :alt: The Code Access Security overview
    :scale: 90%

When requesting an InformationItem
==================================
  

