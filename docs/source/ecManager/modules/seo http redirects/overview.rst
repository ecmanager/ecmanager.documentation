########
Overview
########

ecManager supports SEO redirects. SEO redirects allow you to redirect a URL based on an exact match or a regular expression. 

This section will describe the HTTP module for SEO redirects which is provided by ecManager. You can read about how it 
can be registered, configured and used.

Visit the `Quick guide <quick-guide.html>`_ page to read more on how to get started using the HTTP redirect module.

.. warning:: ecManager support redirects of the type **Search**, these redirects are excluded from the HTTP redirect module.