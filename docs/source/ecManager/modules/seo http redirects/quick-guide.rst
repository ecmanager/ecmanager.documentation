###########
Quick guide
###########

Introduction
============
This module will catch all requests and checks if there is a redirect defined for the current request URL. 
If defined, it will perform a redirect with the defined status code. The catch is performed before entering the application flow of ASP.NET.

.. warning:: ecManager support redirects of the type **Search**, these redirects are excluded from the HTTP redirect module.

Prerequisites
=============
To start using this module, you need to install the HttpRedirectModule NuGet package.

.. code-block:: PowerShell

	PM> Install-Package ecManager.Modules.HttpModules.HttpRedirectModule

Configuration
=============
To enable the HTTP redirect module in IIS, add the following to your web.config:

.. code-block:: XML
	:linenos:

	<system.webServer>
	    <modules>
	        <add name="HttpRedirectModule" type="ecManager.Modules.HttpModules.HttpRedirectModule, ecManager.Modules.HttpModules"/>
	    </modules>
	</system.webServer>

To configure the HTTP redirect module, add the following to your web.config:

.. code-block:: XML
	:linenos:

	<configuration>
	    <configSections>
	        <section name="ecManager.Modules.HttpModules.HttpRedirectModule" type="ecManager.Modules.HttpModules.Configuration.RedirectModuleConfigurationSection, ecManager.Modules.HttpModules" />
	        // Other config sections
	    </configSections>
	    
	    <ecManager.Modules.HttpModules.HttpRedirectModule>
	        <Identity username="MyRedirectUsername" token="TheToken" />
	    </ecManager.Modules.HttpModules.HttpRedirectModule>
	</configuration>

Permissions
===========
The HTTP redirect module requires the following permissions to function:

+---------------------------------+-------------+
| Entity                          | Permission  |
+=================================+=============+
| Redirect                        | Read        |
+---------------------------------+-------------+
| Caching                         | Read        |
+---------------------------------+-------------+
| Label                           | Read        |
+---------------------------------+-------------+

We recommend to create a specific user and role for the redirects with only the necessary permissions.

.. warning:: Because the module will retrieve all redirects we recommend to enable caching for the redirects to prevent lots of database calls for the redirects.