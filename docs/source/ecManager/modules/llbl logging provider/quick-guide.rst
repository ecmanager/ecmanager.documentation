###########
Quick guide
###########

Introduction
============
The ecManager framework logs a lot of data when it is interacted with. The LLBL logging provider is 
a module which allows you to output those logs to a SQL Server database using the LLBLGen Pro ORM. 

Prerequisites
=============
To start using this module, you need to install the LoggingProvider NuGet package.

.. code-block:: PowerShell

	PM> Install-Package ecManager.Modules.LoggingProvider.LLBL

Registration
============
The LLBLLoggingProvider can be registered in the ``Modules.config`` as follows:

.. code-block:: XML
	:linenos:

	<component
	  type="ecManager.Modules.LoggingProvider.LLBL.LLBLLoggingProvider, ecManager.Modules.LoggingProvider.LLBL"
	  service="ecManager.Common.Interfaces.ILoggingProvider, ecManager.Common"
	  instance-scope="single-instance">
	</component>

Accessing the log
=================
The logs will be output to the ``[dbo].[Log]`` table.

.. figure:: ../../../_static/img/ecManager/modules/llbl-logging-provider/log-table.png
	:alt: log table
	:align: center

	The Log table