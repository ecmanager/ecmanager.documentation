########
Overview
########

This section describes the LLBL logging provider module provided by ecManager. This module allows 
you to output the ecManager framework logs to a SQL Server database using LLBLGen Pro.

Visit the `Quick guide <quick-guide.html>`_ page to read more on how to get started with the 
LLBL logging provider module.
