=================
Advanced
=================

This page covers a more advanced topics concerning the search index.

1. Document identifiers
~~~~~~~~~~~~~~~~~~~~~~~~~~~

The search index itself is unable to differentiate several document types.
To support several document types, a field called document identifier is added.
The following table lists the document identifiers used by the Lucene Search Engine.

+---------------+-----------------+
| Numeric value |   Entity type   |
+===============+=================+
|             0 | AttributeValue  |
+---------------+-----------------+
|             1 | Combination     |
+---------------+-----------------+
|             2 | FaqItem         |
+---------------+-----------------+
|             3 | InformationPage |
+---------------+-----------------+
|             4 | NewsItem        |
+---------------+-----------------+
|             5 | ProductFamily   |
+---------------+-----------------+
|             6 | ProductGroup    |
+---------------+-----------------+
|             7 | Product         |
+---------------+-----------------+
|             8 | Redirects       |
+---------------+-----------------+



2. Advanced configuration
~~~~~~~~~~~~~~~~~~~~~~~~~

The configuration files for the tools have been moved to separate pages. Please select a page below.

.. toctree::
   :maxdepth: 3
   :titlesonly:
   :glob:

   advanced/*

3. Troubleshooting
~~~~~~~~~~~~~~~~~~~

In this troubleshooting section you will find more information about problems you can encounter, and how you can solve them.

3.1 Common exceptions in the tools
-----------------------------------
The following list shows a set of exceptions you can encounter when creating the search index with the tools:

.. toctree::
   :maxdepth: 3
   :titlesonly:
   :glob:

   troubleshooting/securityexception
   troubleshooting/writexml-invalidoperation
   troubleshooting/xsltransformexception


3.2 Viewing the contents of your search index
-----------------------------------------------
In some cases it is useful to check the contents of the search index. You can do this using a tool called `Luke <https://code.google.com/p/luke/>`_. 
To learn more about inspecting the search index, head over to this page:

.. toctree::
   :maxdepth: 3
   :titlesonly:
   :glob:

   troubleshooting/luke

4. Debugging product search results
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
In some cases you might want to check why a product isn't shown as a result.
To debug these situations, please check the instructions:

.. toctree::
	:maxdepth: 3
	:titlesonly:
	:glob:
	
	debug-product-search-results