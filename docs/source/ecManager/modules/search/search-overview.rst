------------------------
Module overview
------------------------

.. image:: img/lucene-medium.png
    :width: 350px
    :align: right
    :alt: Lucene.NET search library	
	
Lucene.Net is a port of the Lucene search engine library, written in C# and targeted at .NET runtime users. This module is an implementation of the Lucene.Net search engine for the ecManager platform.
You can find more information about Lucene.Net on `the official homepage <https://lucenenet.apache.org/>`_.

This module allows you to search for the following entities:

- Products
- Combinations
- Product groups
- Product families
- Attribute values
- Faq items
- Information items
- News items

There are two parts in this module. The first part is the creation of the search index. 
This process consists of several steps which will be explained in more detail in a separate section. 

The general idea is that an application creates an XML feed, which is converted to an index that can be used by Lucene.
The second part is the integration with the service layer. The goal is to expose the search results using the service layer.
To achieve this, the module integrates using the PredicateProvider system. 
In the implementation a search task made specifically for the module is provided to the service, which is forwarded to the module for further processing.
The module determines the result set and returns this to the service so it can collect actual data.

Please head over to the `Quick guide <search-quick-guide.html>`_ page to learn more about using the Lucene.Net module in an implementation.
Please head over to the `Advanced <search-advanced.html>`_ page to learn more about the details of the indexation process and the integration with the service layer.