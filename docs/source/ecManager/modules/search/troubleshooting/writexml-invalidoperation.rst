==========================
InvalidOperationException
==========================

The tool generating the base feeds might cause an InvalidOperationException in the WriteXML method. A full example of the exception can be found below.

.. code-block :: xml
	
	DEBUG - SearchFeed.Utility.FeedWriter - WriteXml - System.InvalidOperationException: There was an error generating the XML document. ---> System.ArgumentException: ' ', hexadecimal value 0x1F, is an invalid character.
	at System.Xml.XmlEncodedRawTextWriter.InvalidXmlChar(Int32 ch, Char* pDst, Boolean entitize)
	at System.Xml.XmlEncodedRawTextWriter.WriteElementTextBlock(Char* pSrc, Char* pSrcEnd)
	at System.Xml.XmlEncodedRawTextWriter.WriteString(String text)
	at System.Xml.XmlWellFormedWriter.WriteString(String text)
	at System.Xml.XmlWriter.WriteElementString(String localName, String ns, String value)
	at Microsoft.Xml.Serialization.GeneratedAssembly.XmlSerializationWriterProductExportEntity.Write2_ProductText(String n, String ns, ProductText o, Boolean isNullable, Boolean needType)
	at Microsoft.Xml.Serialization.GeneratedAssembly.XmlSerializationWriterProductExportEntity.Write5_ProductExportEntity(String n, String ns, ProductExportEntity o, Boolean isNullable, Boolean needType)
	at Microsoft.Xml.Serialization.GeneratedAssembly.XmlSerializationWriterProductExportEntity.Write6_Product(Object o)
	- End of inner exception stack trace -
	at System.Xml.Serialization.XmlSerializer.Serialize(XmlWriter xmlWriter, Object o, XmlSerializerNamespaces namespaces, String encodingStyle, String id)
	at SearchFeed.Extensions.ISerializeToXmlExtensions.Serialize(Type t, Object s, Type[] subTypes) in d:\Projects\DNZ.EP\branches\Release_2.5.x\Modules\ecManager.Search\LuceneNET\Source\SearchFeed\FeedRunner\SearchFeed\Extensions\ISerializeToXmlExtensions.cs:line 71
	at SearchFeed.Utility.FeedWriter.WriteXml() in d:\Projects\DNZ.EP\branches\Release_2.5.x\Modules\ecManager.Search\LuceneNET\Source\SearchFeed\FeedRunner\SearchFeed\Utility\FeedWriter.cs:line 186


This exception is caused by the data in the database. The type that causes the error is mentioned in the stacktrace:

at Microsoft.Xml.Serialization.GeneratedAssembly.XmlSerializationWriterProductExportEntity. **Write2_ProductText** (String n, String ns, ProductText o, Boolean isNullable, Boolean needType)



In the given example a product text is causing the error. The error describes data that cannot be converted to valid XML. 
The query below checks for the character with hexadecimal value '0x1F', which seems to go wrong most of the time. The exception states which character causes the error. 
If this is a different character in the error, you will have to modify the query by replacing the following part: 'CHAR(0x1F)'.

.. note :: The service layer has been updated so it checks the texts for these characters, but data that has been imported before this change might still cause problems.

.. code-block :: sql
		
		DECLARE @sql_controle nvarchar(MAX);
		DECLARE @sql_controlecount nvarchar(MAX);
		DECLARE @col_controleid int;
		DECLARE @schematabelcolom nvarchar(MAX);
		DECLARE @aantal int;
		DECLARE @var int;
		
		DECLARE controle_cursor CURSOR
		FOR SELECT c.column_id, 'select * FROM ' + QUOTENAME(s.name) + '.' + QUOTENAME( t.name) + ' WHERE PATINDEX(''%'' + CHAR(0x1F) + ''%'', '+ QUOTENAME(c.name) + ' ) > 0;' AS Query, 'select @var = COUNT(1) FROM ' + QUOTENAME(s.name) + '.' + QUOTENAME( t.name) + ' WHERE PATINDEX(''%'' + CHAR(0x1F) + ''%'', '+ QUOTENAME(c.name) + ' ) > 0;' AS QueryCount, QUOTENAME(s.name) + '.' + QUOTENAME( t.name) + '.' + QUOTENAME( c.name) AS TotaalNaam
		FROM	sys.columns c INNER JOIN sys.tables t ON c.[object_id] = t.[object_id] INNER JOIN
		sys.schemas s ON t.[schema_id] = s.[schema_id]
		WHERE t.[type] = 'U' AND c.system_type_id IN (167, 231, 175)

		OPEN controle_cursor
		FETCH NEXT FROM controle_cursor 
		INTO @col_controleid, @sql_controle, @sql_controlecount, @schematabelcolom

		WHILE @@FETCH_STATUS = 0
		BEGIN
 
			EXECUTE sp_executesql @sql_controlecount, N'@var int output', @var = @aantal output
			IF(@aantal > 0)
			BEGIN
				SELECT @schematabelcolom;
				EXECUTE sp_executesql @sql_controle;
			END 

		FETCH NEXT FROM controle_cursor 
		INTO @col_controleid, @sql_controle, @sql_controlecount, @schematabelcolom
		END

		CLOSE controle_cursor;
		DEALLOCATE controle_cursor;

.. warning :: This query is slow and might impact the performance of your database during it's execution. Consider executing this query outside peak hours.

.. image:: ../img/xml-show-all-characters.png
    :width: 350px
    :align: right
    :alt: Show all character in Notepad++

When you execute the query you will get a list of column names which contain the invalid data, and the rows that contain the actual invalid data.
Edit the data that is reported by the query. These characters should be removed. 
It can be useful to copy the data to a text editor you prefer to remove all strange characters.
The characters are often not visible without changing some options in your text editor.
For example, Notepad++ has an option for showing all symbols, shown in the image on the right.
