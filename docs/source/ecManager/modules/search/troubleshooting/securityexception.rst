=================
SecurityException
=================

One of the tools might cause a SecurityException. The cause of this exception can vary. 
The exception message should give you more information which problem you are encountering.
In the following sub sections you will find more details about the exceptions.


1.1 The access token could not be validated.
--------------------------------------------
This exception is caused by an invalid access token.
Please check the 'AuthenticationIdentity' and 'AuthenticationToken' properties in your configuration file.
For more details about the configuration, please head over to the `Quickstart <../search-quick-guide.html>`_ page.
The stacktrace of this exception may vary. 

An example of these exceptions can be found below.

.. code-block :: text
	
	Unhandled Exception: System.Security.SecurityException: The access token could not be validated.
	   at ecManager.Security.Logic.ApplicationPermissionLogic.GetAuthorizedClaimsIdentity(IIdentity loggedUser, String token)
	   at ecManager.Security.Logic.ApplicationPermissionLogic.Authenticate(IIdentity loggedUser, String token)
	   at ecManager.Services.AuthenticateService.Authenticate(IIdentity loggedUser, String token)
	   at LuceneFeedXslt.Program.Main(String[] args) in d:\Projects\ecManager.Lucene\Tools\FeedTransformer\LuceneFeedXslt\Program.cs:line 25

1.2 Not authorized
------------------
This exception is caused by insufficient permissions for the user that is used to request data from the Service layer. 
The user authenticating with the service layer is defined in the configuration file.
Please head over to the `Quickstart <../search-quick-guide.html#create-a-user-for-the-tools>`__ page to see which resources and actions are required.
The stacktrace of this exception may vary. An example of these exceptions can be found below.

.. code-block :: text
	
	FATAL SearchFeed.Utility.FeedConfiguration - Unable to load language settings
	System.Security.SecurityException: Not authorized
	   at ecManager.Security.Logic.EcManagerPermission.Demand()
	   at System.Security.PermissionSet.DemandNonCAS()
	   at ecManager.Services.CachingService.Get[TResult](String key, Expression`1 expression)
	   at ecManager.Services.LabelService.GetLabels()
	   at FeedUtility.LanguageManager.InitCache() in d:\Projects\ecManager.Lucene\Tools\FeedUtility\LanguageManager.cs:line 58
	   at FeedUtility.LanguageManager.GetInstance() in d:\Projects\ecManager.Lucene\Tools\FeedUtility\LanguageManager.cs:line 97
	   at SearchFeed.Utility.FeedConfiguration..ctor() in d:\Projects\ecManager.Lucene\Tools\FeedRunner\SearchFeed\Utility\FeedConfiguration.cs:line 31
