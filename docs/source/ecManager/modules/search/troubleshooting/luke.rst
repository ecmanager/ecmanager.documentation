=============================
Open a search index with Luke
=============================

This page describes how you can inspect the contents of your search index.

Prerequisites
-------------

- Java Runtime -- https://www.java.com/
- Luke Lucene Index Tool, version 3.5.0 -- https://code.google.com/p/luke/
- A valid search index

Inspecting a search index with Luke
-----------------------------------
As soon as you open Luke, the program will ask you for the path to a search index.

.. image:: ../img/luke-index-prompt.png
    :alt: Indexation process overview
    :align: center

.. note :: By default, the 'Read-Only mode' option isn't checked. It is recommended that you check this option, **especially** on a production environment.

The search engine creates a search index per label, per language. 
If your website label 1 has three languages according to the database, you should have three search indices for that label. 
The default path has the following structure::

	Path\To\Indices\wl_{website-label-id}\{languagecode}

As you can see in the image, the path to the index is the path used in the Quickstart page::

	D:\LuceneSearchModule\Indices\wl_1\nl

Press 'OK' to open the search index. 
If the path is incorrect, Luke will display a popup with extra information.
To confirm your search index has opened correctly you should see a number behind the 'Number of documents' label.

.. image:: ../img/luke-index-overview.png
    :alt: Indexation process overview
    :align: center

Once you've opened a search index, head over to the 'Search' tab. 
In this tab you can query the search index using Lucene search expressions. 
The Lucene expressions use the following format::
	
	field: value

Every document has a document type field and an identifier field.
The document type field indicates whether the document represents a product or a news item.

.. note:: The numeric values of the document identifiers are listed on the `Advanced page <../search-advanced.html#document-identifiers>`_.

The identifier field represents the ID of the entity. 
When inspecting a product, the identifier field represents the product identifier.
If you have a specific product you are looking for, you can use the following query::

	documenttype: 7
	identifier: {your-product-id-here}

Press the 'Search' button to execute the query. 
The result should be listed in the overview below the search button. 
The column names are the field names, and the values in the columns represent the data in the document.
To display the details of a single document, double click on a row in the overview.
This will show you the document and all the fields it has values for.

.. note :: If a document has two values for a single field, the value is displayed in a single column separated by a space in the search overview. This might look strange. The single page is displayed by double clicking the result. In this view the fields will be listed separately.

Wildcards in Lucene expressions
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. image:: ../img/luke-allow-wildcards.png
    :alt: Indexation process overview
    :align: right

You can  use a wildcard operation using the '*'-operator. 
If you start a value with a wildcard, you will have to enable a feature in Luke. 
Select the 'QueryParser' tab in the section above the search button, and check the 'Allow leading * in wildcard queries'.