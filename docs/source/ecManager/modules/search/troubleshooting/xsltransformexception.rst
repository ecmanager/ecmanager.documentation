==========================
XslTransformException
==========================

The feed transformer can throw an XslTransformException during the transformation process.
An example of this exception is::

	FATAL LuceneFeedXslt.XmlTransformer - Error occurred during the transformation
	System.Xml.Xsl.XslTransformException: An error occurred while loading document '../output/wl_1/en/Attributes_en.xml'. 
	See InnerException for a complete description of the error. ---> 
	System.IO.DirectoryNotFoundException: Could not find a part of the path 'D:\\output\\wl_1\\en\\Attributes_en.xml'.
	
	at System.IO.__Error.WinIOError(Int32 errorCode, String maybeFullPath)
	at System.IO.FileStream.Init(String path, FileMode mode, FileAccess access, Int32 rights, Boolean useRights, FileShare share, Int32 bufferSize, FileOptions options, SECURITY_ATTRIBUTES secAttrs, String msgPath, Boolean bFromProxy, Boolean useLongPath, Boolean checkHost)
	at System.IO.FileStream..ctor(String path, FileMode mode, FileAccess access, FileShare share, Int32 bufferSize)
	at System.Xml.XmlDownloadManager.GetStream(Uri uri, ICredentials credentials, IWebProxy proxy, RequestCachePolicy cachePolicy)
	at System.Xml.XmlUrlResolver.GetEntity(Uri absoluteUri, String role, Type ofObjectToReturn)
	at System.Xml.Xsl.Runtime.XmlQueryContext.GetDataSource(String uriRelative, String uriBase)
	--- End of inner exception stack trace ---
	at System.Xml.Xsl.Runtime.XmlQueryContext.GetDataSource(String uriRelative, String uriBase)
	at <xsl:template match="AttributeValueIds">(XmlQueryRuntime {urn:schemas-microsoft-com:xslt-debug}runtime, XPathNavigator {urn:schemas-microsoft-com:xslt-debug}current)
	at <xsl:template match="Product">(XmlQueryRuntime {urn:schemas-microsoft-com:xslt-debug}runtime, XPathNavigator {urn:schemas-microsoft-com:xslt-debug}current)
	at <xsl:template match="Products">(XmlQueryRuntime {urn:schemas-microsoft-com:xslt-debug}runtime, XPathNavigator {urn:schemas-microsoft-com:xslt-debug}current)
	at <xsl:apply-templates>(XmlQueryRuntime {urn:schemas-microsoft-com:xslt-debug}runtime, XPathNavigator )
	at Root(XmlQueryRuntime {urn:schemas-microsoft-com:xslt-debug}runtime)
	at Execute(XmlQueryRuntime {urn:schemas-microsoft-com:xslt-debug}runtime)
	at System.Xml.Xsl.XmlILCommand.Execute(Object defaultDocument, XmlResolver dataSources, XsltArgumentList argumentList, XmlSequenceWriter results)
	at System.Xml.Xsl.XmlILCommand.Execute(Object defaultDocument, XmlResolver dataSources, XsltArgumentList argumentList, XmlWriter writer)
	at System.Xml.Xsl.XslCompiledTransform.Transform(String inputUri, XsltArgumentList arguments, TextWriter results)
	at LuceneFeedXslt.XmlTransformer.Transform() in d:\\Projects\\ecManager.Lucene\\Tools\\FeedTransformer\\LuceneFeedXslt\\XmlTransformer.cs:line 85

This exception occurs when processing the Products.xslt file. 
During the transformation, the translated xml file for Attributes is included. 
The XSLT file is unable to find the file mentioned in the exception. 
To solve this problem, you need to update the XSLT file. 
Locate the variable definition for the 'preFix' variable:

.. code-block:: xml
    
    <xsl:variable name ="preFix" select="string(concat('../Feeds/Translated/wl_',$websiteLabelId,'/',$languageCode,'/'))" />
	
Edit the first argument of the concat operation. 
The actual path should refer to the relative path of the _translated_ xml files (the output folder of the XSL operation).
The path should be relative to the folder containing the XSLT files.