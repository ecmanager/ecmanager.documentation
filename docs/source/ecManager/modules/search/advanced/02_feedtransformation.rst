========================================
LuceneFeedXslt/FeedTransformation.config
========================================
This page cover the options in the FeedTransformation configuration file.
The file is part of the LuceneFeedXslt application.
The first row is the name of the element, the rows after that describe the attributes on that element.

+-----------------------------------+---------------------------------------+------------------------------------------------------------------+
|              **Element**          |            **Allowed values**         |                         **Explanation**                          |
+-----------------------------------+---------------------------------------+------------------------------------------------------------------+
| /feedTransformation               | --                                    | --                                                               |
+-----------------------------------+---------------------------------------+------------------------------------------------------------------+
|  @InputDirectory                  | Windows path with trailing slash      | The directory containing the output of the FeedRunner application|
+-----------------------------------+---------------------------------------+------------------------------------------------------------------+
|  @OutputDirectory                 | Windows path with trailing slash      | The path in which the XML files will be generated                |
+-----------------------------------+---------------------------------------+------------------------------------------------------------------+
|  @XsltDirectory                   | Windows path with trailing slash      | The path in which the XML files will be generated                |
+-----------------------------------+---------------------------------------+------------------------------------------------------------------+