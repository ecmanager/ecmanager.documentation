====================================
FeedIndexer/SearchFeedIndexer.config
====================================
This page covers the options in the SearchFeedIndexer configuration file.
The file is part of the FeedIndexer application.
The first row is the name of the element, the rows after that describe the attributes on that element.

+-----------------------------------+---------------------------------------+-----------------------------------------------------------------+
|              **Element**          |            **Allowed values**         |                         **Explanation**                         |
+===================================+=======================================+=================================================================+
| /ecManager.Search.LuceneNET       | --                                    | --                                                              |
+-----------------------------------+---------------------------------------+-----------------------------------------------------------------+
|  @inputDirectory                  | true/false                            | The path in which the raw XML files are located                 |
+-----------------------------------+---------------------------------------+-----------------------------------------------------------------+
|  @outputDirectory                 | Windows path with trailing slash      | The path in which the XML files will be generated               |
+-----------------------------------+---------------------------------------+-----------------------------------------------------------------+
|  @complexAddModifications         | true/false                            | Construct complex search queries.                               |
+-----------------------------------+---------------------------------------+-----------------------------------------------------------------+