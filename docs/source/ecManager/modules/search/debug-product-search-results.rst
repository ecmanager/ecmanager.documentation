==========================================
Debugging product search results
==========================================

There might be a scenario where you expect a product in the search results, but you cannot find it. 
This guide will describe how you can figure out why a product is missing in the search index.

1. Check the applications logs
===============================

The first step to debugging your search results is making sure the tools finish their tasks without errors. Check the log files to make sure the keyword 'ERROR' isn't present.
If you do not see any errors, please proceed to the next step. Otherwise the errors need to be resolved before continuing. 
Common errors are described in the `Troubleshooting <search-advanced.html#common-exceptions-in-the-tools>`_ section.
Once the errors have been resolved, build a new search index and check to see if the product is returned in the search results.

2. Check the translated XML files
=================================

If a product doesn't show up in the search results, the first place to check is the translated XML files.
The LuceneFeedXslt tool translates a raw XML format to a more specific format which is appropriate for Lucene.
A part of this transformation is separating the content per label and language code.
The translated files have a language code in the end of their filename.
Open the folder containing the translated XML files, and search for the following XML::

	<ProductId>{Fill-in-the-id-of-your-product}</ProductId>

Make sure you insert the correct product id in the snippet above.

When the product is present in the XML file, make sure all the texts are correct. 
If the data is correct, double check the output of the SearchFeedIndexer application.
To confirm the data is in the search index, you can use a tool called Luke to inspect it.
A short guide on using Luke can be found `here <troubleshooting/luke.html>`_.

If the data is present in the search index, confirm the paths configured in the website are correct. 
If this seems to be the case, please contact the ecManager support team for assistance.

In case the product is missing, proceed to the next chapter. 

3. Check raw input
===============================

If the product isn't present in the translated XML, check the original XML, which is translated with the XSLT tool.
The original XML files are translated by the XSLT tool.
Open the folder containing the XML files, and search for the following XML::

	<ProductId>{Fill-in-the-id-of-your-product}</ProductId>

Make sure you insert the correct product id in the snippet above.
When the product isn't present in the XML, continue to the next section.
If the product is present, please check the following points:

- Check if the product is connected to the website label. A product has a 'WebsiteLabelIds' element, containing the website labels ids. If the expected website label isn't present, please check the product groups related to the product. A product is connected to a label, if at least one product group is linked in the navigation.

.. code-block :: xml
	
	<WebsiteLabelIds>
		<WebsiteLabelId>1</WebsiteLabelId>
		<WebsiteLabelId>2</WebsiteLabelId>
	</WebsiteLabelIds>

- Check if the product has a text for the expected language code and label. The search indices are built per label, per language. If the product does not have a text for the label, it won't be added to the search index.

.. code-block :: xml
	
	<ProductTexts>
		<ProductText>
			<Tags />
			<LanguageCode>en</LanguageCode>
			<ProductName />
			<LongDescription />
			<ProductUrl>http://my.ecmanager.site/product/en/product/17/</ProductUrl>
			<WebsiteLabelId>1</WebsiteLabelId>
		</ProductText>

	</ProductTexts>

- Check the output of the XSLT processor. If you have completed the previous steps the product should be present in the search index, there seems to be an issue in the translation of the XML feeds. Check the configuration of the processor, make sure all folders are configured correctly. The *output* of the FeedRunner application should be the *input* of the XSLT transformation tool. If the configuration seems to be correct and the application doesn't return errors, contact ecManager support. 

4. Check the product
====================

.. warning :: Do not disable the product group export. Due to a bug, the product export depends on the productgroup export.

A product must meet several criteria before it is exported to the search index. Some of these criteria are configurable in the config files. Please read `Search Advanced <search-advanced.html>`_ for more information.

Check the following settings using the ecManager CMS:

- The product is active. If a product isn't active, it will not be exported.
- The product has texts for all languages.
- The product is related to a productgroup which is linked in the navigation.
- The product has stock.

These settings are the defaults for Entity State, when your rules are different you need to check other criteria.

.. note :: It is possible to include products which are sold out. Head over to the `advanced page <search-advanced.html>`_ to learn more about these configuration settings.