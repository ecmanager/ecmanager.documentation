===========
Quick guide
===========

This quick guide can be divided into two parts. The first part will describe configuring the tools that create the search index. The second part is the integration in the implementation.

1. Creating the search index
============================

This section describes how you can create a search index for the Lucene search module. 
A search index is created by running three applications. 
The first application is called FeedRunner. The FeedRunner generates an XML feed. 
The second program is called LuceneFeedXslt. The LuceneFeedXslt translates this feed into a new XML feed that only contains data that is relevant for the search index.
The third application is called SearchFeedIndexer. The SearchFeedIndexer is responsible for converting the XML feed into a search index for Lucene.
It is important to note if any of the applications fail, your search index will not be updated because they all depend on eachother.

.. image:: img/applications.png
    :alt: Indexation process overview
    :align: center
    :width: 400px

This section focusses on creating the search index using three tools.
To learn more about the indexation process, please head over to the `Advanced <search-advanced.html>`_ page.

1.1 Prerequisites
-------------------------------------------

#) An ecManager environment with some data in it. The required data varies per implementation. When the search module is used for searching products, a few products should be present in the database and so on.
#) The following applications
    #) FeedRunner
    #) LuceneFeedXslt including the XSLT stylesheets.
    #) SearchFeedIndexer
#) Optional: Access to the task scheduler

To start using this module, you need to install the Search.LuceneNET NuGet package.

.. code-block:: PowerShell

	PM> Install-Package ecManager.Search.LuceneNET


.. note:: Make sure to run the ``CreateEntitiesAndEntityfields`` stored procedure, to be able to manage all the search settings.

1.2 Preparing an output folder
-------------------------------------------

Create a new folder in which the search index will be created. Inside this folder we will need three folders.

#) Feeds
#) Indices
#) Xslt

Open the 'Feeds' folder and create two sub folders inside this folder.

#) Raw
#) Translated

Place the XSLT stylesheets shipped with the LuceneFeedXslt application in the 'Xslt' folder.
To make sure the translation works correctly, open the 'Products.xslt' stylesheet. 
Navigate to the line declaring the xsl:variable called 'preFix'.
Make sure the line matches this value:

.. code-block:: xml
    
    <xsl:variable name ="preFix" select="string(concat('../Feeds/Translated/wl_',$websiteLabelId,'/',$languageCode,'/'))" />

During the rest of this guide the following path is used::
    
    D:\LuceneSearchModule

1.3 Create a user for the tools
-------------------------------------------

Create a new role for the search tools with the following permissions:

+---------------------------------+-------------+
| Caching entity                  | Read/Write  |
+---------------------------------+-------------+
| Navigation item entity          | Read        |
+---------------------------------+-------------+
| Shipping country entity         | Read        |
+---------------------------------+-------------+
| Transport category entity       | Read        |
+---------------------------------+-------------+
| Transport entity                | Read        |
+---------------------------------+-------------+
| Url generate result             | Read        |
+---------------------------------+-------------+
| Label entity                    | Read        |
+---------------------------------+-------------+
| Product entity                  | Read        |
+---------------------------------+-------------+

Create a new user. Make sure that:

#) The user is not locked
#) The user doesn't have to change the password on next login
#) The user has the role you have just created
#) The user is not an administrator
#) The user is not a system administrator
#) The user is an application
#) The user has a token

1.4 Configuring FeedRunner.exe
-------------------------------------------

This application generates a base XML feed. The generated feed will be used as input for the rest of the process. 
Open the 'SearchFeed.config' file. Navigate to the 'searchFeed' element and edit the value of the 'outputDirectory' attribute.
The value should be::

    D:\LuceneSearchModule\Feeds\Raw

Open the 'FeedRunner.exe.config' file. Validate the settings in this file. Make sure the following appSettings are correct:

#) Main.ConnectionString -- This should contain connection string for the database.
#) AuthenticationIdentity -- The username of the user created in `step 1.3 <#create-a-user-for-the-tools>`_
#) AuthenticationToken --The token of the user created in `step 1.3 <#create-a-user-for-the-tools>`_

1.5 Configuring LuceneFeedXslt.exe
-------------------------------------------

Open the 'FeedTransformation.config' file. Make sure the configuration matches the configuration below:

.. code-block:: xml

    <?xml version="1.0" encoding="utf-8" ?>
    <feedTransformation
        InputDirectory="D:\LuceneSearchModule\Feeds\Raw\"
        OutputDirectory="D:\LuceneSearchModule\Feeds\Translated\"
        XsltDirectory="D:\LuceneSearchModule\Xslt\" />

Open the 'LuceneFeedXslt.exe.config' file. Validate the settings in this file. Make sure the following appSettings are correct:

#) Main.ConnectionString -- This should contain connection string for the database.
#) AuthenticationIdentity -- The username of the user created in `step 1.3 <#create-a-user-for-the-tools>`_
#) AuthenticationToken --The token of the user created in `step 1.3 <#create-a-user-for-the-tools>`_

1.6 Configuring SearchFeedIndexer.exe
-------------------------------------------

Open the 'SearchFeedIndexer.exe.config' file. Validate the settings in this file. Make sure the following appSettings are correct:

#) Main.ConnectionString -- This should contain connection string for the database.
#) AuthenticationIdentity -- The username of the user created in `step 1.3 <#create-a-user-for-the-tools>`_
#) AuthenticationToken --The token of the user created in `step 1.3 <#create-a-user-for-the-tools>`_

Check the properties of the of the 'ecManager.Search.LuceneNET' element.
You should have the following configuration:

.. code-block:: xml

    <ecManager.Search.LuceneNET 
		inputDirectory="D:\LuceneSearchModule\Feeds\Translated\" 
		outputDirectory="D:\LuceneSearchModule\Indices\" 
		complexAddModifications="true" />

1.7 Run all applications
-------------------------------------------

To verify all applications work as expected, please follow these steps:

#) Verify the structure created in `step 1.2 <#preparing-an-output-folder>`_ exists and that the folders are empty.
#) Run FeedRunner.exe
#) Open D:\\LuceneSearchModule\\Feeds\\Raw\\ and verify that you have 12 XML files with a file size of at least 1 Kb.
#) Run LuceneFeedXslt.exe
#) Open D:\\LuceneSearchModule\\Feeds\\Translated\\ and verify that you have 12 XML files in every folder with a file size of at least 1 Kb. There should be one folder per label.
#) Run LuceneFeedXslt.exe
#) Open D:\\LuceneSearchModule\\Indices and verify that you have one folder per label.

.. note::  It might be useful to run the application in a command line so you can review the output. Make sure the application does not report an error.

1.8 Optional: Setting up the task scheduler
-------------------------------------------

Most search indices are generated periodically. To achieve this it useful to create a small bat file with the following contents.

::

    "Path to FeedRunner Application"\FeedRunner.exe
    "Path to FeedTransformer Application"\LuceneFeedXslt.exe
    "Path to FeedIndexer Application"\SearchFeedIndexer.exe

This will run all three applications in a row. Reference the bat file in the Task Scheduler and configure a period that suits the needs of the implementation. Generating the search index once every 24 hours should be fine for most implementations. When the search log shows that there are a lot of documents filtered each query, it might be useful to build the index more often.

1.9 Optional: Configure tools using NAnt
----------------------------------------

The application configuration can be generated using NAnt. The following files need to be processed:

+---------------------------------------------------+---------------------------------------------------+
| **Format file**                                   | **Output file**                                   |
+---------------------------------------------------+---------------------------------------------------+
| FeedIndexer\\SearchFeedIndexer.exe.format.config  | FeedIndexer\\SearchFeedIndexer.exe.config         |
+---------------------------------------------------+---------------------------------------------------+
| FeedRunner\\FeedRunner.exe.format.config          | FeedRunner\\FeedRunner.exe.config                 |
+---------------------------------------------------+---------------------------------------------------+
| FeedRunner\\SearchFeed.format.config              | FeedRunner\\SearchFeed.config                     |
+---------------------------------------------------+---------------------------------------------------+
| FeedTransformer\\FeedTransformation.format.config | FeedTransformer\\SearchFeed.config                |
+---------------------------------------------------+---------------------------------------------------+
| FeedTransformer\\LuceneFeedXslt.exe.format.config | FeedTransformer\\LuceneFeedXslt.exe.config        |
+---------------------------------------------------+---------------------------------------------------+

The following properties need to be defined.

+-------------------------------+---------------------------------------------------------------------------------+
| **Property name**             | **Example value**                                                               |
+-------------------------------+---------------------------------------------------------------------------------+
| RunnerOutputDirectory         | D:\\LuceneSearchModule\\Feeds\\Raw\\                                            |
+-------------------------------+---------------------------------------------------------------------------------+
| TransformerInputDirectory     | D:\\LuceneSearchModule\\Feeds\\Raw\\                                            |
+-------------------------------+---------------------------------------------------------------------------------+
| TransformerOutputDirectory    | D:\\LuceneSearchModule\\Feeds\\Translated\\                                     |
+-------------------------------+---------------------------------------------------------------------------------+
| TransformerXsltDirectory      | D:\\LuceneSearchModule\\Xslt\\                                                  |
+-------------------------------+---------------------------------------------------------------------------------+
| IndexerInputDirectory         | D:\\LuceneSearchModule\\Feeds\\Translated\\                                     |
+-------------------------------+---------------------------------------------------------------------------------+
| IndexerOutputDirectory        | D:\\LuceneSearchModule\\Indices\\                                               |
+-------------------------------+---------------------------------------------------------------------------------+
| AuthenticationIdentity        | The username of the user created at `step 1.3 <#create-a-user-for-the-tools>`_  |
+-------------------------------+---------------------------------------------------------------------------------+
| AuthenticationToken           | The token of the user created at `step 1.3 <#create-a-user-for-the-tools>`_     |
+-------------------------------+---------------------------------------------------------------------------------+

.. note:: The ecManager configuration is required for the SearchFeedIndexer configuration. Make sure the properties for this configuration are available.

2. Integration in the implementation
====================================

This section describes how you can query the search index using the service layer. Please note that this guide focusses on retrieving data using the search module. The visual representation of the data is out of scope.

2.1 Prerequisites
----------------------------------------- 

#) An up-to-date search index
#) Access to the ecManager.Search.LuceneNET package version 1.0.6.4 or newer.


2.2 Updating the configuration
-----------------------------------------

The following sub sections describe the changes in the configuration.

2.2.1 Module configuration
~~~~~~~~~~~~~~~~~~~~~~~~~~

There are three kinds of modules that need to be defined.

#) Utility modules for search
#) Document filters
#) Predicate providers

The required modules are documented in the code snippet below.

.. container:: toggle

    .. container:: header

        **Show/Hide Code**

    .. code-block :: xml

		<component
			type="ecManager.Search.LuceneNET.LuceneNetSearchProvider, ecManager.Search.LuceneNET"
			service="ecManager.Search.Core.ISearchProvider, ecManager.Search.Core">
		</component>

		<component
			type="ecManager.Search.LuceneNET.Configuration.FileLocatorProvider.DefaultFileLocator, ecManager.Search.LuceneNET"
			service="ecManager.Search.LuceneNET.Configuration.FileLocatorProvider.IFileLocatorProvider, ecManager.Search.LuceneNET"
			instance-scope="single-instance">
		</component>

		<component
			type="ecManager.Search.LuceneNET.Configuration.DirectoryProviders.DefaultDirectoryProvider, ecManager.Search.LuceneNET"
			service="ecManager.Search.LuceneNET.Configuration.DirectoryProviders.IDirectoryProvider, ecManager.Search.LuceneNET"
			instance-scope="single-instance">
		</component>

		<component
			type="ecManager.Search.LuceneNET.Searching.Filtering.AttributeValueFilter, ecManager.Search.LuceneNET"
			service="ecManager.Search.LuceneNET.Searching.Filtering.IEntityFilter, ecManager.Search.LuceneNET"
			instance-scope="single-instance">
		</component>

		<component
			type="ecManager.Search.LuceneNET.Searching.Filtering.CombinationFilter, ecManager.Search.LuceneNET"
			service="ecManager.Search.LuceneNET.Searching.Filtering.IEntityFilter, ecManager.Search.LuceneNET"
			instance-scope="single-instance">
		</component>

		<component
			type="ecManager.Search.LuceneNET.Searching.Filtering.FaqFilter, ecManager.Search.LuceneNET"
			service="ecManager.Search.LuceneNET.Searching.Filtering.IEntityFilter, ecManager.Search.LuceneNET"
			instance-scope="single-instance">
		</component>

		<component
			type="ecManager.Search.LuceneNET.Searching.Filtering.GroupFilter, ecManager.Search.LuceneNET"
			service="ecManager.Search.LuceneNET.Searching.Filtering.IEntityFilter, ecManager.Search.LuceneNET"
			instance-scope="single-instance">
		</component>

		<component
			type="ecManager.Search.LuceneNET.Searching.Filtering.InformationFilter, ecManager.Search.LuceneNET"
			service="ecManager.Search.LuceneNET.Searching.Filtering.IEntityFilter, ecManager.Search.LuceneNET"
			instance-scope="single-instance">
		</component>

		<component
			type="ecManager.Search.LuceneNET.Searching.Filtering.NewsFilter, ecManager.Search.LuceneNET"
			service="ecManager.Search.LuceneNET.Searching.Filtering.IEntityFilter, ecManager.Search.LuceneNET"
			instance-scope="single-instance">
		</component>

		<component
			type="ecManager.Search.LuceneNET.Searching.Filtering.ProductFilter, ecManager.Search.LuceneNET"
			service="ecManager.Search.LuceneNET.Searching.Filtering.IEntityFilter, ecManager.Search.LuceneNET"
			instance-scope="single-instance">
		</component>

This should cover the changes in your module configuration.

2.2.2 Web.config 
~~~~~~~~~~~~~~~~

Open your web.config. Add the following element to the /configuration/configSections-element:

.. code-block :: xml

    <section name="ecManager.Search.LuceneNET" type="ecManager.Search.LuceneNET.Configuration.ConfigSection, ecManager.Search.LuceneNET" />

Add the following element to your /configuration-element:

.. code-block :: xml

    <ecManager.Search.LuceneNET 
        inputDirectory="D:\LuceneSearchModule\Indices" 
        outputDirectory="D:\LuceneSearchModule\Indices" 
        complexAddModifications="true" />


This should cover the changes in your web.config file.

2.2.3 log4net configuration
~~~~~~~~~~~~~~~~~~~~~~~~~~~

The search engine uses log4net for logging. If there is no configuration present for log4net, add the steps in this section.
Install the following package in your solution::

	Install-Package log4net

Open your web.config. Add the following element to the /configuration/configSections-element:

.. code-block :: xml

    <section name="log4net" type="log4net.Config.Log4NetConfigurationSectionHandler,Log4net" />

Add the following element to your /configuration-element:

.. code-block :: xml

    <log4net debug="false">
        <appender name="LogFileAppender" type="log4net.Appender.RollingFileAppender">
            <file value="D:\LuceneSearchModule\Logs\searchlog.txt" />
            <param name="DatePattern" value="-yyyy-MM-dd'.txt'" />
            <appendToFile value="true" />
            <rollingStyle value="Date" />
            <layout type="log4net.Layout.PatternLayout">
                <header value="[Header]&#xD;&#xA;" />
                <footer value="[Footer]&#xD;&#xA;" />
                <conversionPattern value="%date [%thread] %-5level %logger [%ndc] - %message%newline" />
            </layout>
        </appender>
        <root>
            <level value="DEBUG" />
            <appender-ref ref="LogFileAppender" />
        </root>
    </log4net>

.. note::  When the search engine is used frequently, the logging output can become rather large. Keep this in mind when configuring log4net. It might be wise to limit the file size or schedule a task that cleans up log files periodically.

The final step for the log4net configuration is forcing log4net to load the configuration file.
Open the part of your application that is called once at startup.
In MVC applications *Application_Start* method in the 'Global.asax' file would be the correct place.

Add the following line of code:

.. code-block :: c#

    log4net.Config.XmlConfigurator.Configure();

2.2 The first search operation
------------------------------

The code below executes a search task for products. In case your implementation does not index products, you can select a different search task.

.. code-block :: c#

    // Create a search task, use the search term 'Test'.
    var productSearchTask = new ecManager.Search.LuceneNET.SearchTasks.ProductSearchTask
    {
        SearchTerm = "Test",
        PageSize = 15,
        SortColumn = ProductSearchTask.SortColumnOptions.Relevance,
        SortDirection = ecManager.Common.SortDirection.Descending
    };
    
    // Send the search task to the product service
    var productsResult = ProductService.SearchProducts(productSearchTask);

If the operation completed without errors, the search operation was completed successfully. If there is an error you should check the following file for more details: 

.. code-block :: text

    D:\LuceneSearchModule\Logs\searchlog.txt

.. note :: Nesting is not supported with the Lucene search tasks. Combining ecManager search tasks and Lucene search tasks isn't supported.

2.3 Facets 
----------

Filters in the search engine are called facets.
The way facets are constructed differs from the way ecManager works.
Due to this difference you can provide a product search task to the attribute service.
The important step is adding facet information to the search task.

.. code-block :: c#

    // Create a search task, use the search term 'Test'.
    var productSearchTask = new ecManager.Search.LuceneNET.SearchTasks.ProductSearchTask
    {
        SearchTerm = "Test",
        PageSize = 15,
        SortColumn = ProductSearchTask.SortColumnOptions.Relevance,
        SortDirection = ecManager.Common.SortDirection.Descending
    };
    
    // Add a facet
    productSearchTask.AddFacet(new SelectionCode("BND"));
    
    // Send the search task to the product service
    var productsResult = ProductService.SearchProducts(productSearchTask);
    var attributeResult = AttributeService.SearchAttributes(productSearchTask);

This will give you the Attribute entities you also use in the regular product lister.
The same logic can be applied to build the filters. 
Keep in mind that handling the filter information does differ from the regular search task.

.. note :: There are several overloads of the AddFacet method.

.. warning :: If you provide the Lucene ProductSearchTask to the AttributeService without facet information, an exception will be thrown. If there are no facets defined, no attributes can be returned. Therefore, it does not make sense to call the search engine.

2.4 Filtering search result
---------------------------

You can use Facets to add filters to your lister.
The selected filters have to be set on the ProductSearchTask.

.. code-block :: c#

    // Create a search task, use the search term 'Test'.
    var productSearchTask = new ecManager.Search.LuceneNET.SearchTasks.ProductSearchTask
    {
        SearchTerm = "Test",
        PageSize = 15,
        SortColumn = ProductSearchTask.SortColumnOptions.Relevance,
        SortDirection = ecManager.Common.SortDirection.Descending
    };
    
    // Add a filter
    productSearchTask.AddFilter(new AttributeId(32), new AttributeValueId(89));
    
    // Send the search task to the product service
    var productsResult = ProductService.SearchProducts(productSearchTask);

.. note :: There are several overloads of the AddFilter method.