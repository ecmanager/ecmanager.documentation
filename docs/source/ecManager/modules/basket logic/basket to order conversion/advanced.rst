########
Advanced
########

Overriding behaviour
====================
It is possible to override the default behaviour of the DefaultConvertBasketToOrder module. You can 
do this by creating a class that inherits from DefaultConvertBasketToOrder, and overriding methods 
of the inherited class. 

In the following example it is shown how you can do some additional manipulation to the conversion 
of an Order.Voucher:

.. code-block:: C#
	:linenos:

	public class CustomConvertBasketToOrder : DefaultConvertBasketToOrder
	{
	    public CustomConvertBasketToOrder(ServiceRequestContext context, IDictionary<EntityTypes, EntityStateContext> entityStateDictionary)
	        : base(context, entityStateDictionary)
	    {
	    }
	
	    public override Order.Voucher ToOrderVoucher(VoucherReference reference, Voucher voucher)
	    {
	        var orderVoucher = base.ToOrderVoucher(reference, voucher);
	        orderVoucher.Description = "A converted voucher.";
	
	        return orderVoucher;
	    }
	}

Don't forget to register your custom module. This can be done as follows:

.. code-block:: XML
	:linenos:

	<component
	    type="MyAssembly.CustomConvertBasketToOrder, MyAssembly"
	    service="ecManager.Customer.Interfaces.IConvertBasketToOrder, ecManager.Customer">
	</component>

Of course it's also possible to implement a new ConvertBasketToOrder module from scratch.