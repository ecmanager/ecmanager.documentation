########
Overview
########

This section will describe the DefaultConvertBasketToOrder module provided by ecManager. The module is 
responsible for converting a basket to an order.

Visit the `Quick guide <quick-guide.html>`_ page to read more on how to get started with the module, 
or the `Advanced <advanced.html>`_ page for more advanced topics, like overriding default behaviour.