###########
Quick guide
###########

Introduction
============
This page describes everything you need to get started quickly using this module. 

Prerequisites
=============
To start using this module, you need to install the DefaultBasketLogic NuGet package.

.. code-block:: PowerShell

	PM> Install-Package ecManager.Modules.Basket.DefaultBasketLogic

When this module is accessed, it requires that the EntityState dictionary contains entity state for 
Promotions and Vouchers.

Registration
============
To use the DefaultConvertBasketToOrder module, you need to register it as follows:

.. code-block:: XML
	:linenos:

	<component
	  type="ecManager.Modules.Basket.DefaultBasketLogic.DefaultConvertBasketToOrder, ecManager.Modules.Basket.DefaultBasketLogic"
	  service="ecManager.Customer.Interfaces.IConvertBasketToOrder, ecManager.Customer">
	</component>

Default behaviour
=================
The DefaultConvertBasketToOrder module contains behaviour for the conversion which is predefined by ecManager.
The following table shows how the module converts data from an instance of the IBasket interface to a new 
Order object.

============================================= ========================== ============================================================
Input                                         Output                     Note                                                        
============================================= ========================== ============================================================
Customer                                      Order.Customer                                                                        
IBasket.BasketTotalDiscounts                  Order.Discount                                                                         
IBasket.BasketProductItem                     Order.Line                 In fact is an Order.ProductLine                            
IBasket.BasketCombinationItem                 Order.Line                 In fact is an Order.CombinationLine                        
IBasket.CustomerId                            Order.CustomerId                                                                       
IBasket.DeliveryDate                          Order.DeliverOn                                                                        
IBasket.LabelId                               Order.LabelId                                                                          
IBasket.QuoteNumber                           Order.QuoteNumber                                                                      
IBasket.PickupLocation                        Order.PickupLocationId                                                                 
IBasket.PartialPaymentAmount                  Order.PartialPaymentAmount                                                             
IBasket.PartialPayment                        Order.IsPartialPayment                                                                 
IBasket.References.PaymentMethodCostReference Order.PaymentCosts         First or default                                            
IBasket.References.PaymentMethodCostReference Order.PaymentMethodId      First or default                                            
IBasket.References.PromotionActionReference   Order.Promotion.Action     Converted if actual promotion action is found               
IBasket.References.PromotionReference         Order.Promotions           Converted if actual promotion is found                      
IBasket.References.TransporterCostReference   Order.ShippingCosts        Single or default                                           
IBasket.References.VoucherReference           Order.Vouchers             Converted if actual voucher is found                        
IBasket.Remarks                               Order.Remarks                                                                          
IBasket.Subtotal                              Order.Subtotal                                                                         
IBasket.Total                                 Order.Total                                                                            
\                                             Order.OrderedOn            The time of the conversion                                  
\                                             Order.VisibleForCustomer   Set to true                                                 
IBasket.Costs                                 Order.Configuration        Read more `here <#custom-basket-configuration-conversion>`_.
IBasket.Discounts                             Order.Configuration        Read more `here <#custom-basket-configuration-conversion>`_.
IBasket.References.TaxReference               Order.Configuration        Read more `here <#custom-basket-configuration-conversion>`_.
IBasket.Configuration                         Order.Configuration        Read more `here <#custom-basket-configuration-conversion>`_.
============================================= ========================== ============================================================

Promotions and vouchers will only be converted if they are found using the ServiceRequestContext and 
EntityStateContext passed to the module. If they are not found, the Order will not contain those 
promotions and vouchers regardless if the original basket had references to them.

.. note:: Order.Customer.LanguageCode is the first LanguageCode in the ServiceRequestContext.

Basket configuration conversion
===============================

Basket to Order configuration
-----------------------------
The IBasket interface has a Configuration property which can contain XML. This XML can be used to store 
extra data for a basket. It might be necessary to convert some of this data to an Order, ecManager 
supports this. Order also has a property to store XML in. By default, some basic basket data is stored 
in the XML configuration of an Order. This data contains taxes, discounts and costs, as can be seen in 
the following XML snippet:

.. code-block:: XML
	:linenos:

	<config>
	    <Taxes>
	        <Tax>
	           <Percentage>21.00</Percentage>
	           <Total>110.74462809921000000000000000</Total>
	        </Tax>
	    </Taxes>
	    <Discounts>
	        <Discount>
	            <InclVat>-70.9000000000</InclVat>
	            <ExclVat>-58.595041322310</ExclVat>
	            <Texts>
	                <!-- Text elements -->
	            </Texts>
	        </Discount>
	    </Discounts>
	    <Costs>
	        <Cost>
	            <InclVat>6</InclVat>
	            <ExclVat>4.9586776859504132231404958678</ExclVat>
	            <Texts>
	                <Text>
	                    <Title>PostNL Pakketten</Title>
	                    <LanguageCode>nl</LanguageCode>
	                    <Type>ecManager.Customer.Entities.TransporterCostReference</Type>
	                </Text>
	            </Texts>
	        </Cost>
	    </Costs>
	</config>


Basket items configuration conversion
-------------------------------------
When converting basket items that are of type BasketProductItem to an Order.ProductLine, some 
properties are stored in the Order.ProductLine.Configuration property, similar to the way the basket 
is converted. Possible data converted are discounts, costs, totals, subtotals, references and VAT.
The following snippet shows an example XML result after conversion:

.. code-block:: XML
	:linenos:

	<config>
	    <Discounts>
	        <Discount>
	            <InclVat>-5.0000000000</InclVat>
	            <ExclVat>-4.1322314050</ExclVat>
	            <Texts />
	        </Discount>
	    </Discounts>
	    <Costs />
	    <Reference />
	    <Vat>21.00</Vat>
	</config>
	
Custom Basket/BasketProductItem configuration conversion
--------------------------------------------------------
The XML configuration of a basket might also contain other custom data. If you want specific custom 
data to be converted from the IBasket.Configuration to the Order.Configuration or IBasketItem.Configuration to the Order.ProductLine.Configuration, you can use the 
convention shown in the following example:

.. code-block:: XML
	:linenos:

	<CustomRoot>
	    <CustomNode>
	        <CustomElement id="1" name="example1">
	    </CustomNode>
	    <CustomNode persist-in-order="true">
	        <CustomElement id="2" name="example2">
	        <CustomElement id="3" name="example3">
	    </CustomNode>
	</CustomRoot>

All child elements of elements that contain the attribute *persist-in-order* will be added to the 
root of the Order configuration XML. Converting a basket to an order using the previous example would 
result in the following Order.Configuration XML:

.. code-block:: XML
	:linenos:

	<config>
	    <!-- Tax, discount and cost elements -->
	    <CustomElement id="2" name="example2">
	    <CustomElement id="3" name="example3">
	</config>
