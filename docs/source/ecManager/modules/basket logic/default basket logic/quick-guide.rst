###########
Quick guide
###########

Introduction
============
When using the BasketService, the logic is responsible to handle the changes and change request. 
Because the logic may vary per project, the basket logic are delegated to a separate module 
within the framework. This allows you to customize the logic by creating your own implementation 
of the BasketLogic module.

In this page we will explain how the logic is designed to work, and how you can customize it to your own needs. We will take a look at the basic technical details, followed by a more detailed explanation on using the default module provided by ecManager.

Prerequisites
=============
To start using this module, you need to install the DefaultBasketLogic NuGet package.

.. code-block:: PowerShell

	PM> Install-Package ecManager.Modules.Basket.DefaultBasketLogic

Registration
============
The default services in the default basket logic module can be registered in the ``Modules.config``
file as follows:

.. code-block:: XML
	:linenos:

	<component
	  type="ecManager.Modules.Basket.DefaultBasketLogic.DefaultBasketReferenceCheckModule, ecManager.Modules.Basket.DefaultBasketLogic"
	  service="ecManager.Customer.Interfaces.ICheckBasket, ecManager.Customer">
	</component>

	<component
	  type="ecManager.Modules.Basket.DefaultBasketLogic.DefaultBasketPaymentCheckModule, ecManager.Modules.Basket.DefaultBasketLogic"
	  service="ecManager.Customer.Interfaces.ICheckBasket, ecManager.Customer">
	</component>

	<component
	  type="ecManager.Modules.Basket.DefaultBasketLogic.DefaultBasketPromotionLogic, ecManager.Modules.Basket.DefaultBasketLogic"
	  service="ecManager.Customer.Interfaces.ICheckBasket, ecManager.Customer">
	</component>

	<component
	  type="ecManager.Modules.Basket.DefaultBasketLogic.DefaultBasketVoucherLogic, ecManager.Modules.Basket.DefaultBasketLogic"
	  service="ecManager.Customer.Interfaces.ICheckBasket, ecManager.Customer">
	</component>
	
	<component
	  type="ecManager.Modules.Basket.DefaultBasketLogic.DefaultBasketTransporterCheckModule, ecManager.Modules.Basket.DefaultBasketLogic"
	  service="ecManager.Customer.Interfaces.ICheckBasket, ecManager.Customer">
	</component>

	<component
	  type="ecManager.Modules.Basket.DefaultBasketLogic.DefaultBasketShippingCountryCheckModule, ecManager.Modules.Basket.DefaultBasketLogic"
	  service="ecManager.Customer.Interfaces.ICheckBasket, ecManager.Customer">
	</component>

	<component
	  type="ecManager.Modules.Basket.DefaultBasketLogic.DefaultBasketPromotionLogic, ecManager.Modules.Basket.DefaultBasketLogic"
	  service="ecManager.Customer.Interfaces.ICleanBasket, ecManager.Customer">
	</component>

	<component
	  type="ecManager.Modules.Basket.DefaultBasketLogic.DefaultBasketTransportersProvider, ecManager.Modules.Basket.DefaultBasketLogic"
	  service="ecManager.General.Interfaces.IBasketTransportersProvider, ecManager.General">
	</component>

	<component
	  type="ecManager.Modules.Basket.DefaultBasketLogic.DefaultBasketVoucherLogic, ecManager.Modules.Basket.DefaultBasketLogic"
	  service="ecManager.Customer.Interfaces.IProcessChangeRequest, ecManager.Customer">
	</component>
	
	<component
	  type="ecManager.Modules.Basket.DefaultBasketLogic.DefaultBasketTransporterCheckModule, ecManager.Modules.Basket.DefaultBasketLogic"
	  service="ecManager.Customer.Interfaces.IProcessChangeRequest, ecManager.Customer">
	</component>

	<component
	  type="ecManager.Modules.Basket.DefaultBasketLogic.SynchronizationChangeRequestProcessor, ecManager.Modules.Basket.DefaultBasketLogic"
	  service="ecManager.Customer.Interfaces.IProcessChangeRequest, ecManager.Customer">
	</component>

	<component
	  type="ecManager.Modules.Basket.DefaultCalculateBasket.DefaultCalculateBasketLogic, ecManager.Modules.Basket.DefaultCalculateBasket"
	  service="ecManager.Customer.Interfaces.ICalculateBasket, ecManager.Customer">
	</component>

	<component
	  type="ecManager.Modules.Basket.DefaultBasketLogic.DefaultBasketLogic, ecManager.Modules.Basket.DefaultBasketLogic"
	  service="ecManager.Customer.Logic.ABasketLogic, ecManager.Customer.Logic">
	</component>

Default behaviour
=================
Changing a basket can be done with changes and change request. This functionality can be overridden 
by extending the ABasketLogic all method in this class are marked virtual of abstract. The 
following diagram describes which methods can be overridden. 

.. image:: /_static/img/services/basket/basket-16.png
	:scale: 90%
	:align: center

Entity state in the basket
==========================

During the construction of the BasketService a dictionary with the entity states is passed as an argument. The enumeration ecManager.Common.EntityTypes is used as a key, and the EntityStateContext entity is used as a value. This dictionary will be passed on to the modules configured in the module configuration. The modules must use these entity state contexts to retrieve data from the service layer. This also means the required entity states are defined by the modules. The following table lists the entity states required by the module.

+---------------------------------------------------------------------------------+------------------------+
| Module                                                                          | Required entity state  |
+=================================================================================+========================+
| ecManager.Services.BasketService                                                | | Voucher              |
+---------------------------------------------------------------------------------+------------------------+
| ecManager.Modules.Basket.DefaultBasketLogic.DefaultBasketLogic                  | | Product              |
|                                                                                 | | Combination          |
+---------------------------------------------------------------------------------+------------------------+
| ecManager.Modules.Basket.DefaultBasketLogic.DefaultBasketVoucherLogic           | | Product              |
|                                                                                 | | Voucher              |
+---------------------------------------------------------------------------------+------------------------+
| ecManager.Modules.Basket.DefaultBasketLogic.DefaultBasketPromotionLogic         | | Product              |
|                                                                                 | | ProductGroup         |
|                                                                                 | | Voucher              |
|                                                                                 | | Promotion            |
+---------------------------------------------------------------------------------+------------------------+
| ecManager.Modules.Basket.DefaultBasketLogic.DefaultBasketTransportersProvider   | | -                    |
+---------------------------------------------------------------------------------+------------------------+
| ecManager.Modules.Basket.DefaultBasketLogic.DefaultBasketTransporterCheckModule | | -                    |
+---------------------------------------------------------------------------------+------------------------+
| ecManager.Modules.Basket.DefaultBasketLogic.DefaultBasketReferenceCheckModule   | | -                    |
+---------------------------------------------------------------------------------+------------------------+
| ecManager.Modules.Basket.DefaultBasketLogic.DefaultBasketPaymentCheckModule     | | -                    |
+---------------------------------------------------------------------------------+------------------------+
| ecManager.Modules.Basket.DefaultBasketLogic.DefaultConvertBasketToOrder         | | Promotion            |
|                                                                                 | | Voucher              |
+---------------------------------------------------------------------------------+------------------------+

.. warning:: If an entity state context is missing an exception will occur.  
