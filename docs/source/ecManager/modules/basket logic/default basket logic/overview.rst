########
Overview
########

This section will describe the DefaultBasketLogic module provided by ecManager. The module is 
responsible for the logic of baskets.

Visit the `Quick guide <quick-guide.html>`_ page to read more on how to get started with the module, 
or the `Advanced <advanced.html>`_ page for more advanced topics.

.. warning:: For the basket logic to work there must be exclusive and inclusive prices this can be done by activating calculate vat. Read more about `Calculate VAT <../../../introduction/configuring-ecManager/configuring-the-framework.html#calculate-vat>`_.