########
Advanced
########

Adding behaviour
================
ProcessExternalChangeRequest
----------------------------

It's possible to let other modules handle some request which aren't handled in the default basket 
module. For this ProcessExternalChangeRequest can be used this method handles all change request 
which aren't process before. This method sends the change request to all the registered modules 
which implement the interface ICheckBasket.The constructor of this class needs as first parameter 
the ServiceRequestContext and as second parameter the Dictionary<EntityTypes, EntityStateContext>.

.. image:: /_static/img/services/basket/basket-17.png
	:scale: 90%
	:align: center

The module can be registered like in the sample configuration below:

.. code-block:: C#
	:linenos:

	<component
	    type="ecManager.Modules.Basket.DefaultBasketLogic.DefaultBasketVoucherLogic, ecManager.Modules.Basket.DefaultBasketLogic"
	    service="ecManager.Commerce.Interfaces.ICheckBasket, ecManager.Commerce">
	</component>
