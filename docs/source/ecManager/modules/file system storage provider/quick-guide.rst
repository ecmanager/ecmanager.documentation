###########
Quick guide
###########

Introduction
============
This module offers a storage provider that uses the disk-based file system for IO operations. This 
page will describe how to get up and running with this module.

Prerequisites
=============
To start using this module, you need to install the ``ecManager.Modules.StorageProvider.FileSystem``
NuGet package.

.. code-block:: PowerShell

	PM> Install-Package ecManager.Modules.StorageProvider.FileSystem

Registration
============
You can register an instance of the ``FileSystemStorageProvider`` in the ``modules.config`` file as 
shown in the following example:

.. code-block:: XML
	:linenos:
	
	<component
	  type="ecManager.Modules.StorageProvider.FileSystem.FileSystemStorageProvider, ecManager.Modules.StorageProvider.FileSystem"
	  service="ecManager.Common.Interfaces.IStorageProvider, ecManager.Common"
	  name="myprovider">
	</component>

You can register multiple instances with different names.

Configuration
=============
You can read the `Storage documentation <../../services/storage/quick-guide.html>`_ to learn how to 
configure storage providers. This module requires no additional configuration.

Searching for files
===================
.. warning:: 
    The FileSystemStorageProvider search implementation is based on `Directory.GetFiles(string,string) <https://msdn.microsoft.com/en-us/library/wz42302f(v=vs.110).aspx>`_. 
    The implementation in the AzureBlobStorageProvider mimics this behavior, however it does **NOT** convert filenames to the 8.3 filename format when checking.