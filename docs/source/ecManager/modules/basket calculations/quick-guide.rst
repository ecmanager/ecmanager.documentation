###########
Quick guide
###########

Introduction
============
The basket calculation module is very simple by design. We only sum up or multiply data that is 
already present in the basket. To be able to do this, we need to make a set of rules/assumptions:

- Every module has put it's own data in the correct places in the basket. E.g. the voucher module places the discounts of the vouchers in the baskets.
- Every modification of the price of the basket is either a cost or a discount, and should be placed in the corresponding collection in the basket item.
- A discount is a negative number, but positive numbers are not excluded.
- A cost is a positive number, but negative numbers are not excluded.
- A basket can not have a total price of less than 0.

When you stick to these rules, you should get the desired outcome.

Prerequisites
=============
To start using this module, you need to install the DefaultCalculateBasket NuGet package.

.. code-block:: PowerShell

	PM> Install-Package ecManager.Modules.Basket.DefaultCalculateBasket

Registration
============
The default calculation module can be registered in the ``Modules.config`` file as follows:

.. code-block:: XML
	:linenos:

	<component
	  type="ecManager.Modules.Basket.DefaultCalculateBasket.DefaultCalculateBasketLogic, ecManager.Modules.Basket.DefaultCalculateBasket"
	  service="ecManager.Customer.Interfaces.ICalculateBasket, ecManager.Customer">
	</component>

Calculations
============

In the table below you will find the properties of basket items that will be calculated and how 
these properties are calculated. 

+----------------------------+----------------------------------+-----------------------------------------------------------------------------------------------------------------+
| Field name                 | ABasketItem                      | ABasketCollectionItem                                                                                           |
+============================+==================================+=================================================================================================================+
| ABasketItem.ListPrice      | defined during the add to basket | Σ ABasketCollectionItem.Items.ListPrice                                                                         |
+----------------------------+----------------------------------+-----------------------------------------------------------------------------------------------------------------+
| ABasketItem.SubTotal       | ABasketItem.Quantity x ABasketItem.SellingPrice                                                                                                    |
+----------------------------+----------------------------------+-----------------------------------------------------------------------------------------------------------------+
| ABasketItem.SellingPrice   | defined during the add to basket | (Σ ABasketCollectionItem.Items.Total) + ABasketCollectionItem.TotalCosts + ABasketCollectionItem.TotalDiscounts |
+----------------------------+----------------------------------+-----------------------------------------------------------------------------------------------------------------+
| ABasketItem.TotalCosts     | Σ ABasketItem.Costs                                                                                                                                |
+----------------------------+----------------------------------------------------------------------------------------------------------------------------------------------------+
| ABasketItem.TotalDiscounts | Σ ABasketItem.Discounts                                                                                                                            |
+----------------------------+----------------------------------------------------------------------------------------------------------------------------------------------------+
| ABasketItem.Total          | ABasketItem.SubTotal + ABasketItem.TotalCosts + ABasketItem.TotalDiscounts                                                                         |
+----------------------------+----------------------------------------------------------------------------------------------------------------------------------------------------+

The table below contains the calculations that only apply to ABasketCollectionItem instances.

+----------------------------------------------------+----------------------------------------------+
| Field name                                         | Formula                                      |
+====================================================+==============================================+
| ABasketCollectionItem.CollectionItemsDiscountTotal | Σ ABasketCollectionItem.Items.TotalDiscounts |
+----------------------------------------------------+----------------------------------------------+
| ABasketCollectionItem.CollectionItemsCostTotal     | Σ ABasketCollectionItem.Items.TotalCosts     |
+----------------------------------------------------+----------------------------------------------+
| ABasketCollectionItem.CollectionItemsTotal         | Σ ABasketCollectionItem.Items.Total          |
+----------------------------------------------------+----------------------------------------------+

In the table below you will find the properties of the basket that will be calculated and how these 
properties are calculated.

+----------------------------------+-------------------------------------------------------------+
| Field name                       | Formula                                                     |
+==================================+=============================================================+
| Basket.SubTotal                  | Σ Basket.Items.Total                                        |
+----------------------------------+-------------------------------------------------------------+
| Basket.TotalDiscounts            | Σ Basket.Discounts                                          |
+----------------------------------+-------------------------------------------------------------+
| Basket.TotalCosts                | Σ Basket.Costs                                              |
+----------------------------------+-------------------------------------------------------------+
| Basket.BasketItemsDiscountsTotal | Σ Basket.Items.TotalDiscounts                               |
+----------------------------------+-------------------------------------------------------------+
| Basket.BasketItemsCostsTotal     | Σ Basket.Items.TotalCosts                                   |
+----------------------------------+-------------------------------------------------------------+
| Basket.BasketTotalDiscounts      | Basket.BasketItemsDiscountsTotal + Basket.TotalDiscounts    |
+----------------------------------+-------------------------------------------------------------+
| Basket.BasketTotalCosts          | Basket.BasketItemsCostsTotal + Basket.TotalCosts            |
+----------------------------------+-------------------------------------------------------------+
| Basket.Total                     | Basket.SubTotal + Basket.CostsTotal + Basket.DiscountsTotal |
+----------------------------------+-------------------------------------------------------------+

The information above should give you an idea on how the basket calculation behaves when using the 
default module.