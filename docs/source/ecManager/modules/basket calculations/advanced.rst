########
Advanced
########

Customizing basket calculations
===============================
When using the BasketService, calculating (i.e. totals) is an important task. Because calculation 
may vary per project, calculations are delegates to a separate module within the framework. This 
allows you to customize the calculation by creating your own implementation of the Calculation module.

When getting a Basket using the BasketService, you will receive a basket entity which is fully 
calculated. The calculation is triggered by searching for an implementation of the ICalculateBasket 
interface (configured through modules.config). This interface is really minimalistic as you can see 
in the following diagram:

.. image:: /_static/img/services/basket/basket-13.png
	:scale: 90%
	:align: center

By default, when no other module is configured, the DefaultCalculateBasket module is used which 
implements ICalculateBasket.

The following methods are implemented in the DefaultCalculateBasketLogic:

+----------------------------------+-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| Property / Method                | Purpose                                                                                                                                                                           |
+==================================+===================================================================================================================================================================================+
| CalculateBasket                  | Implementation of the ICalculateBasket interface and entry point for calculating a basket                                                                                         |
+----------------------------------+-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| CalculateBasketItem (item)       | Method to calculate the totals for a IBasketItem                                                                                                                                  |
+----------------------------------+-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| CalculateBasketItem (collection) | Method to calculate the totals for a ABasketCollectionItem                                                                                                                        |
+----------------------------------+-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| CalculateBasketItemSubTotal      | Method that is used for calculating the IBasketItem.SubTotal of all IBasketItem instances. The formula is IBasketItem.Quantity x IBasketItem.SellingPrice by default.             |
+----------------------------------+-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| CalculateBasketItemTotal         | Method that calculates the IBasketItem.Total of all IBasketItem instances. The formula is IBasketItem.SubTotal + IBasketItem.TotalCosts + IBasketItem.TotalDiscounts by default.  |
+----------------------------------+-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| CalculateCollectionPrice         | Method to calculate the ABasketItem.ListPrice and ABasketItem.SellingPrice for a ABasketCollectionItem. The ABasketItem.ListPrice is the sum of the ABasketItem.SubTotal of the   |
|                                  | collection items. The ABasketItem.SellingPrice is the sum of the ABasketItem.Total of the collection items.                                                                       |
+----------------------------------+-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| CalculateItem                    | Method to calculate a IBasketItem. This method delegates the calculation to the correct calculation method (either CalculateBasketItem(item) or CalculateBasketItem(collection)). |
+----------------------------------+-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| CalculateSum (collection)        | Method to calculate the sum of a set of prices in a collection of IBasketItem instances.                                                                                          |
+----------------------------------+-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| CalculateSum (prices)            | Calculate the sum of a set of Price instances.                                                                                                                                    |
+----------------------------------+-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| CalculateSum (amounts)           | Calculate the sum of a set of IItemAmountRule instances.                                                                                                                          |
+----------------------------------+-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+

Except for the CalculateBasket method, all other methods are marked virtual which allows overriding 
on detailed level. Creating a custom implementation with a small modification it done in the next chapter.

Steps to create a custom implementation
---------------------------------------

The following steps will create a custom implementation of the NotSoldOut criterion:

1) Create library Project in Visual Studio
2) Add references to the following libraries:

     a. ecManager.Modules.Basket.DefaultCalculateBasket
     b. ecManager.Commerce
     c. ecManager.Common
3) Create a "MyCustomBasketCalculation" class for your custom calculation

	 a. Make it Inherit from DefaultCalculateBasketLogic
	 b. Override the CalculateBasketItemSubTotal implementation and change the behavior
	  
.. code-block:: C#
	:linenos:
	:caption: New implementation for calculating SubTotal

	/// <summary>
	///     Method that calculates the <see cref="ABasketItem.SubTotal"/> of all <see cref="ABasketItem"/> instances.      
	/// </summary>
	/// <param name="basketItem">The basket item whose <see cref="ABasketItem.SubTotal"/> should be calculated.</param>
	protected override void CalculateBasketItemSubTotal(IBasketItem basketItem)
	{
	    basketItem.SubTotal = new Price
	    {
	        /* default behavior
	        ExclVat = basketItem.Quantity * basketItem.SellingPrice.ExclVat,
	        InclVat = basketItem.Quantity * basketItem.SellingPrice.InclVat,
	        */
	        ExclVat = (basketItem.Quantity * basketItem.SellingPrice.ExclVat) + basketItem.TotalCosts.ExclVat,
	        InclVat = (basketItem.Quantity * basketItem.SellingPrice.InclVat) + basketItem.TotalCosts.InclVat,
	    };
	}

	c. Override the CalculateBasketItemTotal and change the behavior

.. code-block:: C#
	:linenos:

	/// <summary>
	/// Method that calculates the <see cref="ABasketItem.Total"/> of all <see cref="ABasketItem"/> instances.     
	/// </summary>
	/// <param name="basketItem">The basket item whose <see cref="ABasketItem.Total"/> should be calculated.</param>
	protected override void CalculateBasketItemTotal(IBasketItem basketItem)
	{
	    basketItem.Total = new Price
	    {
	        /* default bahavior
	        ExclVat = basketItem.SubTotal.ExclVat + basketItem.TotalCosts.ExclVat + basketItem.TotalDiscounts.ExclVat,
	        InclVat = basketItem.SubTotal.InclVat + basketItem.TotalCosts.InclVat + basketItem.TotalDiscounts.InclVat,
	        */
	        ExclVat = basketItem.SubTotal.ExclVat + basketItem.TotalDiscounts.ExclVat,
	        InclVat = basketItem.SubTotal.InclVat + basketItem.TotalDiscounts.InclVat,
	    };
	}

4) Compile the module
5) Configure your applications to use new implementation

     a. open modules.config
     b. add (or replace) a new component that implementes the ICalculateBasket interface

.. code-block:: XML
	:linenos:
	:caption: New basket calculation config

	<component
	    type="CustomBasketCalculation.MyCustomBasketCalculation, CustomBasketCalculation"
	    service="ecManager.Commerce.Interfaces.ICalculateBasket, ecManager.Commerce">
	</component>

6) That's it; lets get ready to roll!