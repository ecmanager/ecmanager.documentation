###########
Quick guide
###########

Introduction
============
This module offers possibilities to limit access to your implementation with a login page. This 
page describes how to get up and running with this module.

Prerequisites
=============
To start using this module, you need to install the ``ecManager.Modules.HttpModules.LoginModule``
NuGet package.

.. code-block :: PowerShell

	PM> Install-Package ecManager.Modules.HttpModules.LoginModule

Configuration
=============
To enable the HTTP Login Module in IIS, add the following to your web.config:

.. code-block :: XML
	:linenos:

	<system.webServer>
	    <modules>
	        <add name="HttpLoginModule" type="ecManager.Modules.HttpModules.LoginModule.HttpLoginModule, ecManager.Modules.HttpModules.LoginModule"/>
	    </modules>
	</system.webServer>

To configure the HTTP Login Module, add the following to your web.config:

.. code-block :: XML
	:linenos:

	<configuration>
	    <configSections>
	        <section name="ecManager.Modules.HttpModules.LoginModule" type="ecManager.Modules.HttpModules.LoginModule.Configuration.LoginModuleConfigurationSection, ecManager.Modules.HttpModules.LoginModule" />
	        // Other config sections
	    </configSections>
	    
	    <ecManager.Modules.HttpModules.LoginModule 
	        enabled="true" 
	        username="admin" 
	        password="password" 
	        ignoreDirectories="/Content/Login/"
	        ignoreFiles=""
	        ignoreIPAddresses="192.168.34.2,87.195.2.3">
	    </ecManager.Modules.HttpModules.LoginModule>
	</configuration>

.. note ::

	| If you're testing payments on your test or acceptance environment with this module enabled, make sure you add the payment update urls to your ignore files list.