########
Overview
########

This section describes the HTTP Login Module that can be used to limit access to your 
implementation. You can read about how it can be registered, configured and used.

Visit the `Quick guide <quick-guide.html>`_ page to read more on how to get started.

.. figure:: ../../../_static/img/ecManager/modules/http-login/login-partial.png
	:alt: loginpage example
	:align: center

	Loginpage example