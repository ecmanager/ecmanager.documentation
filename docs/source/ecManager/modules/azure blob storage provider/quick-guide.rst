###########
Quick guide
###########

Introduction
============
This module offers a storage provider that uses Azure blob storage for IO operations. This 
page will describe how to get up and running with this module.

Prerequisites
=============
To start using this module, you need to install the ``ecManager.Modules.StorageProvider.AzureBlobStorage``
NuGet package.

.. code-block:: PowerShell

	PM> Install-Package ecManager.Modules.StorageProvider.AzureBlobStorage

Registration
============
You can register an instance of the ``AzureBlobStorageProvider`` in the ``modules.config`` file as 
shown in the following example:

.. code-block:: XML
	:linenos:
	
	<component
	  type="ecManager.Modules.StorageProvider.AzureBlobStorage.AzureBlobStorageProvider, ecManager.Modules.StorageProvider.AzureBlobStorage"
	  service="ecManager.Common.Interfaces.IStorageProvider, ecManager.Common"
	  name="myprovider">
	  <parameters>
	    <parameter name="connectionStringName" value="myazureconnectionstring" />
	    <parameter name="storageContainer" value="mystoragecontainer" />
	  </parameters>
	</component>

You can register multiple instances with different names.

The value of ``ConnectionStringName`` should be the name of the connection string which contains the 
connection string for the storage account this ``AzureBlobStorageProvider`` should use. The 
``StorageContainer`` property is the name of the container in blob storage.

Configuration
=============
You can read the `Storage documentation <../../services/storage/quick-guide.html>`_ to learn how to 
configure storage providers. All configuration required for this module is done in 
``modules.config``.