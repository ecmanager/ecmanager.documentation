########
Overview
########

This section will describe the storage provider that uses Azure blob storage for IO operations. You 
can read about how it can be registered, configured and used.

Visit the `Quick guide <quick-guide.html>`_ page to read more on how to get started using the 
``AzureBlobStorageProvider``.