###########################
Tasks and Scheduling
###########################

.. toctree::
   :maxdepth: 1
   :titlesonly:   
   :glob:

   overview
   quick-guide
   task-types
   advanced