###########
Quick guide
###########

Introduction
============
Hangfire is an "easy way to perform background
processing in .NET and .NET Core applications." - `Hangfire.io <https://hangfire.io>`__.

We used Hangfire to implement a scheduler for repeating tasks in the ecManager platform. This gives
you the ability to automate common tasks in a uniform way.

Background information
======================
This background information will help you understand the design of ecManager Scheduling.
Hangfire uses a persistent storage to save jobs and jobs related information. 

.. figure :: ../../../_static/img/ecManager/modules/task-scheduling/hangfire-workflow.png
	:alt: Hangfire workflow
	:align: right
	:scale: 80%

	Hangfire workflow

**Client**
----------
The client is the application that handles all calls to the database. The client is used to create,
save, update and delete tasks. Besides that the client is also used to retrieve monitoring 
information. Please note that the module ``ecManager.Modules.Scheduling.Hangfire`` uses the 
hangfire client.

**Server**
----------
The Hangfire server is the tool that processes the tasks. The Hangfire server will read the 
database, and run the tasks based on the given schedule. A server can have configuration and 
queues, please look at advanced for extra information regarding configuration and queues.

Setting up the CMS
==================
Make sure the ``ecManager.Modules.Scheduling.Hangfire`` is installed in the CMS.

.. code-block :: PowerShell

    PM> Install-Package ecManager.Modules.Scheduling.Hangfire

Install all tasks you are going to use. For example install the default tasks ships with the
platform:

.. code-block :: PowerShell

	PM> Install-Package ecManager.Modules.Scheduling.Tasks

**Modules.config**
------------------
The module and tasks can be registered in the ``Modules.config`` as follows:

.. code-block :: XML

    <component
      type="ecManager.Modules.Scheduling.HangfireScheduler, ecManager.Modules.Scheduling.Hangfire"
      service="ecManager.Scheduling.IScheduler, ecManager.Scheduling">
    </component>
    <component
      type="ecManager.Modules.Scheduling.Tasks.ExecutionableTask, ecManager.Modules.Scheduling.Tasks"
      service="ecManager.Scheduling.BaseTask, ecManager.Scheduling">
    </component>
    <component
      type="ecManager.Modules.Scheduling.Tasks.SqlCommandTask, ecManager.Modules.Scheduling.Tasks"
      service="ecManager.Scheduling.BaseTask, ecManager.Scheduling">
    </component>
    <component
      type="ecManager.Modules.Scheduling.Tasks.HttpRequestTask, ecManager.Modules.Scheduling.Tasks"
      service="ecManager.Scheduling.BaseTask, ecManager.Scheduling">
    </component>

**Web.config**
--------------
As Hangfire uses a predefined connectionstring ``HangfireConnectionString``. Make sure you set this
connectionstring in the ``Web.config``.

Setting up Servers
==================
To create a Hangfire server install the ``ecManager.Modules.Scheduling.Hangfire.Server`` 
NuGet package in a console application.

.. code-block :: PowerShell

	PM> Install-Package ecManager.Modules.Scheduling.Hangfire.Server

Install all tasks you are going to use. For example install the default tasks ships with the
platform:

.. code-block :: PowerShell

	PM> Install-Package ecManager.Modules.Scheduling.Tasks

.. note ::

    No Modules.config is needed.

Below a sample server with all possible configuration options.

.. code-block :: C#

    using ecManager.Modules.Scheduling.Hangfire.Server;

    namespace TestServer
    {
        class Program
        {
            static void Main(string[] args)
            {
                // Standard example
                EcManagerHangfireExtensions.UseServer();

                //Advanced example
                EcManagerHangfireExtensions.UseServer(new EcManagerHangfireOptions
                {
                	WorkerCount = 30,
                	HeartbeatInterval = new System.TimeSpan(0,0,1,0,0),
                	AutomaticRetryAttemps = 21,
                	Queues = new [] {"Critical", "Default"}
                });
            }
        }
    }

**App.config**
--------------
As Hangfire uses a predefined connectionstring ``HangfireConnectionString``. Make sure you set this
connectionstring in the ``App.config``.