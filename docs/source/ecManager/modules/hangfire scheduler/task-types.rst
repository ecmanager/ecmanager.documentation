##########
Task types
##########

ecManager created a default implementation of the following task types.

Executable task
===============
This task is an executable task for handling .exe / .bat files.

SqlCommand task
===============
This task is a SQL command task for handling SQL stored procedures.

**App.config**
--------------
The SqlCommand task uses a predefined connectionstring ``SqlCommandConnectionString``. Make sure
you set this connectionstring in the ``App.config`` of your server.

.. important ::

    | **Best practice**
    | Because this task uses a seperated connectionstring, you are able to limit the accessrights of the SQL user. Make sure this SQL user only has access to the stored procedures which are executed with this task.

HttpRequest task
================
This task is a HTTP request task for handling requests to http resources.

.. note ::

    | This task uses the .NET HttpClient. The current implementation closes every connection after the request is done. This is because the HttpClient doesn't honor DNS changes if it isn't closed.
    | More on this topic can be read |httprequesttask_link1| and |httprequesttask_link2|.


.. EXTERNAL LINKS
.. ==============

.. |httprequesttask_link1| raw:: html

   <a href="http://www.nimaara.com/2016/11/01/beware-of-the-net-httpclient/" target="_blank">here</a>

.. |httprequesttask_link2| raw:: html

   <a href="http://byterot.blogspot.com/2016/07/singleton-httpclient-dns.html" target="_blank">here</a>

.. ==============