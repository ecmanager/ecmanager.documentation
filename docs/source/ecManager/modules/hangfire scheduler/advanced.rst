########
Advanced
########

Queues
======
ecManager Scheduling supports queues. Adding new queues is simple by adding a string wich represents
the name of the queue. The order is important, workers will fetch jobs from the critical queue 
first, and then from the default queue.

.. code-block :: C#

    var options = new EcManagerHangfireOptions
    {
    	Queues = new[] { "critical", "default" }
    };

You can also set up multiple servers to listen to different queues, if you want to separate
processing power for reliability, it is possible to set up a new server wich only listens to a
specific queue.

Once the server is started the queue will be available in the CMS.

Recurring tasks can be scheduled to run on a given queue only. This is particularly handy if you
want to make sure blocking tasks have priority over default tasks.

Creating tasks
==============
The ecManager Scheduling system is set up to support any kind of task. We only require
implementation of an interface and a few methods. 

- Tasks should end on “Task” as in good practice.
- Tasks have to be based on the abstract class ``BaseTask`` (in ``ecManager.Scheduling``).
- Implementing the CreateParameterBluePrint method is required. This method creates the parameters blueprint that will be used in the CMS. When a new task is being made with a specific TaskName, the blueprint will be loaded, so the parameters that belong to the task can be filled. 
- Implementing the RunTask method is required. This method must contain the logic of the task. If you want to create a task that cleans the database log files, the logic to do exactly that needs to be called inside the RunTask method.

Logic behind tasks
==================
ecManager tasks & scheduling will search for the type used on the task. Hangfire uses the Execute
method of basetask to fire up the task.

.. code-block :: C#

    Execute(ICollection<TaskParameter> parameters);

We manage the logic inside the execute method. The following methods are called:

- Start()
- RunTask()
- And Finally Finish() 

The Start and Finally methods act as pre- and post execute events that gives us the capalility to
implement logging and other logic.

Creating Parameters
===================
Tasks can have parameters. Parameters are provided in the CMS to give context to the execution of
the task. Parameters are of the type ``StringTaskParameter`` and can be optional. Parameters are
set in the ``CreateParameterBlueprint`` method. An example from the ``ExecutableTask`` below.

The labels and translations of the parameters are provided by the ``ResourceService`` and can be 
altered in the .Resx files.

.. code-block :: C#

		/// <summary>
		/// The parameters blueprint.
		/// </summary>
		public override void CreateParameterBlueprint()
		{
			Parameters.Add(new StringTaskParameter("Path", "", Constants.PathLabelKey));
			Parameters.Add(new StringTaskParameter("Arguments", "", Constants.ArgumentsLabelKey, false));
		}

Please take a look at the following repository for some inspiration: `https://bitbucket.org/ecmanager/ecmanager.modules.scheduling.tasks/src/master/ <https://bitbucket.org/ecmanager/ecmanager.modules.scheduling.tasks/src/master/>`_

Hangfire Processing behaviour
=============================

- Once a task is fired, it can’t be stopped.
- When one task is fired multiple times directly after each other (within one polling interval) it will only fire the job once.
- When a job fails, Hangfire server will retry the task if this is configured. Default configuration is zero retries but this can be configured, shown in the section `Setting up Servers <quick-guide.html#setting-up-servers>`_.

