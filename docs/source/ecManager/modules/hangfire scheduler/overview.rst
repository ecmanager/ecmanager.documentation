########
Overview
########

ecManager supports the creation of scheduled tasks. This will allow you to execute a piece of (custom) logic at a given time or interval.

This section will describe the installation and usage of the Hangfire implementation of ecManager Scheduling.

Visit the `Quick guide <quick-guide.html>`_ page to read more on how to get started using the Hangfire module.

Visit the `Advanced <advanced.html>`_ page to read detailed information about the Hangfire module.