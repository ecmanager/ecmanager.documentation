###########
Quick guide
###########

Introduction
============
Auditing taps in on the database actions of ecManager. The actions are redirected to the 
LLBLAuditLoggingProvider which will write the audit log entries to the database.

Prerequisites
=============
To start using this module, you need to install the AuditLoggingProvider NuGet package.

.. code-block:: PowerShell

	PM> Install-Package ecManager.Modules.AuditLoggingProvider.LLBL

Registration
============
The AuditLoggingProvider can be registered in the ``Modules.config`` as follows:

.. code-block:: XML
	:linenos:

	<component
	  type="ecManager.Modules.AuditLoggingProvider.LLBL.LLBLAuditLoggingProvider, ecManager.Modules.AuditLoggingProvider.LLBL"
	  service="ecManager.Auditing.Interfaces.IAuditLoggingProvider, ecManager.Auditing"
	  instance-scope="single-instance">
	</component>

Configuration
=============
Add the following configuration section to your web.config or app.config:

.. code-block:: XML
	:linenos:

	<configSections>
	  <section name="audit" type="ecManager.Common.Configurations.Cms.AuditConfigurationSection, ecManager.Common" />
	</configSections>

	<audit>
	  <AuditConfiguration ResourceActions="Create,Update,Delete" Enabled="true" />
	</audit>

The configuration allows you to enable or disable auditing completely. You can also choose which 
actions should be logged.
The value of ResourceActions can be found in the enumeration defined below:

.. code-block:: C#
	:linenos:

	public enum ResourceActions
	{
		/// <summary>
		///     No action [value: 0]
		/// </summary>
		None = 1,

		/// <summary>
		///     Read action [value: 1]
		/// </summary>
		Read = 2,

		/// <summary>
		///     Create action: Insert [value: 2]
		/// </summary>
		Create = 4,

		/// <summary>
		///    Update action [value: 4]
		/// </summary>
		Update = 8,

		/// <summary>
		///   Delete action [value: 16]
		/// </summary>
		Delete = 16,

		/// <summary>
		/// An example of this action is when the OrderEntity instance X is no longer referencing the CustomerEntity instance C. 
		/// Another example is removing OrderEntity instance X from the Orders collection of CustomerEntity instance C [value: 32]
		/// </summary>
		Dereference = 32,

		/// <summary>
		/// An example of this action is when the OrderEntity instance X is now referencing the CustomerEntity instance C.
		/// Another example is adding OrderEntity instance X to the Orders collection of CustomerEntity instance c. [value: 64]
		/// </summary>
		Reference = 64
	}

Accessing the Audit log
=======================
There are two ways of accessing the data collected by the LLBLAuditLoggingProvider. The recommended
way is by using the ecManager CMS. You can find the Audit log in ``Autorisation -> Audit``.
The second way is accessing the database directly. The logs are writtin to the ``[dbo].[AuditLog]``
table.

=========== ===========================================================================
Column      Description
=========== ===========================================================================
DateTime    The date and time when the audit was created.
UserName    The user name of the user currently logged in, in the CMS.
Action      The action executed, see enum "ResourceActions" below for more information.
ResourceIds The id of the resource, linked to the cms.Resources table.
=========== ===========================================================================