########
Overview
########

This section will describe the Tweakwise module provided by ecManager. 
This module integrates the services provided by Tweakwise into the service layer. 
Building product lister, searching for products and an autocomplete functionality will be added by the module.

Visit the `Quick guide <quick-guide.html>`_ page to read more on how to get started with the Tweakwise module.