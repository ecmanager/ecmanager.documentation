###########
Quick guide
###########

This page will guide you through the installation and configuration of the Tweakwise module.

Installation
=============
To start using this module, you need to install the Tweakwise NuGet package.

.. code-block:: PowerShell

	PM> Install-Package ecManager.Modules.PredicateExpressionProvider.Tweakwise

Module configuration
====================
After installing the module using Powershell, 
it is time to register the the component as a module of ecManager.
The module provides you with three searchtask implementations.
Each search task is designed to cover a specific use case:

*	ProductListerSearchTask - Navigational operations like filling product listers. 
*	AutocompleteProductSearchTask - Autocomplete operations. Site-wide or within a specific category.
*	ProductSearchTask - Generic search operations.

All search tasks are handles by the PredicateExpressionProvider class in the module. 
Open your modules.config file, and add the following lines:

.. code-block:: XML
	:linenos:

	<component
	  type="ecManager.Modules.PredicateExpressionProvider.Tweakwise.PredicateProvider, ecManager.Modules.PredicateExpressionProvider.Tweakwise"
	  service="ecManager.PredicateExpressionProvider.IPredicateProvider`2[[ecManager.Modules.PredicateExpressionProvider.Tweakwise.SearchTasks.ProductSearchTask, ecManager.Modules.PredicateExpressionProvider.Tweakwise],[ecManager.Catalog.Entities.Product, ecManager.Catalog]],ecManager.PredicateExpressionProvider">
	</component>

	<component
	  type="ecManager.Modules.PredicateExpressionProvider.Tweakwise.PredicateProvider, ecManager.Modules.PredicateExpressionProvider.Tweakwise"
	  service="ecManager.PredicateExpressionProvider.IPredicateProvider`2[[ecManager.Modules.PredicateExpressionProvider.Tweakwise.SearchTasks.AutocompleteProductSearchTask, ecManager.Modules.PredicateExpressionProvider.Tweakwise],[ecManager.Catalog.Entities.Product, ecManager.Catalog]],ecManager.PredicateExpressionProvider">
	</component>

	<component
	  type="ecManager.Modules.PredicateExpressionProvider.Tweakwise.PredicateProvider, ecManager.Modules.PredicateExpressionProvider.Tweakwise"
	  service="ecManager.PredicateExpressionProvider.IPredicateProvider`2[[ecManager.Modules.PredicateExpressionProvider.Tweakwise.SearchTasks.ProductListerSearchTask, ecManager.Modules.PredicateExpressionProvider.Tweakwise],[ecManager.Catalog.Entities.Product, ecManager.Catalog]],ecManager.PredicateExpressionProvider">
	</component>


Configuration
==============
Tweakwise does not support multilingual content. 
If you want to differ content per label and/or language, 
you should create a Tweakwise environment for every combination of language and label. 
Each environment has it's own authorization key which needs to be configured.
Integrate the following configuration into your own web.config, and modify it to your own needs.
The authorization keys is the key depicted on the Endpoints-page in the Navigator environment.	

.. code-block:: XML
	:linenos:
	
	<configuration>
		<configSections>
			<section name="ecManager.Modules.PredicateExpressionProvider.Tweakwise" 
				type="ecManager.Modules.PredicateExpressionProvider.Tweakwise.Configuration.TweakwiseConfigurationSection, ecManager.Modules.PredicateExpressionProvider.Tweakwise" />
		</configSections>
		<ecManager.Modules.PredicateExpressionProvider.Tweakwise>
			<EndpointTypes>
				<Endpoint languageCode="nl" labelId="1" authorizationKey="######1" />
				<Endpoint languageCode="en" labelId="1" authorizationKey="######2" />
			</EndpointTypes>
		</ecManager.Modules.PredicateExpressionProvider.Tweakwise>
	</configuration>

This should cover the configuration of the Tweakwise predicate provider module. 
You can now create your first query using the newly available search tasks.

Caching configuration
#####################
To optimize performance, the Tweakwise predicate provider module has caching built in.
All cache keys start with the following identifier:

.. code-block:: text
	
	http://schemas.ecmanager.nl/ecmanager/2012-1/modules/tweakwisepredicateexpressionprovider/entities/cacheentry
	
You should enable caching for these items, with an expiration timeout of at least 1 minute.
Feel free to experiment with increasing the expiration timeout in order to find out the ideal 
balance between cache load and HTTP requests.

.. warning :: 

	It is strongly recommended that you enable caching for Tweakwise requests. 
	Caching avoids sending every request twice to Tweakwise, which will impact performance.

Category IDs
=============
Tweakwise works with category IDs. In the current setup, the category ID is mapped to the navigation
item ID. The navigation item ID itself cannot be inserted directly into Tweakwise. We need to modify
the ID using the following calculation:

.. code-block:: text

	categoryId = navigationItemId + 2
	
The search tasks have been equipped with methods that do this for you 
(f.e. AutocompleteProductSearchTask.SetCategoryId). These methods each have two implementations. 
The first implementation takes a NavigationItemId as an argument. This method should be used when
you manually set the category ID that requires a 'translation'. 

When you set the category ID using the data from facets, the overload with the integer parameter 
should be used. The data in the facets is provided by Tweakwise and does not require a 'translation'.
	
Troubleshooting
===============

The `Tweakwise Navigator <https://navigator.tweakwise.com/>`_ environment provides a demo shop 
that allows you to test queries and features.
You can use this environment to compare the behavior.
If the behavior of the demo shop does not meet your expectations, 
you should contact the Tweakwise support for help.
If the behavior in the Tweakwise demo shop seems to be correct, 
but the behavior in the shop does not you should contact the ecManager support for help.

If you enable debug logging, the requests URL's sent to Tweakwise should appear in the log. 
This might provide insight while troubleshooting.