.. _quick-guide:

###########
Quick guide
###########

Introduction
============

This page describes the steps required to get up and running with the ``MemoryCache`` module.

Prerequisites
=============
If you want to use the MemoryCache module you will need to install the correct package. This is done 
by running the following command in the NuGet package manager console:

.. code-block:: PowerShell

	PM> Install-Package ecManager.Modules.Cache.MemoryCache

Registration
============
Next you will need to register the module in your ``Modules.config`` file:

.. code-block:: XML

	<component 
	  type="ecManager.Modules.Cache.MemoryCache.MemoryCacheProvider, ecManager.Modules.Cache.MemoryCache"
	  service="ecManager.Common.Interfaces.ICacheProvider, ecManager.Common"
	  instance-scope="single-instance"
	  name="memoryCacheProvider">
	</component>
	
	<component 
	  type="ecManager.Modules.Cache.MemoryCache.MemoryCacheKeyProvider, ecManager.Modules.Cache.MemoryCache"
	  service="ecManager.Common.Interfaces.ICacheKeyProvider, ecManager.Common"
	  instance-scope="single-instance"
	  name="memoryCacheKeyProvider">
	</component>
 
.. note::

  You can choose your own name for the components, however they should be unique.

Configuration
=============
The MemoryCache implementation uses a custom configuration section which allow you to control what 
is cached and when the cache entries expire. You can find an example below:

.. code-block:: XML

	<configSections>
	  <section name="ecManager.Modules.Cache.MemoryCache" type="ecManager.Modules.Cache.MemoryCache.Configuration.MemoryCacheConfigurationSection, ecManager.Modules.Cache.MemoryCache"/>
	</configSections>   
	
	<!--
	  enabled="true" enable the cache
	  enabled="false" disable the entire cache. Enabling the cache on a sub-level will have no effect.
	  expiration="60" the default cache refresh timeout in minutes.
	--> 
	<ecManager.Modules.Cache.MemoryCache enabled="true" expiration="60">
	  <cacheConfigurationItems>
	    <!--
	      enabled="true" allow you to change the refresh time out for a specific entity/search task/domain.
	      expiration the new expiration time out.
	       
	      cacheIdentifier is a search term which will be matched against the cache keys. The search is done by a startsWith string operator.
	    -->
	    <specificCacheConfiguration cacheIdentifier="general/entities/transportcategorysearchtask" enabled="true" expiration="20" />
	    <!--
	      enabled="false" disables the cache for a specific entity/search task//domain.
	      expiration is ignored in this cache.
	    -->
	    <specificCacheConfiguration cacheIdentifier="marketing/entities/redirectsearchtask" enabled="false" expiration="20" />
	  <cacheConfigurationItems>
	</ecManager.Modules.Cache.MemoryCache>
