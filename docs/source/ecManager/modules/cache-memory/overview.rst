########
Overview
########
The ``MemoryCache`` module uses the `MemoryCache <https://msdn.microsoft.com/en-us/library/system.runtime.caching.memorycache(v=vs.110).aspx>`_ 
object. When you configure this module to be used for caching, service calls will be cached in memory.

Visit the `Quick guide <quick-guide.html>`__ page to read more on how to get started with the module.