########
Advanced
########

Troubleshooting
===============

Permissions for reading from a queue
------------------------------------

MSMQs created by an application that runs in IIS are automatically grant access only to the user of the application pool.
Depending on your setup, your account might not have permissions to read the queue. To gain access, you can follow these steps:

#) Right-click on a queue in computer management and select properties
#) Click on the security tab
#) Click on advanced
#) Click on the owner tab
#) Select a new owner of the queue
#) Click apply

You are now able to assign and remove permissions to users of your choice.