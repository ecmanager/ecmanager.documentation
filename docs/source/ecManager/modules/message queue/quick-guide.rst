###########
Quick guide
###########

Prerequisites
==============

#) A Windows machine with the Microsoft Message Queue Server Core feature turned on.

.. note:: To read more about how you can enable this feature, go `here <https://msdn.microsoft.com/en-us/library/aa967729(v=vs.110).aspx>`_. 

.. figure:: img/windowsfeatures.png
	:alt: windows features
	:align: center

	Windows features

To start using this module, you need to install the MessageQueue.MSMQ NuGet package.

.. code-block:: PowerShell

	PM> Install-Package ecManager.Modules.MessageQueue.MSMQ

Registration
============
To use a MSMQ, you should register a module as follows, using a unique identifier as parameter:

.. code-block:: XML
	:linenos:
	
	<component
	  type="ecManager.Modules.MessageQueue.MSMQ.MSMQ, ecManager.Modules.MessageQueue.MSMQ"
	  service="ecManager.MessageQueue.IMessageQueue, ecManager.MessageQueue">
	  <properties>
	    <property name="Identifier" value="queue1"/>
	  </properties>
	</component>

	<component
	  type="ecManager.Modules.MessageQueue.MSMQ.MSMQ, ecManager.Modules.MessageQueue.MSMQ"
	  service="ecManager.MessageQueue.IMessageQueue, ecManager.MessageQueue">
	  <properties>
	    <property name="Identifier" value="queue2"/>
	  </properties>
	</component>

You should register one component for each MSMQ you want to use, each with its own identifier.

Configuration
-------------
Configuration should be specified individually for each registered MSMQ. Registration can be done in your 
app.config or web.config by adding the following:

.. code-block:: XML
	:linenos:

	<configSections>
	     // Other config sections
	    <section name="ecManager.Modules.MessageQueue.MSMQ" type="ecManager.Modules.MessageQueue.MSMQ.Configuration.MSMQConfigurationSection, ecManager.Modules.MessageQueue.MSMQ" />
	</configSections>

	<ecManager.Modules.MessageQueue.MSMQ>
	    <queues>
	        <queue identifier="queue1" address=".\Private$\queue1" ispersistent="false" istransactional="true" outputformat="json" />
	        <queue identifier="queue2" address=".\Private$\queue2" ispersistent="true" istransactional="false" outputformat="xml" />
	    </queues>
	</ecManager.Modules.MessageQueue.MSMQ>

+-----------------+-----------------+---------------+-----------------------------------------------------------------------------------+
|    Attribute    | Allowed values  | Default value |                                    Description                                    |
+=================+=================+===============+===================================================================================+
| identifier      | Any string      | None          | Unique identifier for this queue.                                                 |
+-----------------+-----------------+---------------+-----------------------------------------------------------------------------------+
| address         | Any string      | None          | Address for this queue.                                                           |
+-----------------+-----------------+---------------+-----------------------------------------------------------------------------------+
| ispersistent    | true/false      | false         | Indicates whether messages sent to this queue should be persisted, or not.        |
+-----------------+-----------------+---------------+-----------------------------------------------------------------------------------+
| istransactional | true/false      | false         | Indicates whether this queue is transactional, or not.                            |
+-----------------+-----------------+---------------+-----------------------------------------------------------------------------------+
| outputformat    | json/xml/binary | binary        | Defines the format in which the message body should be serialized for this queue. |
+-----------------+-----------------+---------------+-----------------------------------------------------------------------------------+

.. note:: The identifier specified in the registration should match the identifier in your configuration.

Sending messages to the queue
-----------------------------

You can read how you can use the MessageQueueService to send messages `on the quick guide page <../../services/message%20queue/quick-guide.html>`_. 
Using our example registration and configuration, we would send an object to the MessageQueueService as follows:

.. code-block:: C#
	:linenos:

	var obj = new ExampleObject { ExampleProperty = "example" };
	var service = new MessageQueueService();
	service.Publish(obj, new string[] { "queue1", "queue2" });

.. figure:: img/queues.png
	:alt: MSMQ
	:align: right

	Computer management

MSMQ creates the queue if it doesn't exist yet. queue1 would receive our message serialized in JSON format
and the message for queue2 would be in XML. On a windows machine you can view your MSMQs in computer 
management under "Services and Applications", as shown on the right. 

From this interface you can view properties of your queues, peek at messages to see their content and more.

.. figure:: img/queue1content.png
	:alt: queue1
	:align: right

	JSON message in queue1

- DataContractSerializer is used for XML serialization
- JSON.NET is used for JSON serialization
- BinaryFormatter is used for binary serialization

To the right you can see the content the messages published to queue1 and queue2. XML is formatted by the 
view, the message contains an array of bytes, not a raw XML string.

.. figure:: img/queue2content.png
	:alt: queue2
	:align: right

	XML message in queue2

Each message has a label, which is the fully qualified assembly name. This can be useful for your consumer 
application. If the consumer has access to the same types, you can deserialize your messages to typed 
objects. 

For more information on how to consume these messages, you can find an example on the Advanced page of the 
MessageQueueService `advanced page <../../services/message%20queue/advanced.html>`_. The example uses this module 
to consume messages.