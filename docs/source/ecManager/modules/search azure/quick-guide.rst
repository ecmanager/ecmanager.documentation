###########
Quick guide
###########

Introduction
============
This module offers integration with Azure Search. There are a few steps involved in order for you to
start utilising search which will be described in the following sections.

Creating an Azure Search resource
=================================
The first thing you should do is create an Azure Search resource in Azure. This can be done through
the portal or by ARM template. After the resource has been allocated, generate a key under the 
"Keys" blade.

Please note that each pricing tier has several limits. An index is created for each language per 
label, so if you have two multilingual labels you must choose **at least** the Basic tier, because
the Free tier only allows for a maximum of 3 indices.

Another limitation that should be kept in mind is the maximum number of fields per index. To provide
faceted search, ecManager attributes are translated into fields. If you have a large number of 
filterable attributes, you might need more fields than the Basic tier allows.

If high availability is important to you, make sure you have at least 2 replicas of the service.
This will distribute workloads across the replicas and ensure that if one replica is down, the other
will still be available. Replication is not available for the free tier.

To read more about Azure Search service limits, click `here <https://docs.microsoft.com/en-us/azure/search/search-limits-quotas-capacity>`__.

Configuration
=============
The following configuration is required in order to use Azure Search. You should add this 
configuration in your web application and the tooling which create indices.

.. code-block:: XML
	:linenos:

	<configSections>
	  <section name="ecManager.Search.AzureSearch" type="ecManager.Search.AzureSearch.Configuration.AzureSearchConfigurationSection, ecManager.Search.AzureSearch" />
	</configSections>

	<ecManager.Search.AzureSearch searchServiceName="mySearchService" apiKey="myApiKey" fuzzySearchEnabled="true" fuzzySearchDistance="1" />

=================== =================================================================================================================================================================================
Attribute           Explanation
=================== =================================================================================================================================================================================
searchServiceName   The name of the Azure Search resource. This is not the URL but just the name.
apiKey              The key which can be found in the Azure portal. Use an admin key for indexation and a query key for searching.
fuzzySearchEnabled  This setting can be used to enable fuzzy search. By default this is enabled.
fuzzySearchDistance A value of 0-2, defaulting to 1. More information can be found `here <https://docs.microsoft.com/en-us/rest/api/searchservice/lucene-query-syntax-in-azure-search#bkmk_fuzzy>`__.
=================== =================================================================================================================================================================================

Indexation
==========
The creation of indices is the same as Lucene.NET Search module, with one exception. You can visit 
`this <../search/search-quick-guide.html>`_ page to read about the tooling used for for creating the 
indices. The only difference in this process is the third step. Instead of using the 
``SearchFeedIndexer`` tool, you should use the ``SearchFeedIndexer.Azure`` tool. Along with the 
configuration described in `Configuration`_, add the following to the ``SearchFeedIndexer.Azure``:

.. code-block:: XML
	:linenos:

	<configSections>
	  <section name="ecManager.Search.AzureSearchIndexer" type="SearchFeedIndexer.Azure.Configuration.AzureSearchIndexerConfigurationSection, SearchFeedIndexer.Azure" />
	</configSections>
	
	<ecManager.Search.AzureSearchIndexer inputDirectory="C:\path\to\transformeroutput" indexPricesInclVat="true" />

================== =====================================================================================
Attribute          Explanation
================== =====================================================================================
inputDirectory     The path to the directory containing XML files as a result of running the first tools.
indexPricesInclVat Determines whether prices should be indexed including or excluding VAT.
================== =====================================================================================

The tool will create the necessary indices for you. Every time the tool runs, the indices are 
re-created. Because Azure Search is a managed cloud service, you might run into the issue where the
service is unavailable. Because of this, we recommend monitoring the logs for issues.

It is recommended to create a scheduled task to periodically rebuild the index. You can use tools 
like the Windows scheduler for this if you are running the indexer on-premises, or as an
Azure WebJob if you are running the indexer in the cloud.

Integration in your web application
===================================
To start using this module, you need to install the ``ecManager.Search.AzureSearch`` NuGet package:

.. code-block:: PowerShell

	PM> Install-Package ecManager.Search.AzureSearch

Make sure the correct binding redirects for the dependent packages are added to the ``Web.config``.

Registration
============
First of, you should register the ``AzureSearchProvider`` in ``Modules.config`` to be the 
implementation of the ``ISearchProvider`` interface:

.. code-block:: XML
	:linenos:
	
	<component
	  type="ecManager.Search.AzureSearch.AzureSearchProvider, ecManager.Search.AzureSearch"
	  service="ecManager.Search.Core.ISearchProvider, ecManager.Search.Core">
	</component>

In addition, you should register the predicate providers for each type of search task. In the 
following example you can see how this is done:

.. container:: toggle

	.. container:: header

		**Show/Hide SearchTask/PredicateProvider registrations**

	.. code-block:: XML
		:linenos:
	
		<component
		  type="ecManager.Search.AzureSearch.AzureSearchProvider, ecManager.Search.AzureSearch"
		  service="ecManager.Search.Core.ISearchProvider, ecManager.Search.Core">
		</component>

		<component
		  type="ecManager.Search.Core.AttributePredicateProvider, ecManager.Search.Core"
		  service="ecManager.PredicateExpressionProvider.IPredicateProvider`2[[ecManager.Search.Core.AttributeSearchTask, ecManager.Search.Core],[ecManager.Catalog.Entities.Attribute, ecManager.Catalog]], ecManager.PredicateExpressionProvider">
		</component>

		<component
		  type="ecManager.Search.Core.ProductPredicateProvider, ecManager.Search.Core"
		  service="ecManager.PredicateExpressionProvider.IPredicateProvider`2[[ecManager.Search.Core.ProductSearchTask, ecManager.Search.Core],[ecManager.Catalog.Entities.Product, ecManager.Catalog]], ecManager.PredicateExpressionProvider">
		</component>

		<component
		  type="ecManager.Search.Core.CombinationPredicateProvider, ecManager.Search.Core"
		  service="ecManager.PredicateExpressionProvider.IPredicateProvider`2[[ecManager.Search.Core.CombinationSearchTask, ecManager.Search.Core],[ecManager.Catalog.Entities.Combination, ecManager.Catalog]], ecManager.PredicateExpressionProvider">
		</component>

		<component
		  type="ecManager.Search.Core.NewsItemPredicateProvider, ecManager.Search.Core"
		  service="ecManager.PredicateExpressionProvider.IPredicateProvider`2[[ecManager.Search.Core.NewsItemSearchTask, ecManager.Search.Core],[ecManager.Information.Entities.NewsItem, ecManager.Information]], ecManager.PredicateExpressionProvider">
		</component>

		<component
		  type="ecManager.Search.Core.InformationItemPredicateProvider, ecManager.Search.Core"
		  service="ecManager.PredicateExpressionProvider.IPredicateProvider`2[[ecManager.Search.Core.InformationSearchTask, ecManager.Search.Core],[ecManager.Information.Entities.InformationItem, ecManager.Information]], ecManager.PredicateExpressionProvider">
		</component>

		<component
		  type="ecManager.Search.Core.FaqItemPredicateProvider, ecManager.Search.Core"
		  service="ecManager.PredicateExpressionProvider.IPredicateProvider`2[[ecManager.Search.Core.FaqItemSearchTask, ecManager.Search.Core],[ecManager.Information.Entities.Faq, ecManager.Information]], ecManager.PredicateExpressionProvider">
		</component>

		<component
		  type="ecManager.Search.Core.ProductGroupPredicateProvider, ecManager.Search.Core"
		  service="ecManager.PredicateExpressionProvider.IPredicateProvider`2[[ecManager.Search.Core.ProductGroupSearchTask, ecManager.Search.Core],[ecManager.Catalog.Entities.ProductGroup, ecManager.Catalog]], ecManager.PredicateExpressionProvider">
		</component>

Testing the integration
=======================
Now that you have arrived at this step, you are ready to see if Azure Search is working for you.
Execute a service call using one of the registered search tasks, and check your results. If you run
into issues, please check out the troubleshooting documentation on the `Advanced <advanced.html#troubleshooting>`_ 
page.