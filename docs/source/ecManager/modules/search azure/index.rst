############
Azure Search
############

.. toctree::
   :maxdepth: 1
   :titlesonly:   
   :glob:

   overview
   quick-guide
   advanced