########
Advanced
########

Troubleshooting
===============
Because applications running in the cloud should be resilient, that's exactly how this integration
was built. This means, if Azure Search is temporary unavailable, search calls will just return empty
results. In this case, you should check for fatal log messages. This will give you a better 
indication of what went wrong.

The service can be unavailable while the indexing process is happening or when Azure is performing
scaling or maintainance.

If you see unexpected search results, you should check the search logs in the CMS. This can show you
the performed query and how many results it yielded in Azure Search and ecManager.

.. figure:: img/searchlog.png
	:alt: searchlog
	:align: center

	CMS search log

You can then use the Search Explorer in the Azure portal to execute the query and perform an 
analysis. Make sure to select the correct index.

.. figure:: img/searchexplorer.png
	:alt: searchexplorer
	:align: center

	Search explorer in the Azure portal

Overriding search behaviour
===========================
In case you want to influence the way queries are built or to add additional behaviour, you have a 
couple of options. You can:

- Change the configuration value of fuzzy search to be less or more strict when analyzing search terms
- Change the `scoring profile <https://docs.microsoft.com/en-us/rest/api/searchservice/add-scoring-profiles-to-a-search-index>`__ for specific search fields in the CMS
- Override the default ``AzureSearchProvider`` class.

To give you a better insight on how to override search behaviour, we will provide an example of how 
this can be achieved:

.. code-block:: C#
	:linenos:

	public class CustomQueryBuilder : IQueryBuilder
	{
	    public string BuildQuery(string terms)
	    {
	      // Build a custom query.
	    }
	}

	public class CustomAzureSearchProvider : AzureSearchProvider
	{
	    public CustomAzureSearchProvider() : base(
	        new Logging.Logger(),
	        new SearchTermModificationProvider(new CachingService(null)),
	        new SearchTermModificationProcessor(),
	        new CustomQueryBuilder(), // The custom component.
	        new IndexNameProvider(),
	        new FilterBuilderFactory(),
	        new SearchServiceClient(AzureSearchConfigurationSection.Current.SearchServiceName, new SearchCredentials(AzureSearchConfigurationSection.Current.ApiKey))),
			new FacetBuilder()
	    {
	    }
	}

As can be seen in the example, your custom search provider should have a parameterless constructor
which calls the base constructor which accepts several instances of interface implementations. These 
are all components which can be replaced. Using the previous example, you should replace the default
registration in ``Modules.config`` like this:

.. code-block:: XML
	:linenos:
	
	<component
	  type="MyApp.CustomAzureSearchProvider, MyApp"
	  service="ecManager.Search.Core.ISearchProvider, ecManager.Search.Core">
	</component>