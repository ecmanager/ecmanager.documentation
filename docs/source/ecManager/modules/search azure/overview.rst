########
Overview
########

.. figure:: img/azuresearch.png
	:alt: azuresearch
	:scale: 50%
	:align: right

This section will describe the Azure Search integration for ecManager. Azure Search is a managed
search cloud solution offered by the Microsoft Azure platform. This means that you don't have to 
manage any servers or scaling operations yourself. Azure Search offers several advantages, including
but not limited to:

- Full text search
- Automatic scaling
- Microsoft language analyzers
- Scoring profiles

More information about the Azure Search service can be found `here <https://docs.microsoft.com/en-us/azure/search/search-what-is-azure-search>`__.

This module is an alternative for the Lucene.NET search module and is more suitable for cloud
environments. Visit the `Quick guide <quick-guide.html>`_ page to read more on how to get started
using Azure Search in your application. You can read the `Advanced <advanced.html>`_ page to
get more in-depth information about the integration.