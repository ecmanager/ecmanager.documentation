###########
Quick guide
###########

Introduction
============
For development it can be useful to use a local Redis server. The installer for windows can be found
`here <https://github.com/MSOpenTech/redis/releases>`__.

Redis is a technology which allows you to have a distributed cache for your application(s). There
are several advantages this brings, the first one of course is availability. Another one is sharing
caches between multiple instances of your application or different applications. 

More information on Redis can be found on the Redis `webpage <http://redis.io/topics/introduction>`__.

Prerequisites
=============

If you want to use the redis module you will need to install the correct package. This is done by
running the following command in the nuget package manager console:

.. code-block :: PowerShell

  PM> Install-Package ecManager.Modules.Cache.Redis

Next you will need to register the module in your modules.config file:

.. code-block :: xml

  <component
    type="ecManager.Modules.Cache.Redis.RedisCacheProvider, ecManager.Modules.Cache.Redis"
    service="ecManager.Common.Interfaces.ICacheProvider, ecManager.Common"
    instance-scope="single-instance"
    name="Redis">
  </component>
  
  <component
    type="ecManager.Modules.Cache.Redis.RedisCacheKeyProvider, ecManager.Modules.Cache.Redis"
    service="ecManager.Common.Interfaces.ICacheKeyProvider, ecManager.Common"
    instance-scope="single-instance"
    name="Redis">
  </component>

.. note ::

	The name of the components should be the same as the example by convention.

.. warning ::

	Be aware that **not** running the redis cache module in single instance mode will result in a 
	performance penalty.
 
.. warning ::

	Be sure to remove any previous caching module registrations from the modules.config file.

Configuring the module
======================

To configure the module you will need to add a configuration section to the web.config file. This
looks like the following.
  
.. code-block :: xml

  <configSections>
    <section name="ecManager.Modules.Cache.Redis" type="ecManager.Modules.Cache.Redis.Configuration.RedisCacheConfigurationSection, ecManager.Modules.Cache.Redis"/>
  </configSections>

  <ecManager.Modules.Cache.Redis 
    enabled="true" 
    connectionstring="localhost"	 
    expiration="60"
    database="0">
  </ecManager.Modules.Cache.Redis>

For the Redis module we can configure several properties. **enabled** specifies whether the cache is
on or off, **expiration** determines the time an object will be cached in minutes. The
**connectionstring** property is the connection information required to connect to the redis server.
More information on how these are formatted can be found `here <https://github.com/ServiceStack/ServiceStack.Redis#redis-connection-strings>`__.

Selecting which database to use
===============================

You are able to configure which Redis database you wish to use. This is done by adding the database
attribute to your Redis configuration. This looks like the following:

.. code-block :: xml

  <ecManager.Modules.Cache.Redis
    enabled="true"
    connectionstring="localhost"
    expiration="60"
    database="5">
  </ecManager.Modules.Cache.Redis>

For more information on how databases work in Redis please check the `Redis documentation <http://www.rediscookbook.org/multiple_databases.html>`__.

Enabling hashing of keys
========================

You can enable hashing of keys in the Redis cache. This optimizes speed. To enable this, add the
following property ``hashing`` to your configuration:

.. code-block :: xml

  <ecManager.Modules.Cache.Redis
    enabled="true"
    connectionstring="localhost"
    expiration="60"
    database="5"
    hashing="true">
  </ecManager.Modules.Cache.Redis>

.. note ::

	If you enable hashing, make sure you enable this in the storefront and the CMS.

Configuring additional options
==============================

You are able to configure additional options to your Redis configuration. For example you can enable
the admin mode on the Redis connection. Add the following option to the ``connectionstring``
property:

.. code-block :: xml

  <ecManager.Modules.Cache.Redis
    enabled="true"
    connectionstring="localhost,allowAdmin=true"
    expiration="60"
    database="5">
  </ecManager.Modules.Cache.Redis>

For more information on these options please check the `Redis configuration <https://stackexchange.github.io/StackExchange.Redis/Configuration.html#configuration-options>`__.