########
Overview
########
The redis caching module enables the implementation party to use `Redis <http://redis.io/>`__ as a
caching solution within ecManager.

When choosing Redis as your caching module you should be aware that it is not as fast as the
MemoryCache module. This is because unlike MemoryCache, Redis has to serialize and de-serialize 
objects when placing them into the cache.

The advantage of Redis cache however is that it supports caching for multi-instance website out of
the box whereas this is harder to achieve with MemoryCache.

Visit the `Quick guide <quick-guide.html>`__ page to read more on how to get started with the
module, or the Advanced page for more advanced topics, such as overriding default behaviour.