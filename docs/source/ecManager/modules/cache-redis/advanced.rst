########
Advanced
########

Configuring per entity caching
==============================

Similar to the MemoryCache module you are able to configure how you want to handle caching for specific entities.
This is done by editing the **ecManager.Modules.Cache.Redis** configuration section in the following way.

.. code-block:: xml

  <ecManager.Modules.Cache.Redis
    enabled="true"
    connectionstring="localhost"
    expiration="60">
	  <specificCacheConfiguration 
	    cacheIdentifier="general/entities/transportcategorysearchtask"  
	    enabled="true" 
	    expiration="20" />
  </ecManager.Modules.Cache.Redis>
  
As can be seen in the example above you can specify the cacheIdentifier you want to apply custom settings on. 
The cacheIdentifier can also be a partial match, this means that 
::

  general/entities

will match both of the following examples

::

  general/entities/generalentity

::

  general/entities/basketentity. 

This way you can detemine caching behaviour for specific domains/methods/entities.
If you want to disable caching on an entity you can set enabled to true and that entity will not be cached. 
With the expiration attribute you can specify a custom cache duration for an entity allowing shorter or longer cache durations where needed.