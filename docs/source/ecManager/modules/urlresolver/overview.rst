########
Overview
########

This section will describe the default URL Resolver module provided by ecManager. This module allows 
you the convert virtual URLs retrieved from the ``StorageService`` to absolute paths. Those absolute
URLs can be used to link to static content like images and files. 

Visit the `Quick guide <quick-guide.html>`_ page to read more on how to get started with the 
default UrlResolver module.
