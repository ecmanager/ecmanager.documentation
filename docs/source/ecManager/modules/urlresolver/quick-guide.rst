###########
Quick guide
###########

Introduction
============
The default URL resolver is a module which can resolve absolute URLs from virtual URLs which are 
provided by the ecManager framework. Resolving URLs can be done by using the ``UrlService`` as 
described `here <../../services/urls/quick-guide.html>`__. The service uses a UrlResolver under the 
hood. This page will describe how you can get started using this module.

Prerequisites
=============
To start using this module, you need to install the UrlResolver NuGet package.

.. code-block:: PowerShell

	PM> Install-Package ecManager.Modules.UrlResolver.Default

Registration
============
The default UrlResolver can be registered in the ``Modules.config`` as follows:

.. code-block:: XML
	:linenos:

	<component
	  type="ecManager.Modules.UrlResolver.Default.DefaultUrlResolver, ecManager.Modules.UrlResolver.Default"
	  service="ecManager.Common.Interfaces.IUrlResolver, ecManager.Common">
	</component>

Configuration
=============
The default URL resolver module requires configuration to determine which the absolute path for a 
virtual URI. In the configuration you can set the rules for resolving URLs. For example, if you are 
using a CDN for product images you can use the following configuration to prefix all virtual URIs 
which start with ``/productimages`` with ``https://cdn.com``. 

.. code-block:: XML
	:linenos:

	<configSections>
	  <section name="ecmanager.Modules.UrlResolver.DefaultUrlResolver" type="ecManager.Modules.UrlResolver.Default.Configuration.UrlResolverConfigurationSection, ecManager.Modules.UrlResolver.Default" />
	</configSections>
	
	<ecmanager.Modules.UrlResolver.DefaultUrlResolver>
	  <urlConfigurations>
	    <urlConfiguration parsePattern="/cmsdata" patternType="StartsWith" prefix="https://mywebsite.com" labelId="1" />
	    <urlConfiguration parsePattern="/productimages" patternType="StartsWith" prefix="https://cdn.com" labelId="1" />
	  </urlConfigurations>
	</ecmanager.Modules.UrlResolver.DefaultUrlResolver>

.. note:: The patterns are defined per label.

There are three types of patterns:

* StartsWith: matches virtual URLs that start with the specified pattern
* EndWith: matches virtual URLs that end with the specified pattern
* Regex: matches virtual URLs that match with the specified regex

These pattern types should give you enough flexibility to choose which content is prefixed by which 
prefix.

.. warning:: If possible, it is recommended to not use the regex pattern type because it will have a performance penalty.


