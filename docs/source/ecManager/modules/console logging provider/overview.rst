########
Overview
########

This section describes the console logging provider module provided by ecManager. This module allows 
you to output the ecManager framework logs to a console window.

Visit the `Quick guide <quick-guide.html>`_ page to read more on how to get started with the 
console logging provider module.
