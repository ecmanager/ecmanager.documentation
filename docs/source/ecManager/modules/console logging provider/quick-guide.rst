###########
Quick guide
###########

Introduction
============
The ecManager framework logs a lot of data when it is interacted with. The console logging provider
is a module which allows you to output those logs to a console window.

Prerequisites
=============
To start using this module, you need to install the LoggingProvider NuGet package.

.. code-block:: PowerShell

	PM> Install-Package ecManager.Modules.LoggingProvider.Console

Registration
============
The ConsoleLoggingProvider can be registered in the ``Modules.config`` as follows:

.. code-block:: XML
	:linenos:

	<component
	  type="ecManager.Modules.LoggingProvider.Console.ConsoleLoggingProvider, ecManager.Modules.LoggingProvider.Console"
	  service="ecManager.Common.Interfaces.ILoggingProvider, ecManager.Common">
	</component>