#######################
Configuring the catalog
#######################

Introduction
============
With the introduction of a B2B catalogus we made some changes in how ecManager services handles
data calls. Based on configuration (anonymous and customer levels) and given customer identifiers
the returned data can change.

Configuration
=============
The configuration of the catalog is shown below.

The ``orderable`` property tells the services if a product or combination is orderable. 

- If the value is `true` then the orderable property of the product is used.
- If the value is `false` then this overrides the orderable property of the product.

The ``show`` property tells the services if products or combinations are returned as result in the
anonymous catalog (not logged-in users).

- If the value is `true`, then the result is returned.
- If the value is `false`, then the result is empty.

The ``filter`` property tells the services if products or combinations are returned as result in the
customer catalog (logged-in users).

- If the value is `true`, then the result is returned and filtered with customer identifiers.
- If the value is `false`, then the result is returned and not filtered with customer identifiers.

.. code-block :: xml
	:linenos:
	:caption: Configuration catalog section

	<catalog>
	 <anonymousCatalog orderable="true" show="true"/>
	 <customerCatalog orderable="true" filter="false"/>
	</catalog>

Customer identifiers
====================
If you are requesting for example a list of products, you are making a request to the Product
Service. The service checks two things:

- Does the ServiceRequestContext contains customer identifiers?
- Does the SearchTask contains customer identifiers?

Based on these checks the service uses the section customerCatalog or anonymousCatalog. So if there
are customer identifiers available, the service adds these customer identifiers as a filter to the
result.

.. important ::

	If there are customer identifiers applied to the ServiceRequestContext as well as the
	SearchTask, the SearchTask is then the leading property.