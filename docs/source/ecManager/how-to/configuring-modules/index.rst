################################
Configuring modules in ecManager
################################

Introduction
============
ecManager is a product that has a modular design. This enables the implementation party to choose which combination of technologies they wish to use. 

By default ecManager does not have any modules configured. ecManager however has default implementations for every module which means you can get started right away. 
For an overview of which modules we have available you can check the `documentation <../../modules/modules.html>`_. 
Some modules have custom configuration that needs to be done to set them up, therefore it's recommended you check the documentation when adding a module. 


Modules.config
==============
In order to register and manage these dependencies we use the AutoFac library. 
This allows you to register which modules you wish to use. This is done in the **Modules.config** file and looks like the following:

.. code-block:: xml

  <?xml version="1.0" encoding="utf-8" ?>
  <autofac defaultAssembly="ecManager.Common">
   <components>
	<component
		type="Full type name of concrete class, assembly of concrete class"
		service="Full type name of interface, assembly of interface"
		/>
   </components>
 </autofac>


Let's say we want to add the FileSystemStorageProvider module to our application. First we'll need to add the required NuGet package to our project. 
This can be done by using the command shown below or using the visual interface in your Visual Studio.

::

  PM> Install-Package ecManager.Modules.StorageProvider.FileSystem


After installing the nuget package the module has to be registered. This is done by adding the following to your modules.config inside the **<components>** element

.. code-block:: xml
	
	<component
	  type="ecManager.Modules.StorageProvider.FileSystem.FileSystemStorageProvider, ecManager.Modules.StorageProvider.FileSystem"
	  service="ecManager.Common.Interfaces.IStorageProvider, ecManager.Common">
	</component>

.. Warning::
  When replacing a module be sure to remove the old module from the modules.config file
  
.. Warning::
  Some modules such as the payment module can have more than one implementating module active. The order in which they are registered does make a difference in those cases.
  If you are unsure of this please check the documentation for the corresponding module.

In this xml we recognize two attributes. The type attribute is the type implementing the module. Please note that this needs to be the 
`assembly qualified name <https://msdn.microsoft.com/en-us/library/system.type.assemblyqualifiedname(v=vs.110).aspx>`_. 
The second attribute called service is the interface the module is implementing. In this case this is the ``IStorageProvider`` interface. 
This also need to include assembly of the interface.

Advanced
========
When a module is resolve by default a new instance of that module is created and return. 
However sometimes you want an instance to persist throughout the lifetime of your application as a single instance. 
In order to do this you need to add an extra attribute to your module registration. This is the instance-scope attribute and a looks like this.

.. code-block:: xml

	<component
	  type="ecManager.Modules.StorageProvider.FileSystem.FileSystemStorageProvider, ecManager.Modules.StorageProvider.FileSystem"
	  service="ecManager.Common.Interfaces.IStorageProvider, ecManager.Common"
	  instance-scope="single-instance">
	</component>

.. Warning::
  Be mindfull that using single-instance can cause errors depending on how a module is implemented. 
  For more information see the `Autofac documentation <http://docs.autofac.org/en/latest/lifetime/instance-scope.html>`_.