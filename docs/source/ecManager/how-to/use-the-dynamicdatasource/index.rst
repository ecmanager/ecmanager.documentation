#########################
Use the DynamicDataSource
#########################

The dynamic data source 
=======================

Make a Conficuration section for the DynamicDataSource in your config:

.. code-block:: C#
	:linenos:

	<section name="DynamicDataSources" type="ecManager.Common.Configurations.DynamicDataSourceConfigurationSection, ecManager.Common"/>

Description of the configuration section
----------------------------------------

.. rubric:: DynamicDataSource Element

- Name, name to get the data source by
- Id, type id for backwards compatibility
- ParentSourceName, needed when there is a separate data source needed as input for the CMS points the the Name attribute on the DynamicDataSource element
- LibraryType, points to the service or class on which the data method is called
- DataMethodName, the short name of the method to retrieve the data
- DataMethodReturnKeyType, the property on the returned type that will function as key (MUST BE UNIQUE!) use reflection full name  to ascertain the value
- DataMethodReturnValueType, the property on the returned type that will function as value, use reflection full name  to ascertain the value	

.. rubric:: DataMethodParameters Element

Used to define the parameters on the data method and their types. Make for each parameter a new ParameterTypeConfiguration Element.

Leave empty when there are no parameters.

- ParameterTypeConfiguration Element
	- Name, the name of the property in the data method
	- Type, the type of the property in the data method

.. rubric:: LibraryConstructorParameters Element

Used to define the parameters on the library constructor and their types. Make for each parameter a new ParameterTypeConfiguration Element.

Leave empty when there are no parameters.

- ParameterTypeConfiguration Element
	- Name, the name of the property in the data method
	- Type, the type of the property in the data method

Sample DynamicDataSource
------------------------

.. code-block:: C#
	:linenos:
	
	<DynamicDataSource Name="AttributeValue" Id="1" ParentSourceName="Attribute" LibraryType="ecManager.Services.AttributeService,ecManager.Services"
	DataMethodName="GetAttributeValues"  DataMethodReturnKeyType="ecManager.Catalog.Entities.LocalizedAttributeValue, ecManager.Catalog;Id"
	DataMethodReturnValueType="ecManager.Catalog.Entities.LocalizedAttributeValue,ecManager.Catalog;Texts|ecManager.Catalog.Entities.AttributeValue+Text,ecManager.Catalog;Title">
	 
	<DataMethodParameters>
	<ParameterTypeConfiguration Name="attributeId" Type="ecManager.Common.ValueTypes.AttributeId,ecManager.Common"/>
	</DataMethodParameters>
	 
	<LibraryConstructorParameters>
	<ParameterTypeConfiguration Name="context" Type="ecManager.Common.ComplexTypes.ServiceRequestContext,ecManager.Common"/>
	</LibraryConstructorParameters>
	 
	</DynamicDataSource>

