######################
Customize Entity State
######################

Introduction
============

When building webshops, the business rules for a product being visible may vary. So "Webshop A" requires products to have a price and texts before apearing on the website where "Webshop B" also requires images before the product appears on the website. An entity can be displayed in several contexts (f.e. product detail, product lister and in the search results). With EntityState it is possible to define custom rules to determine whether an entity (i.e. Product) gets online in a certain context. The rules can vary per website and per context.

.. image:: /_static/img/how-to/customize-entity-state/customize-entity-state.png
	:scale: 90%
	:align: center

In the ecManager CMS, EntityState is triggered to inform you about the state of entities. It lists per context the criteria and its state. The image below shows a product lister in the ecManager CMS. Per product a colored bullet is shown and when you mouseover the bullet, more details are displayed. Here you can see that this product is offline in every context because the pricing data is missing.

.. image:: /_static/img/how-to/customize-entity-state/customize-entity-state-1.png
	:scale: 90%
	:align: center

Using your custom entity state module you will be able to define the state in every context.
You also have full control on the contexts you'd like to use in your implementation. 

In this page we will explain how the entity state is designed to work, and how you can customize it to your own needs.
We will first start with a list of entities that are handled by the entity state module.
After that we will take a look at the basic technical details, followed by a more detailed explanation on using the default module provided by ecManager.

Stateful entities
=================

Not all entities are processed by the entity state module, simply because there is no real way to determine the state of such entities. Entities that do implement EntityState can be recognized because of the "IStatefulEntity" interface implementation. Below you will find a list of entities that are currently processed.


The following entities have entity state:

+------------------+
| Entities         |
+==================+
| Navigation items |
+------------------+
| Blocks           |
+------------------+
| Products         |
+------------------+
| Combinations     |
+------------------+
| Promotions       |
+------------------+
| Vouchers         |
+------------------+
| Product group    |
+------------------+
| News item        |
+------------------+
| Meta tag         |
+------------------+
| Vacancy          |
+------------------+

Technical introduction
======================

When an entity is handled by the EntityState module, it implements the IStatefulEntity interface. This interface has a property to get the type of the entity and the method to get the ID of the entity without having to cast the entity.

.. image:: /_static/img/how-to/customize-entity-state/customize-entity-state-3.png
	:scale: 90%
	:align: center

+-------------------+---------------------------------------------------------------------------------------------------------------------------+
| Property / Method | Purpose                                                                                                                   |
+===================+===========================================================================================================================+
| EntityStates      | The collection of states that is related to the entity                                                                    |
+-------------------+---------------------------------------------------------------------------------------------------------------------------+
| EntityType        | Identifier for the type of entity. Value comes from the ecManager.Common.EntityTypes enumeration. For product this is 100 |
+-------------------+---------------------------------------------------------------------------------------------------------------------------+
| GetEntityId()     | Wrapper to retrieve the identifier of the entity without casting the entity                                               |
+-------------------+---------------------------------------------------------------------------------------------------------------------------+

All requests regarding the entity state are handled using the EntityStateService. When a stateful entity (i.e. Product) is saved its service(i.e. ProductService) the save operation triggers an update of the entity state by calling the method UpdateEntityState on the EntityStateService. This method can also be called directly from any authorized application. The EntityStateService forwards all calls to the ecManager.Common.Logic.StateLogic. This class implements the IStateLogic interface, and acts as a dependency injection wrapper. All calls will be forwarded to the IStateLogic module that is configured in the 'Modules.config' file. The IStateLogic interface definition looks like this:

.. image:: /_static/img/how-to/customize-entity-state/customize-entity-state-4.png
	:scale: 90%
	:align: center

+------------------------------------------------------------------------------------------------------+-------------------------------------------------------------------------------------------+
| Property / Method                                                                                    | Purpose                                                                                   |
+======================================================================================================+===========================================================================================+
| GetEntityState( IStatefulEntity statefulEntity, ServiceRequestContext context );                     | Gets the state per contexts for a single IStatefulEntity                                  |
+------------------------------------------------------------------------------------------------------+-------------------------------------------------------------------------------------------+
| GetEntitiesState( IEnumerable<IStatefulEntity> statefulEntities, ServiceRequestContext context );    | Gets the state per contexts for a set of IStatefulEntity                                  |
+------------------------------------------------------------------------------------------------------+-------------------------------------------------------------------------------------------+
| GetEntityCriteria( IStatefulEntity statefulEntity, ServiceRequestContext context );                  | Gets the criteria per context for a single IStatefulEntity. Criteria can vary per context |
+------------------------------------------------------------------------------------------------------+-------------------------------------------------------------------------------------------+
| GetEntitiesCriteria( IEnumerable<IStatefulEntity> statefulEntities, ServiceRequestContext context ); | Gets the criteria per context for a set of IStatefulEntity. Criteria can vary per context |
+------------------------------------------------------------------------------------------------------+-------------------------------------------------------------------------------------------+
| UpdateEntityState(IStatefulEntity entity, ServiceRequestContext context);                            | Updates the state for the supplied entity                                                 |
+------------------------------------------------------------------------------------------------------+-------------------------------------------------------------------------------------------+
| UpdateEntitiesState(IEnumerable<IStatefulEntity> entities, ServiceRequestContext context);           | Updates the state for the supplied collection of entities                                 |
+------------------------------------------------------------------------------------------------------+-------------------------------------------------------------------------------------------+
| DeleteEntityState(IStatefulEntity entity);                                                           | Deletes the entity state for the supplied entity                                          |
+------------------------------------------------------------------------------------------------------+-------------------------------------------------------------------------------------------+
| DeleteEntitiesState(IEnumerable<IStatefulEntity> entities);                                          | Deletes the entity state for the supplied entities                                        |
+------------------------------------------------------------------------------------------------------+-------------------------------------------------------------------------------------------+

In this interface there are two important types of get methods. The first is the get method will retrieve the state of an entity. This function will return the state of the entity in every context.The other get method returns the entity state criteria for every context. This will return the state in every context, including the criteria that determine the actual state. Each criterion has the property 'IsMet', marking wether the entity satisfied the criterion. These get methods are designed to be used by the CMS. The criterion will be shown to the user when hovering the entity state indicator in the list. Using this overview, the user will know what criteria should be modified to make the product visible on the website.

The default entity state module
-------------------------------

The default module is located in the following binary: 'ecManager.Modules.EntityState.DefaultStateLogic.dll'. The entry point for this module is the 'DefaultStateLogic' class, which implements th 'IStateLogic'.

.. image:: /_static/img/how-to/customize-entity-state/customize-entity-state-5.png
	:scale: 90%
	:align: center

For this default module, we decided to delegate the logic to several classes. Each stateful entity has a sub logic. For example, the state logic related to products are handled by the 'ProductStateLogic' class.

.. image:: /_static/img/how-to/customize-entity-state/customize-entity-state-6.png
	:scale: 90%
	:align: center

Your custom entity state implementation
=======================================

Extending the entity state to make it match your project can be done on several levels in the module. In this sample we'll kick-off with an easy extension allowing your to modify the behavior of an existing criterion. More specific; in this sample we will extend the EntityState of the Product Entity to apply custom NotSoldOut rules:

+----------------------+---------------------------------------------------------------------------------------------------------------------------------+----------------------------------------------------+
| Criterion            | Default behavior                                                                                                                | Wanted behavior                                    |
+======================+=================================================================================================================================+====================================================+
| NotSoldOut criterion | The NotSoldOut criterion is met when a product has the Product.IsSoldOut property set to false (so the product is NOT sold out) | The NotSoldOut criterion is met when the Stock > 0 |
+----------------------+---------------------------------------------------------------------------------------------------------------------------------+----------------------------------------------------+

Steps to create a custom entity state implementation
----------------------------------------------------

The following steps will create a custom implementation of the NotSoldOut criterion:

1) Create library Project in Visual Studio
2) Add references to the following libraries:

     a. ecManager.Modules.EntityState.DefaultStateLogic
     b. ecManager.Common.Logic
     c. ecManager.Common
     d. ecManager.Catalog (maybe other domains depending on which entity you want to extend)
     
3) Create a "MyCustomProductStateLogic" class for your custom ProductStateLogic

     a. Make it Inherit from ProductStateLogic
     b. Implement the constructor

     .. code-block:: C#
     	:linenos:
     	:caption: Constructor

     	public MyCustomProductStateLogic(ServiceRequestContext context) : base(context)
		{
		}

	 c. Override the NotSoldOut criterion implementation and determine what rules you want to apply on the IsMet value

     .. code-block:: C#
     	:linenos:
     	:caption: NotSoldOut implementation

     	/// <summary>
		/// This methods override the default "NotSoldOut" criterion.
		/// </summary>
		/// <param name="entity">The product to evaluate</param>
		/// <param name="addResources">Should we add resources to the <see cref="Criterion"/> for the user interface</param>
		/// <returns>Returns a new <see cref="Criterion"/> instance.</returns>
		protected override Criterion GetNotSoldOutCriterion(Product entity, bool addResources)
		{
		    var returnItem = new Criterion
		    {
		        IsMet = entity.Stock.HasValue && entity.Stock.Value > 0, // Complex statement to see if SoldOut criterion is met
		        Name = (addResources) ? "My resource describing the name of the criterion" : "", // Name is for displaying purpose in the CMS
		        Description = (addResources) ? "Description of the criterion" : "" // Description is for displaying purpose in the CMS
		    };
		  
		    return returnItem;
		}

4) Create MyCustomStateLogic class which inherits from DefaultStateLogic

     a. Override GetProductStateLogic method and return new instance of custom created ProductStateLogic class
     
     .. code-block:: C#
     	:linenos:
     	:caption: Override ProductStateLogic instantiation

     	/// <summary>
		/// Overrides the default <see cref="DefaultStateLogic.GetProductStateLogic"/> allowing you te return a custom <see cref="ProductStateLogic"/> implementation.
		/// </summary>
		/// <param name="context"></param>
		/// <returns>My custom product entity state implementation</returns>
		protected override ISubStateLogic GetProductStateLogic(ServiceRequestContext context)
		{
		    return new MyCustomProductStateLogic(context);
		}

5) Compile the module
6) Configure your applications to use new implementation

	a. open modules.config
	b. replace default ecManager config from

     .. code-block:: C#
     	:linenos:
     	:caption: Default entity state config

     	<component
                type="ecManager.Modules.EntityState.DefaultStateLogic.DefaultStateLogic, ecManager.Modules.EntityState.DefaultStateLogic"
                service="ecManager.Common.Interfaces.IStateLogic, ecManager.Common"
                >
		</component>

	to

     .. code-block:: C#
     	:linenos:
     	:caption: New entity state config

     	<component
                type="CustomStateLogic.MyCustomStateLogic, CustomStateLogic"
                service="ecManager.Common.Interfaces.IStateLogic, ecManager.Common"
                >
		</component>

7) That's it; lets get ready to roll!		

Manually updating entity state
==============================
If you are updating data in the ecManager database directly instead of using ecManager services, you 
will notice that the entity state will not be updated. This can be solved using the EntityState 
Application (read more `here <../../../tools/EntityState%20Application/overview.html>`_), however you might not 
always want to use this. The EntityStateApplication updates the entity state for ALL stateful entities in 
your database and might take a long time and use a lot of resources. You also risk having incorrect entity 
state if there are big time gaps inbetween the execution of the application.

Instead, you can use the EntityStateService to update the entity state of stateful entities. Doing this 
makes sure that entity state is updated as soon as possible and is not such an expensive operation. 
Up next, scenarios will be described when to use the EntityStateService.

.. note:: The EntityStateService uses the entity state module registered in your module configuration.

Inserting and updating data
---------------------------
If you insert data directly in your database, you need to make sure that the entity state gets updated.
If the data is inserted in a table of a stateful entity (e.g. product or voucher), you need to make sure 
to update the entity state for the new row. You can do this as shown in the following example:

.. code-block:: C#
	:linenos:

	// Dummy id of the product that was inserted in the database.
	var productId = new ProductId(1);
	
	var service = new EntityStateService(ServiceRequestContext);
	service.UpdateEntityState(new Product { Id = productId }, ServiceRequestContext);

Updating the entity state for existing stateful entities works the same way. You can use the provided 
example to update the entity state for existing stateful entities.

There are overloads of most methods on the EntityStateService that accept collections of entities 
instead of a single one.

.. code-block:: C#
	:linenos:

	// Dummy collection of products that were updated, their entity state needs to be updated.
	var products = new List<Product> 
	{
	    new Product { Id = 1 },
	    new Product { Id = 2 },
	    new Product { Id = 3 }
	};
	
	var service = new EntityStateService(ServiceRequestContext);
	service.UpdateEntityState(products, ServiceRequestContext);


Deleting data
-------------
If you delete any stateful data from the database, you can delete the corresponding entity state as 
follows:

.. code-block:: C#
	:linenos:

	// The id of the voucher that was deleted from the database.
	var voucherId = new VoucherId(1);
	
	var service = new EntityStateService(ServiceRequestContext);
	service.DeleteEntityState(new Voucher { Id = voucherId });