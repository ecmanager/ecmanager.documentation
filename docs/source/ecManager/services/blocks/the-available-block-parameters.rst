##############################
The available block parameters
##############################

Within the block parameters there are several types of parameters available. This page describes all the available types including their parameters and sample XML definitions. 

DescriptionBlockParameter
=========================

The DescriptionBlockParameter is used to inform the user. The contents will be displayed in the CMS.

+-------------+---------------------------------------------+
| Property    | Description                                 |
+=============+=============================================+
| Description | The text which will be displayed in the CMS |
+-------------+---------------------------------------------+

.. code-block:: C#
	:linenos:
	:caption: Sample XML definition of a descriptive parameter

	<Parameter groupName="General">
	    <Label></Label>
	    <Id>Info</Id>
	    <Control>Description</Control>
	    <Description>This block can display up to 4 products.</Description>
	</Parameter>

TextboxBlockParameter
=====================

The TextboxBlockParameter is for inserting a text in the parameter and using it in the front-end. The text should be plain text and can be used for descriptive texts, url's and any other textual value.

+----------------+------------------------------------------------------------------------------------------+
| Property       | Description                                                                              |
+================+==========================================================================================+
| Rows           | The size of the textbox to display; in Rows                                              |
+----------------+------------------------------------------------------------------------------------------+
| Columns        | The size of the textbox to display; in Columns                                           |
+----------------+------------------------------------------------------------------------------------------+
| IsMultilingual | Is the Textbox multilingual. If true, it is actually a MultiLingualTextboxBlockParameter |
+----------------+------------------------------------------------------------------------------------------+
| Text           | The text inserted by the user                                                            |
+----------------+------------------------------------------------------------------------------------------+
| IsWysiwyg      | Flag to determine if Wysiwyg is enabled                                                  |
+----------------+------------------------------------------------------------------------------------------+

.. code-block:: C#
	:linenos:
	:caption: A sample plain textbox XML definition with a large(rows) and a small textbox

	<Parameter>
	    <Label>txt</Label>
	    <Id>txt</Id>
	    <Control>TextBox</Control>
	    <Rows>5</Rows>
	    <Text>My Default Text</Text>
	</Parameter>
	 <Parameter>
	    <Label>url</Label>
	    <Id>url</Id>
	    <Control>TextBox</Control>
	    <Text>http://</Text>
	</Parameter>

MultiLingualTextboxBlockParameter
=================================

The MultiLingualTextboxBlockParameter a variation of the TextboxBlockParameter where the input is localized. There are no additional properties.

+-------------+---------------------------------------------------+
| Property    | Description                                       |
+=============+===================================================+
| Texts       | The texts collection whit a text per LanguageCode |
+-------------+---------------------------------------------------+

.. code-block:: C#
	:linenos:
	:caption: A sample multilingual definition

	<Parameter>
	    <Label>txt</Label>
	    <Id>txt</Id>
	    <Control>TextBox</Control>
	    <MultiLanguage>True</MultiLanguage>
	    <IsWysiwyg>True</IsWysiwyg>
	</Parameter>

CheckboxBlockParameter
======================

The CheckboxBlockParameter is for giving the user a yes/no decision. It will display a checkbox in the CMS which the user can check/uncheck.

+-------------+------------------------------------------------------------------+
| Property    | Description                                                      |
+=============+==================================================================+
| IsChecked   | Tells if the block is saved with the checkbox in a checked state |
+-------------+------------------------------------------------------------------+

.. code-block:: C#
	:linenos:
	:caption: A sample XML definition

	<Parameter groupName="General">
	    <Label>check</Label>
	    <Id>check</Id>
	    <Control>checkbox</Control>
	    <SelectedValue>true</SelectedValue>
	</Parameter>

LibraryCheckboxBlockParameter
=============================

The LibraryCheckboxBlockParameter creates a list of checkboxes based on the call to an external code library.

+----------------+--------------------------------------------------------------------------------------------------------------------+
| Property       | Description                                                                                                        |
+================+====================================================================================================================+
| SelectedValues | The collection of checked values                                                                                   |
+----------------+--------------------------------------------------------------------------------------------------------------------+
| DataSource     | The LibraryBlockParameterDataSource that is used for loading values from an external code library using reflection |
+----------------+--------------------------------------------------------------------------------------------------------------------+

.. code-block:: C#
	:linenos:
	:caption: A sample XML definition of a datasource filled checkbox collection

	<Parameter groupName="General">
	    <Label>Available checkboxes</Label>
	    <Id>checkId</Id>
	    <Control>checkbox</Control>
	    <SelectMethod>GetMyData</SelectMethod>
	    <TypeName>Namespace.MyClass</TypeName>
	    <TextField>DisplayProperty</TextField>
	    <ValueField>ValueProperty</ValueField>
	    <SelectedValue></SelectedValue>
	</Parameter>

LibraryDropdownBlockParameter
=============================

The LibraryDropdownBlockParameter creates a dropdown the user can pick a value from. The possible values are set by calling an external code library.

+----------------+--------------------------------------------------------------------------------------------------------------------+
| Property       | Description                                                                                                        |
+================+====================================================================================================================+
| DataSource     | The LibraryBlockParameterDataSource that is used for loading values from an external code library using reflection |
+----------------+--------------------------------------------------------------------------------------------------------------------+	

.. code-block:: C#
	:linenos:
	:caption: A sample XML definition with parameters on the datasource

	<Parameter groupName="General">
	    <Label>The dropdown</Label>
	    <Id>dropdownId</Id>
	    <Control>DropDown</Control>
	    <SelectMethod>GetValues</SelectMethod>
	    <TypeName>MyNamespace.MyClass, Library</TypeName>
	    <SelectParameters>
	        <SelectParameter>
	            <Name>Emailaddress</Name>
	            <Type>string</Type>
	            <DefaultValue></DefaultValue>
	        </SelectParameter>
	    </SelectParameters>
	    <TextField>Value</TextField>
	    <ValueField>Key</ValueField>
	    <SelectedValue></SelectedValue>
	</Parameter>

UploadBlockParameter
====================

The UploadBlockParameter enables the user to upload a file that can be displayed on the front-end.

+----------------+-------------------------------------------------------------------------------------------------------------------------------------------------------+ 
| Property       | Description                                                                                                                                           |
+================+=======================================================================================================================================================+
| File           | The referece to the file                                                                                                                              |
+----------------+-------------------------------------------------------------------------------------------------------------------------------------------------------+
| MediaType      | The type of file that can be uploaded. Options are limited to "Image", "Video", "File"                                                                |
+----------------+-------------------------------------------------------------------------------------------------------------------------------------------------------+
| IsMultiLingual | Tells of the upload is multilingual. If true, it is probably a MultiLingualMediaLibraryBlockParameter as there is no MultilingualUploadBlockParameter |
+----------------+-------------------------------------------------------------------------------------------------------------------------------------------------------+

A sample XML definition:

.. code-block:: C#
	:linenos:
	:caption: A sample Image upload XML definition

	<Parameter>
	    <Label>upload</Label>
	    <Id>upload</Id>
	    <Control>DnzUpload</Control>
	    <UploadType>Image</UploadType>
	</Parameter>

MediaLibraryBlockParameter
==========================

The MediaLibraryBlockParameter enables the user to select a file from the medialibrary. It is an variation on the UploadBlockParameter and has no additional properties.

+-------------+---------------------------------------------------+
| Property    | Description                                       |
+=============+===================================================+
|             |                                                   |
+-------------+---------------------------------------------------+	

A sample XML definition:

.. code-block:: C#
	:linenos:
	:caption: A sample medialibary XML definition

	<Parameter>
	    <Id>media</Id>
	    <Label>media</Label>
	    <Control>MediaLibraryFile</Control>
	    <UploadType>Image</UploadType>
	</Parameter>

MultiLingualMediaLibraryBlockParameter
======================================

The MultiLingualMediaLibraryBlockParameter is a variation on the MediaLibraryBlockParameter and enables the user to select a file per language.

+-------------+--------------------------------------------------------------------------+
| Property    | Description                                                              |
+=============+==========================================================================+
| Files       | Collection of localized files where each file is related to the language |
+-------------+--------------------------------------------------------------------------+

A sample XML definition:

.. code-block:: C#
	:linenos:
	:caption: A sample XML definition for a multilingual Video upload

	<Parameter>
	    <Id>media</Id>
	    <Label>media</Label>
	    <Control>MediaLibraryFile</Control>
	    <UploadType>Video</UploadType>
	    <MultiLanguage>True</MultiLanguage>
	</Parameter>