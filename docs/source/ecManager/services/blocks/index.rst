######
Blocks
######

.. rubric:: Introduction

A page on an ecManager based website is setup using different and independent building blocks. This pages informs you about the concept op blocks in general. There is also a page with more detailed informaton on `configuring the block <../blocks/configuring-block-parameters.html>`_.
The blocks can be found in de CMS via "Commerce -> Navigation -> Section: blocks". The following screenshot displays a set of blocks that exist on a certain NavigationItem. In the screenshot you can see the status (meaning: off- online), the Id, the panel it is positioned in, the type of block and a description.


.. image:: /_static/img/services/blocks/blocks-1.png
    :width: 400px
    :align: center
    :height: 400px
    :alt: alternate text    

.. toctree::
   :maxdepth: 1

   configuring-block-parameters
   the-available-block-parameters