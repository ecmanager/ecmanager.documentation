############################
Configuring block parameters
############################

Blocks are based on the concept of repeatable structure and behavior where parameters control the behavior and output. This page describes the various parameters including samples and their usages. See the blocks page for a more overall view on the blocks.

The Block and its Parameters
============================

From a user point of view a Block can be added to a NavigationItem(page) for displaying carrousels, products and general information. The block represents logic, structure and input fields. All of them are defined design time(developer) but in the end, the user creates new instances of a block and sets the input fields making it a new, unique block while reusing the logic and structure.

From a technical point of view the Block entity has a Parameters property of the BlockParametersCollection type. This collection is basically an implementation of List<BlockParameter> with some additional logic. This logic allows easier access to the parameters using an indexer or a GetParameter method:

.. code-block:: C#
	:linenos:

	// Instantiate NavigationService
	navigationService.RelationDataFlag.Add(NavigationRelatedDataFlags.Blocks)
	var navigationItem = navigationService.GetNavigationItem(12345);
	var block = navigationItem.Blocks.First();
	 
	 
	// Access through indexer
	var parameter = block.Parameters["header_image"] as MediaLibraryBlockParameter;
	 
	// Access through GetParameter method
	var parameter = block.Parameters.GetParameter<MediaLibraryBlockParameter>("header_image");

The parameters in the Parameters collection are derived from the BlockParameter abstract class and is (de)serialized using XML.

.. code-block:: C#
	:linenos:
	:caption: Sample of a block parameter collection

	<?xml version="1.0" encoding="utf-8"?>
	<UserControlParameters>
	    <Parameter groupName="General">
	        <Label></Label>
	        <Id>Info</Id>
	        <Control>Description</Control>
	        <Description>This block can display formatted content using a WYSIWYG editor.</Description>
	    </Parameter>
	    <Parameter groupName="General">
	        <Label>Text</Label>
	        <Id>text</Id>
	        <Control>WysiwygEditor</Control>
	        <MultiLanguage>True</MultiLanguage>
	        <Text></Text>
	    </Parameter>
	</UserControlParameters>

The parameter storage
---------------------

As the parameters vary per block, there is a number of parameters in the BlockParametersCollection. Serialization is used to persist them in a generic way. Currently the parameters are stored on disk in the CmsData\ControlConfig folder. Here you will find two types of files. A definition and an instance(per created blok). The parameter definition is stored using a filename {blocktype}.xml where instance parameters are stored in a file called {blocktype}{blockid}.xml.

The following listing shows the configurations of a Banners, Carousel, CheckoutBasket and Faq block. The selected items represent the parameter definition (blueprint) where the others represent instances the user created blocks and include user inserted data. Editing the blueprint will make the ecManager CMS display different parameters.

.. image:: /_static/img/services/blocks/blocks-2.png
	:scale: 90%
	:align: center

Configuring the parameters on a Block
=====================================

When developing a new block, you'll also have to think about the parameters/arguments making a block more dynamic and reusable. For example; a carousel. You could create a complete new block per carousel but that would require a software engineer every time. Instead of hard coded; the block should have parameters for images and their links so the user can add a block and define what kind of data must displayed in the carousel.

The developer has several types of parameters available for configuring the block. As parameters are created in an inheritance structure, every parameter shares some generic properties. Besides that; parameters can have additional properties. The following properties are part of the base BlockParameter class. In the next section we'll discuss the possible parameter implementations.

+---------------+--------------------------------------------------------------------------------------------------------------+
| Property      | Description                                                                                                  |
+===============+==============================================================================================================+
| Id            | Identifier of the parameter. Must be unique within the collection of block parameters                        |
+---------------+--------------------------------------------------------------------------------------------------------------+
| Label         | The text to display in the UI(CMS) as label for the parameter                                                |
+---------------+--------------------------------------------------------------------------------------------------------------+
| GroupName     | Parameters can be displayed per group. The groupname will be used for displaying and grouping the parameters |
+---------------+--------------------------------------------------------------------------------------------------------------+
| HintText      | A text which can be used for additional information on what to do with the parameter                         |
+---------------+--------------------------------------------------------------------------------------------------------------+
| IsReadOnly    | Parameters which are displayed in the UI(CMS) but will not be persisted                                      |
+---------------+--------------------------------------------------------------------------------------------------------------+
| ParameterType | The type of the parameter defined in the BlockParametersTypes enumeration                                    |
+---------------+--------------------------------------------------------------------------------------------------------------+
| Xml           | The Raw Xml of the persisted parameter                                                                       |
+---------------+--------------------------------------------------------------------------------------------------------------+

The available parameter types
-----------------------------

The possible parameters are documented on a separate page: `The available block parameters <../blocks/the-available-block-parameters.html>`_

Parameters with an external datasource
======================================

Per type of parameter there are various options to configure. One of the possibilities is using an external datasource for Checkboxes and Dropdowns.The following screenshot illustrates a parameter where the values in the dropdown are loaded from the MyExtensionForDataAccess class.

.. image:: /_static/img/services/blocks/blocks-3.png
	:scale: 90%
	:align: center

There are some useful scenario's for loading data from external libraries:

- displaying typed/predefined values to the customer instead of letting them type an Id or String. This can be useful when working with fixed layout/color schemes within you project where the schemes are loaded from a library you defined.
- implementing behavior depending on values you define within your project (ie. displaying reviews in several views are with more or less items)
- passing a value to an external system when the page is loaded (ie. loading a set of vacancies from an external vacancy database where you want to supply the type of vacancy to load)
  
This section describes the datasource and the parameters for using the datasource. On the `parameters page <../blocks/the-available-block-parameters.html>`_ you'll find samples on using the datasources. In essence; the datasource needs to know where it should retrieve data from, what parameters need to be supplied while retrieving and how the response of the library must be handled to return data. The method should return a type that matches IEnumerable<object>.  	

.. rubric:: Loading an external library

When configuring the BlockParameter; you'll supply a hint on which type/library should be loaded. ecManager will try to load it using the .NET Type.GetType(<your input>) method (described on MSDN: https://msdn.microsoft.com/en-us/library/w3f99sx1%28v=vs.110%29.aspx). Valid values can be like "MyNameSpace.Class, MyAssembly". There is some backward compatibility which allows values like "MyNamespace.Class" where ecManager tries to resolve using the namespace as the assembly name.

.. rubric:: Supplying parameters

When calling an external assembly, you probably also need to supply some parameters to fill the arguments of the method. Parameters can be supplied in the parameters collection and are defined using a Name, Type and DefaultValue. Important are the Type and DefaultValue. The DefaultValue is for specifying the value for the argument. The Type is used for resolving. When resolving it will try the following steps one after another:

1) Type.GetType(<your input>) ; the default .NET type resolving
2) Type.GetType(System.<your input>) ; tries to resolve in context of System namespace
3) Type.GetType(ecManager.Common.ValueType.<your input>) ; tries to resolve in the context of the ecManager valuetypes (i.e. ProductId)

.. rubric:: Invoking

After the assembly is being loaded and the parameters are parsed, the supplied MethodName will be used to invoke a call. The external library is called and waiting for a response.

.. rubric:: Parsing the result

After the InvokeMember method is being triggered, the response must of of type IEnumerable<object>. ecManager will run through the response and create a Key/Value dictionary. The properties for determining the Key and Value are supplied using the parameter configuration using the TextField and ValueField.
