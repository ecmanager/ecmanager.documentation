###############
Payment methods
###############

Introduction
============

In the latest version of ecManager we have made changes concerning payment methods. You can still define payment methods using the ecManager CMS. As of now, a payment method should be linked to a payment method module. The details of this change will be described in the sections below. In the following section we'll be using a few terms that might seem confusing. We'll start off by taking a look at these terms.

+-------------------------------+----------------------------------------------------------------------------------------------------------------------------------------------------------+
| Term                          | Definition                                                                                                                                               |
+===============================+==========================================================================================================================================================+
| Payment method                | | A payment method is a way of paying for an order. For example iDEAL or VISA                                                                            |
+-------------------------------+----------------------------------------------------------------------------------------------------------------------------------------------------------+
| Payment provider              | | A payment provider is a service provider that handles the actual payment for you. The provider usually offers multiple payment methods                 |
|                               | | When a customer reached the payment step in the checkout process, the customer is usually forwarded to the secured environment of the payment provider |
|                               | | After the transaction is completed the customer is sent back to the webshop by the payment provider                                                    |
|                               | | Examples of payment providers are: WorldPay, PayPal or GlobalCollect                                                                                   |
+-------------------------------+----------------------------------------------------------------------------------------------------------------------------------------------------------+
| Payment provider module       | | The payment provider module is a representation of the payment provider in the ecManager ecosystem                                                     |
|                               | | This representation contains (a sub set of) the payment methods provided by the payment provider                                                       |
|                               | | There can be several modules for the same payment provider                                                                                             |
|                               | | A module is created by implementing the IPaymentProvider interface                                                                                     |
+-------------------------------+----------------------------------------------------------------------------------------------------------------------------------------------------------+
| Payment method implementation | | This is a payment method provided                                                                                                                      |
|                               | | This is the bridge between an actual payment method, and the ecManager ecosystem                                                                       |
|                               | | The implementation is a representation of a payment method from a payment provider                                                                     |
+-------------------------------+----------------------------------------------------------------------------------------------------------------------------------------------------------+
| PaymentMethod                 | | A payment method entity provided by the ecManager framework                                                                                            |
|                               | | You can manage the payment methods using the CMS                                                                                                       |
|                               | | This payment method is connected to an implementation of the IPaymentMethod interface                                                                  |
+-------------------------------+----------------------------------------------------------------------------------------------------------------------------------------------------------+
| IPaymentProvider              | | The interface for the implementation of a payment provider module within the ecManager ecosystem                                                       |
+-------------------------------+----------------------------------------------------------------------------------------------------------------------------------------------------------+
| IPaymentMethod                | | The interface for the implementation of a payment method that belongs to a IPaymentProvider implementation                                             |
+-------------------------------+----------------------------------------------------------------------------------------------------------------------------------------------------------+

Goals
=====

During the changes in payment methods we wanted to achieve a few goals. In this following section we will describe the goals we wanted to achieve, and which problems we wanted to solve. All ecManager releases up to version 2.7.3 were relying on an enumeration of payment methods.  This enumeration was a limited set of options, and some of the options were no longer relevant. These options were deactivated or renamed in the CMS to give them a new definition. We no longer want to rely on an hard coded enumeration, and allow you to register only the data you need. In the DNZ.EP libraries there were methods to convert data from several payment providers to data known to DNZ.EP. For example, the status "ok" was translated to PaymentStatus.Paid. As time progressed, payment providers continued to develop their platforms and the feedback they provide. This sometimes means the status they provide changes as well, which means the DNZ.EP logic had to be changed. To prevent the change of DNZ.EP logic or the new ecManager logic for every new payment method the knowledge about the payment methods is moved to one or more modules. These modules allow partners to modify and customize the payment method status information. For example it will be possible to handle one payment method by serveral providers based on custom conditions.

Technical overview
==================

The payment method and the providers consists of a structure of classes. These classes are shown in the diagram below.

.. warning:: In the section about implementing your own module we will describe in detail what the methods are used for, and what kind of output is expected.

.. image:: /_static/img/services/payment-methods/payment-methods.png
	:scale: 90%
	:align: center

Each IPaymentProvider can provide ecManager with a set of IPaymentMethod implementations. In an ideal situation you would create a single module for payment provider X, which you can redistribute among several implementations that use the services of this payment provider. It is also possible to mix payment methods from several providers. For example, I could process iDEAL transactions using WorldPay and let Adyen handle VISA payments. In order for this to work, every IPaymentMethod implementation connected to a PaymentMethod in the CMS should be functional in the webshop. This could be achieved by adding the logic for handling the payment transaction inside the IPaymentMethod implementation, which you could call using a custom interface. A major advantage now is that each payment method is able to give their own meaning to the payment status numbers. This means that payment status 12 could be interpreted as 'Paid' for an iDEAL payment, but it could mean 'Technical Error' for a VISA payment.

.. warning:: Interpretations of a payment status differ per payment status, per payment provider. Interpretations of the iDEAL payment status handled by WorldPay might not map with the interpretation of the iDEAL module for Buckaroo and vice versa.

Module identifier
=================

The new payment method implementations have a module identifier. The module identifier should be an unique string, preferable containing information about the version and payment method it represents. Including this information ensures you have an unique identifier that does not conflict with other modules.

OrderPaymentStatuses
====================

In ecManager we want to determine the payment status. The payment method implementation could have a large variety of statuses, but only a small set is relevant for ecManager. The relevant set of statuses has been defined in the OrderPaymentStatus enumeration. An IPaymentMethod implementation can be asked to translate the status of a payment to an OrderPaymentStatus. This will done using the GetStatus method with the IPayment argument. You will only receive the payments handled by your module implementation. The output of this method is not yet implemented in the rest of the framework, but we strongly recommend that you create a proper translation. When we implement the final changes in a future release, you will be able to upgrade without any impact.

This is a list of statuses and their definition:

+--------------------------+------------------------------------------------------------------------------------------------------------+
| OrderPaymentStatus value | Definition                                                                                                 |
+==========================+============================================================================================================+
| Initial                  | | This is the default value for a payment status on the order                                              |
|                          | | When an order is created, the payment method information might not be selected yet                       |
|                          | | Another scenario is that a payment is registered, but not yet started according to the payment provider  |
+--------------------------+------------------------------------------------------------------------------------------------------------+
| Started                  | | The payment procedure has started                                                                        |
+--------------------------+------------------------------------------------------------------------------------------------------------+
| Canceled                 | | The payment procedure has been canceled by the user                                                      |
+--------------------------+------------------------------------------------------------------------------------------------------------+
| Failed                   | | The payment procedure has failed                                                                         |
|                          | | A technical error occurred, or the customer's credentials could not be verified                          |
+--------------------------+------------------------------------------------------------------------------------------------------------+
| Partial                  | | There has been a payment, but it did not cover the full amount that had to be paid                       |
|                          | | For example: I have to pay €25 but due to an typo I wire transfer €2,50                                  |
|                          | | Ideally you never use this status                                                                        |
+--------------------------+------------------------------------------------------------------------------------------------------------+
| Paid                     | | The payment has been completed successfully                                                              |
+--------------------------+------------------------------------------------------------------------------------------------------------+

Orders with multiple payments
-----------------------------

Nowadays it is common to pay for your order using several payment methods. For example, you redeem a €50 voucher and you pay the remaining €24,95 using iDeal. This means that the payment status of an order actually is a composition of the status of the related payments. At the time of writing the actual implementation of this part is yet to be done, but we envision a system that works as depicted in the tables below. The examples are created for an order with a value of €75 euro.

+-----------+----------+----------------+--+-----------+---------+----------------+--+-----------+----------+--------------------------------+
| Payment   | Value    | Payment status |  | Payment   | Value   | Payment status |  | Payment   | Value    | Payment status                 |
+===========+==========+================+==+===========+=========+================+==+===========+==========+================================+
| Payment 1 | €25      | Paid           |  | Payment 1 | €25     | Initial        |  | Payment 1 | €25      | Paid                           |
+-----------+----------+----------------+--+-----------+---------+----------------+--+-----------+----------+--------------------------------+
| Payment 2 | €25      | Paid           |  | Payment 2 | €25     | Paid           |  | Payment 3 | €25      | Failed                         |
+-----------+----------+----------------+--+-----------+---------+----------------+--+-----------+----------+--------------------------------+
| Payment 3 | €25      | Paid           |  | Payment 3 | €25     | Paid           |  | Payment 3 | €25      | Paid                           |
+-----------+----------+----------------+--+-----------+---------+----------------+--+-----------+----------+--------------------------------+
| Order payment status | Paid           |  | Order payent status | Partial        |  | Order payment status | Paid                           |
+----------------------+----------------+--+---------------------+----------------+--+----------------------+--------------------------------+

+-----------+----------+----------------+--+-----------+---------+----------------+--+-----------+----------+--------------------------------+
| Payment   | Value    | Payment status |  | Payment   | Value   | Payment status |  | Payment   | Value    | Payment status                 |
+===========+==========+================+==+===========+=========+================+==+===========+==========+================================+
| Payment 1 | €25      | Initial        |  | Payment 1 | €25     | Paid           |  | Payment 1 | €25      | Paid                           |
+-----------+----------+----------------+--+-----------+---------+----------------+--+-----------+----------+--------------------------------+
| Payment 2 | €25      | Initial        |  | Payment 2 | €25     | Failed         |  | Payment 3 | €25      | Partial(expected 25, got 2,50) |
+-----------+----------+----------------+--+-----------+---------+----------------+--+-----------+----------+--------------------------------+
| Payment 3 | €25      | Initial        |  | Payment 3 | €25     | Paid           |  | Payment 3 | €25      | Paid                           |
+-----------+----------+----------------+--+-----------+---------+----------------+--+-----------+----------+--------------------------------+
| Order payment status | Initial        |  | Order payent status | Failed         |  | Order payment status | Paid                           |
+----------------------+----------------+--+---------------------+----------------+--+----------------------+--------------------------------+

+-----------+----------+----------------+--+-----------+---------+----------------+
| Payment   | Value    | Payment status |  | Payment   | Value   | Payment status |
+===========+==========+================+==+===========+=========+================+
| Payment 1 | €25      | Paid           |  | Payment 1 | €25     | Paid           |
+-----------+----------+----------------+--+-----------+---------+----------------+
| Payment 2 | €25      | Cancelled      |  | Payment 2 | €25     | Started        |
+-----------+----------+----------------+--+-----------+---------+----------------+
| Payment 3 | €25      | Paid           |  | Payment 3 | €25     | Paid           |
+-----------+----------+----------------+--+-----------+---------+----------------+
| Order payment status | Cancelled      |  | Order payent status | Partial        |
+----------------------+----------------+--+---------------------+----------------+

Implementing new versions of a payment method modules
-----------------------------------------------------

When introducing a new version of a payment provider module, you should implement and configure the payment module as described below. Then the new module should be connected to a new payment method using the CMS. Make sure your implementation in the webshop is ready to use the new  version. Information about the implementation in your webshop can be found in the sections below. Connect this payment method to a payment method group and you should be able to use the payment in your webshop. It is highly recommended that the module identifier of your new payment method module is unique. This allows you to keep the old modules intact for the correct translation of payment statuses.

.. warning:: If you reuse module identifiers, you need to keep in mind that payment statuses in orders paid with the old payment methods will be 'interpreted' by the new module.

Backward compatibility
======================

To minimize the impact for existing implementations we have created a module that simulates the legacy payment statuses. After upgrading your database, you should update your payment methods using the CMS. All active payment methods need to be connected to a payment method module. Orders paid using a payment method without a module will not display correct payment status information. You an identify the backwards compatibility module by the name 'Legacy ecManager'. When selecting a payment method module you should see '<old name> by Legacy ecManager'.  Despite the difference in names, all payment method return the same translations. 

.. image:: /_static/img/services/payment-methods/payment-methods-1.png
	:scale: 90%
	:align: center

Implementing your own Payment Module
====================================

Your custom payment provider module starts by creating a new class library. Then you create an implementation of the IPaymentProvider interface. In the table below we describe the expected outcome of the methods.

+------------------------+------------------------------------------------------------------------------+----------------------------------------------------------------------------------------------------------+
| Method                 | Expected outcome                                                             | Usage                                                                                                    |
+========================+==============================================================================+==========================================================================================================+
| GetName                | | The name of the payment provider.                                          | | The name is used in the detail page of a payment method.                                               |
|                        |                                                                              | | It is used to construct '<PaymentMethodName> by <PaymentProviderName>' in the dropdown.                |
+------------------------+------------------------------------------------------------------------------+----------------------------------------------------------------------------------------------------------+
| GetPaymentMethod       | | If one of the payment methods provided by the payment provider has this    | | When viewing an order, we need to translate the payment method status.                                 |
|                        | | module identifier, return it. Otherwise return null.                       | | We locate the payment method used for the payment, then locate the matching payment method.            |
|                        |                                                                              | | The GetStatus method on the payment method is called to gather a translation of the status.            |
+------------------------+------------------------------------------------------------------------------+----------------------------------------------------------------------------------------------------------+
| GetPaymentMethods      | | Return a list of all the payment methods provided by the payment provider. | | On the detail page of a payment method in the CMS, we list all the possible payment methods.           |
+------------------------+------------------------------------------------------------------------------+----------------------------------------------------------------------------------------------------------+
| GetProviderIndentifier | | The unique identifier of a payment method.                                 | | This method isn't used by ecManager right now, but we might use it in future optimizations.            |
|                        | | This is similar for the module identifier of the payment method.           | | We recommend combining the ProviderIdentifier with the ModuleIdentifier to ensure a unique identifier. |
+------------------------+------------------------------------------------------------------------------+----------------------------------------------------------------------------------------------------------+
| GetVersion             | | Return the version of the payment provider.                                | | This method isn't used by ecManager right now, but we might use it in future optimizations.            |
|                        |                                                                              | | For example, displaying the version of the payment method implementation in the list of                |
|                        |                                                                              | | payment methods in the CMS                                                                             |
+------------------------+------------------------------------------------------------------------------+----------------------------------------------------------------------------------------------------------+

The provider returns one or more implementations of the IPaymentMethod interface. In the table below we describe the expected outcome of the methods.

+----------------------+-----------------------------------------------------------------------+------------------------------------------------------------------------------------------------------+
| Method               | Expected outcome                                                      | Usage                                                                                                |
+======================+=======================================================================+======================================================================================================+
| GetModuleIdentifier  | The unique identifier of the payment method                           | | The unique identifier is used by ecManager to locate the correct module                            |
+----------------------+-----------------------------------------------------------------------+------------------------------------------------------------------------------------------------------+
| GetName              | The name of the payment method                                        | | The name is used in the detail page of a payment method                                            |
|                      |                                                                       | | It is used to construct '<PaymentMethodName> by <PaymentProviderName>' in the dropdown             |
+----------------------+-----------------------------------------------------------------------+------------------------------------------------------------------------------------------------------+
| GetVersion           | Return the version of the payment provider                            | | This method isn't used by ecManager right now, but we might use it in future optimizations         |
|                      |                                                                       | | For example, displaying the version of the payment method implementation                           |
|                      |                                                                       | | in the list of payment methods in the CMS                                                          |
+----------------------+-----------------------------------------------------------------------+------------------------------------------------------------------------------------------------------+
| GetStatus (short)    | Method to get a translation for a payment status                      | | In an order we store a payment method status as a short                                            |
|                      |                                                                       | | When viewing an order, we want to show a text instead of a short                                   |
|                      |                                                                       | | We use this method to get a translation for the status                                             |
+----------------------+-----------------------------------------------------------------------+------------------------------------------------------------------------------------------------------+
| GetStatus (IPayment) | Method to get the OrderPaymentStatus translation for a payment status | | As described in the OrderPaymentStatus section we want to determine the payment status of an order |
|                      |                                                                       | | The payment status can have a variety of statuses, but only a few statuses interest us             |
|                      |                                                                       | | Using this method the module can translate the status specific for the payment,                    |
|                      |                                                                       | | to a status that is useful for ecManager                                                           |
+----------------------+-----------------------------------------------------------------------+------------------------------------------------------------------------------------------------------+
| GetStatuses          | Method to get a set of translations of payment statuses               | | This method isn't used by ecManager right now, but we might use it in future optimizations         |
+----------------------+-----------------------------------------------------------------------+------------------------------------------------------------------------------------------------------+

After creating and building your class library copy this file to the bin folder of your applications. Add a new component to your modules.config file.

.. code-block:: C#
	:linenos:
	:caption: Component syntax

	<component
	    type="The.Namespace.Of.Your.PaymentProvider, The.Name.Of.Your.Assembly" // edit this line
	    service="ecManager.Common.Interfaces.IPaymentProvider, ecManager.Common">
	</component>

The final step would be starting the CMS, and connecting the new payment method implementations to a payment method in the CMS. 

Implementing a payment module in the webshop
============================================

When you have correctly configured the payment method in the CMS, a customer should be able to select the payment method in the webshop. After selecting the payment method during the check out, the payment should be initiated. The first step is to extract the payment information from the basket. Based on this information we can forward the customer to the payment provider.

.. code-block:: C#
	:linenos:

	PaymentMethodId paymentMethodId = PaymentMethodId.Parse((int)order.PaymentMethod); // This should be changed to a PaymentMethodId in the future.
	PaymentMethodService.PaymentMethodRelationDataFlags.Add(PaymentMethodRelatedDataFlags.Texts);
	var paymentMethod = PaymentMethodService.GetPaymentMethod(paymentMethodId);
	if (paymentMethod == null)
	{
	    // This is bad!
	    Logger.Fatal(string.Format("Error, undefined payment method {0} in order {1}", order.PaymentMethod, order.Id));
	    return null;
	}
	  
	switch (paymentMethod.ModuleIdentifier)
	{
	    case "WorldPay-Visa-Soap":
	        // initiate the world pay logic.
	        // call the logic
	    break;
	    (...)
	}

You can add the logic inside the IPaymentMethod implementation. You can use the ecManager.Payment.PaymentInterface class to get your payment provider. After loading the correct IPaymentMethod implementation you should be able to downcast it and call methods to handle the payment.

.. warning:: If your CMS and webshop run on different environments, it is very important to keep the available payment modules in sync between these environments. If your customer chooses a payment method module that isn't available on the server hosting the webshop, an exception will be thrown. 

Troubleshooting
===============

In the following section we will describe problems you might encounter. If needed, we will add items to this section using your feedback.

My payment statuses aren't translated correctly / Two payment methods have the same identifier
==============================================================================================

When two IPaymentMethod implementations use the same ModuleIdentifier key, the first module encountered is used. This might be the cause of incorrect translations of payment statuses.		