######
Orders
######

.. rubric:: Introduction

The order holds all Products and Combinations the customer did buy and it holds all the
Vouchers and Discounts you get when ordered the items in the order.

.. toctree::
   :titlesonly:
   
   overview
   quick-guide
   flow