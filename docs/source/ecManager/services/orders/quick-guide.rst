###########
Quick guide
###########

Introduction
============
This section describes the basic information about orders. 
We will first take a look at the concepts regarding orders. 
Then, in the section about mandatory fields, we will describe which fields must be filled when
saving a order. In the recommended fields section we describe the fields we recommend you fill while
creating an order. Then we describe the optional fields you could use to store information.
After the optional fields we will place a few side notes concerning the customer snapshots.

When you create an order, you make a simplified copy of your basket.
Data related to the order will be stored in a 'snapshot' so we always know the original data of the order.
Information that is stored is:

- Order lines and order line options
- Data related to promotions on the order
- Customer data including addresses.
- Vouchers used in the order.
- Payment information

We store this snapshot because a user might edit the user data after placing an order, and we still want to see to which address the orders originally shipped to.

.. warning:: The customer should be saved before saving the order.

Mandatory fields
================

When you are saving an order there are a few mandatory fields. 
When you fill these fields you can save a basic order.

+-------------------+--------------------+--------------------------------------------------------------------------------------------------------+
| Field name        | Expected value     | Description                                                                                            |
+===================+====================+========================================================================================================+
| LabelId           | A valid LabelId    | The id of the website label the order was placed on                                                    |
+-------------------+--------------------+--------------------------------------------------------------------------------------------------------+
| CustomerId        | A valid CustomerId | The id of the customer                                                                                 |
+-------------------+--------------------+--------------------------------------------------------------------------------------------------------+
| OrderedOn         | A valid DateTime   | The time the order was placed                                                                          |
+-------------------+--------------------+--------------------------------------------------------------------------------------------------------+
| Subtotal          | A Price            | The subtotal of the order                                                                              |
+-------------------+--------------------+--------------------------------------------------------------------------------------------------------+
| Total             | A Price            | The total of the order                                                                                 |
+-------------------+--------------------+--------------------------------------------------------------------------------------------------------+
| VisibleToCustomer | A boolean          | Should the order be shown in the 'Order history' page of the 'My Account' area default value is 'false |
+-------------------+--------------------+--------------------------------------------------------------------------------------------------------+

Recommended fields
==================

We recommend that you fill the following properties of the order before saving.

+--------------------+--------------------------------------+----------------------------------------------------------------+
| Field name         | Expected value                       | Description                                                    |
+====================+======================================+================================================================+
| CustomerSnapshot   | An Order.Customer instance           | The shipping information and personal details of the customer  |
+--------------------+--------------------------------------+----------------------------------------------------------------+
| Payments           | One or more Payment instances        | One or more payment transactions related to this order         |
+--------------------+--------------------------------------+----------------------------------------------------------------+
| Lines              | One or more Order.Line instances     | The contents of the order, like products and combinations      |
+--------------------+--------------------------------------+----------------------------------------------------------------+
| IsConfirmationSent | True or false                        | Is the order confirmation mail sent                            |
+--------------------+--------------------------------------+----------------------------------------------------------------+
| PaymentStatus      | A PaymentStatusses value             | The status of the payment                                      |
+--------------------+--------------------------------------+----------------------------------------------------------------+
| PaymentMethod      | A PaymentMethods value               | The payment method                                             |
+--------------------+--------------------------------------+----------------------------------------------------------------+
| Status             | An OrderStatusses value              | The status of the order. Default value: New                    |
+--------------------+--------------------------------------+----------------------------------------------------------------+
| Number             | An actual order number, or a pattern | The order number. Patterns are processed by a database trigger |
+--------------------+--------------------------------------+----------------------------------------------------------------+

Optional fields
===============

This section describes fields that are optional, but might be usefull for your implementation.

+----------------------+-----------------------------------------+---------------------------------------------------------------------------------------------------------+
| Field name           | Expected value                          | Description                                                                                             |
+======================+=========================================+=========================================================================================================+
| Configuration        | An XML document                         | | In some cases you want to store information you cannot fit into the order object                      |
|                      |                                         | | In this XML document you can add additional information                                               |
|                      |                                         | | This document will be persisted in the database and returned to you when loading the order            |
+----------------------+-----------------------------------------+---------------------------------------------------------------------------------------------------------+
| Credits              | One or more CreditTransaction instances | | If your webshop uses credits or reward points, you can use this field to register transactions        |
|                      |                                         | | When placing an order, you can redeem a credits, but also recieve some                                |
+----------------------+-----------------------------------------+---------------------------------------------------------------------------------------------------------+
| Vouchers             | One or more Order.Voucher instances     | | If your webshop uses vouchers, you can use this property to store the vouchers used in this order     |
+----------------------+-----------------------------------------+---------------------------------------------------------------------------------------------------------+
| Promotions           | One or more Order.Promotion instances   | | If your webshop uses promotions, you can use this property to store the promotions used in this order |
+----------------------+-----------------------------------------+---------------------------------------------------------------------------------------------------------+
| IsStockProcessed     | True or false                           | | Is the stock of all items in the basket reduced with the amount of products required by this order    |
+----------------------+-----------------------------------------+---------------------------------------------------------------------------------------------------------+ 
| PickupLocationId     | A LocationId                            | | The location the customer will pick up his order                                                      |
+----------------------+-----------------------------------------+---------------------------------------------------------------------------------------------------------+
| IsPartialPayment     | True or false                           | | Is the order partially paid                                                                           |
+----------------------+-----------------------------------------+---------------------------------------------------------------------------------------------------------+
| PartialPaymentAmount | An Amount                               | | The amount that has been paid                                                                         |
+----------------------+-----------------------------------------+---------------------------------------------------------------------------------------------------------+
| TrackTrace           | A string                                | | The track and trace code provided by the transporter                                                  |
+----------------------+-----------------------------------------+---------------------------------------------------------------------------------------------------------+
| Remarks              | A string                                | | You can allow your customer to fill in remarks when placing the order                                 |
+----------------------+-----------------------------------------+---------------------------------------------------------------------------------------------------------+
| ShippingMethod       | A ShippingMethods value                 | | The way the order is shipped                                                                          |
+----------------------+-----------------------------------------+---------------------------------------------------------------------------------------------------------+
| Discount             | A Price                                 | | The discount given on the order                                                                       |
+----------------------+-----------------------------------------+---------------------------------------------------------------------------------------------------------+
| PaymentCosts         | A Price                                 | | The costs charged for the selected payment method(s)                                                  |
+----------------------+-----------------------------------------+---------------------------------------------------------------------------------------------------------+
| ShippingCosts        | A Price                                 | | The costs charged for the selected shipping method                                                    |
+----------------------+-----------------------------------------+---------------------------------------------------------------------------------------------------------+
| DeliverOn            | A DateTime                              | | The date and time the order was delivered                                                             |
+----------------------+-----------------------------------------+---------------------------------------------------------------------------------------------------------+

Retrieving orders
=================
There are multiple methods to retrieve orders using the OrderService. When using one of these methods, 
payment information (status and description) is projected onto the orders. To do this, the payment 
modules used to perform payments for this order need to be configured to retrieve the correct payment
information. If the payment modules are not configured, an error will occur.

Customer snapshot
=================

The order contains a customer snapshot. 
This snapshot contains three addresses: InvoiceAddres, DeliveryAddress and PickupAddress.
These three properties cannot be set to null, and will always be present in the object.
This is something to consider when exporting order data to a back office application. 

Search on customer snapshot address
===================================

With the SnapShotAddress property on OrderSearchTask can be searched on different parts of the customer snap shot address: invoice address, delivery address and pick-up address.
SnapShotAddress is of type OrderSearchTask.CustomerSnapShotAddress which has a property AddressType. AddressType is a flagable enumeration SnapShotAddressTypes,  a choice can be made on which address type(s) the search operation is applied.

.. code-block:: C#
	:linenos:

	var searchTask = new OrderSearchTask();
	searchTask.SnapShotAddress = new CustomerSnapShotAddress
	{
		AddressType = SnapShotAddressTypes.InvoiceAddres | SnapShotAddressTypes.DeliveryAddres,
		PostalCode = "1234AB",
		HouseNumber = "1",
		HouseNumberAddition = "a"
	}