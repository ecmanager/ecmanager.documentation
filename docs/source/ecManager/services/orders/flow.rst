####
Flow
####

Introduction
============
This section describes the default order flow. To know more about the definition of fields, visit
the `Quick guide <quick-guide.html>`__ page.

Steps
=====

.. csv-table::
    :header: "Step", "Method", "Description"
    :widths: 1,1,1

    "Basket entity populated", "", "The basket entity must be correctly populated with data before creating an order."
    "Create an order based on the basket", "CreateOrder", "The order can be created based on the baseket."
    "Apply payments if necessary", "", "The order can be payed with the configured payment methods."
