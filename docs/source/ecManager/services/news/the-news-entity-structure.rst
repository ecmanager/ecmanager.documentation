#########################
The news entity structure
#########################

The NewsService and it's entities are designed for news items on a page. The service manages Newsgroup and NewsItem entities including the comments. The service implements a ImageService for structured images with the service it's possible to retrieve an image which is linked to a newsitem.

.. image:: /_static/img/services/news/news.png
	:scale: 90%
	:align: center