####
News
####

.. rubric:: Introduction

The NewsService is a service for managing Newsgroups, News items and comments on the items. It is a hybrid solution for News and Blog features.

.. toctree::
   :maxdepth: 1

   the-news-entity-structure



	