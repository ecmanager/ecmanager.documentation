===================
Predicate providers
===================

Introduction
============

In the ecManager data layer we are currently relying on LLBLgen for gathering data from the database. LLBLgen uses a predicate system to construct queries. From the ecManager point of view, every search task is converted to a predicate expression. On this page we will explain the concept of predicate providers and what they can do for you.

Goals
=====

There are two goals we wanted to achieve by creating the predicate providers. The first goal is to cover scenarios we needed to combine the result of two search operations. For example, on a product lister you want to show a set of attribute filters with a count next to ut of the amount of products that satisfy this filter. And you also want to leave out the filter options that will result in 0 product results. You can only get this data by filtering your attribute value search result with the result of the product search task. Passing product id's would not be an option due to the potential size of a product lister. We need to solve this by nesting queries. A problem with this approach is that you might need to nest a search task that isn't part of your domain. To solve this, we have chosen to move the predicate providers to modules. The second goal is providing more freedom to the implementation. We provide you with a basic search task, with a defined set of options. This might not be enough for your shop. In this case you are able to make your own search task which does have the options you need. This is also achieved by the predicate provider mechanism. 

Technical overview
==================

We started off by creating a set of interfaces for search tasks. The structure is depicted in the diagram below.

.. warning:: At the time of writing, not all logics are using a predicate provider. The easiest way to check what logics do would be checking the modules.config of the default cms provided by us. This should contain a list of implemented predicate providers.

.. image:: /_static/img/services/predicate-providers/predicate-providers.png
	:scale: 90%
	:align: center

Everything starts off as an ISearchTask. In our domain all search tasks are paged(IPagedSearchTask), but we did not want to exclude unpaged search tasks in our model. Below the IPagedSearchTask interface you will find a variety of interfaces that represent search tasks for several entities. As you can see, most interfaces are empty. You are free to define the actual properties on your search task. The concept is that you provide the service layer with an implementation of the IProductSearchTask. This implementation will be processed by the matching predicate provider using the flow depicted below. 

.. image:: /_static/img/services/predicate-providers/predicate-providers-1.png
	:scale: 90%
	:align: center

Please keep in mind that the product search task flow is used as an example. Other paths will be implemented in a similar way. The service passes the search task to the corresponding logic. The logic hands it over to the PredicateProvider so it can resolve the corresponding provider implementation. As we mentioned earlier on in the goals section there are scenario's where you want to use the filters from a different search task(s) within your result set. In this scenario the provider implementation will call the PredicateProvider class to retrieve the IRelationPredicateBucket of the nested search task. The result of this call can be used in a sub query to filter the result set. 

The DefaultProductPredicateProvider implements the IPredicateProvider interface. The interface is shown below. 

.. image:: /_static/img/services/predicate-providers/predicate-providers-2.png
	:scale: 90%
	:align: center

There is one method that has to be implemented by the interface. You have to construct an instance of the IRelationPredicateBucket based on the search task that was given to you. For documentation on using the LLBLgen Predicate system we would like to refer you to the online manual on the LLBLgen website.

.. warning:: At the time of writing, the ProductService only accepts a ProductSearchTask as an argument in the SearchProducts method. This will be changed in the near future so it accepts any implementation of IProductSearchTask. If you need this functionality right now, you need to derive from the ProductSearchTask type. 

Implementing your own predicate provider
========================================

In this section we will describe how you can create your own predicate provider.

Create your own search task
---------------------------

When you feel that the default search task does not provide you with the right properties or behavior, you are free to create your own search task. Keep in mind that this is an optional step. You could create your custom implementation of a provider for a search task in ecManager. Just make sure you remove our original copy from the configuration to provide strange behavior. Your custom search task should implement  the same interface as the search task you want to replace. So if you want to replace the ProductSearchTask, you need to implement the IProductSearchTask. After implementing the mandatory properties you are free to add all the properties you would like.

.. warning:: Right now you might need to inherit from the original search task due to limitations in the service layer. We tend to fix this issue in the near future. 

An example could be this:

.. code-block:: C#
	:linenos:

	namespace My.Custom.Namespace // Assembly name is My.Custom This will be important later on.
	{
	    using System.Collections.Generic;
	 
	    using ecManager.Common.ValueTypes;
	    /// <summary>
	    /// My smaller version of the Attribute Search Task.
	    /// What else do you need?
	    /// </summary>
	    public class MiniAttributeSearchTask : PagedSearchTask, IAttributeSearchTask
	    {
	        public MiniAttributeSearchTask()
	        {
	            AttributeIds = new List<AttributeId>();
	        }
	     
	        /// <summary>
	        /// A collection of attribute ids.
	        /// </summary>
	        public ICollection<AttributeId> AttributeIds { get; set; }
	         
	        /// <summary>
	        /// The CmsDescription of the Attribute to search for
	        /// </summary>
	        public string CmsDescription { get; set; }
	    }
	}

Create an implementation of the IPredicateProvider interface
------------------------------------------------------------

Create a new C# project that will contain one or more predicate providers, depending on your needs. You will need to add the following references:

- References to the assemblies containing the search task you want to service
- References to the LLBL libraries in ecManager (right now the are name DNZ.EP.LLBL)
- A reference to the  'ecManager.PredicateExpressionProvider' assembly.
- A reference to the 'SD.LLBLGen.Pro.DQE.SqlServer.NET20' assembly
- A reference to the 'SD.LLBLGen.Pro.ORMSupportClasses.NET20' assembly

Then add a new class that implements the IPredicateProvider<> interface. Insert the search task you want to implement as the generic argument. In our example we will stick with the MiniAttributeSearchTask 
we've made in the previous step. 

.. code-block:: C#
	:linenos:

	namespace My.Custom.Provider.Namespace// Assembly name is My.Custom This will be important later on.
	{
	    using DNZ.EP.LLBL.EntityClasses;
	    using DNZ.EP.LLBL.HelperClasses;
	  
	    using ecManager.PredicateExpressionProvider;
	  
	    using My.Custom.Namespace;
	  
	    using SD.LLBLGen.Pro.ORMSupportClasses;
	 
	    /// <summary>
	    /// Predicate provider for the <see cref="MiniAttributeSearchTask"/>
	    /// </summary>
	    public class MiniAttributePredicateExpressionProvider : IPredicateProvider<MiniAttributeSearchTask>
	    {
	        /// <summary>
	        /// Creates a <see cref="IRelationPredicateBucket"/> using the <see cref="MiniAttributeSearchTask"/>
	        /// </summary>
	        /// <param name="searchTask">The search task that will determine the predicates in the <see cref="IRelationPredicateBucket"/>.</param>
	        /// <returns>Returns a new <see cref="IRelationPredicateBucket"/> instance with a predicate expression.</returns>
	        public SD.LLBLGen.Pro.ORMSupportClasses.IRelationPredicateBucket CreatePredicateExpression(
	            MiniAttributeSearchTask searchTask)
	        {
	            var predicateBucket = new RelationPredicateBucket();
	  
	            if (searchTask.AttributeIds.Any())
	            {
	                predicateBucket.PredicateExpression.Add(AttributeFields.AttributeId ==
	                                                        searchTask.AttributeIds.Select(item => item.Value).ToList());
	            }
	  
	            if (!string.IsNullOrWhiteSpace(searchTask.CmsDescription))
	            {
	                predicateBucket.PredicateExpression.Add(SearchHelper.SearchLike(AttributeFields.CmsDescription,
	                    searchTask.CmsDescription));
	            }
	  
	            return predicateBucket;
	        }
	    }
	}

This should be your initial implementation of a predicate provider. The given example is very simple and doesn't use joins. Advanced operations are out of scope for this page. For more information about how to perform join operations in LLBLgen we would like to refer you to the LLBLgen documentation.

Optional: Filter your results with a nested search task
-------------------------------------------------------

In some cases you want to use the results of a search task to filter the results of your own query. You would have to add a property to your search task to allow this.

.. code-block:: C#
	:linenos:

	namespace My.Custom.Namespace // Assembly name is My.Custom This will be important later on.
	{
	    (...)
	    /// <summary>
	    /// My smaller version of the Attribute Search Task.
	    /// What else do you need?
	    /// </summary>
	    public class MiniAttributeSearchTask : PagedSearchTask, IAttributeSearchTask
	    {
	        (...)
	        /// <summary>
	        /// The CmsDescription of the Attribute to search for
	        /// </summary>
	        public string CmsDescription { get; set; }
	  
	        /// <summary>
	        /// A nested search task!
	        /// </summary>
	        public IProductSearchTask ProductSearchTask { get; set; }
	    }
	}

Then you would have to modify your predicate provider accordingly.

.. code-block:: C#
	:linenos:

	namespace My.Custom.Provider.Namespace// Assembly name is My.Custom This will be important later on.
	{
	    (...)
	    public class MiniAttributePredicateExpressionProvider : IPredicateProvider<MiniAttributeSearchTask>
	    {
	        (...)
	        public SD.LLBLGen.Pro.ORMSupportClasses.IRelationPredicateBucket CreatePredicateExpression(
	                MiniAttributeSearchTask searchTask)
	        {
	            (...)
	  
	            if (searchTask.ProductSearchTask != null)
	            {
	                // Get the relation predicate bucket for products.
	                var productPredicateBucket = searchTask.ProductSearchTask.GetPredicateBucket();
	                var productAttributeValueRelation = AttributeValueEntity.Relations.ProductAttributeValueEntityUsingAttributeValueId;
	                var addProductAttributeValueRelation = true;
	                foreach (var relation in bucket.Relations)
	                {
	                    // loop all relations in the bucket.
	                    if (relation == productAttributeValueRelation)
	                    {
	                        // relation already resides in the bucket, no need to add it again.
	                        addProductAttributeValueRelation = false;
	                        break;
	                    }
	                }
	                if (addProductAttributeValueRelation)
	                {
	                    bucket.Relations.Add(productAttributeValueRelation);
	                }
	                // grab the relation, set the filter and add it to the bucket.
	                var productEntityRelation = ProductAttributeValueEntity.Relations.ProductEntityUsingProductId;
	                // http://explainextended.com/2009/06/16/in-vs-join-vs-exists/
	                // there is no difference between in, join and exists when joined on indexed columns
	 
	                bucket.PredicateExpression.Add(new FieldCompareSetPredicate(ProductAttributeValueFields.ProductId, null, ProductFields.ProductId, null, SetOperator.In, productPredicateBucket.PredicateExpression, productPredicateBucket.Relations));
	                bucket.Relations.Add(productEntityRelation);
	                // add all required relations according to the product predicate bucket.
	                foreach (IRelation relation in productPredicateBucket.Relations)
	                {
	                    bucket.Relations.Add(relation);
	                }
	                 
	            }
	        }
	    }
	}	

The extension method mentioned in the code looks like this:

.. code-block:: C#
	:linenos:

	using System;
	using System.Reflection;
	 
	using ecManager.Common.Interfaces;
	using ecManager.PredicateExpressionProvider;
	using SD.LLBLGen.Pro.ORMSupportClasses;
	  
	/// <summary>
	/// Helper class that converts an implementation of the <see cref="ISearchTask"/> interface into the <see cref="IRelationPredicateBucket"/>
	/// </summary>
	internal static class PredicateProviderHelpers
	{
	    /// <summary>
	    /// Method to get a predicate bucket for a search task that is known as an interface implementation.
	    /// For example, I have a search task with a <see cref="IProductSearchTask"/> property.
	    /// We have to 'downcast' this search task before the <see cref="PredicateProvider{TSearchTask}"/> can resolve the matching implementation.
	    /// </summary>
	    /// <typeparam name="T">The type of the search task, must be an implementation of <see cref="ISearchTask"/></typeparam>
	    /// <param name="searchTask">The search task to downcast and process.</param>
	    /// <returns>Returns the <see cref="IRelationPredicateBucket"/> returned by the predicate provider.</returns>
	    /// <exception cref="ApplicationException">When there is no predicate provider found for a search task an ApplicationException occurs.</exception>
	    public static IRelationPredicateBucket GetPredicateBucket<T>(this T searchTask) where T : class, ISearchTask
	    {
	        // Get the actual type of the search task.
	        var type = searchTask.GetType();
	         
	        // Grab the method, and make it generic.
	        var methodInfo = typeof(PredicateProviderHelpers).GetMethod("getPredicateBucket",BindingFlags.NonPublic | BindingFlags.Static);
	        var genericMethod = methodInfo.MakeGenericMethod(type);
	         
	        // no try catch, we want the exception to go up the stack.
	        var result = genericMethod.Invoke(null, new[] { searchTask }) as IRelationPredicateBucket;
	         
	        return result;
	    }
	 
	 
	    /// <summary>
	    /// Method that is invoked with a generic argument by <see cref="GetPredicateBucket{T}"/>.
	    /// Instantiates a <see cref="PredicateProvider{TSearchTask}"/> and calls the <see cref="PredicateProvider{TSearchTask}.CreatePredicateExpression"/> method.
	    /// </summary>
	    /// <typeparam name="T">The type of the search task, must be an implementation of <see cref="ISearchTask"/>.
	    /// It should be the the actual type of the search task, not the interface.</typeparam>
	    /// <param name="searchTask">The search task that should be provided.</param>
	    /// <returns>Returns the <see cref="IRelationPredicateBucket"/> returned by the predicate provider.</returns>
	    /// <exception cref="ApplicationException">When there is no predicate provider found for a search task an ApplicationException occurs.</exception>
	    private static IRelationPredicateBucket getPredicateBucket<T>(ISearchTask searchTask) where T:class,ISearchTask
	    {
	        var typedSearchTask = searchTask as T;
	        if (typedSearchTask == null)
	        {
	            return null;
	        }
	        var provider = new PredicateProvider<T>();
	        return provider.CreatePredicateExpression(typedSearchTask);
	        }
	    }
	}

After getting the predicate bucket we instruct LLBLgen to add a subquery using the predicate expression and relations provided by the predicate provider implementation. If you define the correct fields and relations you should get a valid query with a sub query containing filters of the nested search task. 

Optional: Filter your results with a multiple nested search task
----------------------------------------------------------------

In some cases you want to use the results of multiple search tasks to filter the results of your own query. You would have to add a property to your search task to allow this.

.. code-block:: C#
	:linenos:

	namespace My.Custom.Namespace // Assembly name is My.Custom This will be important later on.
	{
	    (...)
	    /// <summary>
	    /// My smaller version of the Attribute Search Task.
	    /// What else do you need?
	    /// </summary>
	    public class MiniProductSearchTask : PagedSearchTask, IAttributeSearchTask
	    {
	        (...)
	        /// <summary>
	        /// The CmsDescription of the Attribute to search for
	        /// </summary>
	        public string CmsDescription { get; set; }
	  
	        /// <summary>
	        /// A nested search task!
	        /// </summary>
	        public ICollection<IAttributeSearchTask> AttributeSearchTasks { get; set; }
	    }
	}

Then you would have to modify your predicate provider accordingly.

.. code-block:: C#
	:linenos:

	namespace My.Custom.Provider.Namespace// Assembly name is My.Custom This will be important later on.
	{
	    (...)
	    public class MiniProductPredicateExpressionProvider : IPredicateProvider<MiniProductSearchTask>
	    {
	        (...)
	        public SD.LLBLGen.Pro.ORMSupportClasses.IRelationPredicateBucket CreatePredicateExpression(
	                MiniProductSearchTask searchTask)
	        {
	            (...)
	  
	            if (searchTask.AttributeValueSearchTasks.Any())
	            {
	                // Get the relation predicate bucket for products.
	                var attributeValuePredicateBuckets = searchTask.AttributeValueSearchTasks.Select(n => n.GetPredicateBucket());
	                 
	                var productAttributeValueRelation = ProductEntity.Relations.ProductAttributeValueEntityUsingProductId;
	                if (predicateBucket.Relations.Cast<object>().All(relation => relation != productAttributeValueRelation))
	                {
	                    predicateBucket.Relations.Add(productAttributeValueRelation);
	                }
	                // grab the relation, set the filter and add it to the bucket.
	                var productEntityRelation = ProductAttributeValueEntity.Relations.AttributeValueEntityUsingAttributeValueId;
	                predicateBucket.Relations.Add(productEntityRelation);
	                // http://explainextended.com/2009/06/16/in-vs-join-vs-exists/
	                // there is no difference between in, join and exists when joined on indexed columns
	                foreach (var attributeValuePredicateBucket in attributeValuePredicateBuckets)
	                {
	                    attributeValuePredicateBucket.Relations.Add(ProductAttributeValueEntity.Relations.AttributeValueEntityUsingAttributeValueId);
	                    predicateBucket.PredicateExpression.AddWithAnd(new FieldCompareSetPredicate(ProductFields.ProductId, null, ProductAttributeValueFields.ProductId, null,
	                        SetOperator.In, attributeValuePredicateBucket.PredicateExpression, attributeValuePredicateBucket.Relations));
	                }
	                 
	            }
	        }
	    }
	}

The extension method mentioned in the code can be found in the section 'Filter your results with a nested search task'. The class is named PredicateProviderHelpers

After getting the predicate bucket we instruct LLBLgen to add a subquery using the predicate expression and relations provided by the predicate provider implementation. If you define the correct fields and relations you should get a valid query with a sub query containing filters of the nested search task. 	

Create unit tests to make sure your predicate provider works
------------------------------------------------------------

We strongly recommend that you write a set of unit tests to test your predicate provider. This is an easy way to determine if the predicates you have made translate into a valid SQL query. An example test could be:

.. code-block:: C#
	:linenos:

	namespace My.Custom.Tests
	{
	    using System;
	 
	 
	    using DNZ.EP.LLBL.DatabaseSpecific;
	    using DNZ.EP.LLBL.EntityClasses;
	    using DNZ.EP.LLBL.HelperClasses;
	     
	    using Microsoft.VisualStudio.TestTools.UnitTesting;
	 
	 
	    using My.Custom.Namespace;
	  
	    [TestClass]
	    public class MiniAttributePredicateProviderTests
	    {
	        [TestMethod]
	        public void AttributeByCmsDescriptionTest()
	        {
	            var searchTask = new MiniAttributeSearchTask();
	            searchTask.CmsDescription = "My search task";
	            var provider = new MiniAttributePredicateExpressionProvider();
	            var relationPredicateBucket = provider.CreatePredicateExpression(searchTask);
	            var collection = new EntityCollection<AttributeEntity>();
	            try
	            {
	                using (var adapter = new DataAccessAdapter())
	                {
	                    adapter.FetchEntityCollection(collection, relationPredicateBucket);
	                }
	            }
	            catch (Exception ex)
	            {
	                // An error occurred while building
	                Assert.Fail("The query was incorrect", ex);
	            }
	            // Assert your entity resides inside the collection.
	        }
	    }
	}

This test grabs the output of the predicate provider, and runs the query on a SQL database. This is just an example how you can write your own predicate provider and test the query it produces.  This might help you speed up development because you can see the result before adding it to an ecManager environment. We recommend you think carefully about how you want to test this mechanism. Configure your predicate provider module.

The final step would be adding your own predicate provider to the module configuration. The syntax for generics in the module configuration is somewhat complicated. Here is the format you can use in the configuration.

.. code-block:: C#
	:linenos:

	<component type="{Fully qualified name to my predicate provider, Name of the assembly the provider resides in}"
	    service="ecManager.PredicateExpressionProvider.IPredicateProvider`1[[{Fully qualified name to my search task, Name of my assembly the search task resides in}]],ecManager.PredicateExpressionProvider" />

Your configuration could look like this:

.. code-block:: C#
	:linenos:

	<component type="My.Custom.Provider.Namespace.MiniAttributePredicateExpressionProvider, My.Custom"
	    service="ecManager.PredicateExpressionProvider.IPredicateProvider`1[[My.Custom.Namespace.MiniAttributeSearchTask , My.Custom]],ecManager.PredicateExpressionProvider" />

Entity state inside the default Predicate Provider modules
==========================================================

When building predicate expression you might need filter the result set using the EntityState data. In the table below we show the entity states that are used in the default modules.

+-----------------------------------------------------------+---------------+----------------------------------------------------------------------------------------------------------+
| Module                                                    | Entity states | Comments                                                                                                 |
+===========================================================+===============+==========================================================================================================+
| DefaultAttributePredicateExpressionProvider               | x             | | The AttributeSearchTask might contain AttributeValueSearchTasks. These might require entity state      |
+-----------------------------------------------------------+---------------+----------------------------------------------------------------------------------------------------------+
| DefaultAttributeValuePredicateExpressionProvider          | x             | | The AttributeValueSearchTask might contain ProductSearchTasks. These might require entity states       |
+-----------------------------------------------------------+---------------+----------------------------------------------------------------------------------------------------------+
| DefaultOptimizedAttributePredicateExpressionProvider      | product       | | The entity state for products is only used when the entity state context is provided and the meta data |
|                                                           |               | | 'ProductCountPerAttributeValue' is requested.                                                          |
|                                                           |               | | The AttributeSearchTaskmight contain AttributeValueSearchTasks. These might require entity states.     |
+-----------------------------------------------------------+---------------+----------------------------------------------------------------------------------------------------------+
| DefaultOptimizedAttributeValuePredicateExpressionProvider | x             | | The AttributeValueSearchTask might contain ProductSearchTasks. These might require entity states.      |
+-----------------------------------------------------------+---------------+----------------------------------------------------------------------------------------------------------+
| DefaultOptimizedProductPredicateExpressionProvider        | product       | | The OptimizedProductPredicateProvider is designed to return online products.                           |
+-----------------------------------------------------------+---------------+----------------------------------------------------------------------------------------------------------+
| DefaultProductPredicateExpressionProvider                 | x             | |                                                                                                        |
+-----------------------------------------------------------+---------------+----------------------------------------------------------------------------------------------------------+
| DefaultCombinationPredicateExpressionProvider             | combination   | |                                                                                                        |
+-----------------------------------------------------------+---------------+----------------------------------------------------------------------------------------------------------+
| DefaultNewsItemPredicateExpressionProvider                | NewsItem      | |                                                                                                        |
+-----------------------------------------------------------+---------------+----------------------------------------------------------------------------------------------------------+

