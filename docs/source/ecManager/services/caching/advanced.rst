########
Advanced
########

Creating a custom caching module
================================
ecManager contains two implementations of caches by default, an in-memory cache and a Redis cache 
which can be used for distributed caching. However, you might feel the need to use another cache.
To create your own caching implementation you need to implement two interfaces: ``ICacheProvider`` 
and ``ICacheKeyProvider``.

ICacheProvider
--------------
The implementation of the ``ICacheProvider`` contains the hard link to the cache backend and is 
responsible for read, write and purge actions.

**Add**

Add a new entry to the cache with a unqiue string as key and an object as value. It's possible a key 
is added multiple times, the value should be overwritten.

**Get**

Retrieve a cached object or null if no entry is present.

**Purge**

Remove one entry from the cache based on the cache key. Removing a non-existing entry should NOT 
result in an error.

**Clear**

Remove all entries from the cache.

**SearchCacheEntries**

Search cache entries based on a (part of) cache key.

ICacheKeyProvider
-----------------
The implementation of the ``ICacheKeyProvider`` holds the administration of the dependencies between 
multiple cached objects. It is based on one key (cacheEntryKey) and a collection of dependent objects.

**Add**

Add the cacheEntryKey to a collection with references to all dependent keys.

**Get**

Retrieve the dependencies for a cacheItemKey. This method searches in all collections which depend 
on this cacheItemKey.

**Purge**

Purges one single entry of dependencies.