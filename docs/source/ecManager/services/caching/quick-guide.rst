###########
Quick guide
###########

Introduction
============
This page will describe the caching behavior in ecManager and get you started when you want to use
ecManager caching for your own application.

Configuration
=============
Because you can configure multiple caches, you need to add configuration which specifies which cache
should be used by default. The value provided here is the name of the cache which is configured in
``Modules.config``. You can also specify an alternative cache for specific cache identifiers, as 
shown below:

.. code-block:: XML

	<configSections>
	  <section name="ecManager.Caching" type="ecManager.Caching.Configuration.CacheConfigurationSection, ecManager.Caching"/>
	<configSections>

	<ecManager.Caching enabled="true" purgeOnly="false" defaultCacheProvider="Redis" defaultCacheKeyProvider="Redis">
	  <cacheIdentifiers>
	    <cacheIdentifier cacheIdentifier="a" cacheProvider="MemoryCache" cacheKeyProvider="MemoryCache" />
	    <cacheIdentifier cacheIdentifier="b" cacheProvider="MemoryCache" cacheKeyProvider="MemoryCache" />
	  </cacheIdentifiers>
	</ecManager.Caching>

The cache identifier is the first sequence of a cache key. So in the previous example, cache items
that starts with "a" and "b" will be handled by the configured providers.

It's possible to disable the entire can by setting the ``enabled`` flag on false. This can be useful
for applications which always needs up-to-date information. When using a distributed cache like
redis we recommend to enable the ``purgeOnly`` in the CMS to improve the cache invalidation. Updates
on cache items will be automatically purged.

.. note ::

	If you want to manage your cache from the CMS you must set the ``enabled`` flag to true. Make
	sure to set the ``purgeOnly`` flag to true as well. If not, the CMS will store its data to the 
	cache.

CachingService
==============
The ``CachingService`` is the service which can be used for caching. This service is also used by the 
framework itself. Many other services use the ``CachingService`` to perform caching of their output.

The service can also be used directly to cache data. So if you would like to cache some data either
locally or distributed, you can use the service and then configure it however you like as described
in `Configuration`_.

.. note:: Make sure your cache keys are unique, otherwise you might experience cache key collision. 

Adding and Retrieval
====================

To fill and read the cache you can use the ``Add`` and ``Get`` methods. The method we recommend to 
use is explained in the example below:

.. code-block:: C#
	:linenos:

	public string GetData(int someInput)
	{
	  var data = CachingService.Get("UniqueCacheKey", () => this.GetNonCacheData(someInput));
	  return data;
	}
	
	public string GetNonCacheData(int someInput)
	{
	  return "Data";
	}

In this example is shown how to retrieve data from the cache by passing an unique cache key and an 
expression which will retrieve the actual data. The logic in this method checks if the data already 
exists in the cache.

If you want to control the flow of the cache and don't want to use an expression, you can use a 
combination of the ``Add`` and ``Get`` methods to get the same result. See below for an example:

.. code-block:: C#
	:linenos:

	public string GetData(int someInput)
	{
	  var cacheKey = "UniqueCacheKey";
	  var data = CachingService.Get(cacheKey);
	  if (data == null)
	  {
	    data = this.GetNonCacheData(someInput);
	    CachingService.Add(cacheKey, data);
	  }
	  return data as string;
	}
	
	public string GetNonCacheData(int someInput)
	{
	  return "Data";
	}

To save the data in the cache in a way that dependent entries are purged it's important to add the 
entries to the ``CacheKeyProvider``. an example how this can be implemented is shown below:

.. code-block:: C#
	:linenos:

	public Data GetData(int someInput)
	{
	  var data = GetNonCacheData(someInput);
	  if (data == null)
	  {
	    data = this.GetNonCacheData(someInput);
	    CachingService.Add(data, new ServiceRequestContext(), new EntityStateContext(), new Dependencies(), new FlagStore<short>());
	  }
	  return data as Data;
	}
	
	public ICacheEntryIdentifiable GetNonCacheData(int someInput)
	{
	  return new Data();
	}
	
	public class Data : ICacheEntryIdentifiable
	{
	  private string _data = "data";
	  public string GetCacheEntryKey(ServiceRequestContext serviceRequestContext, EntityStateContext stateContext, IEnumerable<short> flags)
	  {
	    return "EntryKey";
	  }
	}
	
	public class Dependencies : ICacheItemCollectionIdentifiable
	{
	  public HashSet<string> GetCacheItemKeys()
	  {
	    return new HashSet<string>{"key1","key2"};
	  }
	}

In this example there are two main object: the Data and the Dependencies. The data object has two 
dependent objects which can be in the cache. The keys are defined in the Dependencies object. When 
purging one of the dependencies the cached data object will be become invalid and will be purged too. 
This can be useful when handling search operations which return multiple objects. The full result 
will be cached but if one of these objects becomes invalid the whole search result becomes invalid.

Purge and Clear
===============

To clear the entire cache you can use the ``Clear`` method as shown below:

.. code-block:: C#
	:linenos:

	public void ClearCache()
	{
	  //Some logic
	  CachingService.Clear();
	}

Purging is the removing of one or more items from the cache. See below for some examples:

.. code-block:: C#
	:linenos:

	public void Purge()
	{
	  //Purging by cache key.
	  CachingService.Purge("CacheKey");
	  //Purging by cached item.
	  var data = new Data();
	  CachingService.Purge(data);
	  //Purging by a list of cached items.
	  var data1 = new Data();
	  var data2 = new Data();
	
	  CachingService.Purge(new List<ICacheItemIdentifiable>{data1,data2});
	  //Purging by a list of cache keys
	  CachingService.Purge(new List<string> { "Key1", "Key2" });
	}
	
	public class Data : ICacheItemIdentifiable
	{
	  public string GetCacheItemKey()
	  {
	      return "CacheKey";
	  }
	}

Search
======

For management purposes you may want to search for cache entries based on a (partial) cache key. 
This allows you the get all caches entries for a specific entity type, search task or domain. See 
below for an example:

.. code-block:: C#
	:linenos:

	var result = CachingService.SearchCacheEntries(new CacheEntrySearchTask { CacheKey = "partOfCacheKey" });       