########
Overview
########

Caching is important for any framework because of performance reasons. ecManager has its own caching
mechanism which is abstracted away when using the ecManager services. Caching is performed 
automatically when retrieving entities (if caching is enabled).

Caching in the ecManager framework consists of two parts. The first is the ``CacheProvider``. The 
provider handles storing and retrieving values in the cache. 

The second is the ``CacheKeyProvider``, this keeps an administration of all the cache keys and their 
dependencies in the cache so we can purge an entity and all its dependencies. This allows you to 
increase cache periods because when changes are made, the cache is automatically purged for that 
entity and its dependencies.

.. warning::

  The ``MemoryCache`` module does not support purging the cache as described above.

ecManager allows you to use multiple caches, so you can decide which entities should go to which 
cache. This can be very useful if you want to use an in-memory cache and distributed cache at the 
same time. For example, if you cache data that will rarely ever change you might cache it 
in-memory even if you are running on multiple servers.