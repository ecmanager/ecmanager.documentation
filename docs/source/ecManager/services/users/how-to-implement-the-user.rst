#########################
How to implement the User
#########################

Login
=====

A user can identified on it's unique login through the User.Login property. For authentication of a user the IsLoginValid() method can be used. This method return true if the combination of login and password is correct. 

.. warning:: The password is stored encrypted but this method expect the password unencrypted

UserService
===========

The UserService is the entry point for managing User entities. It supports reading, writing and deletion of users as well as additional features like login check. The following schema displays the methods on the service which will be discusses later on. 

+-------------+-----------------------------------------------------------------------------------+
| Method      | Description                                                                       |
+=============+===================================================================================+
| DeleteUser  | | Method to delete a user using a CustomerId or a UserId                          |
+-------------+-----------------------------------------------------------------------------------+
| DeleteUsers | | Method to delete users using a collection with UserIds                          |
+-------------+-----------------------------------------------------------------------------------+
| IsLoginUsed | | Check whether the log in is already in use                                      |
+-------------+-----------------------------------------------------------------------------------+
| IsLoginValid| | Method to return the user when the combination of a login and password is valid |
+-------------+-----------------------------------------------------------------------------------+
| GetUser     | | Method to get a single User by UserId or CustomerId                             |
+-------------+-----------------------------------------------------------------------------------+
| GetUsers    | | Method to get a list of User by a list of CustomerId or a list of UserIds       |
+-------------+-----------------------------------------------------------------------------------+
| SaveUser    | | Save a user                                                                     |
|             | | This Method checks if there is a new password                                   |
|             | | In this cade, the password will be encrypted                                    |
|             | | Otherwise we will maintain the current password                                 |
+-------------+-----------------------------------------------------------------------------------+
| SaveUsers   | | Save a set of Users                                                             |
|             | | This Method checks if there is a new password                                   |
|             | | In this case, the password will be encrypted                                    |
|             | | Otherwise we will maintain the current password                                 |
+-------------+-----------------------------------------------------------------------------------+
| SearchUsers | | Search a set of Users                                                           |
+-------------+-----------------------------------------------------------------------------------+  