############################
The voucher entity structure
############################

The Voucher entity is a top level entity within the Commerce domain. It has properties that identify the voucher, the voucher code and a date-range in which the voucher is valid. It also contains information of the type of voucher and the affected discount. In addition to the voucher, there is the GiftVoucher. A gift voucher is a voucher that is given as a present(e.g. as a birthday present) and thus represents actual money. The GiftVoucher entity relates to the Voucher and contains additional information about the sender and recipient of the voucher.

The schema below shows the various entities and their relations.

.. image:: /_static/img/services/vouchers/vouchers-2.png
	:scale: 90%
	:align: center

VoucherTypes
============

A webshop can have several types of vouchers. The type is indicated by the Voucher.Type property. The possible values are documented in the VoucherTypes enumeration. The type of voucher determines how the voucher is treated by the service. The table below describes the types and their behavior:

+----------+-------+---------------------------------------------------------------------------------------+
|          | Value | Description                                                                           |
+----------+-------+---------------------------------------------------------------------------------------+
| Action   | 0     | An Action voucher is a normal voucher which can be redeemed multiple times            |
+----------+-------+---------------------------------------------------------------------------------------+
| Campaign | 1     | A campaign voucher is a normal voucher which can be redeemed once                     |
+----------+-------+---------------------------------------------------------------------------------------+
| Gift     | 2     | A gift voucher is a voucher with additional gift information. It can be redeemed once |
+----------+-------+---------------------------------------------------------------------------------------+

VoucherDiscountTypes
====================

A voucher can have several types of discounts. The type is indicated by the Voucher.DiscountType property. The possible values are documented in the VoucherDiscountTypes enumeration. The type of discount determines how the actual discount is calculated. The table below describes the types and thei behavior.

+--------------+-------+---------------------------------------------------------------+
|              | Value | Description                                                   |
+==============+=======+===============================================================+
| None         | 0     | No discount                                                   |
+--------------+-------+---------------------------------------------------------------+
| FreeShipping | 1     | A discount to give free shipping                              |
+--------------+-------+---------------------------------------------------------------+
| Percentage   | 2     | This type can be used to give a percentage discount (e.g. 5%) |
+--------------+-------+---------------------------------------------------------------+
| Amount       | 3     | A fixed amount discount (e.g. € 10,00)                        |
+--------------+-------+---------------------------------------------------------------+
| FreeProduct  | 4     | Gives you a free product                                      |
+--------------+-------+---------------------------------------------------------------+

VoucherService
==============

The VoucherService is the entry point for managing Voucher and GiftVoucher entities. It supports reading, writing and deletion of vouchers as well as additional features like generating new codes and (un)redeeming existing vouchers.  The following schema displays the methods contained by the service which will be discussed later on.

.. image:: /_static/img/services/vouchers/vouchers-3.png
	:scale: 90%
	:align: center

+-------------------------+----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| Method                  | Description                                                                                                                                                                      |
+=========================+==================================================================================================================================================================================+
| GetGiftVoucher          | Gets a single GiftVoucher using the supplied VoucherId                                                                                                                           |
+-------------------------+----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| GetVoucher(s)           | Gets one or more Voucher entities using Voucher ids, VoucherCodes or by a PromotionConditionId. When the onlyValid flag is set to true; additional requirements are              |
|                         | set to only return valid vouchers (see "IsValidVoucher")                                                                                                                         |
+-------------------------+----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| SearchVouchers          | Searches the Voucher entities using the criteria in the supplied VoucherSearchTask                                                                                               |
+-------------------------+----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| SaveGiftVoucher(s)      | Saves GiftVouchers                                                                                                                                                               |
+-------------------------+----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| SaveVoucher(s)          | Saves Voucher entities using a transaction. Each voucher is checked to have a unique voucher code                                                                                |
+-------------------------+----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| DeleteGiftVoucher(s)    | Deletes one or more GiftVoucher entities using the supplied entities. Deletion is done within a transaction                                                                      |
+-------------------------+----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| DeleteVoucher(s)        | Delete one or more Voucher entities using the supplied entities or Id's. Deletion is done within a transaction                                                                   |
+-------------------------+----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| IsValidPromotionVoucher | Checks if the supplied VoucherCode is valid for usage in a Promotion. If the voucher is valid and related to a promotion;                                                        |
|                         | the PromotionConditionId argument will be set and true is returned                                                                                                               |
+-------------------------+----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| IsValidVoucher          | Checks if the supplied VoucherCode is valid (can be redeemed). True is returend when the voucher is valid and meets the following criteria                                       |
|                         | Valid voucher criteria                                                                                                                                                           |
|                         | A voucher is valid when the following criteria are met:                                                                                                                          |
|                         | The label of the voucher matches the label in the ServiceRequestContext                                                                                                          |
|                         | The current datetime is within the ActiveFrom/To range                                                                                                                           |
|                         | The voucher is not redeemed OR the voucher is of type Action(can be redeemed multiple times)                                                                                     |
+-------------------------+----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| IsVoucherCodeTaken      | Method to check if a VoucherCode exists in the database. Voucher must be unique within their label and the ServiceRequestContext label is used for checking                      |
|                         | Supplying the optional VoucherId will exclude that voucher from the check                                                                                                        |
+-------------------------+----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| GenerateVoucherCode     | Generates a new voucher code. The code is checked to be unique within the Label defined in the ServiceRequestContext                                                             |
+-------------------------+----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| RedeemVoucherCode       | Redeems the Voucher with the supplied id. If the voucher can be redeemed, the RedeemedOn and IsRedeemed properties are updated and true is returned. Otherwise false is returned |
+-------------------------+----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| UnRedeemVoucher         | Unredeeming a voucher will set the IsRedeemed to false and RedeemendOn will be cleared                                                                                           |
+-------------------------+----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+