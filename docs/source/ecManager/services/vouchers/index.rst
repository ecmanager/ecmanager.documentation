########
Vouchers
########

.. rubric:: Introduction

Almost every web shop offers the possibility of redeeming a voucher code which will give you a discount on the order. The ecManager framework also offers voucher functionality.This manual is written for developers and explains how to implement vouchers in a web shop that is built on ecManager.+9*/The manual first describes the impact on the order flow and how you can make a basic integration in a web shop. Then the manual gives a more in-depth view of the entity structure and the service methods you can use for more advanced scenarios.

.. image:: /_static/img/services/vouchers/vouchers.png
	:scale: 90%
	:align: center

.. toctree::
   :maxdepth: 1

   vouchers-in-the-webshop
   the-voucher-entity-structure



		
	

		

