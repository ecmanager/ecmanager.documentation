#######################
Vouchers in the webshop
#######################

This section describes how vouchers are integrated into a web shop. The first step is taking a look at the impact of vouchers in the order flow. This gives you an understanding of the modifications you will have to make in your webshop to properly support vouchers. This section is followed up by a look at the basic integration of vouchers in the basket.

The process of vouchers in a webshop
====================================

When implementing the concept of vouchers in the webshop, timing and completing the process is probably the hardest part. For example; when the payment failed or an order is canceled, it is arguable that the voucher should be unredeemed. Therefor you need to figure out a process which works for the implementation and covers several scenarios of redeeming and unredeeming a voucher. The following BPMN diagram is a sample implementation where the green activities are ecManager calls (including voucher dependencies). The diagram is a sample flow to display the cooperation between the webshop and the ecManager framework but is not complete as more scenarios can be identified.

.. image:: /_static/img/services/vouchers/vouchers-1.png
	:scale: 90%
	:align: center

As you can see, there are several points where vouchers are possibly unredeemed. You could, in addition, think of having a backgound-process which collects vouchers which are redeemed but are related to an order which is not further processed (possibly the customer just closed it's browser so no cancellation is triggered).

Integrating vouchers in your basket
===================================

This section describes a basic implementation of vouchers in your web shop. First off is the part where vouchers are added to the basket and giving the user the correct feedback if something goes wrong. Then there is a description on how to undo redemption of a voucher when the transaction is cancelled.

.. rubric:: Adding vouchers to a basket

The first step in adding vouchers is allowing the user to insert a voucher code. This usually requires an input field and a button to submit the voucher code. This may differ per implementation so this guide continues assuming you have constructed this yourself. The next step is processing the voucher.

.. warning:: Vouchers are calculated/applied in the order they are added

The following example method shows you how the voucher should be provided to the service.

.. code-block:: C#
	:linenos:

	/// <summary>
	/// Method that adds a voucher to the basket.
	/// </summary>
	/// <param name="basketService">The basket service used for updating the basket.</param>
	/// <param name="basket">The basket which should get the voucher</param>
	/// <param name="voucherCode">The voucher code inserted by the user.</param>
	/// <returns>Returns <c>true</c> if the voucher is added to the basket. Returns <c>false</c> if an error occurred.</returns>
	protected bool AddVoucherToBasket(IBasketService basketService, IBasket basket, string voucherCode)
	{
	    // Create a change request for the voucher code.
	    var voucherAddChangeRequest = new UpdateVoucherChangeRequest
	    {
	        VoucherCode = voucherCode
	    };
	    // Let the basket service process the change request
	    basketService.UpdateBasket(basket, new Collection<AChange> { voucherAddChangeRequest });
	    if (basket.Errors.Any())
	    {
	        // Something went wrong, handle the basket errors.
	        return false;
	    }
	    else
	    {
	        // Voucher applied successfully
	        return true;
	    }
	}

If you copy the method above and provide the correct arguments, vouchers will be added to your basket. If you provide an invalid voucher, the voucher should not be added to the basket as a valid voucher. Keep in mind that this method does not provide feedback for the customer. This is covered in the next section.

.. warning:: A voucher should be added once to the basket. You can add it multiple times, but the discount is only applied once.

When adding a voucher an error may occur. 

+-----------+----------------------------------------------------------------------------------------------------------------------------------------------+-------------------------+
| Error key | Description                                                                                                                                  | Voucher added to basket |
+===========+==============================================================================================================================================+=========================+
| DBVL_1    | Voucher not found                                                                                                                            | Yes                     |
+-----------+----------------------------------------------------------------------------------------------------------------------------------------------+-------------------------+
| DBVL_2    | Voucher discount type doesn't exist. The value in the Voucher.DiscountType does not map to a value ecManager is familiar with                | No                      |
+-----------+----------------------------------------------------------------------------------------------------------------------------------------------+-------------------------+
| DBVL_3    | A voucher that provides a defined discount (e.g. 10 euro) in the basket does not have a discount defined. The field Voucher.Discount is null | No                      |
+-----------+----------------------------------------------------------------------------------------------------------------------------------------------+-------------------------+
| DBVL_4    | A voucher that provides a free product does not have a product defined. The field Voucher.FreeProductId is null                              | No                      |
+-----------+----------------------------------------------------------------------------------------------------------------------------------------------+-------------------------+
| DBVL_5    | A voucher that provides a percentage discount in the basket does not have a percentage defined. The field Voucher.DiscountPercentage is null | No                      |
+-----------+----------------------------------------------------------------------------------------------------------------------------------------------+-------------------------+

.. warning:: The vouchers are calculated as soon as they are added. So the basket metrics are not recalculated per voucher. This means that two 5% vouchers will get the same discount instead of the second voucher getting a slightly lower discount.

.. rubric:: Handling voucher errors

When a customer adds a voucher to the basket, this voucher may or may not be valid. To correctly communicate the status of a voucher, the voucher is always added to the basket unless the voucher does not have valid data (see the errors in the table in the section above). The voucher remain present until you remove them yourself. You can choose to remove the invalid vouchers directly after showing the user the voucher isn't valid. When evaluating a voucher, you can use the VoucherReference.IsValid field to determine if a voucher is valid or not. The VoucherReference.Errors field is meant to provide you with more detailed feedback about whats wrong with the voucher. In the following code example you can see how you can translate the error messages to a message you can present to the customer. 

.. code-block:: C#
	:linenos:

	/// <summary>
	/// Method convert a <see cref="VoucherReference.VoucherErrorFlags"/> to a human readable text.
	/// </summary>
	/// <param name="flag">The flags to convert.</param>
	/// <returns>Returns a string with the translated message.</returns>
	protected string GetVoucherErrorTranslation(VoucherReference.VoucherErrorFlags flag)
	{
	    if (flag.HasFlag(VoucherReference.VoucherErrorFlags.VoucherNoLongerActive))
	    {
	        return Checkout.VoucherError_VoucherExpired;
	    }
	    if (flag.HasFlag(VoucherReference.VoucherErrorFlags.RedeemedVoucher))
	    {
	        return Checkout.VoucherError_VoucherAlreadyRedeemed;
	    }
	    if (flag.HasFlag(VoucherReference.VoucherErrorFlags.VoucherNotYetActive) ||
	        flag.HasFlag(VoucherReference.VoucherErrorFlags.WrongLabel))
	    {
	        return Checkout.VoucherError_VoucherInvalid;
	    }
	    return null;
	}

This information should be used to notify the user of the voucher status in a way that fits your implementation. 

.. rubric:: Removing a voucher from the basket

Removing a voucher from your basket is very similar to the add operation. The following code example shows how you can remove a voucher from the basket.

.. code-block:: C#
	:linenos:

	/// <summary>
	/// Method that removes a voucher fom the basket.
	/// </summary>
	/// <param name="basketService">The basket service used for updating the basket.</param>
	/// <param name="basket">The basket which should contain the voucher</param>
	/// <param name="voucherCode">The voucher code inserted by the user.</param>
	/// <returns>Returns <c>true</c> if the voucher is removed ro the basket. Returns <c>false</c> if an error occurred.</returns>
	protected bool RemoveVoucherFromBasket(IBasketService basketService, IBasket basket, string voucherCode)
	{
	    // Create a change request for the voucher code.
	    var voucherRemoveChangeRequest = new RemoveVoucherChangeRequest
	    {
	        VoucherCode = voucherCode
	    };
	    // Let the basket service process the change request
	    basketService.UpdateBasket(basket, new Collection<AChange> { voucherRemoveChangeRequest });
	    if (basket.Errors.Any())
	    {
	        // Something went wrong, handle the basket errors.
	        return false;
	    }
	    else
	    {
	        // Voucher applied successfully
	        return true;
	    }
	}

.. rubric:: Undo redeeming a voucher

At some point in the process you might need to undo the redemption of a voucher. The following code block shows you how you can unredeem a voucher by either the voucher code or the voucher id.

.. code-block:: C#
	:linenos:

	/// <summary>
	/// Method to undo redeeming a voucher using the <paramref name="voucherId"/>.
	/// </summary>
	/// <param name="service">The voucher service used to undo redeeming the voucher</param>
	/// <param name="voucherId">The ID of the voucher</param>
	protected void UnRedeemVoucher(IVoucherService service, VoucherId voucherId)
	{
	    service.UnRedeemVoucher(voucherId);
	}
	 
	 
	/// <summary>
	/// Method to undo redeeming a voucher using the <paramref name="voucherCode"/>.
	/// </summary>
	/// <param name="service">The voucher service used to undo redeeming the voucher</param>
	/// <param name="voucherCode">The voucher code</param>
	protected void UnRedeemVoucher(IVoucherService service, VoucherCode voucherCode)
	{
	    var voucher = service.GetVoucher(voucherCode);
	    service.UnRedeemVoucher(voucher.Id);
	}