####
FAQs
####

The FAQs are entities designed for displaying frequently asked questions on a website questions can be categorized into FaqGroups.
A FaqGroup has a Title which describes the group of FaqEntitites.
A FaqEntitiy has a Question and an Answer. 
Both the Faq and the FaqGroup contain a Rank property by which it's possible to sort the entitiy. In the search task the preferred order can be given so the result is sorted at low level.

Faq items can be access through the FaqService and FaqGroups.

.. note:: An FaqGroup can be referenced from a NavigationItem using the NavigationItem.NavigationTypeId which means a FaqGroup can be part of the navigation.

