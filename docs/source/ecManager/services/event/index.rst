#####
Event
#####

.. rubric:: Introduction

The EventService is a simple service for managing Events.

.. toctree::
   :maxdepth: 1

   the-event-entity-structure

	