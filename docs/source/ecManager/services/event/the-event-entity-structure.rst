##########################
The Event entity structure
##########################

The EventService and it's entities is designed for displaying events of physical shops on a page. The service manages event entities. The diagram below shows the Event entities.

.. image:: /_static/img/services/event/event.png
	:scale: 90%
	:align: center