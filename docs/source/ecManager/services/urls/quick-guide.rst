###########
Quick guide
###########

Introduction
============
Handling URLs is an important component of the ecManager framework. There are different kind of 
interactions with URLs, this page will provide you with the basic information you need to handle 
URLs.

URL service
===========
The ``UrlService`` is responsible for all the URL-related operations. These operations include:

* Parsing URLs
* Retrieving (generating) URLs for ecManager entities
* Resolving absolute URLs from virtual URIs 

Parsing URLs
============
In the context of a website, you will retrieve incoming URLs. The ``UrlService`` can parse those 
URLs to extract information like the label, the language code, and more. That information can 
determine which page will be shown to a user. As an example, you have the following incoming URL:

.. code-block:: batch

	https://mywebsite.com/en/professional-blender/product/88/

Using a matching pattern, you can extract the following information:

* The label (mywebsite.com)
* The language is English (en)
* The entity type is ``Product`` (product)
* The entity id is 88

Retrieving URLs
===============
In many scenarios you may want to retrieve a URL for an ecManager entity like a ``Product`` or a 
``Location``. The ``UrlService`` has ``GetUrl`` methods which can be used to retrieve URLs. It will 
generate the URL based on patterns which can be configured. 

.. note:: You can find the URL configuration documentation `here <../../introduction/configuring-ecManager/configuring-urls.html>`__.

Resolving URLs
==============
As described on the `concepts in short <../../introduction/concepts-in-short.html#file-io>`__ page,
ecManager uses virtual paths to handle file IO. While this flexibility gives us several advantages, 
we also need to have a way to resolve these URI's to an absolute path so we can actually use those 
files. 

The ``UrlService`` offers this functionality through the ``ResolveUrl`` method. This will resolve a 
virtual path to an absolute path. This is done by using a UrlResolver module.

A UrlResolver module contains the business rules which determine an absolute URL for a virtual path. 
However when calling the module no check is performed to see if a file really exists. Therefore if 
you need to make sure that a file exists, you should first call the ``Exist`` method on the 
``StorageService`` to make sure the file exists.

For more information on how the default UrlResolver module works, check the 
`UrlResolver <../../modules/urlresolver/index.html>`__ documentation.