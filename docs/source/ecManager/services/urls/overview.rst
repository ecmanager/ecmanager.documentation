########
Overview
########

ecManager offers extensive options for configuring and resolving URLs. The URL service can be used 
to interact with URLs within the ecManager framework. 

The `quick guide page <quick-guide.html>`__ describes the basics you need to know when dealing with 
any URL related configuration or operations.