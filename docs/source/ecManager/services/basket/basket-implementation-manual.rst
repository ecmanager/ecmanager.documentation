############################
Basket implementation manual
############################

Introduction
============

The basket is a complex structure of entities, changes, services and modules. This manual is written for developers and explains how to implement basket in a webshop that is build on ecManager. It starts by explaining the entity structure and the architecture. It ends with describing the service and how to modify the basket contents using changes.

The basket entity structure
===========================

The Basket type is a top level entity and can be handled using a service. It has properties that identify the basket, user, costs, discounts and totals. The basket also has collections for Items, Costs, Discounts and References.

.. image:: /_static/img/services/basket/basket-1.png
	:scale: 90%
	:align: center

Items
-----

The Basket contains Items. Each item has the following properties:

+---------------+---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| Property      | Purpose                                                                                                                                                                                                                                           |
+===============+===================================================================================================================================================================================================================================================+
| ListPrice     | The list price is an optional price which is used for storing the recommended retail price and can be used for displaying purposes. The list price can be € 99,95 where the actual price is € 95,00.                                              |
+---------------+---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| SellingPrice  | The selling price(required) is the actual price when buying the item. It may differ from the list price.                                                                                                                                          |
+---------------+---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| Costs         | An item can have additional costs like payment or shipping costs. Costs are saved in a collection on the item.                                                                                                                                    |
+---------------+---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| Discounts     | Besides the costs; there can be discounts. Discounts are saved in their own collection on the item. The promotion and voucher discounts related to an item will also be saved here.                                                               |
+---------------+---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| SubTotal      | The SubTotal is the price of an Item in the basket without costs or discounts (actual behavior may vary of you create custom module). For example; an item with quantity 3 with a selling price of € 4,95 results in a SubTotal of € 14,85.       |
+---------------+---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| Total         | The Total is for saving the actual price for an item in the basket. For the default behavior the total is the subtotal + costs and discounts. For example; An item with a subtotal of € 14,85 and a € 5,00 discount results in a Total of € 9,85. |
+---------------+---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| Configuration | You can use this field to store custom data in the basket item.                                                                                                                                                                                   |
+---------------+---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+


The Items property on the basket is a collection of ABasketItem entities which is an abstract class. For product it has a concrete implementation called BasketProductItem. For collection types (i.e. Combinations) an additional abstract is created called ABasketCollectionItem which has a concrete implementation called BasketCombinationItem. An ABasketCollectionItem extends the ABasketItem by adding some totals and a collection of ABasketItem. More concrete: A combination consists of a set of products which have a package price.

.. image:: /_static/img/services/basket/basket-3.png
	:scale: 90%
	:align: center

Costs & Discounts
-----------------

The Basket and every Item in the basket have separate collections for Costs and Discounts. Both Cost and Discount implement the AItemAmountRule abstract which contains pricing information and references. The properties of a AItemAmountRule are described in the table below.

+--------------+----------------------------------------------------------------+
| Property     | Purpose                                                        |
+==============+================================================================+
| ExclVat      | The amount excluding VAT                                       |
+--------------+----------------------------------------------------------------+
| InclVat      | The amount including VAT                                       |
+--------------+----------------------------------------------------------------+
| Reference    | A reference to the entity that 'caused' the cost or discount.  |
+--------------+----------------------------------------------------------------+
| TaxReference | The reference to a tax that was applied                        |
+--------------+----------------------------------------------------------------+

.. image:: /_static/img/services/basket/basket-4.png
	:scale: 90%
	:align: center

References
----------

The References collection on the Basket entity is a set of metadata that is referenced from anywhere in the basket. It allows you to maintain metadata in the basket which does not belong to the primary basket data. For example; A Discount can be added to the Basket because of a Voucher submitted by the customer. The discount is directly relevant for the basket and it calculations but the voucher itself is maintained as metadata as it has no additional value besides referencing.

.. image:: /_static/img/services/basket/basket-5.png
	:scale: 90%
	:align: center

Some references have a property Type this property can help you to show the reference text on the right place in the basket.

Overview
--------

In a complete overview the basket looks like this:

.. image:: /_static/img/services/basket/basket-6.png
	:scale: 90%
	:align: center

.. rubric:: Calculation sheet

In addition to the class diagram we've created a sample calculation sheet. In this sheet you'll see the Quantity and SellingPrice on Items (Products and Combinations) but also see the SubTotal, DiscountTotal, CostTotal and Total on Items and Totals. The colors in the diagram link related fields. For example; the discounts on product get summed-up to the Item.DiscountTotal property and the SubTotal in the Totals section is a sum of all Items.Total values.

The basket logic architecture
-----------------------------
In the downloadable Excel file you see a sample calculation which can help you to identify the different costs, discounts and prices in the basket.
Download the file here: `basket.xlsx </_static/download/services/basket/basket.xlsx>`_.

The basket logic architecture
=============================

The working with the Basket and it's logic; the BasketService is het Service to communicate through. This service handles all calls on retrieving and managing Basket entities but delegates significant tasks to a modular part of the logic. This paragraph will give you an idea on the architecture and possibilities.

As you can see in the diagram below; the BasketService passes it's calls to an internal BasketLogic. This logic will delegate two important tasks to two modules.

.. image:: /_static/img/services/basket/basket-7.png
	:scale: 90%
	:align: center

The two modular parts are:

- DefaultBasketLogic; this module is responsible for updating the baskets contents. For example how a product is added to the basket. This module will not calculate.
- DefaultCalculateBasket; this module is responsible for calculating the basket. This module will not modify the baskets contents except for calculation fields.

Both modules can be replaced independently.	

How to implement the basket
===========================

Here will be explained what is the minimal implementation of the basket

BasketService
-------------

The BasketService is the ecManager service that is used to interact with the basket. The following schema shows the available methods on the BasketService which will be discusses in the following table.

+-----------------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| Method          | Description                                                                                                                                                                    |
+=================+================================================================================================================================================================================+
| Constructor     | | The constructor requires a ServiceRequestContext which is very important for the basket. It requires Languages to display the correct texts, it requires                     |
|                 | | Pricelists to retrieve the correct prices and requires Shipping Countries to apply the correct taxes and duties. Make sure that the values in the context are correct.       |
+-----------------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| CreateBasket    | | The CreateBasket is used to create a new basket for a user session. Creating a basket requires a unique key(User reference) which allows connecting the basket to the        |
|                 | | client that currently creates the basket.                                                                                                                                    |
|                 | | The ASP.NET sessionID can be used as userreference when sessions have a descent expiration time.                                                                             |
|                 | | The newly created basket is empty but already saved to the database. When creating a new basket it is important to use CreateBasket and not to instantiate it yourself using |
|                 | | new Basket() constructor. This is because all basket updates require that there is a existing basket.                                                                        |
+-----------------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| GetActiveBasket | | The GetActiveBasket is used to get the basket (with state Active) for a session. The method checks the baskets that are related to the user reference. If there are no       |
|                 | | baskets with state Active and the correct user reference, the method creates a new basket if the CreateNewBasket parameter is set otherwise it will return null.             |
|                 | | All baskets are processed by the configured modules before they are returned. For example, the promotions are checked to see if they are still valid.                        |
+-----------------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| GetBasket       | | The GetBasket method retrieves a basket on it's unique identifier, regardless of the state of the basket.                                                                    |
|                 | | The contents of the basket are not processed, and the totals are calculated in it's current state.                                                                           |
|                 | | This method should be used for exports etc.                                                                                                                                  |
+-----------------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| AbandonBasket   | | This explicit Abandon call updates the supplied Basket entity to the status Abandoned and the DateTime of Abandon. When the abandoned status is set; the basket is           |
|                 | | automatically saved as well.                                                                                                                                                 |
+-----------------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| UpdateBasket    | | To gain control over the Basket and the process of creating/managing baskets it is not recommended to apply updates(i.e. add products) on the Basket entity itself.          |
|                 | | To manage the baskets contents the UpdateBasket method is available. The UpdateBasket is used to apply changes on the basket.                                                |
|                 | | Think about adding a product, changing the quantity or setting the transporter. All the changes on the basket should be made through the                                     |
|                 | | UpdateBasket method. More details on the changes is discusses in the next section.                                                                                           |
|                 | | The UpdateBasket function will also calculate, apply vouchers/promotions and save the basket.                                                                                |
+-----------------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| DeleteBasket(s) | | Deletes the Basket(s) matching the supplied Id(s) within a single transaction.                                                                                               |
+-----------------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| SyncBasket      | | Synchronizes the Basket. The method checks the basket items for availability.                                                                                                |
+-----------------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+

.. note ::

	The methods CreateBasket, GetActiveBasket, UpdateBasket and ReActivateBasket all require that
	the BasketRelatedDataFlags.Items is set or a InvalidOperationException is thrown. This is done
	to prevent the basket getting corrupt.

Modifying the contents of the basket
------------------------------------

The structure of the Basket and it's contents is rather complex. To cover this we've created the "change" request methodology. Basket changes are the objects used in the UpdateBasket method to change the basket and its contents. For example; adding a Product to the basket is a BasketChange.

There are two types of basket changes namely: Change and ChangeRequest. Changes are small objects that allows you really change the contents of the basket. They are very small parts which can used in relation to each other. On a more higher level, and easier to use are the ChangeRequest entities. Change requests are shortcuts for common scenario's to simplify the usage of the basket. ChangeRequests are converted into Changes before they get applies on the basket. More on Changes and ChangeRequests in the next sections.

.. note:: We strongly discourage modifying the contents of the basket outside the basket modules.

The properties of the basket are not protected. You are able to modify the properties, but we discourage you to do so. If you modify the basket contents in your website we cannot guarantee the basket will function as expected. If the required data isn't present errors may occur. 

If you want to make alterations to the contents of the basket we suggest you create a module that integrates with the basket sequence. This module will be integrated in every call you and the ecManager framework will do regarding the baskets, allowing you to consistently influence the contents of the basket regardless of the calling application (CMS, webshop, etc.).

.. rubric:: ChangeRequest

The ChangeRequest is a request that will trigger changes to the contents of the basket. They are a simplified call that will take care of the complicated part of the basket change. For instance a AddCombinationRequest will take a combination id as input and will result in the combination and its products and combination discounts being added to the basket.

The ChangeRequest is the type to use when implementing the basket in a webshop.

.. code-block:: C#
	:linenos:

	// Create a new AddCombinationChangeRequest
	var combinationChange = new AddCombinationChangeRequest
	{
	    BasketId = basket.Id,
	    CombinationId = combination.Id,
	    Quantity = 1,
	};
	  
	// Instantiate service so the basket can be updated
	BasketService service = new BasketService(new ServiceRequestContext(), BasketEntityStateDictionary);
	// Apply change on the basket
	service.UpdateBasket(new Collection<AChange> { combinationChange });

.. rubric:: The following ChangeRequests are available

.. warning:: When you use quantity '0'  ecManager does not throw an exception. The '0' value is used, so it's possible to have a BasketItem without quantity.

+-----------------------+---------------------------------------+----------------------------------------------------------------+--------------------------------------------------------------------------------------------+
| Entity                | ChangeRequest                         | Purpose                                                        | Possible exceptions when processing the request                                            |
+=======================+=======================================+================================================================+============================================================================================+
| Product               | UpdateProductChangeRequest            | | Updates an existing product in the basket                    | | - NullReferenceException: when UpdateProductChangeRequest is null                        |
|                       |                                       | |                                                              | | - InvalidDataException: when Product for requested ProductId cannot be found in database |
|                       |                                       | |                                                              | | - InvalidDataException: when Product to apply update on cannot be found in basket        |
|                       |                                       | |                                                              | | - InvalidDataException: when ProductOption for requested ProductOptionId cannot be       |
|                       |                                       | |                                                              | | found on the product                                                                     |
+-----------------------+---------------------------------------+----------------------------------------------------------------+--------------------------------------------------------------------------------------------+
|                       | RemoveProductChangeRequest            | | Removes a product from the basket                            | | - NullReferenceException: when RemoveProductChangeRequest is null                        |
|                       |                                       | |                                                              | | - InvalidDataException: when Product to remove cannot be found in basket                 |
+-----------------------+---------------------------------------+----------------------------------------------------------------+--------------------------------------------------------------------------------------------+
|                       |                                       | |                                                              | |                                                                                          |
+-----------------------+---------------------------------------+----------------------------------------------------------------+--------------------------------------------------------------------------------------------+
| Combination           | UpdateCombinationChangeRequest        | | Updates an existing combination in the basket                | | - NullReferenceException: when UpdateCombinationChangeRequest is null                    |
|                       |                                       | |                                                              | | - InvalidDataException: when Combination for requested CombinationId cannot be           |
|                       |                                       | |                                                              | | found in database                                                                        |
|                       |                                       | |                                                              | | - InvalidDataException: when Combination to apply update on cannot be found in basket    |
+-----------------------+---------------------------------------+----------------------------------------------------------------+--------------------------------------------------------------------------------------------+
|                       | RemoveCombinationChangeRequest        | | Removes a combination from the basket                        | | - NullReferenceException: when RemoveCombinationChangeRequest is null                    |
|                       |                                       | |                                                              | | - InvalidDataException: when Combination to remove cannot be found in basket             |
+-----------------------+---------------------------------------+----------------------------------------------------------------+--------------------------------------------------------------------------------------------+
|                       |                                       | |                                                              | |                                                                                          |
+-----------------------+---------------------------------------+----------------------------------------------------------------+--------------------------------------------------------------------------------------------+
| Product Option choice | UpdateProductOptionChoiceChangeRequest| | Updates the OptionChoice                                     | | - NullReferenceException: when UpdateProductOptionChangeRequest is null                  |
|                       |                                       | |                                                              | | - InvalidDataException: when Product for requested ProductId cannot be found in database |
|                       |                                       | |                                                              | | - InvalidDataException: when OptionChoice to apply update on cannot be found in basket   |
|                       |                                       | |                                                              | | - InvalidDataException: when ProductOption for requested OptionChoiceId cannot be found  |
|                       |                                       | |                                                              | | on the product                                                                           |
+-----------------------+---------------------------------------+----------------------------------------------------------------+--------------------------------------------------------------------------------------------+
|                       | RemoveProductOptionChoiceChangeRequest| | Removes the OptionChoice from an item in the basket          | | - NullReferenceException: when RemoveProductOptionChangeRequest is null                  |
|                       |                                       | |                                                              | | - InvalidDataException: when Item (Product) to remove cannot be found in basket          |
|                       |                                       | |                                                              | | - InvalidDataException: when OptionChoice to remove cannot be found in basket            |
+-----------------------+---------------------------------------+----------------------------------------------------------------+--------------------------------------------------------------------------------------------+
|                       |                                       | |                                                              | |                                                                                          |
+-----------------------+---------------------------------------+----------------------------------------------------------------+--------------------------------------------------------------------------------------------+
| Payment               | UpdatePaymentMethodChangeRequest      | | Updates the PaymentMethod in the basket                      | | - NullReferenceException: when UpdatePaymentMethodChangeRequest is null                  |
|                       |                                       | |                                                              | | - InvalidDataException: when new PaymentMethod cannot be found in database               |
|                       |                                       | |                                                              | | - InvalidDataException: when the current paymentmethod(by Reference property             |
|                       |                                       | |                                                              | | cannot be found in basket                                                                |
+-----------------------+---------------------------------------+----------------------------------------------------------------+--------------------------------------------------------------------------------------------+
|                       | RemovePaymentMethodChangeRequest      | | Removes a PaymentMethod from basket                          | | - NullReferenceException: when RemovePaymentMethodChangeRequest is null                  |
|                       |                                       | |                                                              | | - InvalidDataException: when the referenced PaymentMethod to remove cannot be            |
|                       |                                       | |                                                              | | found in basket                                                                          |
+-----------------------+---------------------------------------+----------------------------------------------------------------+--------------------------------------------------------------------------------------------+
|                       |                                       | |                                                              | |                                                                                          |
+-----------------------+---------------------------------------+----------------------------------------------------------------+--------------------------------------------------------------------------------------------+
| Transporters          | UpdateTransporterMethodChangeRequest  | | Updates the Transporter in the basket                        | | - NullReferenceException: when UpdateTransporterMethodChangeRequest is null              |
|                       |                                       | |                                                              | | - InvalidDataException: when new Transporter cannot be found in database                 |
|                       |                                       | |                                                              | | - InvalidDataException: when the current Transporter(by Reference property               |
|                       |                                       | |                                                              | | cannot be found in basket                                                                |
+-----------------------+---------------------------------------+----------------------------------------------------------------+--------------------------------------------------------------------------------------------+
|                       | RemoveTransporterMethodChangeRequest  | | Removes the Transporter from the basket                      | | - NullReferenceException: when RemoveTransporterMethodChangeRequest is null              |
|                       |                                       | |                                                              | | - InvalidDataException: when the referenced Transporter to remove cannot be              |
|                       |                                       | |                                                              | | found in basket                                                                          |
+-----------------------+---------------------------------------+----------------------------------------------------------------+--------------------------------------------------------------------------------------------+
|                       |                                       | |                                                              | |                                                                                          |
+-----------------------+---------------------------------------+----------------------------------------------------------------+--------------------------------------------------------------------------------------------+
| Other                 | UpdateVoucherChangeRequest            | | Update a voucher in the basket using a VoucherCode           | | - ArgumentException: when supplied Basket or UpdateVoucherChangeRequest is null          |
+-----------------------+---------------------------------------+----------------------------------------------------------------+--------------------------------------------------------------------------------------------+
|                       | RemoveVoucherChangeRequest            | | Remove a voucher from the basket using a VoucherCode         | | - ArgumentException: when supplied Basket or RemoveVoucherChangeRequest is null          |
+-----------------------+---------------------------------------+----------------------------------------------------------------+--------------------------------------------------------------------------------------------+
|                       |                                       | |                                                              | |                                                                                          |
+-----------------------+---------------------------------------+----------------------------------------------------------------+--------------------------------------------------------------------------------------------+
|                       | UpdateCustomerChangeRequest           | | Updates the basket to reference the supplied Customer. This  | |                                                                                          |
|                       |                                       | | will not set the User reference                              | |                                                                                          |
+-----------------------+---------------------------------------+----------------------------------------------------------------+--------------------------------------------------------------------------------------------+
|                       | RemoveCustomerChangeRequest           | | Removes the Customer reference from the basket               | |                                                                                          |
+-----------------------+---------------------------------------+----------------------------------------------------------------+--------------------------------------------------------------------------------------------+
|                       |                                       | |                                                              | |                                                                                          |
+-----------------------+---------------------------------------+----------------------------------------------------------------+--------------------------------------------------------------------------------------------+
|                       | UpdateUserChangeRequest               | | Updates the basket to reference the supplied User. This will | |                                                                                          |
|                       |                                       | | also set the Customer reference                              | |                                                                                          |
+-----------------------+---------------------------------------+----------------------------------------------------------------+--------------------------------------------------------------------------------------------+
|                       | RemoveUserChangeRequest               | | Removes the User reference from the basket. This will also   | |                                                                                          |
|                       |                                       | | the Customer reference from the basket.                      | |                                                                                          |
+-----------------------+---------------------------------------+----------------------------------------------------------------+--------------------------------------------------------------------------------------------+


Using the ChangeRequest entities you will be able to manage most of the Baskets contents. The Changes can be used to have more fine grained control but will require more effort to implement.

.. rubric:: Change

.. warning:: This is a more advanced way of changing the basket. In general you should ChangeRequest in your implementation. You could use changes when ChangeRequests do not meet your criteria or usage.

The Change type is the smallest unit of change. ChangeRequests are translated into one or more Change instances, which are applied to the basket. For example adding a product to the basket requires you to create an ItemChange entity and apply it on the basket. 

.. code-block:: C#
	:linenos:

	// Create a new ItemChange to add a Product to the basket
	var productChange = new ItemChange
	{
	    BasketId = <basket id>,
	    EntityId = product.Id,
	    Quantity = 1,
	    EntityNumber = product.Number,
	    ListPrice = price.ListPrice,
	    SellingPrice = price.SellingPrice,
	    Texts = new Collection<BasketItemText>()
	    {
	        new BasketItemText()
	        {
	            LanguageCode = "nl", Title = "Blue shirt", Description = "Blue shirt made of cotton"
	        }
	    }
	};
	 
	// Instantiate service so the basket can be updated
	BasketService service = new BasketService(new ServiceRequestContext(), BasketEntityStateDictionary);
	// Apply change on the basket
	service.UpdateBasket(new Collection<AChange> { productChange });

When you want to apply a discount on the product this will require finding the item you want to update and adding a Discount entity to its Discounts collection.

Changes will only affect the referenced item and nothing else. For instance a UpdateCollectionItemChange will update all data of the referenced collection item but not the subitems, costs or discounts. Updating the SubItems, Costs and Discount requires separate changes.

Storing custom data
-------------------
The basket and all basket items have a Configuration field. 
This XDocument can be used to store data that you might need again in a later stage.
The XML inside will be persisted when saving the entity, and will be restored on load.