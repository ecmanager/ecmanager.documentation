############################
Change request documentation
############################

Introduction
============

On this page we will describe properties on the change requests. The mandatory properties might differ per operation.
Below we will outline the mandatory properties and the expected values per change request.

Update change request
=====================

All add and update operations are handled using UpdateChangeRequest classes. 
All these update changes inherit from the AChangeRequest class.
In this section we will explain which fields are mandatory, and which are optional during an add or an update operation. 

UpdateCombinationChangeRequest
------------------------------

This change request is used for adding or updating combinations to your basket. 
When you add a combination, products will be added to the basket as sub items of the combination.

+----------------------+-------------+----------------------------------------------------------------------------+------------------------------------------------------------------------------------------------------------------+
| Property             | Mandatory   | Add operation                                                              | Update operation                                                                                                 |
+======================+=============+============================================================================+==================================================================================================================+
| CombinationId        | Always      | | The ID of the combination that should be added to the basket.            | | The ID of the combination in the basket.                                                                       |
|                      |             | | If a basket line exists with the given combination, the quantity         | | It isn't possible to change the combination using the update method.                                           |
|                      |             | | of this line will be increased with the.                                 | | You will have to remove the existing combination and add a new one.                                            |
+----------------------+-------------+----------------------------------------------------------------------------+------------------------------------------------------------------------------------------------------------------+
| Quantity             | Always      | | The amount of combinations you want to add                               | | The amount of combinations you want to have in the basket.                                                     |
+----------------------+-------------+----------------------------------------------------------------------------+------------------------------------------------------------------------------------------------------------------+
| BasketItemId         | Always      | | The BasketItemId property should have value 0.                           | | The BasketItemId property should match the BasketItemId of the basket line you want to update.                 |
+----------------------+-------------+----------------------------------------------------------------------------+------------------------------------------------------------------------------------------------------------------+
| BasketItemInternalId | Update only | | x                                                                        | | The BasketItemInternalId property should match the BasketItemInternalId of the basket line you want to update. |
+----------------------+-------------+----------------------------------------------------------------------------+------------------------------------------------------------------------------------------------------------------+
| Configuration        | Optional    | | You can add custom configuration to the combination using this property. | | If you want to set or override the configuration of a combination in the basket set this property.             |
|                      |             |                                                                            | | Existing configurations will remain intact when this property is set to null.                                  |
+----------------------+-------------+----------------------------------------------------------------------------+------------------------------------------------------------------------------------------------------------------+

UpdateCustomerChangeRequest
---------------------------

This change request is used for adding or updating the customer related to the basket.
If the customer has an email address, this address will be applied to the basket.

+------------+-----------+----------------------------------------------+----------------------------------------------+
| Property   | Mandatory | Add operation                                | Update operation                             |
+============+===========+==============================================+==============================================+
| CustomerID | Always    | The ID of the customer related to the basket | The ID of the customer related to the basket |
+------------+-----------+----------------------------------------------+----------------------------------------------+

.. warning:: The customer needs to be added to the basket by the implementation.

UpdatePaymentMethodChangeRequest
--------------------------------

This change request is used for adding or updating the payment method of the basket.
If there is a payment method that matches the given reference, this will be deleted. 

+-----------------+-------------+----------------------------------------------+--------------------------------------------------+
| Property        | Mandatory   | Add operation                                | Update operation                                 |
+=================+=============+==============================================+==================================================+
| PaymentMethodId | Always      | The ID of the payment method you want to add | The ID of the payment method you want to update  |
+-----------------+-------------+----------------------------------------------+--------------------------------------------------+
| Reference       | Update only | x                                            | Use the reference of the existing payment method |
+-----------------+-------------+----------------------------------------------+--------------------------------------------------+

.. warning:: Please note that ecManager does not set a payment method by default. The implementation is responsible for making sure a payment method is selected.

UpdateProductChangeRequest
--------------------------

This change request is used for adding products to the basket, or updating existing products.
You can change to a different product in your order line using an update operation as you can see in the table below. 

+----------------------+-------------+----------------------------------------------------------------------------+------------------------------------------------------------------------------------------------------------------+
| Property             | Mandatory   | Add operation                                                              | Update operation                                                                                                 |
+======================+=============+============================================================================+==================================================================================================================+
| ProductId            | Always      | | The ID of the product you want to add to the basket                      | | The ID of the product you want to update.                                                                      |
|                      |             |                                                                            | | In case you want to change product, fill in the ID of the desired product.                                     |
+----------------------+-------------+----------------------------------------------------------------------------+------------------------------------------------------------------------------------------------------------------+
| Quantity             | Always      | | The amount of products you want to add                                   | | The amount of products in the basket.                                                                          |
+----------------------+-------------+----------------------------------------------------------------------------+------------------------------------------------------------------------------------------------------------------+
| OptionChoices        | Optional    | | Add the option choices if you want to add option choices to the product. | | Add the option choices if you want to add option choices to the product.                                       |
|                      |             | | See the OptionChoiceChange section below.                                | | See the OptionChoiceChange section below.                                                                      |
+----------------------+-------------+----------------------------------------------------------------------------+------------------------------------------------------------------------------------------------------------------+
| BasketItemId         | Always      | | The BasketItemId property should have value 0.                           | | The BasketItemId property should match the BasketItemId of the basket line you want to update                  |
+----------------------+-------------+----------------------------------------------------------------------------+------------------------------------------------------------------------------------------------------------------+
| BasketItemInternalId | Update only | | x                                                                        | | The BasketItemInternalId property should match the BasketItemInternalId of the basket line you want to update. |
+----------------------+-------------+----------------------------------------------------------------------------+------------------------------------------------------------------------------------------------------------------+
| Configuration        | Optional    | | You can add custom configuration to the product using this property.     | | If you want to set or override the configuration of a product in the basket set this property.                 |
|                      |             |                                                                            | | Existing configurations will remain intact when this property is set to null.                                  |
+----------------------+-------------+----------------------------------------------------------------------------+------------------------------------------------------------------------------------------------------------------+
| Reference            | Optional    | | The given reference will be added to the product.                        | | If you want to match a specific (set of) product(s) you need to update, you can do this by matching reference. |
+----------------------+-------------+----------------------------------------------------------------------------+------------------------------------------------------------------------------------------------------------------+

.. warning:: If you add the same product twice to your basket using two different change requests with BasketItemId 0, the result will be merged into one basket item. This only happens when the product, configuration and options are identical in both requests, default the configuration is compared using `XNode.DeepEqual <https://msdn.microsoft.com/en-us/library/system.xml.linq.xnode.deepequals.aspx>`_. The quantity of the second request is added on top of the existing quantity of the basket item.

.. rubric:: UpdateProductChangeRequest.OptionChoiceChange

This is a change inside the UpdateProductChangeRequest. 
You can use this to add options to the product line you are adding/updating.
	
+----------------------+-----------+------------------------------------+-------------------------------------+
| Property             | Mandatory | Add operation                      | Update operation                    |
+======================+===========+====================================+=====================================+
| OptionChoiceId       | Always    | The ID of the option choice        | The ID of the option choice         |
+----------------------+-----------+------------------------------------+-------------------------------------+
| OptionChoiceQuantity | Always    | The quantity of the option choices | The quantity of the option choices  |
+----------------------+-----------+------------------------------------+-------------------------------------+

UpdateProductOptionChoiceChangeRequest
--------------------------------------

This change request allows you to update the the option choices of a product in the basket. 

+----------------------+-------------------------------------------------------------------------------+-------------------------------------------------------------------------------------------------+
| Property             | Add operation                                                                 | Update operation                                                                                |
+======================+===============================================================================+=================================================================================================+
| OptionChoiceId       | | The ID of the option choice.                                                | | The ID of the option choice.                                                                  |
|                      |                                                                               | | When there is already an option choice in on the basket product item which belongs to the     |
|                      |                                                                               | | same option id the option choice is replaced.                                                 |
|                      |                                                                               | | When there is no option choice belonging to the same option the option choice is added.       |
+----------------------+-------------------------------------------------------------------------------+-------------------------------------------------------------------------------------------------+
| Quantity             | | The quantity of the option choice                                           | | The quantity of the option choices.                                                           |
+----------------------+-------------------------------------------------------------------------------+-------------------------------------------------------------------------------------------------+
| BasketItemId         | | This field should contain the basket item id of the line to which           | | The BasketItemId property should match the BasketItemId of the basket line you want to update |
|                      | | the product should be added. Can be zero, see the ProductId field.          | |                                                                                               |
+----------------------+-------------------------------------------------------------------------------+-------------------------------------------------------------------------------------------------+
| BasketItemInternalId | | x                                                                           | | The BasketItemInternalId property should match the BasketItemInternalId of the basket line    |
|                      |                                                                               | | the option choice belongs to.                                                                 |
+----------------------+-------------------------------------------------------------------------------+-------------------------------------------------------------------------------------------------+
| ProductId            | | In some cases you want to add the product and the options in one operation. | | x                                                                                             |
|                      | | At this point you might not know the basket item id of the product yet.     | |                                                                                               |
|                      | | In this case you insert the product ID, so we can validate the add          | |                                                                                               |
|                      | | operation is valid.                                                         | |                                                                                               |
+----------------------+-------------------------------------------------------------------------------+-------------------------------------------------------------------------------------------------+


UpdateTransporterChangeRequest
------------------------------

This change requests sets or change the transport for the order.
If there is an existing transporter, this will be deleted. 
The existing transporter will be located using the reference you set in the change request. 

+---------------+-----------+---------------------------+--------------------------------------------------+
| Property      | Mandatory | Add operation             | Update operation                                 |
+===============+===========+===========================+==================================================+
| TransporterId | Always    | The ID of the transporter | The ID of the transporter                        |
+---------------+-----------+---------------------------+--------------------------------------------------+
| Reference     | Reference | x                         | Use the reference of the existing payment method |
+---------------+-----------+---------------------------+--------------------------------------------------+

.. warning:: Please note that the VAT used for the transporter cost is always the highest VAT percentage for the selected shipping country

.. warning:: Please note that ecManager does not set a transporter by default. The implementation is responsible for making sure a transporter is selected.

UpdateUserChangeRequest
-----------------------

Add or change the user related to the basket.
If there is a customer related to this user, it will be added to the basket as well by using the
``UpdateCustomerChangeRequest``.

+----------+-----------+------------------+--------------------+
| Property | Mandatory | Add operation    | Update operation   |
+==========+===========+==================+====================+
| UserId   | Always    | The voucher code | The ID of the user |
+----------+-----------+------------------+--------------------+

UpdateVoucherChangeRequest
--------------------------

Method to add a voucher to the basket.

+-------------+-----------+------------------+-------------------------------------------------+
| Property    | Mandatory | Add operation    | Update operation                                |
+=============+===========+==================+=================================================+
| VoucherCode | Add-only  | The voucher code | The is no way to update the voucher in a basket |
+-------------+-----------+------------------+-------------------------------------------------+

Remove change requests
======================

The following section documents the properties in the remove operations.
If you want to lower the quantity of a basket line, you should use update operations.
A remove operation deletes the entire basket line. 

RemoveCombinationChangeRequest
------------------------------

The change request for deleting a combination.

+----------------------+-----------+-------------------------------------------------------------------------+
| Property             | Mandatory | Description                                                             |
+======================+===========+=========================================================================+
| BasketItemId         | Always    | The basket item id of the combination you would like to remove          |
+----------------------+-----------+-------------------------------------------------------------------------+
| BasketItemInternalId | Always    | The basket item internal id of the combination you would like to remove |
+----------------------+-----------+-------------------------------------------------------------------------+

RemoveCustomerChangeRequest
---------------------------

The change request for removing the customer from the basket does not have any properties.
If you remove the customer, the user is removed as well.
We only modify the data on the basket, the customer and user in the database remain untouched.

RemovePaymentMethodChangeRequest
--------------------------------

The change request for deleting the payment method.

+-----------+-----------+-----------------------------------+
| Property  | Mandatory | Description                       |
+===========+===========+===================================+
| Reference | Never     | The reference is currently unused |
+-----------+-----------+-----------------------------------+

.. warning:: Please note that the VAT used for the payment cost is always the highest VAT percentage for the selected shipping country

RemoveProductChangeRequest
--------------------------

The change request for deleting a product.

+----------------------+-----------+---------------------------------------------------------------------+
| Property             | Mandatory | Description                                                         |
+======================+===========+=====================================================================+
| BasketItemId         | Always    | The basket item id of the product you would like to remove          |
+----------------------+-----------+---------------------------------------------------------------------+
| BasketItemInternalId | Always    | The basket item internal id of the product you would like to remove |
+----------------------+-----------+---------------------------------------------------------------------+

RemoveProductOptionChoiceChangeRequest
--------------------------------------

The change request for deleting a product option choice.

+----------------------+-----------+-------------------------------------------------------------------------------------------------+
| Property             | Mandatory | Description                                                                                     |
+======================+===========+=================================================================================================+
| BasketItemId         | Always    | The basket item id of the product which has the option choice you would like to remove          |
+----------------------+-----------+-------------------------------------------------------------------------------------------------+
| BasketItemInternalId | Always    | The basket item internal id of the product which has the option choice you would like to remove |
+----------------------+-----------+-------------------------------------------------------------------------------------------------+
| OptionChoiceId       | Always    | The ID of the option choice you would like to remove                                            |
+----------------------+-----------+-------------------------------------------------------------------------------------------------+

RemoveTransporterChangeRequest
------------------------------

Method to delete the a transporter from the basket.

+-----------+-----------+---------------------------------------------------------------------+
| Property  | Mandatory | Description                                                         |
+===========+===========+=====================================================================+
| Reference | Never     | The reference of the transporter you want to remove from the basket |
+-----------+-----------+---------------------------------------------------------------------+

RemoveUserChangeRequest
-----------------------

This change request does not have any properties.
When a customer is added to the basket this will be removed as well.

RemoveVoucherChangeRequest
--------------------------

Change request to remove a voucher from the basket.
All content in the basket related to the voucher will be removed.

+-------------+-----------+--------------------------------------------------+
| Property    | Mandatory | Description                                      |
+=============+===========+==================================================+
| VoucherCode | Always    | The code of the voucher you would like to remove |
+-------------+-----------+--------------------------------------------------+





