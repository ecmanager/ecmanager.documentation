##############
Basket modules
##############

Introduction
------------
Baskets are complex and we believe we can't build a basket which is right for every implementation, 
so we built a basket which can be modified, extended or overridden to accommodate the needs of most
implementations. For this we created a flow with multiple extension points. Below a short description
of every extension point.

* ``ABasketLogic``:  The overall module with all logic. ``DefaultBasketLogic`` is the default implementation, most methods are virtual so you can modify the behavior.
* ``ICalculateBasket``: Interface for implementations which calculates the baskets totals and subtotals. This module is called a lot so be careful with heavy calculations. Only one instance is allowed.
* ``IProcessChangeRequest``: Interface for implementations which process one or more ``ChangeRequests``.
* ``ICleanBasket``: Interface for implementations which reset the basket to a state without specific modification. For example all promotions are removed every time the basket is retrieved. It's possible to have multiple implementations of the ``ICleanBasket`` interface registered.
* ``ICheckBasket``: Interface for implementations which check the state of the basket and corrects it by returning changes. It's possible to have multiple implementations of the ``ICheckBasket`` interface registered.
* ``IBasketTransportersProvider``: Interface for implementations which perform calculation of transport prices. The implementation of the interface defines the behavior of TransportService.GetTransportCategoryPrices().

Below is a sequence diagram which describes the interaction between the different modules.

.. note:: The Clean and Check modules are also triggered when retrieving a basket with GetActiveBasket.

.. image:: /_static/img/services/basket/basketflow.png
   :scale: 60%

DefaultBasketLogic
------------------
The logic which calls all other basket components. The ``DefaultBasketLogic`` extends from 
``ABasketLogic``. In the abstract ``ABasketLogic`` the basket components are resolved and triggered. 
The basic goal of the ``DefaultBasketLogic`` is defining the behavior of ChangeRequests and Changes.

You can read more about the Changes and ChangeRequests in `Modifying the contents of the basket <basket-implementation-manual.html#modifying-the-contents-of-the-basket>`_.

ICalculateBasket
----------------
An implementation of the ``ICalculateBasket`` interface is responsible for calculating the totals and 
subtotals of the basket and basket items. The only method which needs to be implemented is 
``CalculateBasket``. Because the ``ICalculateBasket`` implementation is only responsible for
calculating, you don't want to change any contents of the basket, this includes new baskets items, 
costs, discounts, etc. To change the contents of the basket use the ``ICheckBasket``, 
``IProcessChangeRequest`` or ``ICleanBasket`` interfaces. This module is allowed to change the 
values of the basket directly.

The name of the default ``ICalculateBasket`` implementation is ``DefaultCalculateBasketLogic``.

.. note:: There can only be one implementation of the ``ICheckBasket`` interface registered.

IProcessChangeRequest
---------------------
Implementations of the ``IProcessChangeRequest`` interface can be used to handle custom 
``ChangeRequests``. You can create your own ``ChangeRequests`` to help you with a specific task. A 
change request can result in ``Changes`` and ``ChangeRequests``. For example you have a 
``ChangeRequest`` which adds a free product. This will result in multiple changes:

* A change to add the product as a basket item
* A change to add a discount for the product
* A change to add a reference between the product and the discount

All change requests not handled by the ``DefaultBasketLogic`` will be passed to every implementation
of ``IProcessChangeRequest``. Therefore you need to check in your implementation which requests are 
meant for you and which aren't. An example of this check can be found in the following code fragment.

.. code-block:: C#
	:linenos:
	
	public IEnumerable<AChange> ProcessChangeRequest(AChangeRequest changeRequest)
	{
	  var result = new Collection<AChange>();
	  var transporterChangeRequest = changeRequest as UpdateTransporterChangeRequest;
	  if (transporterChangeRequest != null)
	  {
	    result.AddRange(ProcessChangeRequest(transporterChangeRequest));
	  }
	  return result;
	}

The following classes implement the ``IProcessChangeRequest`` interface:

* ``SynchronizationChangeRequestProcessor``: Processes change requests of type ``SynchronizationChangeRequest``.
* ``DefaultBasketTransporterCheckModule``: Processes change requests of type ``UpdateTransporterChangeRequest`` and ``RemoveTransporterChangeRequest``.
* ``DefaultBasketVoucherLogic``: Processes change requests of type ``UpdateVoucherChangeRequest`` and ``RemoveVoucherChangeRequest``.

.. note:: Multiple instances of ``IProcessChangeRequest`` can be registered. Only custom change request are handled, the default change request are handled by the ``DefaultBasketLogic`` and are to be overridden there. 

ICleanBasket
------------
Implementations of the ``ICleanBasket`` interface can be used to reset the basket to a clean state. 
This is used to remove all promotions from the basket so we can apply the promotions again without 
having to worry about previously applied promotions. So ``ICleanBasket`` is a cleaning mechanism 
which can make things easier. After every ``ICleanBasket`` implementation has been called, the 
``Changes`` and ``ChangeRequests`` are applied on the basket and the basket is recalculated using 
the registered implementation of the ``ICalculateBasket`` interface. ICleanBasket modules are 
triggered when retrieving and updating baskets.

The ``DefaultBasketPromotionLogic`` class implements the ``ICleanBasket`` interface.

.. note:: Multiple instances of IProcessChangeRequest can be registered. Modules are run in reverse order of registration in the modules.config.

ICheckBasket
------------
Implementations of the ``ICheckBasket`` interface allow you to force business rules on the basket. 
For example, there are ``ICheckBasket`` implementations for promotions and vouchers, and 
implementations to make sure only transport methods and payment methods which are available can be 
in the basket. Any business rules which are not triggered by user interaction can be applied using 
the ``ICheckBasket`` interface. Implementations of the ``ICheckBasket`` interface consist of a 
constructor and two methods, ``GetChanges`` and ``HasNext``. The Basket on which the changes will
be applied is passed to the constructor as a parameter.

In the GetChanges method you specify your business logic. The result needs to be ``Changes`` or
``ChangeRequests`` which can be applied to the basket. For example the adding/removal of basket items,
costs and discounts. 

The ``HasNext`` method allows you to make multiple sets of changes which will be applied
between every iteration. This can be useful when changes are dependent on each other. For example the
first ``Change`` gives a discount on a product, the second ``Change`` uses the total of the basket to 
apply a business rule. Without first applying the discount the total of the basket is incorrect and 
the business rule give a different result. 

After every iteration of each ``ICheckBasket`` implementation (GetChanges) ``Changes`` and 
``ChangeRequests`` are applied on the basket and the basket is recalculated using the implementation 
of ``ICalculateBasket``.

Implementations of ``ICheckBasket`` are triggered when retrieving and updating baskets. The following
classes implement the interface:

* ``DefaultBasketTransporterCheckModule``: Checks if the transporters in a basket are still available.
* ``DefaultBasketReferenceCheckModule``: Checks if there are unnecessary references in the basket.
* ``DefaultBasketVoucherLogic``: Checks if vouchers applied to a basket are still valid.
* ``DefaultBasketShippingCountryCheckModule``: Checks if the shipping country in a basket still matches the shipping country in the ``ServiceRequestContext``.
* ``DefaultBasketPromotionLogic``: Checks which promotions are valid for a basket.
* ``DefaultBasketPaymentCheckModule``: Recalculates payment costs that use a percentage as a value.

.. note:: Multiple instances of IProcessChangeRequest can be registered. Modules are run in reverse order of registration in the modules.config.

IBasketTransportersProvider
---------------------------
The implementation of the ``IBasketTransportersProvider`` interface is responsible for determining 
which transport methods are available for a basket and what the transport methods costs are.

.. note :: 

	There can only be one implementation of the ``IBasketTransportersProvider`` interface registered.

.. warning :: 

	| The default implementation of this provider doesn't have the possibility to filter transporters in the basket based on the field ``TransportCategory`` of the entity ``Product``.
	| You can create your own implementation on this particular question. 
	| For example: if you want to select a default transporter in the basket based on a transport category of a product in the basket, or filter specific transporters based on this category, you can do so.