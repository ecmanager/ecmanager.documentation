######
Basket
######

.. rubric:: Introduction

ecManager is about e-commerce and e-commerce is about Baskets. The basket holds all Products and Combinations the customer wants to buy and it holds all the Vouchers and Discounts you will get when ordering the items in the basket.


.. image:: /_static/img/services/basket/basket.png
    :width: 400px
    :align: center
    :height: 400px
    :alt: alternate text    

.. toctree::
   :maxdepth: 1

   basket-implementation-manual
   change-request-documentation
   basket-modules