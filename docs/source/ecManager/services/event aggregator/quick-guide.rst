###########
Quick guide
###########

This section describes how to register an *EventAggregator* in Autofac followed by some examples how to *subscribe* and *unsubcribe* to an event.

Autofac
========
The *EventAggregator* needs to be registered in *Autofac*. There can to be only one *EventAggregator* instance in the application but this is not enforced. It is important to register the *EventAggregator* as *single-instance*.

.. code-block:: XML
	:linenos:
	
	<component	type="ecManager.Common.Logic.EventAggregator, ecManager.Common.Logic"
			service="ecManager.Common.Interfaces.IEventAggregator, ecManager.Common"
			instance-scope="single-instance" />
			
.. note:: See the *single-instance* instance-scope, without the *single-instance* no events will be triggered.

The *subscribe*/*unsubcribe* you need to retrieve the instance of the *EventAggregator*. Autofac has multiple ways to retrieve an instance, the following example shows one of these ways. For a more detailed description see: `http://autofac.readthedocs.org/en/latest/ <http://autofac.readthedocs.org/en/latest/>`_.

.. code-block:: C#
	:linenos:
	
	var builder = new ContainerBuilder();
	builder.RegisterModule(new ConfigurationSettingsReader("autofac"));
	IContainer dependencyContainer = builder.Build();
	var eventAggregator = dependencyContainer.Resolve<IEventAggregator>();

Subscribe/unsubcribe to an event
================================
The following example describes how to *subscribe* to an event:

.. code-block:: C#
	:linenos:
	
	public void Initialize()
	{
		var subscriptionToken = eventAggregator.Subscribe<OrderPreSaveEvent>(OrderPreSaveEventListener);
	}
	
	public void OrderPreSaveEventListener(OrderPreSaveEvent eventInstance)
	{
		//do something.
	}
	
To *unsubcribe* a listener from an *event* you can use the *unsubcribe* method with the *subscriptionToken* as parameter as shown below:

.. code-block:: C#
	:linenos:
	
	public void Unsubscribe()
	{
		eventAggregator.Unsubscribe(subscriptionToken);
	}
	
	
.. note:: Post events are only published when the event is successful. For example when retrieving a non-existing product, the post event is not published.