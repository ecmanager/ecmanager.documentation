################
Event Aggregator
################

.. rubric:: Introduction

The Event Aggregator gives you the possibility to *Subscribe* to events triggered in the ecManager framework or one of its modules.

.. toctree::
   :titlesonly:
   
   overview
   quick-guide
   advanced



