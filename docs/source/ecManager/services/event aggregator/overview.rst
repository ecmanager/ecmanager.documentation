########
Overview
########

The Event Aggregator gives you the possibility to *Subscribe* to events triggered in the ecManager framework or one of its modules.

The ecManager framework will contain pre- and post events for all service entrances, so it will be possible to *subscribe* to the *OrderPreSaveEvent* which allows you to let your back-end know there is a new order.

Read the `Quick guide <quick-guide.html>`_ to know how to register the *EventAggregator* to Autofac and how to *Subscribe* to events. For more advanced information such as creating your own events read the `Advanced <advanced.html>`_ section.


Currently it's possible to subscribe to the following events:

	* OrderService
		* OrderPostDeleteEvent
		* OrderPostExportEvent
		* OrderPostGetEvent
		* OrderPostGetOrderCountEvent
		* OrderPostGetOrderIdEvent
		* OrderPostGetOrderNumberEvent
		* OrderPostSaveEvent
		* OrderPostSearchEvent
		* OrderPreDeleteEvent
		* OrderPreExportEvent
		* OrderPreGetEvent
		* OrderPreGetOrderCountEvent
		* OrderPreGetOrderNumberEvent
		* OrderPreSaveEvent
		* OrderPreSearchEvent
		
	* CustomerService
		* CustomerPostDeleteEvent
		* CustomerPostExportEvent
		* CustomerPostGetEvent
		* CustomerPostSaveEvent
		* CustomerPostSearchEvent
		* CustomerPreDeleteEvent
		* CustomerPreExportEvent
		* CustomerPreGetEvent
		* CustomerPreSaveEvent
		* CustomerPreSearchEvent
		
	* ProductService
		* ProductPostDeleteEvent
		* ProductPostSaveEvent
		* ProductPreDeleteEvent
		* ProductPreSaveEvent

	* LocationService
		* LocationPostDeleteEvent
		* LocationPostSaveEvent
		* LocationPreDeleteEvent
		* LocationPreSaveEvent

	* NavigationService
		* NavigationItemPostDeleteEvent
		* NavigationItemPostSaveEvent
		* NavigationItemPreDeleteEvent
		* NavigationItemPreSaveEvent
