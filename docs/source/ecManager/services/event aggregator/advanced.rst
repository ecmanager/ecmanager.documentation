########
Advanced
########

This section describes how to create and *publish* your own *events* using the *EventAggregator*. 

.. note:: We recommend to read `Quick guide: Autofac <quick-guide.html#autofac>`_ first to learn the basics about the *EventAggregator*.

Creating Events
===============
*Events* are basic objects which implements the *IEvent* interface. These objects can have properties which give more information about the *Event*.
The default *Events* for the ecManager framework are in this format: **[Entity][Pre|Post][Action]Event** for example: **OrderPreSaveEvent**. Below an example of the implementation of the *OrderPreSaveEvent*.

.. code-block:: C#
	:linenos:
	
	namespace ecManager.Customer.Events
	{
		using ecManager.Common.ComplexTypes;
		using ecManager.Common.Events;
		using ecManager.Common.Interfaces;
		using ecManager.Customer.Entities;

		/// <summary>
		/// The <see cref="IEvent"/> published before an <see cref="Order"/> is saved.
		/// </summary>
		public class OrderPreSaveEvent : BaseEvent
		{
			/// <summary>
			/// The <see cref="Order"/> which will be save.
			/// </summary>
			public Order Order { get; set; }

			/// <summary>
			/// The default constructor which sets the <paramref name="serviceRequestContext"/> and the <paramref name="order"/>.
			/// </summary>
			/// <param name="serviceRequestContext">The <see cref="ServiceRequestContext"/>/</param>
			/// <param name="order">The <see cref="Order"/>.</param>
			public OrderPreSaveEvent(ServiceRequestContext serviceRequestContext, Order order)
				: base(serviceRequestContext)
			{
				Order = order;
			}
		}
	}

.. note:: *Events* can inherent from each other but the inheritance hierarchy will not be followed and only the event from the lowest level will be *published*.

Publishing Events
=================
To *publish* events you retrieve the *payload* for the event then create an instance of the *Event*. After the *Event* is created you can use the *EventAggregator* to *publish* your *event*. Below an example where the *OrderPreSaveEvent* is *published*:

.. code-block:: C#
	:linenos:
	
	EventAggregator.Publish(new OrderPreSaveEvent(ServiceRequestContext, orderToSave));
