###########
Combination
###########

.. rubric:: Introduction

The CombinationService is a simple service for managing Combinations. A combination consists of one or more products whereby a discount can be applied. The discount for a combination can be set per product per price list.

.. toctree::
   :maxdepth: 1

   the-combination-entity-structure
   discounts