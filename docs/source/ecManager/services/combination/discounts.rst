#########
Discounts
#########

For a combination it's possible to add discount to the products that are in a combination. Which will make a combination deal of the products. 
When there is a product with a quantity of more then one the discount price will be calculated on the total quantity of the products.

See the example below:

+-------------------------------------------------------+
| Products                                              |
+================+=============+================+=======+
| Product number | Description | Quantity Price | Price |
+----------------+-------------+----------------+-------+
| J001           | Dress       | 2              | 25    |
+----------------+-------------+----------------+-------+
| JB002          | Blouse      | 1              | 20    |
+----------------+-------------+----------------+-------+

+------------------------------------------------------------------------+
| Discounts                                                              |
+================+=============+====================+====================+
| Product number | Description | Discount Excl. VAT | Discount Incl. VAT |
+----------------+-------------+--------------------+--------------------+
| J001           | Dress       | x                  | 2,5                |
+----------------+-------------+--------------------+--------------------+
| JB002          | Blouse      | x                  | 2,5                |
+----------------+-------------+--------------------+--------------------+

.. |br| raw:: html

   <br />

The dress has a quantity of two in the combination and the blouse one. The discount for the dress is 2,50 euro and for the blouse also. The dress has a quantity of two the discount for the dresses is 2,50 euro in totaal of the two dresses. |br|
**Dress:** 2 × 25 euro = 50 euro |br|
Discount = 2,50 euro |br|
50 euro – 2,50 euro = 47,50 euro |br|
**Blouse:** 1 × 20 euro = 20 euro |br|
Discount = 2,50 euro |br|
20 euro – 2,50 euro = 17,50 euro |br|
**Price of the combination:** dress (2x) 47,50 euro + blouse (1x) 17,50 euro = 65 euro