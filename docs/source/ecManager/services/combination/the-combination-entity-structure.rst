################################
The Combination entity structure
################################

The CombinationService and it's entities is designed for displaying combinations on a page or lister. The service manages combination entities. The diagram below shows the Combination entities.

.. image:: /_static/img/services/combination/combination.png
	:scale: 90%
	:align: center