#######################
Newsletter subscription
#######################

.. rubric:: Introduction

The NewsletterSubscriptionService and it's entities is designed for subscribing and unsubscribing to newsletters.

The NewsletterSubscription is for adding and removing an emailaddress from a newsletter address  list. When the subscribtions are kept in sync with external platforms; the EmailproviderContactId can be used for storing the external id of the platform.

.. toctree::
   :maxdepth: 1

   the-newslettersubscription-entity-structure



