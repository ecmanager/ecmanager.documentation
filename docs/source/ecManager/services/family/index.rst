Family
######

.. rubric:: Introduction

The FamilyService is a simple service for managing Families. In ecManager a family is a collection 
of products that are very similar but differ one (or more) attributes. 

For example one product 
which is available in multiple colors you can make a family for these 
products: 


+---------------+--------+--------+
| **Product 1** |        |        |
+---------------+--------+--------+
|               | Shirt  |        |
+---------------+--------+--------+
|               | Color  | Blue   |
+---------------+--------+--------+
| **Product 2** |        |        |
+---------------+--------+--------+
|               | Shirt  |        |
+---------------+--------+--------+
|               | Color  | Yellow |
+---------------+--------+--------+
| **Product 3** |        |        |
+---------------+--------+--------+
|               | Shirt  |        |
+---------------+--------+--------+
|               | Color  | Red    |
+---------------+--------+--------+
| **Product 4** |        |        |
+---------------+--------+--------+
|               | Bike   |        |
+---------------+--------+--------+
|               | Color  | Green  |
+---------------+--------+--------+




Product one to three can be combined in a family, it is the same product. The only difference is
the color of the product. The advantage of creating a family is to help customers find the product 
they are looking for by making clear there are variations of the product.

Family roll up.
===============
There are multiple ways of presenting the products of a family in the product lister. The default 
way is showing each product individually. The lister will show all product even if it's the same 
product in a diffent color. 

The second way is called family roll up. Family roll up means that only one product of the family is 
shown in the product lister. The product which will be shown is the first in the family. The rank 
of products in the family can be managed in the *ecManager CMS*. To enable family roll up in the
Fundatie webshop read `Configuration of Fundatie <../../../fundatie/configuration/configuration.html>`_.

Below an example of a lister with the same products, one with family roll up on and one with
family roll up off.

.. figure:: ../../../_static/img/services/family/familyrollupoff.png
	:alt: Family roll up off (default)
	:align: left
	:width: 420px
	
	Family roll up off (default)

	
.. figure:: ../../../_static/img/services/family/familyrollupon.png
	:alt: Family roll up on
	:align: right
	:width: 420px
	
	Family roll up on

	
	

