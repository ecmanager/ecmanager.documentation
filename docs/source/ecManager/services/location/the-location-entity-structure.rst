#############################
The location entity structure
#############################

The LocationService and it's entities is designed for displaying locations of physical shops on a page. The service manages Location entities. The service implements a ImageService for structured images with the service it's possible to retrieve an image which is linked to a location.

.. image:: /_static/img/services/location/location.png
	:scale: 90%
	:align: center