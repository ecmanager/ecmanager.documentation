###############
Location Search
###############

The location search task has a way to search for the nearest location. To use this you must deliver the Current Location this is a sub class of the location and the distance this is the the furthest location to look for.
The only way to fill the distance property with a value is filling in the current location coordinates and the distance field in the LocationSearchTask.