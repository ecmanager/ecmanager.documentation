####################
Customer preferences
####################

Subject values dynamic data
===========================

Subject values will get a Preferences configuration in which a link the custom data method is stored the same as the LibraryBlockParemeterDataSource object. Here you can configure a generic pointer to a

.. image:: /_static/img/services/customer/customer.png
	:scale: 90%
	:align: center