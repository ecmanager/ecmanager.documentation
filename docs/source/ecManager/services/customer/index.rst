########
Customer
########

.. rubric:: Introduction

The CustomerService is a service for Managing Customers. A Customer entity is widely used in ecManager. 
For example, a Customer is linked to Addresses, Baskets and Orders.

.. toctree::
   :maxdepth: 1

   customer-preferences
