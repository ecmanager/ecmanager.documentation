############
CheckoutSale
############

A CheckoutSale currently is a simple link to a product. You can use the CheckoutSale anywhere in your site to showcase a product.

CheckoutSaleService
===================

.. image:: /_static/img/services/checkout-sale/checkout-sale.png
	:scale: 90%
	:align: center