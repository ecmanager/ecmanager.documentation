#############
Checkout sale
#############

.. rubric:: Introduction

The CheckoutSaleService is a simple service which can be used to manage checkout sales. Checkout sales can be used to show products during checkout, in order to give those products more attention.

.. toctree::
   :maxdepth: 1

   checkoutsale

	