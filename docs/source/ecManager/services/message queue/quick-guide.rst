###########
Quick guide
###########

This page describes how to get started using the MessageQueueService.

Registration
============
To use message queues, they need to be registered using autofac. You can register any amount of message 
queues of any type as desired. This can be done as shown in the following code snippet:

.. code-block:: XML
	:linenos:
	
	<component
	    type="ecManager.Modules.MessageQueue.MSMQ.MSMQ, ecManager.Modules.MessageQueue.MSMQ"
	    service="ecManager.MessageQueue.IMessageQueue, ecManager.MessageQueue">
	    <properties>
	        <property name="Identifier" value="orderQueue"/>
	    </properties>
	</component>

	<component
	    type="MyCustomModule.MyMessageQueue, MyCustomModule"
	    service="ecManager.MessageQueue.IMessageQueue, ecManager.MessageQueue">
	    <properties>
	        <property name="Identifier" value="myQueue"/>
	    </properties>
	</component>

As you can see, a property called Identifier is specified for each registered component. This identifier 
has to be unique and can be named anything you like. This property is injected so each queue can be 
configured individually.

Configuration
=============
Configuration should be specified for each message queue. How this configuration is done can be different 
for each kind of message queue that you want to use. The MSMQ module implementation by ecManager can be 
configured using a configuration section in your app.config or web.config, you can read more about this 
`here <../../modules/message%20queue/quick-guide.html>`_.

Checking if a queue is registered
=================================
The IsQueueRegistered method checks if a queue has been registered in autofac. This can be useful in 
scenarios where you might not be sure if a queue is registered or not. In most cases you probably already 
know which queue is registered. 

Publishing an object
====================
The Publish method of the MessageQueueService allows you to publish an object. The way this object is 
serialized depends on the message queue modules that are used. 

Using the publish method will require you to specify identifiers for the queues that you want to publish 
to. You can specify any amount of identifiers you wish, as you can see in the following example:

.. code-block:: C#
	:linenos:

	var obj = new ExampleObject { ExampleProperty = "example" };
	var service = new MessageQueueService();
	service.Publish(obj, new string[] { "orderQueue", "myQueue" });
	
The second argument are string parameters which are the names of the queues that the object will be 
published to. 

.. warning:: If you don't specify any queue identifiers, an InvalidOperationException will occur.

You can pass any object into the publish method, but remember that the message queue modules must be able 
to serialize your object. So for example, if a module uses a BinaryFormatter for serialization, your 
object has to be marked with the [Serializable] attribute. Keep this in mind when publishing objects.

The publish method does not have a return value, so the MessageQueueService currently does not cater for 
the Request-Response message queue pattern. Read the `Advanced <advanced.html>`_ page for more information 
about supported patterns.