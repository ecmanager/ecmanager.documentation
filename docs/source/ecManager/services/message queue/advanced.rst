########
Advanced
########

Consuming your message queues
=============================
You can publish messages to your message queues using the MessageQueueService, but as of this moment you 
can't use the service to consume your message queues. The way in which you consume message queues is 
different for each kind. 

Here is an example on how to consume messages using the MSMQ module implementation, assuming the content of 
the messages in the queue are in XML and the consuming application has access to the types used in the queue:

.. code-block:: C#
	:linenos:

	var address = @".\Private$\myqueue";
	
	using (var msmq = new System.Messaging.MessageQueue(address))
	{
	    while (true)
	    {
	        var message = msmq.Receive();

	        var reader = new StreamReader(message.BodyStream);
	        var xml = reader.ReadToEnd();
		
	        // The type is stored in the message label
	        var messageType = Type.GetType(message.Label);
	        var ser = new XmlSerializer(messageType);
	
	        if(messageType == typeof(MyType))
	        {
	            var myType = ser.Deserialize(message.BodyStream) as MyType;
	            // Do something with your typed deserialized object
	        }
	    }
	}

.. note:: To read more about how you can consume a MSMQ, go `here <https://msdn.microsoft.com/en-us/library/ms978430.aspx>`_. 
