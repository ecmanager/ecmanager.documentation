########
Overview
########

The MessageQueueService allows you to interact with message queues which are defined as modules. 
Currently, the MessageQueueService has one function: publishing objects to message queues. 
The consumption of messages inside the queue is not something that is implemented within ecManager as of 
this moment.

Using the MessageQueueService allows you to communicate messages within your application to an external 
system asynchronously.

Visit the `Quick guide <quick-guide.html>`_ page to read more on how to get started using the 
MessageQueueService, or visit the `Advanced <advanced.html>`_ page to learn more about advanced topics, like
consuming your message queues.