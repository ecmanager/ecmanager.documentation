###########
Quick guide
###########

This page describes how to get started using the feeds.

Config
======

Add the config section for feedDataDefinition as shown below.
Set the path to the FeedDataDefinition.config.

.. code-block:: C#
	:linenos:
	:caption: Web/App.Config

	<configuration>
	    <configSections>     
	        <section name="feedDataDefinition" type="ecManager.Marketing.Entities.DataDefinitionConfigurationSection, ecmanager.Marketing"/>     
	    </configSections>
	    <feedDataDefinition configSource="FeedDataDefinition.config" />
	</configuration>
	
Feed data definition
====================

This is the configuration file that stores the query information of all the fields, filters and operations that can be used on the feedgenerator.

In the FeedDataDefinition there are links to images and/or product pages these links must link to the web shop. example: www.MyShop.nl/Images/MyProductImage.jpg. 
The FeedDataDefinition.Format.config has the ${BaseUrl} variable, the "www.MyShop.nl/Images" is the ${BaseUrl} and then renamed to FeedDataDefinition.config , this is best done with Nant.

Make sure the exact same FeedDataDefinition is used in the different applications, This will lead to unwanted differences in the generated feeds.


Create a feed
=============

Use the ecManager CMS to create a feed.
Make sure to include the correct entity state in the filters
See the CMS help for how to include filters in the feed.

Generate the feeds
==================

.. note:: Generate feeds on a regular basis.

There are three options auto generate the feeds:
	* The `GenerateAllFeeds <../../../tools/GenerateAllFeeds/quick-guide.html>`_ tool this will generate all active feeds, this tool can be scheduled with windows scheduler
	* Call the FeedService  GenerateAllActiveFeeds() 
	* Call the FeedService  GenerateFeed(FeedId) for each feed that needs to be generated 

The feeds can also be manually generated in the ecManager CMS Marketing->Feeds->Feed detail -> button "Generate"


