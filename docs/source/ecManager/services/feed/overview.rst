########
Overview
########

ecManager can make feeds for search or advertising. These feeds contain information about the products. 
What information is exported using the feeds can be fully customized. 

.. rubric:: Feed type

The feed type determines the the format and output of the feed.
ecManager supports the requirements of these feeds by giving you the option to create multiple feed types.
The feed type is saved in the database. A feed type is build with a collection of fields. A sample of a field is:

.. code-block:: XML
	:linenos:

	<BaseFeedField i:type="ElementFeedField">
	  <FieldId>id</FieldId>
	  <Field>g:id</Field>
	  <Required>true</Required>
	  <MaxFieldLenght>50</MaxFieldLenght>
	</BaseFeedField>

It's possible to create new feed types to meet new providers or changed requirements.
For an in-depth description of the feed types see: `Feed Types <../feed/advanced/feed-types.html>`_ 

.. rubric:: Feed data definition

The feed data definition is a set of data elements you can use in your feed. For example a ProductId or the selling price of a product. The feed data definition is stored in a XML-file with the name: FeedDataDefinition.config.
For an in-depth description of the feed data definition see: `Feed data definition <../feed/advanced/feed-data-definition.html>`_	

.. rubric::  Feed

The feed is the combination of a Feed type where the field in the field type are filled with the data elements from the feed data definition. The feed is stored in the database and can easily edited with the CMS.

.. rubric:: CMS

A visual representation of these elements can viewed in the CMS. Every part of the feed can be altered in a way to fit your needs.

.. image:: /_static/img/services/feed/feed.png
	:scale: 90%
	:align: center