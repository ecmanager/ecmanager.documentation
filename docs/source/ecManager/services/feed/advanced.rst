########
Advanced
########

To customize which data is available or how filters are applied read the `Feed data definition <../feed/advanced/feed-data-definition.html>`_,
to create new feed types read `Feed Types <../feed/advanced/feed-types.html>`_.

.. toctree::
   :maxdepth: 4
   :titlesonly:
   :glob:

   advanced/feed-data-definition
   advanced/feed-types