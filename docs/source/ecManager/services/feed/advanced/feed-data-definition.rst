####################
Feed data definition
####################

The feed generator is in essence a query builder which combines a feed definition with a data definition to make a query that generates the feed xml with SQL to XML from mssql server.
Auto generate feeds use the executable under tools GenerateAllFeeds make sure The FeedDataDefinition.config is the same as for the ecManager cms and the appconfig the database is accessible. Just schedule the GenerateAllFeeds.exe at the desired time and interval.


feedDataDefinition
==================

This is the configuration file that stores the query information of all the fields, filters and operations that can be used on the feedgenerator.

.. code-block:: xml
	:linenos:
	:caption: FeedDataDefinition

	<?xml version="1.0" encoding="utf-8" ?>
	<feedDataDefinition>
	    <DataDefinition From="from Artikelen a inner join ArtikelTeksten at on A.ArtikelID = at.ArtikelID and at.WebsiteLabelID = {labelid} and at.Taalcode = '{languagecode}' inner join ArtikelPrijzen ap on a.ArtikelID = ap.ArtikelID and ap.AantalMinimum &gt;= 0 and PriceListID = {pricelistid}">
	        <DataFields>
	            <DataField Id="ProductLink" Type="ecManager.Marketing.Entities.SingleDataField" Field="'${BaseUrl}/product/detail/'+ CAST(a.ArtikelID AS VARCHAR)" FieldDataType="Text" />
	        </DataFields>
	        <Operations>
	            <Operation Id="Equal" Type="ecManager.Marketing.Entities.SingleValueEqualityOperation" SupportedDataType="Number" Query="{field} = '{value}'" NumberOfValues="1"/>
	        </Operations>
	        <Functions>
	            <Function Id="ToUpper" Type="ecManager.Marketing.Entities.SingleFunction" SupportedDataType="Text" Query="UPPER({field})" />
	        </Functions>                 
	    </DataDefinition>
	</feedDataDefinition>

- feedDataDefinition: is the root node as configured in the config section in the web/ app.config
- DataDefinition: Holds the main query with all the joins and collection node for DataFields, Operations and Functions
- DataFields: collection node for all the datafields
- Operations: collection node for all the operations
- Functons: collection node for all the functions

DataDefinition
==============

Holds the main query

.. code-block:: xml
	:linenos:
	:caption: DataDefinition

	<DataDefinition From="
	    from Artikelen a 
	    inner join ArtikelTeksten at on A.ArtikelID = at.ArtikelID and at.WebsiteLabelID = {labelid} and at.Taalcode = '{languagecode}' 
	    inner join ArtikelPrijzen ap on a.ArtikelID = ap.ArtikelID and ap.AantalMinimum &gt;= 0 and PriceListID = {pricelistid}
	">

- From: the main query
- labelid}: a placeholder that will be replaced with the selected label id
- {languagecode}: a placeholder that will be replaced with the selected language code
- {pricelistid}: a placeholder that will be replaced with the selected pricelist id

The main query should contain joins to all the used datafields.

Fields

.. code-block:: xml
	:linenos:
	:caption: Datafields

	<DataField Id="Image2Link" Type="ecManager.Marketing.Entities.SingleDataField" Field="'${BaseUrl}/CmsData/Artikelen/Fotos/' + SUBSTRING( a.Artikelnummer , 1 , 1 ) + '/' + SUBSTRING( a.Artikelnummer , 2 , 1 ) + '/' +  a.Artikelnummer + '/' +  a.Artikelnummer + '_main_or_2.jpg'" FieldDataType="Text" />
	<Field Id="ProductId" Type="ecManager.Marketing.Entities.SingleDataField" Field="a.ArtikelID" FieldDataType="Number" />
	<Filter Id="ActiveFrom" Type="ecManager.Marketing.Entities.FilterDataField" Field="a.DatumActief" FieldDataType="Date" />        
	<AggregateDataField Id="Options" Type="ecManager.Marketing.Entities.MultipleDataField" FieldDataType="Text" From="FROM Options o INNER JOIN ProductOptions po ON po.OptionID = o.OptionID AND po.ProductID = a.ArtikelID LEFT JOIN OptionTexts ot ON o.OptionID = ot.OptionID  LEFT JOIN OptionTaxes ota ON o.OptionID = ota.OptionID " Where="ot.LanguageCode = '{languagecode}' AND o.OptionGroupID = '{selectvalue}'" >
	    <DataFields>
	        <Field Id="Options.Title" Type="ecManager.Marketing.Entities.SingleDataField" Field="ot.Title" FieldDataType="Text" />
	        <Field Id="Options.Description" Type="ecManager.Marketing.Entities.SingleDataField" Field="ot.Text" FieldDataType="Date" />
	    </DataFields>
	</AggregateDataField>        
	<InFilter Id="Group"  Type="ecManager.Marketing.Entities.InDataField" FieldDataType="Text" Query="a.ArtikelID in (select ga.ArtikelID from Groepen g inner join GroepArtikel ga on g.GroepID = ga.GroepID WHERE {operators} )">
	    <DataFields>
	        <Field Id="Group.Number" Type="ecManager.Marketing.Entities.FilterDataField" Field="g.GroepNummer" FieldDataType="Text" />
	        <Field Id="Group.DateActive" Type="ecManager.Marketing.Entities.FilterDataField" Field="g.DatumActief" FieldDataType="Date" />
	        <Field Id="Group.DateInactive" Type="ecManager.Marketing.Entities.FilterDataField" Field="g.DatumInactief" FieldDataType="Date" />
	        <Field Id="Group.SystemGroup" Type="ecManager.Marketing.Entities.FilterDataField" Field="g.SysteemGroep" FieldDataType="Bool" />
	    </DataFields>
	</InFilter>

- Field: both usable as datafield and filter field
- DataField: only usable as datafield
- Filter: only usable as filter field
- AggregateDataField: for result set returning more then 1 value
- InFilter: for filters that are not directly useable in the result set 
- ${BaseUrl} nant parameter must be replaced by nant before usage

Field, Datafield, Filter:

- Id: unique id for the field used in the drag interface in the cms and for translations (must be unique over all fields)
- Type: type of the field used in the generator to render the correct inputs
- Field: the sequel field used in the query make sure that the field has the correct table prefix  "o.OptionID" o = options table as defined in the DataDefinition and "OptionID" = the column from the options table
- FieldDataType: datatype used in the cms for validation and helpers  

InFilter:

- Query: must contain the matching field and the sub query and a where which contains  "{operators}" 
- DataFields: contains all the fields usable in the subquery
- Field Id: is a composite from the InFilter Id and its own Id example "Group.DateActive"
- Field Field: the sequel field used in the subquery make sure that the field has the correct table prefix  "g.DatumActief" g = Groepen table as defined in the InFilter and "DatumActief" = the column from the Groepen table

AggregateDataField:

- From: the query part "from" here the extra joins are declared
- Where: the query part "where" here the restrictions on the sub query can be made, use "{selectvalue}" to enable restriction on user input

Operations
==========

.. code-block:: xml
	:linenos:
	:caption: Operations

	<Operations>
	    <Operation Id="Equal" Type="ecManager.Marketing.Entities.SingleValueEqualityOperation" SupportedDataType="Number" Query="{field} = '{value}'" NumberOfValues="1"/>
	    <Operation Id="Between" Type="ecManager.Marketing.Entities.MultipleValueEqualityOperation" SupportedDataType="Number" Query="{field} BETWEEN '{value0}' AND '{value1}'" NumberOfValues="2" />
	</Operations>

Operations:

- Id: unique id for the field used in the drag interface in the cms and for translations (must be unique over all fields)
- Type: the type of operation single or multiple values, ecManager.Marketing.Entities.SingleValueEqualityOperation or ecManager.Marketing.Entities.MultipleValueEqualityOperation
- SupportedDataType: the data type where this operator can be used on (Number,Text,Date) not fully supported yet
- Query: the operation query part "{field} = '{value}'"
- NumberOfValues: numer of values to compair example "2" and then the query must contain {value0},{value1}  ""{field} BETWEEN '{value0}' AND '{value1}'"" 
- {field}: this value will be replaced with the field from the datafield  Example "o.OptionId = '{value}'"
- {value}: user input for a single value
- {value0}: user input for a multiple values and this is the first
- {value1}: user input for a multiple values and this is the second	

Functions

.. code-block:: C#
	:linenos:
	:caption: Functions

	<Functions>
	    <Function Id="ToUpper" Type="ecManager.Marketing.Entities.SingleFunction" SupportedDataType="Text" Query="UPPER({field})" />
	    <Function Id="SubstringFunction" Type="ecManager.Marketing.Entities.AggregateFunction" SupportedDataType="Text" Query="(SELECT SUBSTRING((SELECT '{seperator}' + {field}{select} FOR XML PATH('')),2,200000))" />
	    <Function Id="ConcatenateFunction" Type="ecManager.Marketing.Entities.GroupFunction" SupportedDataType="Text" Query="{fields}"  />
	</Functions>

Functions:

- Id: unique id for the field used in the drag interface in the cms and for translations (must be unique over all fields)
- Type: the type of operation single or multiple values, ecManager.Marketing.Entities.SingleFunction, ecManager.Marketing.Entities.AggregateFunction or ecManager.Marketing.Entities.GroupFunction
- SupportedDataType: the data type where this function can be used on (Number,Text,Date) not fully supported yet
- Query
- {field}: the Datafield on which the function will be preformed
- {select}: this is only for aggregate Datafields and will corresponds to the subselect of the aggregate
- {fields}: (TODO: find out what this does)	

General
=======

The following tags can be used though out the feed data configuration and will be replaced with the values before the query will be run on the server.

- {pricelistid}: the chosen pricelist identifier chosen with the pricelist dropdown
- {languagecode}: the language code "nl", "en"
- {labelid}: the chosen label identifier chosen with the label dropdown

Example
=======

.. code-block:: C#
	:linenos:
	:caption: ZanoxSize nodes

	<AggregateDataField Id="Sizes" Type="ecManager.Marketing.Entities.MultipleDataField" FieldDataType="Text" From="FROM Options o INNER JOIN OptionGroups og ON o.OptionGroupId=og.OptionGroupId AND OptionGroupCode = 'SZE' INNER JOIN ProductOptions po ON po.OptionID = o.OptionID AND po.ProductID = a.ArtikelID INNER JOIN _sw.ProductOptionStock pos ON pos.OptionID = o.OptionID AND pos.ProductID = a.ArtikelID LEFT JOIN OptionTexts ot ON o.OptionID = ot.OptionID " Where="ot.LanguageCode = '{languagecode}' AND po.PriceListID = {pricelistid} for XML PATH('size'), TYPE" >
	    <DataFields>
	        <Field Id="Sizes.ZanoxSizeInfo" Type="ecManager.Marketing.Entities.SingleDataField" Field="ot.Title, CAST((CONVERT(float, po.PriceInclTax) /100) AS VARCHAR) as price, CAST((CONVERT(float, po.FromPriceInclTax) /100) AS VARCHAR) as fromprice, CONVERT(varchar, pos.StockAtWareHouse ,0) as stockamount, CASE WHEN pos.StockAtWareHouse > 0 THEN '1' ELSE '0' END as stockstatus" FieldDataType="Text" />
	    </DataFields>
	</AggregateDataField>
