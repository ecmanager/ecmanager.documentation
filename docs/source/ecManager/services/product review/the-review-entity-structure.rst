###########################
The review entity structure
###########################

The ReviewService and it's entities are designed for review items. The service manages review entities. For the CMS is the ReviewReport functionality available. This allows you to generate an overview of multiple reviews.

.. image:: /_static/img/services/product-review/product-review.png
	:scale: 90%
	:align: center
