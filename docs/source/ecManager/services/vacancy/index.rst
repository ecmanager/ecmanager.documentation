#######
Vacancy
#######

.. rubric:: Introduction

The VacancyService is a service for managing VacancyGroups and Vacancy items.

.. toctree::
   :maxdepth: 1

   the-vacancy-entity-structure


	