############################
The vacancy entity structure
############################

The VacancyService and it's entities are designed for vacancy items and vacancy groups on a page. The service manages Vacancy groups and Vacancy entities.

.. image:: /_static/img/services/vacancy/vacancy.png
	:scale: 90%
	:align: center
