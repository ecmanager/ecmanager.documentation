########
Overview
########

.. rubric:: Introduction

ecManager offers a flexible system for file storage. This allows the implementation party to use different types of storage and have singular way of accessing it.

.. toctree::
   :maxdepth: 1
   

The storage service is responsible for all file IO within ecManager. All files within ecManager have a virtual path at which it can be accessed.
The reason no absolute paths are used is because ecManager want to offer a uniformal way of accessing files regardless of the data source that the data resides in.
Your implementation does not have to worry about the underlying storage solution.