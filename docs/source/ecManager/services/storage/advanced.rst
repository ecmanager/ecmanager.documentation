########
Advanced
########

Caching
=======
The storage flow has a built-in caching mechanism which can cache calls. Caching is done for the 
following methods:

  - Exists
  - FindByExtension
  - Search

When configuring your storage providers you should consider the cost of caching. For example, the 
``FileSystemStorageProvider`` generally is really fast when doing an Exist() check. However for 
providers which rely on making calls to an external service(e.g. the AzureBlobStorageProvider) it 
may save time to retrieve values from the cache.

The performance benefit of caching storage calls heavily depends on the configuration of your system.
The hardware in your system as well as which modules you use for caching and storage all have an 
impact on whether caching calls will increase your performance.

Because of this we advise to start off without caching and enable caching when performance is bad.
Also check the documentation for the modules you are using to see how you can optimize them.

.. warning::

  Be careful when enabling caching for storage providers which share their backing filesystem with 
  other systems. If the cache isn't properly cleaned up when a file is moved, deleted or added, it 
  can result in errors in your application.