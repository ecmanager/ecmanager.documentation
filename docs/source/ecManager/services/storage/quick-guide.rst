###########
Quick guide
###########

Introduction
============
The ``StorageService`` is what you use to interact with storage when using the ecManager framework.
This page will describe what you need to get started when dealing with any kind of storage.

Storage providers
=================
Storage provider modules are the essence of the ecManager file IO. Every module provides the IO for 
a single storage solution.

The ecManager framework currently offers two storage providers:

#) `FileSystem provider <../../modules/file%20system%20storage%20provider/index.html>`__: uses file IO for disk based storage.
#) `AzureBlobStorage provider <../../modules/azure%20blob%20storage%20provider/index.html>`__: uses Azure blob storage for file IO.

Registering storage providers
=============================
To configure which provider you want to use you must register them in several different places. The 
first one is the ``modules.config`` file. Here you need to register the provider class. Be sure to 
configure the ``name`` attribute. The following example shows how to register the FileSystem handler:

.. code-block:: xml
	:linenos:
	
	<component
	  type="ecManager.Modules.StorageProvider.FileSystem.FileSystemStorageProvider, ecManager.Modules.StorageProvider.FileSystem"
	  service="ecManager.Common.Interfaces.IStorageProvider, ecManager.Common"
	  name="filesystemprovider">
	</component>

It is possible to register multiple providers, even of the same type. They are identified by ``name``.
    
.. warning::

  Be careful to not use the same name twice. This might result in unexpected behaviour.
  
Configuring storage providers
=============================
Once all the storage providers you wish to use are registered, you need to configure which provider 
will provide which files. This is done by adding the following section to your ``web.config`` or
``app.config``:

.. code-block:: xml
	:linenos:
    
	<configuration>
	  <configSections>
	    <section name="ecManager.Storage" type="ecManager.StorageManager.Configuration.StorageConfigurationSection, ecManager.StorageManager"/>
	    // Other config sections
	  </configSections>
	
	  <ecManager.Storage>
	    <providers>
	      <provider name="filesystemprovider" pattern="Config/"  patternType="StartsWith" cachingEnabled="true" />
	      <provider name="azureprovider" pattern="Videos/"  patternType="StartsWith" cachingEnabled="true" />
	    </providers>
	  </ecManager.Storage>
	</configuration>
  
The attributes in this XML are as follows:

+------------------+-------------------------------------------------------------------------------+
| Attribute        | Description                                                                   |
+==================+===============================================================================+
| name             | Name of the storage handler instance, should match the name specified in your |
|                  | module.config file                                                            |
+------------------+-------------------------------------------------------------------------------+
| pattern          | Pattern to match file path against                                            |
+------------------+-------------------------------------------------------------------------------+
| patternType      | Type of matching to do. Can be: StartsWith, EndsWith or Regex                 |
+------------------+-------------------------------------------------------------------------------+
| cachingEnabled   | Value indicating if caching should be applied to files matching this pattern  |
+------------------+-------------------------------------------------------------------------------+

Determining which files go to which storage
===========================================
To determine which files go where, the pattern configured in the ``web.config`` or ``app.config`` is 
used. When a file path matches more than one pattern the pattern that is matched first is used. The 
order in which the provider elements are processed is top to bottom.

When we take our example above this means that the instance with name filesystemprovider is checked 
first and the instance with name azureprovider is checked second.

.. hint ::

  We advise you use a "catch all" pattern as the last storage provider, to ensure there is always a 
  provider which matches a file. This can be done by using pattern "" and patterntype "StartsWith".
 
Searching for files
===================
In order to search for files, you can use the ``Search`` method. This method accepts a search task 
as argument. The following example shows how to search for files:

.. code-block:: C#
	:linenos:
	
	var storageService = new StorageService(...);
	var searchTask = new StorageSearchTask 
	{
	    Path = new Uri("documents/", UriKind.Relative),
	    Filter = "*mysheet.xls",
	    SearchOptions = StorageSearchOptions.AllDirectories,
	    SearchInclusions = StorageSearchInclusions.Files
	}
	
	var result = storageService.Search(searchTask);

File name filters
-----------------
There are two wildcard characters you can use when searching for files. The first one is ``?``, the 
second one is ``*``. The question mark means one or zero characters. The asterisk means many or zero 
characters. These can be combined to search for files. Here are a few examples:

.. code-block:: C#
    :linenos:

    "*myfile.ai"          // Finds both "images/myfile.ai" as "images/2016-04-12.myfile.ai" but NOT "images/myfile.ait"
    "*spreadsheet.xls?"   // Finds "documents/spreadsheet.xls" and "documents/spreadsheet.xlsx"
    "spreadsheet.x*"      // Finds "spreadsheet.xls" and "spreadsheet.xlsx"

.. warning:: The filters that are applied are based on Microsoft System.IO implementation. You can 
    read more about any quirks in the "Remarks" section `here <https://msdn.microsoft.com/en-us/library/wz42302f(v=vs.110).aspx>`_.