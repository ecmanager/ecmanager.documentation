########
Overview
########

This section will describe the Redirects provided by ecManager. The service is 
responsible for managing redirects. The actual redirects are performed with the `SEO HTTP Redirect module <../../modules/seo%20http%20redirects/index.html>`_ or in the implementation e.g. fundatie.

Visit the `Quick guide <quick-guide.html>`_ page to read more on how to get started with the redirects.