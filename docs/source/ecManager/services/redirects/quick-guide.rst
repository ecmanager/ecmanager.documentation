###########
Quick guide
###########

Introduction
============
This page gives you an introduction on how to use the ecManage redirects. An ecManager redirects triggers an HTTP Redirect.
There are 3 types of redirects:

	* URL
	* Pattern
	* Search

The first two types are handled in the `SEO HTTP Redirect module <../../modules/seo%20http%20redirects/index.html>`_.
The redirects are performed based on a part of the url (pattern type) or the full url (url type). 

The third type (Search) will be handled by the implementation e.g. fundatie in the search module.
The search terms will be saved in the pattern field of the redirect. To be able to redirect to a language specific page from the pattern the language needs to be included in the pattern.
The following structure is used: **searchTerm**:**LanguageCode** for example: shirt:nl or multiple words: red shirt:nl

.. note:: The structure as shown above is an example, so can be different.

.. note:: It's important that when searching for search redirects exact match search will be used with " (quotation mark).
