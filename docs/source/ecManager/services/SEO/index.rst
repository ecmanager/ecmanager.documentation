###
SEO
###

.. rubric:: Introduction

This section will explain how you can manage the SEO (Search Engine Optimalization) settings available in the ecManager framework.

.. toctree::
   :maxdepth: 1

   seo-design