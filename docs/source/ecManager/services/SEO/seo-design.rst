##########
SEO design
##########

User interface
==============

There are a few interaces for a SEO section as described in SEO Producten.
We will take these as an example.

There are three interfaces

- The list view
	- This view shows the SEO data in a list. The URL will be displayed in this part.
- The detail view
	- This view allows the user to create a SEO specific configuration for an entity.
- The defaults view
	- This view allows the user to create a default SEO configuration.
	- All entities without a specific configuration will get SEO data based on this configuration.

The detail view and the defaults view will have a similar interface.
In this view you will be able to drag and drop your URL.
The user is given a set of blocks which he can use to form his URL.

In case you are editing SEO settings for a specific product, the blocks will be enriched with data that from the entity.
This allows the user to get a direct preview of the URL that will be formed.
In case you are editing the default SEO settings, the user is able to project the current configuration on a entity.

Default, there is no text fallback for the generation of SEO. This can be enabled using the FallbackLabels option in the CMS configuration. 
Read more about the configuration of the CMS in `Configuring the ecManager CMS <../../introduction/configuring-ecManager/configuring-the-ecManager-CMS.html>`_

Configuration
-------------

There are two configurations for SEO.

1) The default SEO configuration.
2) The entity specific configuration

.. rubric:: Defaullt SEO configuration

The configuration will change a few ways.
We will start by looking at the original SEO config.

.. code-block:: C#
	:linenos:
	:caption: Original SEO Config

	<?xml version="1.0" encoding="utf-8" standalone="yes"?>
	<!--
	This file is managed through the ecManager interface using the SEO Defaults
	-->
	<Config>
	  <SeoControl> <!-- A Seo definition for an entity -->
	    <Name>5</Name> <!-- ID 5 means Combination [DNZ.EP.SeoPage] -->
	    <Robot>0</Robot>
	    <ID>0</ID>
	    <WebsiteLabelID>1</WebsiteLabelID>
	    <SeoSubControls>
	      <SeoSubControl>
	        <Name>1</Name> <!-- ID 5 means Url[DNZ.EP.SeoControlType] -->
	        <Items>
	          <Item>
	            <Type>16</Type> <!-- ID 16 means CombinationName[DNZ.EP.SeoItemType] -->
	            <ID>0</ID>
	            <Texts />
	          </Item>
	          <Item>
	            <Type>17</Type>
	            <ID>0</ID>
	            <Texts />
	          </Item>
	        </Items>
	      </SeoSubControl>
	      <SeoSubControl>
	        <Name>2</Name>
	        <Items />
	      </SeoSubControl>
	      <SeoSubControl>
	        <Name>3</Name>
	        <Items />
	      </SeoSubControl>
	      <SeoSubControl>
	        <Name>4</Name>
	        <Items />
	      </SeoSubControl>
	    </SeoSubControls>
	  </SeoControl>
	</Config>

We will modify this configuration to make it more readable.
A suggested layout is the following:

.. code-block:: C#
	:linenos:
	:caption: Renewed SEO Config

	<?xml version="1.0" encoding="utf-8" standalone="yes"?>
	<SeoConfiguration>
	    <EntitySeoConfigurations type="ecManager.Catalog.Entities.Product">
	        <EntitySeoConfiguration> <!-- The type of the entity, replaces the Name-element of SeoControl-->
	            <RobotIndexHint>Index,Nofollow</RobotHint>
	            <EntityId>0</EntityId> <!-- The ID of the entity, replaces ID -->
	            <SeoPageElements> <!-- Replaces SeoSubControls -->
	                <SeoPageElement name="Url"> <!-- Replaces SeoSubControl, type replaced the Name of the SeoSubControl -->
	                    <DataElements>
	                        <ProductNameElement separator="/" />
	                        <ProductShortDescriptionElement separator="-" />
	                        <ProductsTagsElement>
	                            <Top>5<Top>
	                        </ProductsTagsElement>
	                        <AttributeValueElement attributeId="12" />
	                        <FreeTextElement>
	                                <Text language="nl" value="My-FreeText" />
	                        </FreeTextElement>
	                    </DataElements>
	                </SeoPageElement>
	                <SeoPageElement name="PageTitle">
	                </SeoPageElement>
	                <SeoPageElement name="PageDescription">
	                </SeoPageElement>
	                <SeoPageElement name="PageKeywords">
	                </SeoPageElement>
	                <SeoPageElement name="CanonicalUrl">
	                    <DataElements>
	                        <ProductGroupElement separator="/" />
	                        <ProductNameElement separator="-" />
	                        <AttributeValueElement attributeId="12" separator="/" />
	                    </DataElements>
	                </SeoPageElement>
	            </SeoPageElements>
	        </EntitySeoConfiguration>
	    </EntitySeoConfigurations>
	</SeoConfiguration>

EntitySeoConfiguration-elements are now grouped by type. This means all the configurations for products are grouped together.
The main reason to convert to this new format is to make it more readable.
The elements have been renamed, and instead of integers we will now use types for the identification of elements. 
The EntitySeoConfiguration-element is not related to a language, but the FreeTextElement inside can be translated. 

Elements inside a PageElement are the SEO fields.
A SEO field will be enriched with a separating character.
This character is used to separate it from the next SEO fields. 
For example: SEO field 1 is followed up by SEO field 2. These elements will be separated by the separator from SEO field 1. 

In the old implementation is wasn't possible to select a separator.
Every element was separated  using a  '-'.

.. rubric:: Entity Specific configuration	

The SEO specific configuration is similar to the defaults configuration.
The difference is that the EntityId-elements are filled with the actual entity ID, instead of 0.

Architecture
------------

.. image:: /_static/img/services/seo/seo.png
	:scale: 90%
	:align: center

.. rubric:: Seo Logic methods

The SEO Logic will have these methods:

- ApplySeo(Entity entity) : SeoGenerateResult
	- This method updates the entity with new SEO values.
	- All the data should be present in this entity. 
- UpdateSeo(EntityId id) : SeoGenerateResult
	- This method fetches the entity with the required flags, and then updates the SEO data. 
- UpdateSeo(IEnumerable<EntityId> ids) : Dictionary<EntityId,SeoGenerateResult>
	- This method fetches the entity with the required flags, and then updates the SEO data. 
- GetSeoFields(Type EntityType, EntityId? entityId)
	- The first argument is the type of the entity we need the fields for. 
	- The entity ID is an optional element to generate preview data.
	- This method returns a collection of Seo fields that can be used while building an URL.
	- Note: This method is ment to be used in the UI, so you can preview a URL generated for an entity when editing the default settings. 
- SaveSeoConfiguration(SeoConfiguration configuration)
	- Method for saving the SEO configuration for an entity type.
- GetSeoConfiguration(Type EntityType, EntityId? entityId) : SeoConfiguration
	- Method for fetching the SEO configuration for an entity type.
	- In case the entity id isn't null, we will load SEO specific values.
	- Otherwise we will load the default configuration.	

.. rubric:: SEO fields

A SEO field is a building block that is used for building the URL structure.
The fields are determined by the module, allowing a developer to define his custom fields for the URL.

Fields can be marked as mandatory, but this will not be implemented yet.
If a field is mandatory, the data should be present while generating a URL.
In case this data is missing, the SEO url should not be generated.

All SEO fields must derive the SeoFields class. 

.. rubric:: Services for entities with SEO

In case a service works with an entity that has SEO data, the following interface should be implemented: ISeoService.
The ISeoService has one boolean property, UpdateSeoData.
When the service updates an entity, it should also trigger a ApplySeo on the entity in case this property is set to True.

.. rubric:: Custom element serialization

In the configuration section we have shown the new layout of the configuration.
The PageElement-element has a set of sub-items which have a type-specific element name.
By default, C# handles XML serialization of derived classes in a different way.
For an example, see the section called Serializing Derived Classes on MSDN.

In order to achieve these custom element names, we can use XmlAttributeOverrides.