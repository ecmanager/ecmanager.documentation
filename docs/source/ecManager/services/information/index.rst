###########
Information
###########

.. rubric:: Introduction

The InformationService is a simple service for managing InformationItem entities.

.. toctree::
   :maxdepth: 1

   the-informationitem-entity-structure
	

