The InformationItem entity structure
####################################

The InformationService is for displaying content pages on a website. The service manages InformationItem entities and Images. The service implements a ImageService for structured images or images can be part of the Introduction or Description as inline elements.

.. note:: An InformationItem can be referenced from a NavigationItem using the NavigationItem.NavigationTypeId which means a InformationItem can be part of the navigation.

The diagram below displays the InformationItem entities and the relation from NavigationItem.

.. image:: /_static/img/services/Information/information.png
	:scale: 90%
	:align: center
