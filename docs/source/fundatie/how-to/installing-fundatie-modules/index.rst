###########################
Installing Fundatie modules
###########################

Introduction
============
Fundatie is a project which consists of pluggable modules. There are a large number of modules 
available to be installed. A module is a package that contains all the logic that is required to 
have a functioning ecManager 'block' that can be added to a navigation item in the CMS.

There are also modules that are not ecManager blocks, but provide useful features for Fundatie. The
``SitemapHandler`` module is an example of this. The SitemapHandler allows a project based on 
Fundatie to generate sitemaps.

This page will describe how this process works.

Installing a module
===================
Fundatie modules are distributed through NuGet. ecManager uses myget.org instead of nuget.org for 
publishing packages. Each module can contain the following components:

* Class library containing the controller, view models etc.
* Views (placed in ``FundatieViews``)
* Javascript (placed in ``FundatieScripts``)
* ControlConfigs
* SQL migration scripts (placed in ``FundatieMigrations``)

The views and scripts are just examples that can be used to get a module working. We always recommend
to customize these to your liking.

.. tip:: We recommend to check all the content in to source control so you can see when these files are changed in a later version. 

.. tip:: Packages can be installed with `NuGet <https://docs.microsoft.com/en-us/nuget/guides/install-nuget>`_ or `Paket <https://fsprojects.github.io/Paket/>`_.

Moving views
============
The views which are placed in the ``FundatieViews`` directory should be copied to the 
``Views\Blocks`` directory of your web project. If you leave the old version view in the 
``FundatieViews`` directory you will see what changes when you update  

Moving scripts
==============
The Javascript files which are placed in the ``FundatieScripts`` directory should be copied to the
``Scripts\default\modules`` directory.

Moving ControlConfigs
=====================
This step is only required for blocks. If the installed module isn't a block you can skip this step.

The controlconfigs for your block are placed in the ControlConfig folder of the nuget package.
You should copy all the ControlConfig files to the ``ControlConfig`` folder in your ``CmsData``.

As an example, the product lister block has it's ControlConfig in:
``Packages\Fundatie.Web.Modules.ProductLister\ControlConfig\ProductLister.xml`` 

which should be copied to
``CmsData\ControlConfig\ProductLister.xml``

This should be done for all blocks that you have installed in your webshop.

A script was written for the starterkit which copies all files ControlConfig files from the packages to CmsData. 
This script will **not** copy files which already exist at the destination.
You can find this script in ``Tools\CopyControlConfigs\CopyControlConfigs.ps1``

Executing migrations
====================
Some modules include SQL migration scripts which is necessary to get the module working. Execute all
the scripts which are found in the ``FundatieMigrations`` directory.

Resources
=========
The classes and views that are distributed use resources which are not contained in the same NuGet 
package. These can be obtained from the StarterKit repository as described `here <../../quick-guide/quick-guide.html>`__.
You should change these resources to match your project and add translations for the required 
languages.

Adding the block type
=====================
In order to use the new module, you should add a new row the ``BlokSoorten`` table.

.. code-block:: sql
	:linenos:

	INSERT INTO [dbo].[BlokSoorten]([BlokSoortID], [CmsOmschrijving], [Naam])
	VALUES(1, 'Checkout basket', 'CheckoutBasket')

We recommend to store these scripts, so they are reusable across different environments. You could 
do this by creating a database project in Visual Studio and keeping them in there.

Adding a block to a navigation item
===================================

.. figure:: ../../../_static/img/fundatie/how-to/installing-fundatie-modules/blocktypes.png
	:alt: Blocktypes
	:align: right

After adding the block type, it will be possible to place the new block on a navigation item in the
CMS. Navigate to the navigation item on which you want the new block to appear and click ``New block``.
In the dropdown you will now be able to select the new block, as shown on the image to the right.

Next steps
==========
Now that you know how to install a Fundatie module, you can continue reading about 
`how you can create your own modules <../creating-fundatie-modules/index.html>`__ or 
`how you can extend existing modules <../extending-fundatie-modules/index.html>`__.