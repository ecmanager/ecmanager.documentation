######################################
Use Fundatie BlockParameters Generator
######################################

Introduction
============
This page describes how to use the Fundatie BlockParameters Generator.

Arguments
=========
When calling the ``Fundatie.BlockParameters.Generator.exe`` program, it requires multiple
parameters. There are 4 parameters to configure:

1. namespace (example: ``Fundatie.Web.Modules.AccountAddressBook``)
2. projectFolder (example: ``D:\Projects\fundatie\fundatie.web\``)
3. inputFolder, optional (default: ``ControlConfig``)
4. outputFolder, optional (default: ``BlockParameters``)