#######################
Search indices in Azure
#######################

Introduction
============
When using the Lucene search implementation in ecManager, you will need search indices in order to 
return results. ecManager offers tooling to create those indices. When the Azure platform is used as 
a host, a couple of special steps need to be taken to create and consume indices. The goal of this
page is to get a `triggered webjob <https://docs.microsoft.com/en-us/azure/app-service-web/web-sites-create-web-jobs>`_
running in Azure. 

.. warning:: 

	Placing the search indices in blob storage as described here has a limitation because the indices 
	are cached locally. This means that the size of the indices can never be larger than the size 
	which the web app allows.

Finding the tools
=================
The tools for creating a Lucene index can be found in the downloads section of the 
`Fundatie.StarterKit repository <https://bitbucket.org/ecmanager/fundatie.starterkit/downloads>`_ 
under the name ``AzureLuceneSearchTools``.

Configuring the tools
=====================
Each of the three tools have a configuration file which requires the 
`typical ecManager configuration <../../../ecManager/introduction/configuring-ecManager/index.html>`_.
Make sure each tool uses the correct database. In addition to the regular configuration file, some
tools have their own specific configuration file. The following configurations should be used:

.. code-block:: XML
	:linenos:
	:caption: SearchFeed.config

	<?xml version="1.0" encoding="utf-8" ?>
	<searchFeed useIndenting="true" outputDirectory="D:\home\data\search\runneroutput">
	  <!-- Other configuration -->
	</searchFeed>

.. code-block:: XML
	:linenos:
	:caption: FeedTransformation.config

	<?xml version="1.0" encoding="utf-8" ?>
	<feedTransformation
	  InputDirectory="D:\home\data\search\runneroutput\"
	  OutputDirectory="D:\home\data\search\transformeroutput\"
	  XsltDirectory="D:\home\site\wwwroot\app_data\jobs\triggered\searchindex\transformer\xslt" />

.. code-block:: XML
	:linenos:
	:caption: app.config for indexer

	<?xml version="1.0" encoding="utf-8" ?>
	<configSections>
	  <!-- Other config sections -->
	  <section name="ecManager.Search.LuceneNET" type="ecManager.Search.LuceneNET.Configuration.ConfigSection, ecManager.Search.LuceneNET" />
	</configSections>

	<connectionStrings>
	  <!-- Value is the connection string to the storage account where the indices will be created. -->
	  <add name="AzureConnectionString" connectionString="" />
	</connectionStrings>

	<ecManager.Search.LuceneNET inputDirectory="D:\home\data\search\transformeroutput" outputDirectory="D:\home\data\search\transformeroutput" complexAddModifications="true" storageConnectionStringName="AzureConnectionString" />


The root of the tools has a file named ``settings.job``. This contains a CRON expression which 
defines the schedule for the webjob. Read the `Azure documentation <https://docs.microsoft.com/en-us/azure/app-service-web/web-sites-create-web-jobs>`_ 
for more details. 

Deployment
==========

.. figure:: ../../../_static/img/fundatie/how-to/azure-search-index/tree.png
	:alt: Directory tree
	:align: right

The tools must be deployed to an Azure Web App as a webjob. This can be any Web App, so it 
can also be the same as your website. The tool needs to be run as a triggered webjob, so by 
convention it has to be deployed to the following path: ``/site/wwwroot/app_data/jobs/triggered`` 
and the name of the webjob should be ``searchindex``. An example of the directory structure can be 
seen on the right.

Of course you can use any deployment tool for the actual deployment of the tools, like FTP or 
WebDeploy.

.. note:: The value for "Always On" should be on for the Web App, so the triggered webjob can run correctly.

Inspecting the webjob
=====================
To see results of the tool, you need to check the App Service in the Azure portal. In the App 
Service, go to the webjobs tab to see the status, as shown below.

.. figure:: ../../../_static/img/fundatie/how-to/azure-search-index/webjoboverview.png
	:alt: Webjobs overview
	:align: center

You can click on ``Logs`` to see the actual output of the tools.

.. warning:: 

	Because of `an issue <https://github.com/Azure/azure-sdk-for-net/issues/109>`_ in the Azure SDK, 
	you must create the blob containers yourself. The name must be wl-<labelid>-<language> and 
	wl-<labelid>-<language>-spellcheck.

Consuming the indices
=====================
To consume your indices from Azure, you should add or replace the registration of 
``IDirectoryProvider`` in ``Modules.config`` with the following:

.. code-block:: XML
	:linenos:

	<component
	  type="ecManager.Search.LuceneNET.Azure.AzureDirectoryProvider, ecManager.Search.LuceneNET.Azure"
	  service="ecManager.Search.LuceneNET.Configuration.DirectoryProviders.IDirectoryProvider, ecManager.Search.LuceneNET"
	  instance-scope="single-instance">
	</component>

``Web.config`` should be modified with the following:

.. code-block:: XML
	:linenos:

	<connectionStrings>
	  <!-- Value is the connection string to the storage account where the indices are located. -->
	  <add name="AzureConnectionString" connectionString="" />
	</connectionStrings>

	<!-- Search indices will be cached in the output directory. -->
	<ecManager.Search.LuceneNET
	  inputDirectory=""
	  outputDirectory="D:\local" 
	  complexAddModifications="true" 
	  storageConnectionStringName="AzureConnectionString" />