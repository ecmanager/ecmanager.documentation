#######################
Creating a CDN in Azure
#######################
.. warning::
  Be aware that it may take some time before the CDN changes are propegated and the resources are available.
  When adding new resources they should be availible instantly unless the resource has been requested before on the CDN. 
  In that case it will take until the caching period has expired or the cache is purged. This also applies to content that is changed (e.g. a new image is uploaded).

When using Azure blob storage one of the downsides is that it's not made for performance. 
This can result in a webpage taking longer to load. 
In order to increase performance we can use a CDN to serve static content from blob storage.
For more information on Azure CDN and blob storage please see the `Microsoft documentation <https://azure.microsoft.com/en-us/documentation/articles/cdn-create-a-storage-account-with-cdn/>`__.
  
===========
Create CDN
===========

To create a CDN in Azure you will need to create a new resource. 
This is done by clicking on the plus in the top left of the screen.

.. image:: /_static/img/fundatie/how-to/azure-cdn/01.png

Next you will have to search for cdn in the new blade that has opened. A suggestion for CDN will be shown an you can click on this.

.. image:: /_static/img/fundatie/how-to/azure-cdn/02.png

Next select CDN from the list as shown below.

.. image:: /_static/img/fundatie/how-to/azure-cdn/03.png

Fill in you preferences such as which resource group you want to create this resource in or how you want to name this.
For pricing tier you should select S2 Standard Akamai.

+-------------------------+---------------------------------------------------------------------------------------------------+
| Property                | Description                                                                                       |
+=========================+===================================================================================================+
| Name                    | A name for the CDN. This does not influence the cdn url you will have.                            |
+-------------------------+---------------------------------------------------------------------------------------------------+
| Subscription            | Which subscription will be billed for this resource.                                              |
+-------------------------+---------------------------------------------------------------------------------------------------+
| Resource group          | `More info <https://azure.microsoft.com/nl-nl/documentation/articles/resource-group-portal/>`__.  |
+-------------------------+---------------------------------------------------------------------------------------------------+
| Resource group location | Region of your resource group.                                                                    |
+-------------------------+---------------------------------------------------------------------------------------------------+
| Pricing tier            | Type of CDN to use.                                                                               |
+-------------------------+---------------------------------------------------------------------------------------------------+


.. image:: /_static/img/fundatie/how-to/azure-cdn/04.png

======================
Create endpoint
======================
You can use a CDN for several different resources within Azure. To differentiate between these resources we have endpoints. 
An endpoint is an access point for your CDN which serves data from a single source(e.g. website or storage). 
We will need to create one for our storage account. 
To do this we will need to navigate to the newly created CDN.

On the overview page click on the + icon to create a new endpoint.

.. image:: /_static/img/fundatie/how-to/azure-cdn/05.png

Fill in the fields. You should leave the origin path field empty.

+-------------------------+---------------------------------------------------------------------------------------------------+
| Property                | Description                                                                                       |
+=========================+===================================================================================================+
| Name                    | Subdomain of azureedge.net to use to serve content.                                               |
+-------------------------+---------------------------------------------------------------------------------------------------+
| Origin type             | Storage (you can use other settings if you want to serve your website from CDN,                   |
|                         | hower this is not covered here).                                                                  |
+-------------------------+---------------------------------------------------------------------------------------------------+
| Origin path             | Path on the origin from which the cdn should cache files. If you only want to serve               |
|                         | files which are in http://yourdomain.com/cdnfiles/ you should fill in "cdnfiles".                 |
+-------------------------+---------------------------------------------------------------------------------------------------+
| Origin host header      | The origin header the CDN should reply with.                                                      |
+-------------------------+---------------------------------------------------------------------------------------------------+
| Protocol                | Protocol to serve the files over.                                                                 |
+-------------------------+---------------------------------------------------------------------------------------------------+


.. image:: /_static/img/fundatie/how-to/azure-cdn/06.png

Finish creating the endpoint by clicking Add.

======================
Configure endpoint
======================

Navigate to the new endpoint and click on the name.

.. image:: /_static/img/fundatie/how-to/azure-cdn/07.png

Select the storage account you want to use from the origin hostname dropdown.

.. image:: /_static/img/fundatie/how-to/azure-cdn/08.png

Click save to apply the settings.