#########################
Creating Fundatie modules
#########################

Introduction
============
This page will describe how you can make your own Fundatie modules in a way that the module is
re-usable across multiple projects based on Fundatie. These steps are how we **recommend** to do it,
but you can arrange the structure of your module project according to your own preference.

Creating a new project
======================
Seperating each module in their own project is a good practice because it groups the components of a
module and adding/removing it can be as simple as changing a reference. Therefore, a new class 
library should be created for the new module. Internally we use the following convention as project 
structure.

.. tip:: To prevent errors make sure your project targets .NET framework 4.5.2.

Autofac module
==============
Fundatie uses `Autofac <http://docs.autofac.org/en/latest/>`__ for dependency injection. The project 
should contain a class that inherits from ``Autofac.Module`` in which you do all the registrations 
required for that specific Fundatie module. It can look like this:

.. code-block:: C#
	:linenos:

	public class AutofacModule : Autofac.Module
	{
	    protected override void Load(ContainerBuilder builder)
	    {
	        // Registers all the controllers contained in this assembly.
	        builder.RegisterControllers(Assembly.GetExecutingAssembly()).PropertiesAutowired();
	        // Other autofac registrations
	    }
	}

.. tip:: You can read more about Autofac modules `here <http://docs.autofac.org/en/latest/configuration/modules.html>`__.

In the main web project, assembly scanning is used to find these autofac modules. To filter the 
assemblies that require module registration, the ``AutoFacActivator`` attribute from the 
``Fundatie.Core.Modules`` namespace is used. The following should be added to the ``AssemblyInfo.cs`` 
file of the module project:

.. code-block:: C#

	[assembly: AutoFacActivator]

BlockParameters
===============
A block in the context of ecManager requires block parameters. You can read more about the concept 
of block parameters `here <../../../ecManager/services/blocks/configuring-block-parameters.html>`__.
If the block does not require any interaction with the CMS then you can just create an empty 
block parameter file like this:

.. code-block:: xml

	<?xml version="1.0" encoding="utf-8"?>
	<UserControlParameters>
	</UserControlParameters>

The file should be used for two things:

#) Copying it to the ``CmsData\ControlConfig`` directory.
#) Generating a C# class using the ``BlockParameterGenerator``.

These tasks can be easily automated using a pre-build event. The ``BlockParameterGenerator`` is 
available as a NuGet package with the id ``Fundatie.BlockParameters.Generator``, this should be 
installed in your module project. This package contains a command line utility with the following 
usage:

.. code-block:: batch

	Fundatie.BlockParameters.Generator.exe <project name> <project directory> <source directory> <target directory>

The `Fundatie.StarterKit <https://bitbucket.org/ecmanager/fundatie.starterkit>`__ repository 
contains an example of how you can automate this process.

Controller
==========
The controller is the entry point of your module. A navigation can contain multiple blocks. Each 
block has a controller with an index method. When a user navigates to an URL associated with a 
navigation item, those index methods will be invoked to render a partial view. Controllers should 
inherit from the abstract class ``ABlockController<TViewModelType, TBlockParameterType>``.
``TViewModelType`` is the type of the view model associated with this class and 
``TBlockParameterType`` is the class that was generated as described in the `BlockParameters`_ 
chapter.

Because the controller is resolved using dependency injection, you can resolve types in the 
constructor using the constructor injection concept. The following example shows what the controller
can look like:

.. code-block:: C#
	:linenos:

	public class ExampleController : ABlockController<ExampleViewModel, ExampleBlockParameters>
	{
	    private readonly IExampleLogic _exampleLogic;
	    
	    public ExampleController(IBlockLogic blockLogic, ILogger logger, IExampleLogic exampleLogic) : base(blockLogic, logger)
	    {
	       if (exampleLogic == null) throw new ArgumentNullException(nameof(exampleLogic));
	       
	       _exampleLogic = exampleLogic;
	    }
	    
	    public ActionResult Index(BlockViewModel blockViewModel)
	    {
	        if (!ValidateBlock(blockViewModel))
	        {
	            return new HttpNotFoundResult();
	        }
	        
	        var parameters = GetParameters(blockViewModel);
	        var model = CreateViewModel();
	        // Fill your model.
	        return PartialView("_ExamplePartial", FillCustomData(model, parameters, blockViewModel));
	    }
	}

If the ``ExampleLogic`` from the previous code example is only used within this module we recommend
to register it in the ``Autofac.Module``, otherwise register it in your web project.

In the return statement, the ``FillCustomData`` method is used so when someone wants to extend from 
this new block, you can easily modify the model. See the `extending Fundatie modules documentation <../extending-fundatie-modules/index.html>`__ 
for more information.

Views
=====
A controller requires a view. The view that is called by a controller should be placed in the main
web project under the ``Views\Blocks`` directory. Like any ASP.NET MVC application, the view should
use the model that you defined in your controller. 

Adding the block type
=====================
Just like when you install an existing module, you should also add a new blocktype when you create a
new module. `This chapter <../installing-fundatie-modules/index.html#adding-the-block-type>`__ 
describes how to do that. 