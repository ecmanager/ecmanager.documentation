##################
Debugging Fundatie
##################

Introduction
============
Because Fundatie is not only an example project but also the core of any project based on Fundatie,
as a developer you will want to debug the Fundatie NuGet packages to see what behaviour happens 
when making calls to Fundatie or overriding certain features.

Debugging NuGet packages
========================
Fundatie is distributed through NuGet. To allow you to debug the source of these packages, we also
provide so-called Symbol packages. These packages do not get downloaded by NuGet, instead you need 
to instruct Visual Studio to search online for the symbols.

Adding a new symbol location
============================
Within Visual Studio, go to ``Tools -> Options -> Debugging -> Symbols`` and add a new location.
This should be the ecManager NuGet feed URL with ``/symbols/`` appended.

.. figure:: ../../../_static/img/fundatie/how-to/debugging-fundatie/symbolsource.png
	:alt: Symbol locations 
	:align: center

	Symbol locations

Next up, go to the ``Tools -> Options -> Debugging -> General``. Disable ``Enable Just My Code`` 
and enable ``Enable source server support``.

After these steps you should now be able to succesfully step into Fundatie code.