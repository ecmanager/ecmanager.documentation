##########################
Extending Fundatie modules
##########################

Introduction
============
Fundatie has many different modules to offer, and all of these have a certain default behavior. 
Sometimes that behavior can be modified by changing the module configuration, but there will be 
scenarios where you want functionality that is not offered by a module out-of-the-box.

Fundatie modules are created in a way that allows for extensibility. That means you can install a 
module and then create your own version that extends the installed module. This page describes how
you can do that using an example scenario.

Example scenario
================

.. figure:: ../../../_static/img/fundatie/how-to/extending-fundatie-modules/minibasket-old.png
	:alt: Mini basket (default)
	:align: right

	Mini basket (default)

The ``CheckoutMiniBasket`` Fundatie module is used to display a smaller version of the basket. You 
can see the default implementation in the figure to the right. As an example we want to extend the 
module to show a message in the view that says 'Add x amount to your basket to profit from free 
shipping'.

The customer receives free shipping if the basket subtotal is over €30. If the basket subtotal is 
higher, no message is shown.

Creating a custom module
========================
To start of, we install the module that we want to extend using NuGet. After installing the 
``Fundatie.Web.Modules.CheckoutMiniBasket`` package, we will create a couple of new classes for our 
module: 

* View model
* Controller

We will first create the new ViewModel in which we will add the new property that we want:

.. code-block:: C#
	:linenos:

	public class CustomCheckoutMiniBasketViewModel : CheckoutMiniBasketViewModel
	{
	    public string FreeShippingRemainderNotification { get; set; }
	    public bool IsEligibleForFreeShipping { get; set; }
	}

As you can see, the new view model inherits from the existing view model. We will do the same for 
the new controller. We will also override the ``CreateViewModel`` method to return an instance of 
the new view model:

.. code-block:: C#
	:linenos:

	public class CustomCheckoutMiniBasketController : CheckoutMiniBasketController
	{
	    public CustomCheckoutMiniBasketController(/* parameters */) : base(/* parameters */)
	    {
	    }
	    
	    protected override CheckoutMiniBasketViewModel CreateViewModel()
	    {
	        return new CustomCheckoutMiniBasketViewModel();
	    }
	}

.. figure:: ../../../_static/img/fundatie/how-to/extending-fundatie-modules/override-fillcustomdata.png
	:alt: Override FillCustomData
	:align: right

We can use the ``FillCustomData`` method to add data to our new view model. The object returned by
this method is what's eventually passed to the view. The input parameter is the model which is 
filled by the default controller. 

.. code-block:: C#
	:linenos:

	private const decimal _freeShippingStartAmount = 30.0M;

	protected override object FillCustomData(CheckoutMiniBasketViewModel model, BaseBlockParameters parameters, BlockViewModel blockViewModel)
	{
	    var customModel = model as CustomCheckoutMiniBasketViewModel;
	    customModel.IsEligibleForFreeShipping = true;
	    
	    if (model.BasketTotals.SubTotal.InclVat < _freeShippingStartAmount)
	    {
	        customModel.IsEligibleForFreeShipping = false;
	        
	        var remainder = _freeShippingStartAmount - model.BasketTotals.SubTotal.InclVat;
	        var formattedRemainder = remainder.ToString("#.##", CultureInfo.InvariantCulture);
	        var notification = string.Format("Add &euro;{0} to your basket to profit from free shipping!", formattedRemainder);
	        customModel.FreeShippingRemainderNotification = new HtmlString(notification);
	    }
	    
	    return customModel;
	}

In the block above you see that we cast the model to the custom view model so we can fill the newly
added properties. For the sake of the example, we added some simplified logic that fills the 
properties if the the basket total is under €30.

Modifying the view
==================
Using the views that ships with the module package, we change the model type associated with the 
view to the new view model:

.. code-block:: C#
	:linenos:

	@model Fundatie.Custom.CustomCheckoutMiniBasketViewModel

Next, we will add the new notification:

.. code-block:: C#
	:linenos:

	@if (!Model.IsEligibleForFreeShipping)
	{
	    <div class="alert alert-positive">@Model.FreeShippingRemainderNotification</div>
	}

Adding the block type
=====================
In order to use the new module, you should add a new row the ``BlokSoorten`` table. If you 
previously added the module, execute an update query instead of an insert.

.. code-block:: sql
	:linenos:

	INSERT INTO [dbo].[BlokSoorten]([BlokSoortID], [CmsOmschrijving], [Naam])
	VALUES(1, 'Custom mini basket', 'CustomMiniBasket')

.. note:: The ``Naam`` column should match the prefix of your controller class.

Verifying the result
====================

.. figure:: ../../../_static/img/fundatie/how-to/extending-fundatie-modules/minibasket-new.png
	:alt: Mini basket (custom)
	:align: right

	Mini basket (custom)

After these steps, you should now be able to see the result. As you can see on the image to the 
right, we now have the additional notification message.