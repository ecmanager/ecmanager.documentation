######
How-to
######

.. toctree::
   :maxdepth: 1
   
   installing-fundatie-modules/index
   creating-fundatie-modules/index
   extending-fundatie-modules/index
   Use-NAnt/index
   debugging-fundatie/index
   azure-cdn/creating-a-cdn-in-azure
   azure-search-index/index
   use-fundatie-blockparameters-generator/index