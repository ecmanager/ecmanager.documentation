############################
Configuring NAnt in Fundatie
############################

Introduction
============
Within Fundatie NAnt is used to build configuration files. See http://nant.sourceforge.net/ for the 
manual. The Fundatie .config files are created based on format files using fundatie.build, 
default.build and NAnt properties.

ecManager.properties
====================
The project contains an example file ``ecManager.example.properties`` with all the available 
properties in the ``NAnt`` directory. These properties have no value, it serves as a template for 
your personal ``ecManager.properties`` file. The ``ecManager.properties`` file should be placed in 
your user profile directory or anywhere in a parent directory of the Fundatie repository.

You can define multiple ``ecManager.properties`` files. This allows you to override properties for
different environments. For example:

``C:\Projects\Fundatie\ecManager.properties`` contains shared properties for each environment.
You can place another properties file at ``C:\Projects`` which will then add new or overwrite 
existing properties.

default.build
=============
The default.build file contains the definition of the following NAnt tasks:

task.scanrootfolders
--------------------
This task scans directories for an ecManager.properties file until it is found. It starts in the 
working directory and then recursively scans parent directories. The user profile directory is 
also scanned.

configMerge
-----------
Applies the properties to the format files 
source file the location of the format file
destination file the location of the formatted file

sitemapTask
-----------
A .NET task to generate the sitemap configuration.

robotTask
---------
A .NET task to generate the robots.txt configuration.

ecManagerSharedThemeTask
------------------------
A .NET task to generate the ecmanager theme configuration.

fundatie.build
==============
The fundatie.build contains all the project specific NAnt tasks.
It is also the the build file called in the project NAnt build command.
The default.build is included.

configBuild
-----------
The task where all the project specific NAnt actions are defined.