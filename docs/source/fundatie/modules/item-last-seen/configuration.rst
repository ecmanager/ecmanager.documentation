############
ItemLastSeen
############

Configuration
-------------


This configuration will determine settings required for the item last seen block in Fundatie. The following block
shows an example of this configuration:

.. code-block:: xml

	<configSections>
		<section name="fundatie.Web.Modules.ItemLastSeen" type="Fundatie.Web.Modules.ItemLastSeen.Configuration.ItemLastSeenConfigurationSection, Fundatie.Web.Modules.ItemLastSeen" />
	</configSections>

	<!-- numberOfLastSeenItems: The number of items max shown in the last seen block. -->
	<fundatie.Web.Modules.ItemLastSeen numberOfLastSeenItems="8" />

