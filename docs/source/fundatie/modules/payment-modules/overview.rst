########
Overview
########

Payment modules are used to integrate the services of Payment Service Providers(PSP) into Fundatie. 
This page describes:

1.  The goals that are achieved with payment modules
2.  The responsibilities of a payment module
3.  The high-level overview of the payment flow
4.  The high-level overview of the architecture of a payment module.

To avoid confusion, the following table wil outline definitions on this page.

+----------------+------------------------------------------------------------------------+-----------------------------------------------------+
| Term           | Definition                                                             | Example                                             |
+================+========================================================================+=====================================================+
| PSP            | A Payment Service Provider                                             | Buckaroo, World Pay                                 |
+----------------+------------------------------------------------------------------------+-----------------------------------------------------+
| Payment module | A module that integrates the services of a PSP into Fundatie/ecManager | The Nuget package Fundatie.Modules.Payment.Buckaroo |
+----------------+------------------------------------------------------------------------+-----------------------------------------------------+
| Payment method | One of the services a PSP offers which allows you to pay for an order. | Ideal, Mastercard                                   |
+----------------+------------------------------------------------------------------------+-----------------------------------------------------+


Goals
*****
The payment module concept provides a solution for several challenges concerning the integration with payment service providers.

-   Reduce the time to market of a webshop
	When building a new webshop, the integration with the PSP should be a matter of configuration instead implementation.
-   Reduce the time to market for updates
	PSP's will continue to update their platform and offer new features. 
	Because the communication with the payment provider is encapsulated a module, new features and fixes can be released at a faster pace because it does not require a framework update.

Module responsibilities
***********************
A payment module has three responsibilities.

1.  Provide ecManager with the correct payment status information
	When opening an order in the CMS, the payment section lists the details about the payments.
	For every payment, the actual payment status is listed in the overview.
	This payment status is provided by the payment module.   
		
.. image:: img/ecManager-payment-statusses.png
    :alt: Payment details in the ecManager CMS
	
2.  Handle the interaction with a payment service providers
	Every PSP has their own way of exposing their services.
	It often requires interaction with one or more web services. 
	Some payment providers only use server-to-server communication, whilst others require forwarding the customer to their secure environment.
	This interaction is confined to the payment module, which allows Fundatie to support the most common use cases.

3.  Integrate into the checkout flow in Fundatie
	When a customer proceeds through the checkout, the payment proces should be initiated.
	Fundatie passes instructions concerning the payment to the payment module, allowing the payment module to handle the payment from that point on.
	When the payment process is completed the payment module informs Fundatie about the payment status, which is used to resume the checkout flow.

Payment flow
************
Before learning about the composition of a payment module, it is useful to know the payment flow. 
The entire proces starts when a customer visits the webshop, and decides to order one or more products.
The customer proceeds through the checkout and when all the information has been gathered the payment flow is started.
The diagram below depicts the payment flow of a 'standard' payment module in a very simplified way.

.. image:: img/AbstractPaymentFlow.png
    :alt: A simplified view of the payment flow in Fundatie

1.  CreateOrder
	In this step the current basket is converted to an order in ecManager.

2.  InitiatePayment
	As soon as the order is created a payment module is instructed to initiate a payment.
	In these instructions you can expect the amount the customer needs to pay, alongside details of the order that might be relevant.

3.  CreatePayment
	The payment method needs to create a payment record, and add this to the order.
	This payment will contain details like the payment method id, the amount that is being paid and the initial status of the payment.
	The payment is visible in the CMS, as depicted in the section about Module Responsibilities. 
	
4.  InteractWithPsp
	In this step the a web service of the PSP is contacted to 'request' the payment.
	The request will contain details about the payment, and information about how the PSP should report the result of the operation.
	When approved, the web service will return a URL to which the customer should be forwarded.
	
5.  ForwardCustomer
	Once the web service provides the module with a URL, the customer can be forwarded to the environment of the PSP.
	In this step the customer is redirected to the provided URL.
	
6.  DoPayment
	In this step the PSP will gather information that is required to complete the payment, and perform the actual payment.
	For example, the creditcard data is gathered allowing the PSP to make a reservation on the card.
	
7.  ReturnToStore
	The payment provider has completed the DoPayment step and the results will be sent back to the webshop.
	The results are usually communicated by calling a URL defined by the payment module in the InteractWithPsp step.

8.  EvaluatePaymentResult
	The result provided by the payment provider needs to be translated to a status that Fundatie can understand.
	For example: the PSP sends us the statuscode 190 which translates to 'Paid'.
	
9.  ReturnToCheckoutFlow
	In this step the customer is led back into the checkout flow.
	The PaymentModule provides information about the payment status allowing Fundatie to forward the customer to the right page.
	When an order is paid, the customer should be led to the order confirmation page.
	When an order isn't paid, the customer should be led back to the page where he can select payment methods.

.. note :: In order to get a working PaymentFlow some events needs to be handled by the PaymentLogic. 
		   In `this </fundatie/modules/payment-events/overview.html>`_ page a code snippet of the event handling which can be implemented in the Startup.cs.
	
.. note :: 	This section only covers the payment flow with a single payment.
			You can continue reading about checkouts with multiple payment methods `here </fundatie/modules/payment-providers/multiple-payment-methods/overview.html>`_
			
Module architecture
*******************

.. note:: 	This is a high level overview of the module architecture. 
			For a more in-depth look at the architecture it is best to explore the code of the existing modules provided with Fundatie.

Every module has a class representing the payment provider, by implementing the `IFundatiePaymentProvider` interface.
A payment provider exposes a set of payment methods. 
Every payment method refers to a controller using the GetInitiatePaymentUrl operation.
The controller is called by Fundatie and is responsible for starting the actual payment.

A payment method inside a module implements two interfaces:

1.  ecManager `IPaymentMethod` interface
	This interface contains a few operations that allow ecManager to gather information about a payment.
	All ecManager needs to know is the current status of a payment.
2.  Fundatie's `IFundatiePaymentMethod` interface
	The ecManager interface does not contain operations that allow you to do a payment, or create a refund.
	The `IFundatiePaymentMethod` implements the `IPaymentMethod` interface and adds extra operations that allow you to create a payment and a refund.
	These operations are required to start a payment from the webshop, or create a refund from a connector.
	
.. image:: img/payment-interfaces.png
    :alt: Diagram

A payment module usually has two Nuget packages. 	
The first package contains the payment provider and it's payment methods.
It is designed to handle payments and refund initiated by connectors, and integrate with the ecManager CMS for translations.
The second package contains the code required to integrate the payment methods into Fundatie
This has been moved to a dedicated package to avoid dependencies to MVC in connectors and the CMS.