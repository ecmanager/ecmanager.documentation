###########
Quick guide
###########

This page describes how you can install and configure a new payment module in your webshop. 
To avoid confusion a few terms have been defined in the table below.

+----------------+-------------------------------------------------------+
| Term           | Definition                                            |
+================+=======================================================+
| PSP            | A Payment Service Provider                            |
+----------------+-------------------------------------------------------+
| Payment method | A payment method defined in the CMS.                  |
+----------------+-------------------------------------------------------+
| Payment module | A Nuget package containing the integration with a PSP |
+----------------+-------------------------------------------------------+
   
Adding a payment module
***********************

The installation of a payment module requires you to take three steps.

1.  Install the module in the webshop
	Open the solution containing the project for your webshop.
	Open the Nuget Package Manager console, select the webshop project and install to the desired payment module. 
	Please make sure you install the package designed for the Web project.
	Open the Modules.config file and add a new component to the list. 
	The registration differs per payment module. 
	Please consult the documentation of the payment module.
	In this documentation you will find which registration should be used and which configuration is required.
	
.. code-block:: xml
	:linenos:
	:caption: Example component
	
	<component
		type="Fundatie.Modules.Payment.Buckaroo.BuckarooPaymentProvider, Fundatie.Modules.Payment.Buckaroo"
		service="ecManager.Common.Interfaces.IPaymentProvider, ecManager.Common" />
	
2.  Install the module in the CMS
	Open the solution containing the project for your CMS.
	Open the Nuget Package Manager console, select the CMS project and install to the desired payment module. 
	Please make sure you **do not** install the package designed for the Web project.
	Open the Modules.config file and add a new component to the list. 
	This registration matches the Web project.
	Usually the configuration is not required in the CMS. 
	In case this is required, this should be documented in the 
	
3.  Configure the payment method(s)
	The first step is creating a new payment method using the ecManager CMS.
	Select the payment method module that corresponds to the newly installed payment module.
	
.. warning ::   Do not reuse existing payment methods. 
				Always create a new payment method for every payment module.
				When viewing an order, the payment module is asked to give a user-friendly translation of the payment status at runtime.
				Each payment module has their own translations, and they are **not** interchangable.
				When a payment module receives a value that does not make sense, exceptions will be thrown.
				This will make the order unavailable in the CMS.
	
.. image:: img/new-paymentmethod.png
    :alt: A newly configured payment method
	
4.  Add the newly created payment method to a payment method group
	After creating the payment method, it must be added to a payment method group before it becomes visible in the shop.
	Open an existing group, or create a new one. Add the payment method and save the changes. 
	From this point on, the payment method should be visible in the webshop.

.. note:: 	Depending on your caching solution, the changes might not be appear in your webshop directly.