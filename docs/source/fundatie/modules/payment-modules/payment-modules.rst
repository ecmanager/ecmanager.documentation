###############
Payment modules
###############

This section describes the payment modules in Fundatie. 
These modules integrate the payment services provided by a Payment Service Provider (PSP) like Buckaroo into Fundatie, allowing the customer to pay for his or her order.
The overview page describes the general architecture of a payment module, and the optimal flow within Fundatie. 
The Quick guide page shows you how to install and configure a payment module.

.. toctree::
   :titlesonly:
   
   overview
   quick-guide