##############
Mail
##############

Configuration
-------------


This configuration will determine settings required for Mail in Fundatie. The following block
shows an example of this configuration:

.. code-block:: xml

	<configSections>
		<section name="fundatie.Modules.Mail.Core" type="Fundatie.Modules.Mail.Core.Configuration.MailConfigurationSection, Fundatie.Modules.Mail.Core" />
		<section name="fundatie.Modules.Mail.SMTP" type="Fundatie.Modules.Mail.SMTP.Configuration.SmtpConfigurationSection, Fundatie.Modules.Mail.SMTP" />
	</configSections>

	<!-- templatePath: The path of the mail templates -->
	<!-- baseImageUrl: The base path for the images proberly the CDN location. -->
	<!-- baseEcManagerUrl: The base path to the ecManager CMS -->
	<!-- adminEmailAddress: The mail adres of the website administrator. -->
	<!-- senderEmailAddress: The mail adres used to send general mails. -->
	<!-- reviewEmailAddress: The mail adres used to send review mails. -->
	<!-- orderEmailAddress: The mail adres used to send order mails.. -->
	<fundatie.Modules.Mail.Core>
		<labels>
			<label 
				labelId="1" 
				templatePath="C:\data\EmailTemplates" 
				baseImageUrl="" 
				baseEcManagerUrl="" 
				adminEmailAddress="" 
				senderEmailAddress="" 
				reviewEmailAddress="" 
				orderEmailAddress="" />
		</labels>
	</fundatie.Modules.Mail.Core>
	
	<!-- host: The hostname of the SMTP-server. -->
	<!-- port: The port number of the SMTP-server. -->
	<!-- login: The login of the SMTP-server. -->
	<!-- password: The password of the SMTP-server. -->
	<fundatie.Modules.Mail.SMTP host="" port="" login="" password="" />
	  