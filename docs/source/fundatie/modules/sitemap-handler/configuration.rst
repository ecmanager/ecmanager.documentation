##############
SitemapHandler
##############

Introduction
============
SEO is incredibly important for a successful webshop. A webshop should have robot instructions and a 
sitemap available for crawlers in order for search engines to index the content of the webshop
correctly. This page describes how Fundatie helps you setup these tools and keeps them updated.

You should install the NuGet package ``Fundatie.Modules.Web.SitemapHandler`` if you want to use
any of these features. When visiting the URL ``https://example.com/generationhandler/index/`` your
robots.txt and sitemaps will generated. You can make a scheduled task to make sure your robots.txt 
and sitemap stay up-to-date. The configuration for this module can be found 
`here <../../modules/sitemap-handler/configuration.html#configuration>`__.

The generated files can be found in the path: ``/cmsdata/sitemaps/``. Make sure the files are always
stored on disk using the following configuration: 

.. code-block:: xml

	<ecManager.Storage>
	  <providers>
	    <provider pattern="/cmsdata/sitemaps" name="FileSystem" patternType="StartsWith" cachingEnabled="false" />
	  </providers>
	</ecManager.Storage>

Configuration
=============
The configuration determines how sitemaps and robot instructions are generated. The following block
shows an example of this configuration:

.. code-block:: xml

	<configSections>
	  <section name="fundatie.Web.Modules.SitemapHandler" type="Fundatie.Web.Modules.SitemapHandler.Configuration.HandlerConfigurationSection, Fundatie.Web.Modules.SitemapHandler" />
	</configSections>

	<fundatie.Web.Modules.SitemapHandler>
	  <!-- compressSitemap: Needs the sitemap to be compressed (true/false). -->
	  <!-- compressSitemap: Needs the sitemap to be compressed (true/false). -->
	  <!-- sitemapImageXmlNamespace: Namespace of the image xml -->
	  <!-- sitemapVideoXmlNamespace: Namespace of the video xml -->
	  <!-- dateTimeFormat: Date format used in the site map (default:yyyy-MM-ddTHH:mm:ss+01:00) -->
	  <sitemapConfiguration compressSitemap="true" sitemapXmlNamespace="http://www.sitemaps.org/schemas/sitemap/0.9" sitemapImageXmlNamespace="http://www.google.com/schemas/sitemap-image/1.1" sitemapVideoXmlNamespace="http://www.google.com/schemas/sitemap-video/1.1" dateTimeFormat="yyyy-MM-ddTHH:mm:ss+01:00">
	    <!-- typeKey: the key of the entity included in the sitemap. Possible values are (products/locations/navigation) -->
	    <!-- enabled: enable this entity type (true/false) -->
	    <!-- pageSize: The pageSize of the type in the sitemap. This is the amount of results per file, default (100). -->
	    <!-- stateContext: The (entity state) context used to retrieve the data -->
	    <!-- requiredState: The (entity) state used to retrieve the data -->
	    <!-- priority: The priority of the of the page (range from 0.0 to 1.0 default 0.5). -->
	    <!-- changeFrequency: The frequency of the item will change. Possible values are (always/hourly/daily/weekly/monthly/yearly/never). -->
	    <!-- exportImages: Export the images in this sitemap (true/false). -->
	    <sitemap typeKey="products" enabled="true" pageSize="100" stateContext="1" requiredState="100" priority="0.5" changeFrequency="monthly" exportImages="true" />
	    <sitemap typeKey="locations" enabled="true" pageSize="100" stateContext="1" requiredState="100" priority="0.5" changeFrequency="monthly" exportImages="false" />
	    <sitemap typeKey="navigation" enabled="true" pageSize="100" stateContext="1" requiredState="100" priority="0.5" changeFrequency="monthly" exportImages="false" />
	    <sitemap typeKey="newsitems" enabled="true" pageSize="100" stateContext="1" requiredState="100" priority="0.5" changeFrequency="monthly" exportImages="true" />
	  </sitemapConfiguration>
	  <robotConfiguration>
	    <!-- userAgent: The userAgent displayed in the robots.txt -->
	    <!-- labelId: The labelId for the robot.txt -->
	    <robot userAgent="*" labelId="1">
	      <!-- disallow fields -->
	      <disallowed>
	        <disallow path="/ThickBoxes/" />
	        <disallow path="/ecManager/" />
	      </disallowed>
	      <allowed>
	        <allow path="/example/" />
	      </allowed>
	    </robot>
	    <robot userAgent="*" labelId="2">
	      <disallowed>
	        <disallow path="/ThickBoxes/" />
	        <disallow path="/ecManager/" />
	      </disallowed>
	      <allowed>
	        <allow path="/example/" />
	      </allowed>
	    </robot>
	  </robotConfiguration>
	</fundatie.Web.Modules.SitemapHandler>

Robot instructions
==================
One ``robots.txt`` file is generated for each label with a unique name. Adding the following rewrite
rule to ``web.config`` will rewrite ``https://example.com/robots.txt`` to the right controller and
return the correct instructions for the label.

.. code-block:: xml

	<system.webServer>
	  <rewrite>
	    <rules>
	      <rule name="Redirect robots.txt" stopProcessing="false">
	        <match url="robots.txt" />
	        <conditions logicalGrouping="MatchAll" trackAllCaptures="false">
	          <add input="{HTTP_METHOD}" pattern="GET" />
	        </conditions>
	        <action type="Rewrite" url="robothandler/index/" />
	      </rule>
	    </rules>
	  </rewrite>
	</system.webServer>

The content of the robot instructions will be based on the configuration and the available sitemaps.

Sitemaps
========
Generating sitemaps will result in multiple files. An index sitemap per label and language, and
gzipped sitemaps for different entities. Currently sitemaps are generated for the following
entities:

* Navigation
* Products
* Locations
* News items

The URLs to sitemaps in the sitemap index are located at ``https://example.com/sitemaps/``. This 
means you should add a virtual directory in IIS called ``sitemaps`` pointed at the physical path of 
``/cmsdata/sitemaps/``.

Adding the following rewrite rule to ``web.config`` will rewrite ``https://example.com/sitemap.xml``
to the right controller and return the correct instructions for the label:

.. code-block:: xml

	<system.webServer>
	  <rewrite>
	    <rules>
	      <rule name="Redirect sitemap.xml" stopProcessing="false">
	        <match url="sitemap.xml" />
	        <conditions logicalGrouping="MatchAll" trackAllCaptures="false">
	          <add input="{HTTP_METHOD}" pattern="GET" />
	        </conditions>
	        <action type="Rewrite" url="sitemaphandler/index/" />
	      </rule>
	    </rules>
	  </rewrite>
	</system.webServer>

Make sure to allow IIS to serve files with an ``.xml`` or ``.xml.gz`` extension, for example by 
using the following configuration in ``web.config``:

.. code-block:: xml

	<system.webServer>
	  <staticContent>
	    <remove fileExtension=".xml" />
	    <mimeMap fileExtension=".xml" mimeType="text/xml" />
	    <remove fileExtension=".xml.gz" />
	    <mimeMap fileExtension=".xml.gz" mimeType="application/gzip" />
	  </staticContent>
	</system.webServer>


Sitemaps on Azure
=================
To make the sitemaps work on Azure we recommend to storing the FileSystem of the Web app and
creating a virtual directory to the right location. Below some example configuration for the Storage
module.

.. code-block:: xml

  <ecManager.Storage>
    <providers>
		  ...
      <provider name="FileSystem" pattern="/cmsdata/sitemaps" patternType="StartsWith" cachingEnabled="false" />
      <provider name="AzureBlobStorage" pattern="" patternType="StartsWith" cachingEnabled="true" />
    </providers>
  </ecManager.Storage>

Add virtual directory with:

* Virtual path: /sitemaps
* Physical path: site\wwwroot\app_data\cmsdata\sitemaps
* Type: Directory