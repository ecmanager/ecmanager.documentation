######################
TweakwiseProductLister
######################

Configuration
-------------


This configuration will determine settings required for the tweakwise product lister in Fundatie. The following block
shows an example of this configuration:

.. code-block:: xml

	<configSections>
		<section name="fundatie.Web.Modules.TweakwiseProductLister" type="Fundatie.Web.Modules.TweakwiseProductLister.Configuration.ListerConfigurationSection,Fundatie.Web.Modules.TweakwiseProductLister" />
	</configSections>

	<fundatie.Web.Modules.TweakwiseProductLister>
		<!-- enablePager: Enabled the pager (true/false).)
		<!-- numberOfPagerItems: The number of pager items. -->
		<pager enablePager="true" numberOfPagerItems="5" />
	</fundatie.Web.Modules.TweakwiseProductLister>