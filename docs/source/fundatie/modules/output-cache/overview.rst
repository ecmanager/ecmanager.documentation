########
Overview
########
The output caching module enables the implementation party to use output caching as a
caching solution within Fundatie.

Visit the `Quick guide <quick-guide.html>`__ page to read more on how to get started with the
module, or the `Advanced <advanced.html>`__ page for more advanced topics, such as overriding 
default behaviour.