########
Advanced
########

Configuring per entity caching
==============================

Similar to the ecManager MemoryCache module you are able to configure how you want to handle caching 
for specific entities. This is done by editing the **fundatie.Caching** configuration section in the 
following way.

.. code-block :: xml

  <fundatie.Caching
    enabled="true"
    expiration="60"
    debug="false">
      <cacheIdentifiers>
        <cacheIdentifier cacheIdentifier="VisualController" enabled="true" expiration="5" />
        <cacheIdentifier cacheIdentifier="TitleController" enabled="false" />
      </cacheIdentifiers>
  </fundatie.Caching>

Usually the ``cacheIdentifier`` is the name of the controller. This has be to an exact match. This
way you can set specific settings for the controllers this applies to.

Debug
=====

To debug Output Cache you are able to set the ``debug`` property to true. Every identifier that is
added to Output Cache is saved in MemoryCache with a pre formatted key (``outputCacheInfo``), so you
can read this from the MemoryCache.

Data that is saved:

- ControllerName
- ActionName
- Expiration (duration)
- Enabled (true or false)
- VaryByParam
- VaryByCustom

.. warning ::

	This is a performance penalty. Don't enable this on production environments.