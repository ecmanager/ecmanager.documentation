###########
Quick guide
###########

Configuring the module
======================

To configure the module you will need to add a configuration section to the web.config file. This
looks like the following.

.. code-block :: xml

  <configSections>
    <section name="fundatie.Caching" type="Fundatie.Core.Configuration.FundatieCachingConfigurationSection, Fundatie.Core" />
  </configSections>

  <fundatie.Caching 
    enabled="true" 
    expiration="60" 
    debug="false">
  </fundatie.Caching>

For the module we can configure several properties. **enabled** specifies whether the cache is
on or off, **expiration** determines the time an object will be cached in minutes. The
**debug** property to log seperate which items are in the output cache. Make sure this property is
set to false on production environments.