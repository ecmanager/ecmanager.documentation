################
CheckoutDelivery
################

Configuration
-------------

This configuration will determine settings required for the checkout delivery block in Fundatie. The following block
shows an example of this configuration:

.. code-block :: xml

	<configSections>
		<section name="fundatie.Web.Modules.CheckoutDelivery" type="Fundatie.Web.Modules.CheckoutDelivery.Configuration.CheckoutDeliveryConfigurationSection, Fundatie.Web.Modules.CheckoutDelivery" />
	</configSections>

	<fundatie.Web.Modules.CheckoutDelivery>
		<!-- transporterCode: The transporter code used for pick up to fill the CheckoutDeliveryViewModel.PickupDeliveryOptionViewModel. -->
		<!-- resultSize: The resultSize used to limit the delivery locations. Not a required property. Default value is 2. -->
		<pickupTransporter transporterCode="PICKUP" resultSize="2" />
		<transporterOptions>
			<!-- optionKey: The option key used to show additional checkboxes for transporters. This information is passed to the Basket and Order.--> 
			<!-- resourceType: The resource type to translate the description of the transport option. --> 
			<!-- resourceName: The name mapped in the resource type. -->
			<!-- transporterCode: The transportcode of the transporter is options is used for. -->
			<!-- labelId: The website label id -->			
			<transporterOption optionKey="DoNotDeliverNextDoor" resourceType="Fundatie.Resources.CheckoutDelivery, Fundatie.ResourceAssets" resourceName="DoNotDeliverNextDoor" transporterCode="PostNL" labelId="1" />
		</transporterOptions>
		<transporters>
			<!-- transporterCode: The transport code to identify for which the datepicker is enabled -->
			<!-- enableDatePicker: Enable the datepicker for this transporter (true/false) -->
			<transporter transporterCode="PostNL" enableDatePicker="true" />
		</transporters>
	</fundatie.Web.Modules.CheckoutDelivery>