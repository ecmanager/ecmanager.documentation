######
Search
######
Configuration
-------------


This configuration will determine settings required for the search of Fundatie. The following block
shows an example of this configuration:

.. code-block:: xml

	<configSections>
		<section name="fundatie.Web.Modules.Search" type="Fundatie.Web.Modules.Search.Configuration.SearchConfigurationSection, Fundatie.Web.Modules.Search" />
	</configSections>
	
	<fundatie.Web.Modules.Search>
		<!-- pageSize: The pageSize of product in the search result. -->
		<products pageSize="20"/>
		<!-- pageSize: The pageSize of combinations in the search result. -->
		<combinations pageSize="2" />
		<!-- pageSize: The pageSize of faq items in the search result. -->
		<faq pageSize="20" />
		<!-- pageSize: The pageSize of news items in the search result. -->
		<news pageSize="20" />
		<!-- pageSize: The pageSize of information items in the search result. -->
		<information pageSize="20" />
		<!-- pageSize: The pageSize of product groups in the search result. -->
		<productGroup pageSize="20" />
	</fundatie.Web.Modules.Search>

