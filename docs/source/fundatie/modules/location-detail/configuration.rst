################
LocationDetail
################

Configuration
-------------


This configuration will determine settings required for the location detail block in Fundatie. The following block
shows an example of this configuration:

.. code-block:: xml

	<configSections>
		<section name="fundatie.Web.Modules.LocationDetail" type="Fundatie.Web.Modules.LocationDetail.Configuration.LocationDetailConfigurationSection, Fundatie.Web.Modules.LocationDetail" />
	</configSections>

	<!-- googleApiKey: The api key used by the Google Location Services. -->
	<fundatie.Web.Modules.LocationDetail googleApiKey="" />