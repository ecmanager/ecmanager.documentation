#################
ErrorPagesHandler
#################

.. note::

	This page describes the way error pages should be handled in a **production** environment.
	How you configure error pages in a development environment is entirely up to you.

Introduction
============
Custom error pages are used to give a better user experience when an error occurs on the website,
for example if a page is not found or if an exception is uncaught. Fundatie uses two standard ways
of handling errors: on ASP.NET and IIS level.

To get started using error pages in Fundatie, install the ``Fundatie.Web.Modules.ErrorPagesHandler``
NuGet package.

Generating error pages
======================
For the best user experience, an error page should not have a very different look and feel compared
to other pages. For this reason, error pages are generated based on navigation items. Two 
navigation items should be created with selection codes "404" and "500". You can add blocks to these
navigation items the way you want them displayed when an error occurs.

Visiting the URL ``https://example.com/errorpagesgenerationhandler/index/`` will trigger the 
generation of the error pages for each label and language. If you have two labels with two languages
each, this will result in a total of eight error pages.

The generated files can be found in the path: ``/cmsdata/errorpages/``. Make sure the files are 
always stored on disk using the following configuration: 

.. code-block:: xml

	<ecManager.Storage>
	  <providers>
	    <provider pattern="/cmsdata/errorpages" name="FileSystem" patternType="StartsWith" cachingEnabled="false" />
	  </providers>
	</ecManager.Storage>

Handling ASP.NET errors
=======================
To handle ASP.NET errors, the ``customErrors`` element is used in ``web.config``:

.. code-block:: xml

	<system.web>
	  <customErrors mode="On" redirectMode="ResponseRewrite">
	    <error statusCode="404" redirect="~/PageNotFoundHandler/Index" />
	    <error statusCode="500" redirect="~/ServerErrorHandler/Index" />
	  </customErrors>
	<system.web>

As you can see, 404 and 500 errors are configured to be handled by their corresponding controller
actions. Those actions make sure the correct error page for the right label and language is shown
the user.

Handling IIS errors
===================
If an error occurs outside the scope of ASP.NET, errors are handled directly by IIS. The 
``httpErrors`` elements in ``web.config`` is used to handle these errors. You should add the element
as shown in the following example:

.. code-block:: xml

	<httpErrors errorMode="Custom" defaultResponseMode="File" defaultPath="/app_data/cmsdata/errorpages/500_example_nl.html">
	  <clear />
	  <error statusCode="404" path="/app_data/cmsdata/errorpages/404_example_nl.html" responseMode="File" />
	</httpErrors>

Please note that we can only refer to a static file on disk here. It is not possible to match a
language to the session of the user, so choose the files here wisely.