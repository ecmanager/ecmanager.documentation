############
NewsOverview
############

Configuration
-------------


This configuration will determine settings required for the news overview of Fundatie. The following block
shows an example of this configuration:

.. code-block:: xml

	<configSections>
	<section name="fundatie.Web.Modules.NewsOverview" type="Fundatie.Web.Modules.NewsOverview.Configuration.NewsOverviewConfigurationSection, Fundatie.Web.Modules.NewsOverview" />
	</configSections>

	<fundatie.Web.Modules.NewsOverview>
		<!-- numberOfProductsOnPage: Number of news item per page. -->
		<newsItems numberOfProductsOnPage="3" />
		<!-- enablePager: Enabled the pager (true/false).)
		<!-- numberOfPagerItems: The number of pager items. -->
		<pager enablePager="true" numberOfPagerItems="5" />
	</fundatie.Web.Modules.NewsOverview>