##############
ProductCompare
##############

Configuration
-------------


This configuration will determine settings required for the compare page of Fundatie. The following block
shows an example of this configuration:

.. code-block:: xml

	<configSections>
	<section name="fundatie.Web.Modules.ProductCompare" type="Fundatie.Web.Modules.ProductCompare.Configuration.CompareConfigurationSection, Fundatie.Web.Modules.ProductCompare" />
	</configSections>

	<fundatie.Web.Modules.ProductCompare>	
		<!-- enableReviews: use reviews on the compare page (true/false) -->
		<!-- minRating: The minimal rating (stars) that can be given (default 0). -->
		<!-- maxRating: The maximum rating (stars) that can be given (default 5). -->
		<reviews enableReviews="true" minRating="0" maxRating="5" />
	</fundatie.Web.Modules.ProductCompare>