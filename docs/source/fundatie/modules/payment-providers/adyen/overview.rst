########
Overview
########

The Adyen payment module offers projects based on Fundatie the ability to offer Adyen services to 
its customers. Adyen offers many payment methods of which a selection has been integrated in this 
module. The module uses the so-called Hosted Payment Pages (HPP) and HTTP POST notifications offered
by Adyen. You can easily extend the module to add payment methods of your liking.

You can find more information about HPP `here <https://docs.adyen.com/developers/easy-encryption#directorylookupendpoints>`_ 
and notifications `here <https://docs.adyen.com/developers/api-manual#notifications>`__.

Payment methods
===============
The Adyen module offers the following payment methods:

=================== ================
Payment method      Added in version
=================== ================
American Express    1.0             
Bancontact card     1.0             
Diners Club         1.0             
Sofortüberweisung   1.0             
Discover            1.0             
Dotpay              1.0             
Finnish E-Banking   1.0             
GiroPay             1.0             
iDEAL               1.0             
Maestro             1.0             
MasterCard          1.0             
SEPA Direct Debit   1.0             
VISA                1.0             
=================== ================

Transaction flow
================
When showing the available payment methods to the customer, a request is made to Adyen to get the 
"issuers" for the payment methods. As an example, these are banks in the case of iDEAL. These 
issuers cannot be hardcoded in any way because they also differ per environment. For example,
the test environment has 'test bank 1' as a possible issuer, and the live environment has existing
banks like 'Rabobank' or 'ING'.

After selecting a payment method with optionally an issuer, a signature is created based on the 
input. The user will be redirected to the HPP page hosted by Adyen where the customers can finalize
the transaction.

When the customer ends the transaction (successful or not), the customer is redirected back to us.
The URL will include a signature which can be decoded to ensure the authenticity of the request. The
request also includes a status of the payment. If the status is paid or pending, the customer will 
be shown the thank you page, otherwise a redirect to the checkout overview will happen where an 
error message will be shown accordingly. The payment is also updated when the customer returns,
because we are able to validate the authenticity (as opposed to other payment providers).

Adyen sends notifications which inform us of changes to payments that have occurred. We listen to
those notifications to keep our system in sync with theirs.

Implementation
==============
Visit the `Quick guide <quick-guide.html>`_ page to read more on how to get started using the 
Adyen payment module. You can read the `Advanced <advanced.html>`_ for more advanced subjects.

Defaults
========
The default currency of the Adyen payment module is EUR. This can be overridden in your own
implementation.