########
Advanced
########

Introduction
============
This page describes more advanced topics for the Adyen payment module, like adding a new payment 
method and adding processors for notifications.

Adding a payment method
=======================
Adding a new payment method for Adyen is fairly straight forward. You can override the 
abstract ``APaymentMethod`` class which contains most of the default Adyen implementation. See the
following code for an example:

.. code-block:: c#
    :linenos:

	public class PayPal : APaymentMethod
	{
	    public override string BrandCode { get { return "paypal"; } }
	
	    public PayPal(string providerIdentifier, IAdyenConfiguration configuration, IPaymentStatusMapper paymentStatusMapper)
	        : base(providerIdentifier, configuration, paymentStatusMapper)
	    {
	    }
	
	    public override string GetName()
	    {
	        return "PayPal";
	    }
	}

	public class CustomPaymentProvider : AdyenPaymentProvider
	{
	    public CustomPaymentProvider(/* parameters */) : base(/* parameters */)
	    {
	    }

	    protected override void AddPaymentMethods()
	    {
	        base.AddPaymentMethods();
	        PaymentMethods.Add(new PayPal(ProviderIdentifier, Configuration, PaymentStatusMapper));
	    }
	}

.. figure:: ../../../../_static/img/fundatie/payment-providers/adyen/paymentmethods.png
	:alt: Payment method brand code
	:align: right

	Payment method brand code

The ``BrandCode`` property should return the name of the payment method as can be seen on the image
to the right. You can find the payment methods in the Adyen Customer Area.

Registration of the custom payment provider is needed. Make sure to register it both in the 
``Modules.config`` and in the Fundatie autofac container.

Listening for notifications
===========================
Adyen sends notifications via the ``/Adyen/Notification`` endpoint. There are many different types
of notifications, of which many have not been implemented because they are different for each 
project. You can find the different types in the `Adyen documentation <https://docs.adyen.com/developers/api-manual#notifications>`_.

Each type of notification has their own implementation of the ``INotificationProcessor`` interface. 
By default, two notification types have been implemented: ``AUTHORISION`` and ``CANCELLATION``. You
can implement a new notification processor as follows:

.. code-block:: c#
	:linenos:

	public class RefundNotificationProcessor : NotificationProcessorBase
	{
	    public RefundNotificationProcessor(AdyenPaymentProvider paymentProvider, ILogger logger, ILifetimeScope lifetimeScope) 
	        : base(paymentProvider, logger, lifetimeScope)
	    {
	    }
	    
	    public override void Process(NotificationModel notification)
	    {
	        // Create a refund
	    }
	}

You can add any dependencies to the constructor so they will be injected. You should make sure not 
to perform too many long running operations, because Adyen expects a response within 10 seconds. If 
you need to do this, consider using a message queue.

To register the new notification processor, use the following code as an example. The provided name 
is the event code:

.. code-block:: c#
	:linenos:

	builder.RegisterType<RefundNotificationProcessor>().Named<INotificationProcessor>("REFUND");

You can also override the existing notification processors in this way.