###########
Quick guide
###########

Introduction
============
This section describes how to get started on integrating the Adyen payment provider. Afterwards you
will be able to navigate through the payment flow. To actually integrate the entire flow, you will 
also need to read the `Advanced <advanced.html>`_ section.

Prerequisites
=============
You will need an Adyen account in order to use Adyen services. Once you have an account, you have
to create a skin. The skin requires some configuration which you must provide. You have to generate
a ``HMAC Key`` for the different platforms, provide a ``Result URL`` for the paymentresult, see
figure: https://fundatie.nl/adyen/return. 

.. figure:: ../../../../_static/img/fundatie/payment-providers/adyen/configuration.png
	:alt: Adyen configuration
	:align: center

To start using this module, you need to install the ``Fundatie.Modules.Payment.Adyen`` and 
``Fundatie.Modules.Payment.Adyen.Web`` NuGet packages.

.. code-block:: PowerShell

	PM> Install-Package Fundatie.Modules.Payment.Adyen
	PM> Install-Package Fundatie.Modules.Payment.Adyen.Web

Registration
============
To start using the Adyen module, add the following registration to ``Modules.config``:

.. code-block:: xml

	<component
	  type="Fundatie.Modules.Payment.Adyen.AdyenPaymentProvider, Fundatie.Modules.Payment.Adyen"
	  service="ecManager.Common.Interfaces.IPaymentProvider, ecManager.Common">
	</component>

Configuration
=============
To use the Adyen payment module, you need to add a configuration section. In Adyen there is a 
concept of skins. A skin is an environment for a specific brand, which matches the label concept of
ecManager. Therefore, it is possible to configure a skin for a specific label. A skin has two 
environments, ``test`` or ``live``. You also need to specify the account code as merchant account.

.. note:: The configured merchant account must be a valid account for the configured skin.

The following configuration can be used as an example:

.. code-block:: xml

	<configSections>
	  <!-- Other config sections -->
	  <section name="Fundatie.Modules.Payment.Adyen" type="Fundatie.Modules.Payment.Adyen.Configuration.AdyenConfigurationSection, Fundatie.Modules.Payment.Adyen" />
	</configSections>
	
	<Fundatie.Modules.Payment.Adyen>
	  <settings environment="test" username="username" password="password" />
	  <labels>
	    <label id="1" HMACKey="B7D2D626E975E4..." skinCode="zQG..." merchantAccount="EcManagerNL" sessionValidityInMinutes="120" />
	    <label id="2" HMACKey="B7D2D626E975E4..." skinCode="zQG..." merchantAccount="EcManagerNL" sessionValidityInMinutes="120" />
	  </labels>
	</Fundatie.Modules.Payment.Adyen>

.. figure:: ../../../../_static/img/fundatie/payment-providers/adyen/communication.png
	:alt: Adyen communication settings
	:align: right

The username and password are used for HTTP Basic authentication when Adyen sends notifications.
Those credentials can be configured in the Adyen Customer Area as shown in the image to the right.
Make sure the URL and transport method are correct. 

You cannot configure different URLs for different environments in the Adyen Customer Area. This
means that you should either use different merchant accounts for different environments, or only let
the production environment receive notifications from both test and live. Notifications will not be
processed if the environment in the request and the configuration don't match.

The session validity configuration holds the value in minutes  which is added to the current time on 
the server. After that time has passed, the transaction will no longer be valid.

Error messages
==============
Every payment module contains errors that can occur and an error message will be shown to the user.
The following keys should be added to the ``Enumerations.resx`` file in the ``CmsData\Resources``
directory for every supported language for your webshop:

=================================================================== ==================================
Key                                                                 Description                       
=================================================================== ==================================
Fundatie_Modules_Payment_Adyen_AdyenValidationError_IssuerRequired  No issuer was selected by the user
=================================================================== ==================================