###########
Quick guide
###########

Introduction
============
This section describes how to get started on integrating the Klarna payment provider.

Prerequisites
==============
To start using this module, you need to install the ``Fundatie.Modules.Payment.Klarna`` and 
``Fundatie.Modules.Payment.Klarna.Web`` NuGet packages.

.. code-block:: PowerShell

    PM> Install-Package Fundatie.Modules.Payment.Klarna
    PM> Install-Package Fundatie.Modules.Payment.Klarna.Web

Configuration
=============
To use the Klarna payment module, you need to add a configuration section. The following 
configuration can be used as an example:

.. code-block:: xml
    
    <configSections>
      // Other config sections
      <section name="fundatie.Modules.Payment.Klarna.Configuration.KlarnaConfigurationSection" 
               type="Fundatie.Modules.Payment.Klarna.Configuration.KlarnaConfigurationSection, Fundatie.Modules.Payment.Klarna" />
    </configSections>
    
    <fundatie.Modules.Payment.Klarna.Configuration.KlarnaConfigurationSection>
      <eid value="2356" />
      <sharedSecret value="m5LfViTVtGuT4TW" />
      <options>
        <isLiveMode value="false" />
        <defaultCurrency value="EUR" />
        <articleNumberForShipmentCosts value="KLARNA_ShipmentCosts" />
        <articleNumberForPaymentCosts value="KLARNA_PaymentCosts" />
        <articleNumberForDiscount value="KLARNA_Discount" />
        <defaultVatPercentage value="21" />
      </options>
    </fundatie.Modules.Payment.Klarna.Configuration.KlarnaConfigurationSection>

============================== ==================== ==================================================================
Element                        Default value        Description                                                       
============================== ==================== ==================================================================
eid                            None                 The merchant ID provided by Klarna which is used to authenticate  
sharedSecret                   None                 The shared secret provided by Klarna which is used to authenticate
isLiveMode                     false                Determines whether the current environment is a live environment  
defaultCurrency                EUR                  The currency to be used for orders which are sent to Klarna       
articleNumberForShipmentCosts  KLARNA_ShipmentCosts The article number for shipment costs                             
articleNumberForPaymentCosts   KLARNA_PaymentCosts  The article number for payment costs                              
articleNumberForDiscount       KLARNA_Discount      The article number for discounts                                  
defaultVatPercentage           21                   The VAT percentage to use if no other is supplied.                
============================== ==================== ==================================================================

Error messages
==============
Every payment module contains errors that can occur and an error message will be shown to the user.
The following keys should be added to the ``Enumerations.resx`` file in the ``CmsData\Resources``
directory for every supported language for your webshop:

=============================================================================== =======================================
Key                                                                             Description                            
=============================================================================== =======================================
Fundatie_Modules_Payment_Klarna_Enums_KlarnaValidationErrors_IpAddressMissing   The IP address of the user is missing  
Fundatie_Modules_Payment_Klarna_Enums_KlarnaValidationErrors_PhoneNumberMissing The phone number of the user is missing 
Fundatie_Modules_Payment_Klarna_Enums_KlarnaValidationErrors_BirthdateMissing   The birth date of the user is missing  
=============================================================================== =======================================
