########
Overview
########

The Klarna payment module allows projects based on Fundatie to offer the Klarna payment method to 
visitors of the webshop. You can read more about Klarna `on their website <https://www.klarna.com/>`_.

Transaction flow
----------------
The transaction flow for Klarna payment differs from the common payment method scenario.
When a customer pays with Klarna, Klarna vouches for the customer that a payment will be made.
This allows the customer to receive their goods before they actually have to pay for it.
As soon as a payment is initiated in the webshop, an invoice is sent to Klarna. 
Klarna applies a risk-assesment to determine whether they can vouch for the customer.
As soon as the customer is approved, the payment is completed.
The actual financial transaction is handled by Klarna at a later point in time.

Visit the `Quick guide <quick-guide.html>`_ page to read more on how to get started using the Klarna 
payment module.