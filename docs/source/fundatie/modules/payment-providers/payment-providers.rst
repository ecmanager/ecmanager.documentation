=================
Payment providers
=================

.. toctree::
   
   adyen/index
   buckaroo/index
   intersolve/index
   klarna/index
   multisafepay/index
   multiple-payment-methods/index