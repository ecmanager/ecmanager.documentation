###########
Quick guide
###########

Introduction
============
This section describes how to get started on integrating the Buckaroo payment provider.

Prerequisites
==============
To start using this module, you need to install the ``Fundatie.Modules.Payment.Buckaroo`` and 
``Fundatie.Modules.Payment.Buckaroo.Web`` NuGet packages.

.. code-block:: PowerShell

    PM> Install-Package Fundatie.Modules.Payment.Buckaroo
    PM> Install-Package Fundatie.Modules.Payment.Buckaroo.Web

Configuration
=============
To use the Buckaroo payment module, you need to add a configuration section. The following 
configuration can be used as an example:

.. code-block:: xml
  
  <configSections>
    // Other config sections
    <section name="fundatie.Modules.Payment.Buckaroo.Configuration.BuckarooConfigurationSection" 
             type="Fundatie.Modules.Payment.Buckaroo.Configuration.BuckarooConfigurationSection, Fundatie.Modules.Payment.Buckaroo" />
  </configSections>
  
  <fundatie.Modules.Payment.Buckaroo.Configuration.BuckarooConfigurationSection>
    <urls>
      <returnUrl value="/buckaroo/return/" />
      <errorUrl value="/buckaroo/errortransaction/" />
      <rejectedTransactionUrl value="/buckaroo/rejectedtransaction/" />
      <cancelTransactionUrl value="/buckaroo/canceltransaction/" />
      <startTransactionUrl value="/buckaroo/starttransaction/" />
      <updateTransactionUrl value="/buckaroo/updatetransaction/" />
    </urls>
    <environments>
      <testEnvironment url="https://testcheckout.buckaroo.nl/" websiteKey="" secretKey="" channel="Web" />
      <liveEnvironment url="" websiteKey="" secretKey="" channel="Web" />
    </environments>
    <options>
      <includeIpAddress value="false" />
      <useCustomBankTransferInstructions value="false"/>
    </options>
    // If you are using pay per email payment method
    <payPerEmail>
      <merchantSendsEmail value="true" />
      <expirationDays value="21" />
      <paymentMethodsAllowed value="ideal,bancontact,mastercard,visa" />
      <recurringPaymentMethodsAllowed value="ideal,bancontact" />
    </payPerEmail>
  </fundatie.Modules.Payment.Buckaroo.Configuration.BuckarooConfigurationSection>

========================================== ============= =======================================================================================
Element/Attribute                          Default value Description                                                                 
========================================== ============= =======================================================================================
returnUrl                                  None          The URL to be called when a a user is redirected back to the webshop        
errorUrl                                   None          The URL to be called when an error occurs                                   
rejectedTransactionUrl                     None          The URL to be called if the transaction is rejected                         
cancelTransactionUrl                       None          The URL to be called if a transaction is cancelled by a user                
startTransactionUrl                        None          The URL to be called to start a transaction
updateTransactionUrl                       None          The URL to be called by Buckaroo if a transaction is updated                
testEnvironment.url                        None          The URL of the Buckaroo test environment                                             
testEnvironment.websiteKey                 None          The key of the website that the user is using to create a transaction       
testEnvironment.secretKey                  None          The secret key of the website that the user is using to create a transaction
testEnvironment.channel                    None          The channel for the tranasctions, defines which actions are allowed in this environment
liveEnvironment.url                        None          The URL of the Buckaroo live environment                                             
liveEnvironment.websiteKey                 None          The key of the website that the user is using to create a transaction       
liveEnvironment.secretKey                  None          The secret key of the website that the user is using to create a transaction
liveEnvironment.channel                    None          The channel for the tranasctions, defines which actions are allowed in this environment
includeIpAddress                           false         Determines if the users IP address should be included                       
payPerEmail.merchantSendsEmail             None          Buckaroo will send the payment invitation e-mail. In all other cases you have to send the link yourself.
payPerEmail.expirationDays                 None          Determines how long is the payment invitation valid.
payPerEmail.paymentMethodsAllowed          None          Which payment methods are supported. Specify the buckaroo names used for the payment methods.
payPerEmail.recurringPaymentMethodsAllowed None          Which payment methods are supported to start a recurring payment with. NOTE: Please check the buckaroo documentation to see which payment methods support this feature.
useCustomBankTransferInstructions          false         Determines if the the default email with payment instructions should be used
========================================== ============= =======================================================================================

Error messages
==============
Every payment module contains errors that can occur and an error message will be shown to the user.
The following keys should be added to the ``Enumerations.resx`` file in the ``CmsData\Resources``
directory for every supported language for your webshop:

=============================================================================== ==================================
Key                                                                             Description                       
=============================================================================== ==================================
Fundatie_Modules_Payment_Buckaroo_Enums_BuckarooValidationErrors_IssuerRequired No issuer was selected by the user
=============================================================================== ==================================