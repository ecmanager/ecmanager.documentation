########
Overview
########

The Buckaroo payment module allows projects based on Fundatie to offer the payment services provided by Buckaroo. 
The module uses the JSON gateway provided by Buckaroo. 
You can find more information about the JSON gateway on `the Buckaroo website <https://support.buckaroo.nl/index.php/JSON>`_

Payment methods
---------------

The module offers the following payment methods:

+---------------------+------------------+---------------------------------------------------------------+
| Payment method name | Added in version | Notes                                                         |
+=====================+==================+===============================================================+
| American Express    | 1.0              |                                                               |
+---------------------+------------------+---------------------------------------------------------------+
| Bancontact/MrCash   | 1.0              |                                                               |
+---------------------+------------------+---------------------------------------------------------------+
| Bank transfer       | 1.1              | Not ready for production usage. Contact ecManager for details |
+---------------------+------------------+---------------------------------------------------------------+
| Carte Bancaire      | 1.0              |                                                               |
+---------------------+------------------+---------------------------------------------------------------+
| Carte Bleue         | 1.0              |                                                               |
+---------------------+------------------+---------------------------------------------------------------+
| iDeal               | 1.0              |                                                               |
+---------------------+------------------+---------------------------------------------------------------+
| Maestro             | 1.0              |                                                               |
+---------------------+------------------+---------------------------------------------------------------+
| MasterCard          | 1.0              |                                                               |
+---------------------+------------------+---------------------------------------------------------------+
| PayPal              | 1.0              |                                                               |
+---------------------+------------------+---------------------------------------------------------------+
| PayPerEmail         | 1.0              |                                                               |
+---------------------+------------------+---------------------------------------------------------------+
| SepaDirectDebit     | 1.0              |                                                               |
+---------------------+------------------+---------------------------------------------------------------+
| Sofort Banking      | 1.0              |                                                               |
+---------------------+------------------+---------------------------------------------------------------+
| Visa                | 1.0              |                                                               |
+---------------------+------------------+---------------------------------------------------------------+
| Visa Electron       | 1.0              |                                                               |
+---------------------+------------------+---------------------------------------------------------------+
| VPay                | 1.0              |                                                               |
+---------------------+------------------+---------------------------------------------------------------+

Transaction flow
----------------
When initiating a payment a JSON request is sent to Buckaroo. 
Buckaroo returns the URL of the secure payment environment to which the customer should be forwarded.
After completing the payment, the customer will be redirected to the webshop. 
At this point the payment status will be evaluated. 
If the payment completed successfully, the customer will be forwarded to the order confirmation page.
Otherwise the customer will return to the Checkout overview page. 

.. warning:: When a customer returns to the webshop, the payment status is not yet final. You should not use this status for exports to order processing systems.

Once Buckaroo completed the transaction, a request is sent to the webshop confirming the status of a payment.
After this final (server-to-server) update from Buckaroo the implementation should initiate exporting the order to external systems.

.. note:: When we receive an update we only take the transaction identifier from the request. This identifier is used to fetch the payment details using the gateway. This is done to avoid fraude via request manipulation.

Implementation
--------------
Visit the `Quick guide <quick-guide.html>`_ page to read more on how to get started using the 
Buckaroo payment module.