###########
Quick guide
###########

Introduction
============
This section describes how to get started on integrating the Intersolve payment provider.

Prerequisites
==============
To start using this module, you need to install the ``Fundatie.Modules.Payment.Intersolve`` and 
``Fundatie.Modules.Payment.Intersolve.Web`` NuGet packages.

.. code-block:: PowerShell

    PM> Install-Package Fundatie.Modules.Payment.Intersolve
    PM> Install-Package Fundatie.Modules.Payment.Intersolve.Web

You also need to execute any SQL scripts that are part of the installed NuGet package.

Configuration
=============
To use the Intersolve payment module, you need to add a configuration section. The following 
configuration can be used as an example:

.. code-block:: xml
    
    <configSections>
      // Other config sections
      <section name="fundatie.Modules.Payment.Intersolve.Configuration.IntersolveConfigurationSection" 
               type="Fundatie.Modules.Payment.Intersolve.Configuration.IntersolveConfigurationSection, Fundatie.Modules.Payment.Intersolve" />
    </configSections>
    
    <fundatie.Modules.Payment.Intersolve.Configuration.IntersolveConfigurationSection>
      <username value="" />
      <password value="" />
      <options>
        <defaultCurrency value="EUR" />
        <balanceFactor value="100" />
        <maximumAgeInitializedTransactions value="1" />
        <defaultCardNumberPart1 value="" />
        <defaultCardNumberPart2 value="" />
      </options>
    </fundatie.Modules.Payment.Intersolve.Configuration.IntersolveConfigurationSection>

================================= ============= =====================================================================
Element                           Default value Description                                                          
================================= ============= =====================================================================
username                          None          The username to authenticate with the Intersolve payment provider    
password                          None          The password to authenticate with the Intersolve payment provider      
defaultCurrency                   EUR           The currency of the values sent to Intersolve                        
balanceFactor                     100           The factor of the balance that's used to calculate the 'real balance'
maximumAgeInitializedTransactions 1             The maximum age for initialized transactions in hours.               
defaultCardNumberPart1            None          The first default part of every gift card id                         
defaultCardNumberPart2            None          The second default part of every gift card id                        
================================= ============= =====================================================================

You will also need to add the following configuration in order to communicate with Intersolve:

.. code-block:: xml

    <system.serviceModel>
      <bindings>
        <basicHttpBinding>
          <binding name="giftcard_5_3Soap" />
        </basicHttpBinding>
      </bindings>
      <client>
        <endpoint address="" 
                  binding="basicHttpBinding" 
                  bindingConfiguration="giftcard_5_3Soap" 
                  contract="giftcard.giftcard_5_3Soap" 
                  name="giftcard_5_3Soap" />
      </client>
    </system.serviceModel>

.. note:: The address specified for the client endpoint should be different for testing and production environments.

Error messages
==============
Every payment module contains errors that can occur and an error message will be shown to the user.
The following keys should be added to the ``Enumerations.resx`` file in the ``CmsData\Resources``
directory for every supported language for your webshop:

========================================================================================= =====================================
Key                                                                                       Description                       
========================================================================================= =====================================
Fundatie_Modules_Payment_Intersolve_Enums_IntersolveValidationErrors_CardHasNoBalance     The gift card has no balance
Fundatie_Modules_Payment_Intersolve_Enums_IntersolveValidationErrors_IncorrectCombination Incorrect gift card combination
Fundatie_Modules_Payment_Intersolve_Enums_IntersolveValidationErrors_InProcess            The card is currently not usable
Fundatie_Modules_Payment_Intersolve_Enums_IntersolveValidationErrors_NotSelected          The user has not selected a gift card
Fundatie_Modules_Payment_Intersolve_Enums_IntersolveValidationErrors_InvalidData          The user entered invalid data
========================================================================================= =====================================