########
Overview
########

The Intersolve payment module allows projects based on Fundatie to offer the gift card payment method to visitors of the webshop. 
You can find more information about Intersolve `on their website <http://www.intersolve.nl/>`_


Compared to payment modules like Buckaroo, Intersolve is a bit different. 
Other modules integrate with a payment service provider, which offers a set of payment methods. 
The Intersolve module only offers payments via gift cards. 
A second difference is the fact that the customer fills in the details of the giftcard during the checkout in the webshop. 
Details to complete the payment, f.e. credit card data, are often gathered in the secure environment of the payment service provider. 
Because of this difference, payments using the gift cards are processed without forwarding the customer to an external environment.

Visit the `Quick guide <quick-guide.html>`_ page to read more on how to get started using the 
intersolve payment module.