###########
Quick guide
###########

Introduction
============
This section describes how to get started on integrating the MultiSafepay payment provider.

Prerequisites
==============
To start using this module, you need to install the ``Fundatie.Modules.Payment.MSP`` and 
``Fundatie.Modules.Payment.MSP.Web`` NuGet packages.

.. code-block:: PowerShell

    PM> Install-Package Fundatie.Modules.Payment.MSP
    PM> Install-Package Fundatie.Modules.Payment.MSP.Web

Configuration
=============
To use the Intersolve payment module, you need to add a configuration section. The following 
configuration can be used as an example:

.. code-block:: xml
    
    <configSections>
      // Other config sections
      <section name="fundatie.Modules.Payment.MSP.Configuration.MSPConfigurationSection" 
               type="Fundatie.Modules.Payment.MSP.Configuration.MSPConfigurationSection, Fundatie.Modules.Payment.MSP" />
    </configSections>
    
    <fundatie.Modules.Payment.MSP.Configuration.MSPConfigurationSection>
      <apiKey value="86df41ffacfc97580b16200f77966f3545846958" />
      <currency value="EUR">
      <urls>
        <returnUrl value="/multisafepay/return/" />
        <cancelTransactionUrl value="/multisafepay/canceltransaction/" />
        <startTransactionUrl value="/multisafepay/starttransaction/" />
        <updateTransactionUrl value="/multisafepay/updatetransaction/" />
      </urls>
      <environments>
        <testEnvironment url="https://testapi.multisafepay.com/v1/json/" />
        <liveEnvironment url="https://api.multisafepay.com/v1/json/" />
      </environments>
      <options>
        <opensInNewWindow value="false" />
        <useCustomBankTransferInstructions value="false" />
        <includeIpAddress value="false" />
        <includeEmailAddress value="false" />
        <manuallyAcceptCreditCardTransactions value="true" />
        <googleAnalyticsSiteId value="" />
        <paymentLinkLifetime value="30" />
      </options>
    </fundatie.Modules.Payment.MSP.Configuration.MSPConfigurationSection>

==================================== ============= ============================================================================
Element                              Default value Description                                                                 
==================================== ============= ============================================================================
apiKey                               None          The API key for communicating with MultiSafepay                             
currency                             None          The currency of the payment
returnUrl                            None          The URL to be called when a a user is redirected back to the webshop        
cancelTransactionUrl                 None          The URL to be called if a transaction is cancelled by a user                
startTransactionUrl                  None          The URL to be called to start a transaction                                 
updateTransactionUrl                 None          The URL to be called by MultiSafepay if a transaction is updated            
testEnvironment                      None          The URL of the MultiSafepay test environment                                  
liveEnvironment                      None          The URL of the MultiSafepay live environment                                  
opensInNewWindow                     false         Determines if the transaction should take place in a new window             
useCustomBankTransferInstructions    false         Determines if the the default email with payment instructions should be used
includeIpAddress                     false         Determines if the users IP address should be included                       
includeEmailAddress                  false         Determines if the users email address should be included                    
manuallyAcceptCreditCardTransactions false         Determines if credit card transactions should be accepted manually          
googleAnalyticsSiteId                None          The analytics site id to be injected in payment pages for tracking          
paymentLinkLifetime                  None          The lifetime of the payment link in days                                    
==================================== ============= ============================================================================

Error messages
==============
Every payment module contains errors that can occur and an error message will be shown to the user.
The following keys should be added to the ``Enumerations.resx`` file in the ``CmsData\Resources``
directory for every supported language for your webshop:

=============================================================================== ==================================
Key                                                                             Description                       
=============================================================================== ==================================
Fundatie_Modules_Payment_MSP_Enums_MSPValidationErrors_IssuerRequired           No issuer was selected by the user
=============================================================================== ==================================