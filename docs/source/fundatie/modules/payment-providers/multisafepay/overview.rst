########
Overview
########

The MultiSafepay payment module allows projects based on Fundatie to offer the payment services provided by MultiSafepay. 
The module uses the JSON gateway via the Nuget package provided by MultiSafepay. 
You can find more information about the JSON gateway and the Nuget package on `the MultiSafepay website <https://www.multisafepay.com/documentation/doc/Code%20Wrappers/>`_

Payment methods
---------------

The module offers the following payment methods:

* Bank transfer
* Dotpay
* iDeal
* MasterCard
* Santander
* Visa

Transaction flow
----------------
When initiating a payment a JSON request is sent to MultiSafepay. 
MultiSafepay returns the URL of the secure payment environment to which the customer should be forwarded.
After completing the payment, the customer will be redirected to the webshop. 
At this point the payment status will be evaluated. 
If the payment completed successfully, the customer will be forwarded to the order confirmation page.
Otherwise the customer will return to the Checkout overview page. 

.. warning:: When a customer returns to the webshop, the payment status is not yet final. You should not use this status for exports to order processing systems.

Once MultiSafepay completed the transaction, a request is sent to the webshop confirming the status of a payment.
After this final (server-to-server) update from MultiSafepay the implementation should initiate exporting the order to external systems.

.. note:: When we receive an update we only take the transaction identifier from the request. This identifier is used to fetch the payment details using the gateway. This is done to avoid fraude via request manipulation.

Implementation
--------------
Visit the `Quick guide <quick-guide.html>`_ page to read more on how to get started using the 
MultiSafepay payment module.