########
Overview
########

Fundatie allows customers to pay for a single order using multiple payment methods. This way, the 
customer can use payment methods like gift cards in combination with another one like a creditcard. 
A project based on Fundatie can be configured to allow the combination of specific payment methods.

Visit the `Quick guide <quick-guide.html>`_ page to read more on how to get started using multiple 
payment methods, or visit the `Advanced <advanced.html>`_ page to learn more about advanced topics, 
like modifying payment method behavior or integrating your own payment method.