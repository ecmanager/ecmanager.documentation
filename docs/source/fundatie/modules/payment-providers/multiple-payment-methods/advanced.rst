########
Advanced
########

Flow
====

- CheckoutOverview: responsible for getting the basket prepared for payment and conversion to order.
- CheckoutPayment: responsible for redirecting to payment providers

The following sequence diagram shows the flow for adding selected payment methods to the basket:

.. figure:: ../../../../_static/img/fundatie/payment-providers/multiple-payment-methods/CheckoutOverview.png
    :alt: checkoutoverview flow
    :align: center

    Sequence diagram: CheckoutOverview flow

If the flow described in the diagram was successful, the ``Basket`` is converted to an ``Order`` and
the user will be redirected to the ``CheckoutPayment`` page. A basket can have multiple orders 
associated with it. If a payment fails and the user attempts to continue through the checkout again,
a new order is created.

.. figure:: ../../../../_static/img/fundatie/payment-providers/multiple-payment-methods/CheckoutPayment.png
    :alt: checkoutpayment flow
    :align: center

    Sequence diagram: CheckoutPayment flow

After successful payment(s), the user will be redirected to the ``CheckoutThankYou`` page.

Modifying payment module behavior
=================================
Fundatie offers some payment providers and their corresponding payment methods. It is fairly common
for a project based on Fundatie to need those payment methods to behave in a different manner, for 
example to process some data that isn't built into Fundatie. You can modify payment method behavior 
by overriding their existing behavior. As an example, we will add additional validation to the 
Fundatie Klarna payment method. This can be done  as shown in the following code sample:

.. code-block:: c#
    :linenos:
    
    public class CustomKlarna : Klarna
    {
        private readonly string _providerIdentifier;

        public CustomKlarna(string providerIdentifier, Logic.Interfaces.ITranslationLogic translationLogic, ICustomerLogic customerLogic)
            : base(providerIdentifier, translationLogic, customerLogic)
        {
            _providerIdentifier = providerIdentifier;
        }

        // Set the module identifier of the custom payment method
        public override string GetModuleIdentifier()
        {
            return string.Format("{0}-{1}-V{2}{3}{4}",
                _providerIdentifier,
                "CustomKlarna",
                GetVersion().Major,
                GetVersion().Minor,
                GetVersion().Build);
        }

        // Set the version of the custom payment method
        public override Version GetVersion()
        {
            return new Version(1, 0, 0);
        }

        // Add extra validation to the overridden payment method
        public override UserInputValidationResult ValidateUserInput(Dictionary<string, string> userInput, ILifetimeScope scope)
        {
            // Check if the data collection contains our custom expected data. If it is missing, 
            // return the result and an corresponding error message.
            if (!collection.ContainsKey("MyCustomData"))
            {
                return new SpecificDataValidationResult
                {
                    PaymentMethodIdentifier = GetModuleIdentifier(),
                    IsValid = false,
                    ValidationMessage = "My custom data is missing"
                };
            }

            return base.ValidateSpecificData(collection, scope);
        }

        // Parse the extra validated data and add it to the parse result
        public override Dictionary<string, string> GetExtraData(IPaymentDataProcessingContext context)
        {
            var result = base.GetSpecificData(context);

            string myCustomData;
            if (context.RequestDataDictionary.TryGetValue("MyCustomData", out myCustomData))
            {
                result.Add("ParsedMyCustomData", myCustomData);
            }

            return result;
        }
    }

Now that we have our custom payment method, we need the Klarna payment provider to use the custom
one instead of the default payment method. This can be done as follows:

.. code-block:: c#
    :linenos:
    
    // An override of the Klarna payment provider
    public class CustomKlarnaPaymentProvider : KlarnaPaymentProvider
    {
        public CustomKlarnaPaymentProvider(ITranslationLogic translationLogic, ICustomerLogic customerLogic)
            : base(translationLogic, customerLogic)
        {
            ProviderIdentifier = string.Format("CUSTOMKLARNA-V{0}{1}{2}", 
                ProviderImplementationVersion.Major,
                ProviderImplementationVersion.Minor, 
                ProviderImplementationVersion.Build);
        }
        
        public override string GetProviderIdentifier()
        {
            return string.Format("CUSTOMKLARNA-V{0}{1}{2}", 
                ProviderImplementationVersion.Major,
                ProviderImplementationVersion.Minor, 
                ProviderImplementationVersion.Build);
        }
        
        protected override void AddPaymentMethods()
        {
            PaymentMethods.Add(new CustomKlarna(GetProviderIdentifier(), TranslationLogic, CustomerLogic));
        }
    }
    
Finally, the custom payment provider has to be registered using autofac:

.. code-block:: c#
   :linenos:
   
    builder.RegisterType<CustomKlarnaPaymentProvider>()
        .UsingConstructor(typeof(ITranslationLogic), typeof(ICustomerLogic))
        .As<IFundatiePaymentProvider>()
        .PropertiesAutowired();

.. note :: After adding your library containing the custom payment method to the ecManager CMS, you can select when it adding a new payment method.
        
Integrating a custom payment module
===================================
When integrating your own custom payment module, there are a couple of ground rules to keep in mind:

- The provider and method should not assume it is the only chosen one by a user
- The provider and method should each have their own unique identifier

The following members of the ``IFundatiePaymentMethod`` interface should be implemented:

===================================== ==========================================================================================================================
Method                                Description                                                                                                               
===================================== ==========================================================================================================================
GetExtraDataControllerActionReference Determines the controller and method name to call to display fields on the CheckoutOverview page.
GetInitiatePaymentUrl                 Determines the URL to call if a transaction should be started for this payment method.
GetPaymentUrl                         Determines the URL that the user will be redirected to.
GetPaymentStatus                      Translates a payment status known to this payment method to a ecManager order payment status.
ValidateUserInput                     Validates user input.
GetExtraData                          Gets extra data that's needed for this payment method from the user input.
GetPayableAmount                      Determines how much of an amount this payment method can pay, based on the input. This is the entire amount in most cases.
CreateRefund                          Creates a refund for this payment method.
===================================== ==========================================================================================================================

.. warning :: Not every existing payment method in Fundatie has implemented the CreateRefund method.