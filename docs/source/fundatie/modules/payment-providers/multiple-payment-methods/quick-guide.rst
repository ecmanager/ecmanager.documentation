###########
Quick guide
###########

Introduction
============
Fundatie allows users to pay for an order using multiple payment methods. This section will guide 
you through the process of enabling, configuring and understanding partial payments.

Configuration
=============
To enable the use of multiple payment methods for an order, you need to add a configuration section. 
The following configuration can be used as an example:

.. code-block:: xml
    
    <configSections>
      <!-- Other config sections -->
      <section name="fundatie.Modules.Payment.Core" type="Fundatie.Modules.Payment.Core.Configuration.PaymentConfigurationSection, Fundatie.Modules.Payment.Core" />
    </configSection>
    
    <fundatie.Modules.Payment.Core>
      <paymentMethodCombinationGroups>
        <paymentMethodCombinationGroup name="intersolveIdealCombo">
          <paymentMethod paymentModuleIdentifier="Intersolve-V100-Giftcard-V100" orderTotalDistributionSequenceNumber="0" transactionSequenceNumber="1" />
          <paymentMethod paymentModuleIdentifier="MSP-V100-IDEAL-V100" orderTotalDistributionSequenceNumber="1" transactionSequenceNumber="0" />
        </paymentMethodCombinationGroup>
      </paymentMethodCombinationGroups>
    </fundatie.Modules.Payment.Core>

This configuration determines..

- ..which combinations of payment methods can be chosen when paying for an order.
- ..what the sequence is when distributing the order total across the chosen payment methods.
- ..what the sequence of transactions is when the user follows the payment flow.

.. note :: The sequences start at 0.

Sample scenario
---------------
A user wants to place an order for €40. The user has an intersolve gift card with a balance of €10. 
According to the sample configuration shown above, the remaining €30 can be paid for using iDEAL.

Because iDEAL can pay for 100% of the order total, the intersolve payment module should take 
precendence when determining how the order total is distributed. This is configured using the
``orderTotalDistributionSequenceNumber`` attribute. A lower ``orderTotalDistributionSequenceNumber``
means as much of the order total is distributed to that payment module. In this case, €10 of the €40
will be distributed to the intersolve module, and €30 will be distributed to the iDEAL payment 
module.


The ``transactionSequenceNumber`` attribute determines the order in which the actual transactions 
take place. You might want to switch up the sequence if you expect certain payment module is more 
likely to fail. Because the balance of the intersolve gift card is validated before the payment  
transaction process, the intersolve payment module is less likely to fail compared to iDEAL. This is
why in this scenario the iDEAL transaction should take place first. If the iDEAL transaction fails,
the intersolve transaction never happens. This can prevent having to do unnecessary refunds.

.. warning :: Fundatie does not handle refunds. A seperate process should be arranged for those.

User input
==========
When a user selects payment methods and proceeds, an AJAX request is sent to the ``ProcessOrder`` 
method of the ``CheckoutOverviewController``. This method expects form data in a certain format. 
This format is as follows:

.. code-block:: xml

    <MODEL-NAME>.PaymentMethods[<PAYMENT-MODULE-IDENTIFIER>].<PROPERTY-NAME>

For example:

.. code-block:: xml

    CheckoutOverviewEditModel.PaymentMethods[KLARNA-V100-Klarna-V100].IsSelected
    CheckoutOverviewEditModel.PaymentMethods[KLARNA-V100-Klarna-V100].BirthDate

This way, the corresponding payment module can validate and use its own data. The ``IsSelected``
property is mandatory for each payment module. If the ``IsSelected`` form field is missing or its 
value is false, the data for the corresponding payment module will be ignored.

Input validation and processing
===============================
After the user input has been parsed, it is passed to the ``PaymentMethodDelegationLogic``. This 
logic validates, parses and processes the user input and finally passes it to the basket as a 
``UpdatePaymentMethodChangeRequest``. If at any point during this process data turns out to be 
invalid or an error occurs, a ``ExtraDataValidationResult`` is returned which has a property 
``IsValid``. If all required data is present and valid, that property is true, otherwise it is 
false and an enum can be retrieved from the ``ValidationCode`` property. This enum can be translated
to a message which can be shown to the user. 

The following steps are involved when input is passed to the ``PaymentMethodDelegationLogic``:

Validation of user input
------------------------
Passes data to each selected payment module and lets the payment module validate if all user input
is present and valid. For example, Klarna requires a birth date.

Parsing of user input
---------------------
Passes data to each selected payment module and lets the payment module validate parse the input to 
a better usable format. For example, for intersolve the balance is retrieved.

Determination of the transaction sequence
-----------------------------------------
Determines the transaction sequence for the selection of payment methods using the configuration. 
If the selection of payment methods is not configured, it is not allowed and the process will fail.

Determination of the amount distribution
----------------------------------------
Determines the amount each selected payment method needs to pay, using the configuration.

Removal of payment methods from the basket
------------------------------------------
Removes any existing selected payment methods from the basket. This is done because the new 
selection of payment methods should replace the old one.

Addition of payment methods to the basket
-----------------------------------------
Adds the new selection of payment methods to the basket. Each payment method includes:

- Amount to be paid by the payment method
- The number in the sequence of transactions
- Any data belonging to the payment method that was validated and parsed earlier in the process

Conversion to order
-------------------
The basket contains all necessary data at this point, and is converted to an order. This step is 
performed in the ``CheckoutOverviewController``. 

Payment transactions
--------------------
If all previous steps were successful, the user is redirected to the ``CheckoutPaymentController``.
For each payment method in the basket, the user is redirected to the corresponding payment provider 
in order of transaction sequence number. If a transaction fails or is cancelled, the subsequent 
transactions will never occur. 

Defining error messages
========================
The error codes returned by the PaymentMethodDelegationLogic and payment modules are enums which are
translated by the the ecManager ``ResourceService``. This means that the translations will be read 
from the ``Enumerations.resx`` file in the ``CmsData\Resources`` directory. 

The general error messages are translated from the enum ``GeneralValidationMessages`` in the 
``Fundatie.Modules.Payment.Core.Enums`` namespace. You can define the error message as shown in the 
following example:

======================================================================================================== =========================================================
Key                                                                                                      Validation message             
======================================================================================================== =========================================================
Fundatie_Modules_Payment_Core_Enums_GeneralValidationMessages_NoPaymentMethodsSelected                   No payment method was selected
Fundatie_Modules_Payment_Core_Enums_GeneralValidationMessages_InvalidCombinationOfPaymentMethodsSelected A user selected an invalid combination of payment methods  
Fundatie_Modules_Payment_Core_Enums_GeneralValidationMessages_TechnicalError                             A technical error occurred  
======================================================================================================== =========================================================

Each payment module contains an enum with errors for that specific module. You should define the 
error messages for every payment method.

Error handling
==============
If at any point in the flow an error occurs or the user cancels a transaction, the user will be 
redirected to the ``CheckoutOverview`` page and a ``PaymentFlowFailedEvent`` is dispatched. 
You can subscribe to this event in your startup as follows:

.. code-block:: c#
    :linenos:
    
    public void Configuration(IAppBuilder app)
    {
        var eventAggregatorLogic = DependencyResolver.Current.GetService<IEventAggregatorLogic>();
        eventAggregatorLogic.Subscribe<PaymentFlowFailedEvent>(myCallBack);
    }
    
The ``PaymentFlowFailedEvent`` event containts the basket, order and scope at the time of failure.
Any of those can be null.