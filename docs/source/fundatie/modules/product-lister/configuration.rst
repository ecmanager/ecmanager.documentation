#############
ProductLister
#############

Configuration
-------------


This configuration will determine settings required for the product lister of Fundatie. The following block
shows an example of this configuration:

.. code-block:: xml

	<configSections>
	<section name="fundatie.Web.Modules.ProductLister" type="Fundatie.Web.Modules.ProductLister.Configuration.ListerConfigurationSection, Fundatie.Web.Modules.ProductLister" />
	</configSections>

	<fundatie.Web.Modules.ProductLister >
		<!-- products: Product section of the product lister -->
		<!-- numberOfProductsOnPage: The numberOfProductsOnPage per page -->
		<products numberOfProductsOnPage="10">
			<!-- enableFamilies: use families in the lister (true/false) -->
			<!-- familyAttributeSelectionCodes: Which selection codes are used for family grouping separated by "," -->
			<families enableFamilies="true" familyAttributeSelectionCodes="CLR,MAT"/>
			<!-- enableReviews: use reviews in the lister (true/false) -->
			<!-- minRating: The minimal rating (stars) that can be given (default 0). -->
			<!-- maxRating: The maximum rating (stars) that can be given (default 5). -->
			<reviews enableReviews="true" minRating="0" maxRating="5"/>
		</products>
		<!-- filterAttributeSelectionCodes: The selection codes used for the facets on the search page. -->
		<!-- The facets used in the product lister when a product group is listed can be configured in the CMS. -->
		<search filterAttributeSelectionCodes="MRK,WAT,CLR"/>
		<!-- enableCompare: Enable the product comparer (true/false). -->
		<compare enableCompare="true" />
		<!-- enablePager: Enabled the pager (true/false).)
		<!-- numberOfPagerItems: The number of pager items. -->
		<pager enablePager="true" numberOfPagerItems="5" />
	</fundatie.Web.Modules.ProductLister>

