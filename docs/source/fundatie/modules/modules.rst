=======
Modules
=======

.. toctree::
	:maxdepth: 1

	sitemap-handler/configuration
	errorpage-handler/configuration
	checkout-delivery/configuration
	location-overview/configuration
	item-last-seen/configuration
	location-detail/configuration
	mail/configuration
	news-detail/configuration
	news-overview/configuration
	output-cache/index
	product-compare/configuration
	product-detail/configuration
	product-lister/configuration
	search/configuration
	search-autocomplete/configuration
	sitemap/configuration
	tweakwise-autocomplete/configuration
	tweakwise-product-lister/configuration
	payment-events/overview
	payment-modules/payment-modules
	payment-providers/payment-providers