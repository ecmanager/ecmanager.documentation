################
LocationOverview
################

Configuration
-------------


This configuration will determine settings required for the location overview block in Fundatie. The following block
shows an example of this configuration:

.. code-block:: xml

	<configSections>
		<section name="fundatie.Web.Modules.LocationOverview" type="Fundatie.Web.Modules.LocationOverview.Configuration.LocationOverviewConfigurationSection, Fundatie.Web.Modules.LocationOverview" />
	</configSections>

	<!-- googleApiKey: The api key used by the Google Location Services. -->
	<fundatie.Web.Modules.LocationOverview googleApiKey="" />