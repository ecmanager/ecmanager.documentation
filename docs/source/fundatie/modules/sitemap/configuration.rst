#######
Sitemap
#######

Configuration
-------------


This configuration will determine settings required for the sitemap of Fundatie. The following block
shows an example of this configuration:

.. code-block:: xml

	<configSections>
		<section name="fundatie.Web.Modules.Sitemap" type="Fundatie.Web.Modules.Sitemap.Configuration.SitemapConfigurationSection, Fundatie.Web.Modules.Sitemap" />
	</configSections>

	<!-- navigationSelectionCodes: The selection codes of the NavigationItems included in the sitemap. -->
	<fundatie.Web.Modules.Sitemap navigationSelectionCodes="WIN,INF,ACC" />

