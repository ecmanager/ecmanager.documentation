##################
SearchAutocomplete
##################

Configuration
-------------


This configuration will determine settings required for the search autocomplete in Fundatie. The following block
shows an example of this configuration:

.. code-block:: xml

	<configSections>
		<section name="fundatie.Web.Modules.SearchAutocomplete" type="Fundatie.Web.Modules.SearchAutocomplete.Configuration.AutocompleteConfigurationSection,Fundatie.Web.Modules.SearchAutocomplete" />
	</configSections>

	<fundatie.Web.Modules.SearchAutocomplete>
		<elements>
		  <!-- amountOfResults: The pageSize of products in the autocomplete. -->
		  <!-- imageKey: The image key used in the autocomplete. -->
		  <element key="Products" amountOfResults="5" imageKey="Main" />
		  <!-- amountOfResults: The pageSize of Combinations in the autocomplete. -->
		  <!-- imageKey: The image key used in the autocomplete. -->
		  <element key="Combinations" amountOfResults="2" imageKey="Main" />
		  <!-- amountOfResults: The pageSize of InformationItems in the autocomplete. -->
		  <!-- imageKey: The image key used in the autocomplete. -->
		  <element key="InformationItems" amountOfResults="4" imageKey="Main" />
		  <!-- amountOfResults: The pageSize of Faq items in the autocomplete. -->
		  <!-- imageKey: The image key used in the autocomplete. -->
		  <element key="Faq" amountOfResults="1" imageKey="Main" />
		  <!-- amountOfResults: The pageSize of NewsItems in the autocomplete. -->
		  <!-- imageKey: The image key used in the autocomplete. -->
		  <element key="NewsItems" amountOfResults="1" imageKey="Main" />
		  <!-- amountOfResults: The pageSize of ProductGroups in the autocomplete. -->
		  <!-- imageKey: The image key used in the autocomplete. -->
		  <element key="ProductGroups" amountOfResults="1" imageKey="Main" />
		</elements>
	  </fundatie.Web.Modules.SearchAutocomplete>
	  