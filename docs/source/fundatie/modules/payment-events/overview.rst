##############
Payment Events
##############
To make the PaymentFlow work some Events needs to be handled by the PaymentLogic. This page contains a code snippet used to subscribe to these events.


.. code-block:: C#

	var moduleLogic = DependencyResolver.Current.GetService<IPaymentModuleLogic>();
	if (moduleLogic != null)
	{
		moduleLogic.InitializePaymentModules();
	}

	var paymentEventLogic = DependencyResolver.Current.GetService<IPaymentEventLogic>();
	if (paymentEventLogic != null)
	{
		var eventAggregatorLogic = DependencyResolver.Current.GetService<IEventAggregatorLogic>();
		eventAggregatorLogic.Subscribe<PaymentUpdatedEvent>(paymentEventLogic.ProcessOrderPaymentStatus);
		eventAggregatorLogic.Subscribe<PaymentUpdatedEvent>(paymentEventLogic.SendOrderConfirmation);
		eventAggregatorLogic.Subscribe<PaymentFlowFailedEvent>(paymentEventLogic.OnPaymentFailed_ReactivateBasket);
	}