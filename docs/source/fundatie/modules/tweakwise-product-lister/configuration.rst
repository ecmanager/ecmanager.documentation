#####################
TweakwiseAutocomplete
#####################

Configuration
-------------


This configuration will determine settings required for the tweakwise seach autocomplete in Fundatie. The following block
shows an example of this configuration:

.. code-block:: xml

	<configSections>
		<section name="fundatie.Web.Modules.TweakwiseAutocomplete" type="Fundatie.Web.Modules.TweakwiseAutocomplete.Configuration.AutocompleteConfigurationSection,Fundatie.Web.Modules.TweakwiseAutocomplete" />
	</configSections>

	<fundatie.Web.Modules.TweakwiseAutocomplete>
		<!-- amountOfResults: The pageSize of products in the autocomplete. -->
		<!-- imageKey: The image key used in the autocomplete. -->
		<element amountOfResults="5" imageKey="Main" />
	</fundatie.Web.Modules.TweakwiseAutocomplete>
	  