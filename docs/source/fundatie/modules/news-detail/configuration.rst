##########
NewsDetail
##########

Configuration
-------------


This configuration will determine settings required for the news detail of Fundatie. The following block
shows an example of this configuration:

.. code-block:: xml

	<configSections>
		<section name="fundatie.Web.Modules.NewsDetail" type="Fundatie.Web.Modules.NewsDetail.Configuration.NewsDetailConfigurationSection, Fundatie.Web.Modules.NewsDetail" />
	</configSections>

	<fundatie.Web.Modules.NewsDetail>
		<!-- addThisPubId: AddThis publisher id -->
		<!-- Create AddThisIPub at addthis.com -->
		<socialMedia addThisPubId="" />
	</fundatie.Web.Modules.NewsDetail>

