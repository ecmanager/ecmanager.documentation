#############
ProductDetail
#############

Configuration
-------------


This configuration will determine settings required for the product detail page of Fundatie. The following block
shows an example of this configuration:

.. code-block:: xml

	<configSections>
	<section name="fundatie.Web.Modules.ProductDetail" type="Fundatie.Web.Modules.ProductDetail.Configuration.ProductDetailConfigurationSection, Fundatie.Web.Modules.ProductDetail" />
	</configSections>

	<fundatie.Web.Modules.ProductDetail>
	<!-- enableReviews: use reviews on the product detail page (true/false) -->
		<!-- minRating: The minimal rating (stars) that can be given (default 0). -->
		<!-- maxRating: The maximum rating (stars) that can be given (default 5). -->
		<reviews enableReviews="true" minRating="0" maxRating="5" defaultReviewStatus="WaitingForApproval" />
		<!-- videoSourceType: The source of the videos (Default: YouTube), Default options (YouTube, Vimeo, Local) -->
		<!-- Additional video source types can be added by extending the module. -->
		<video videoSourceType="YouTube" />
		<!-- addThisPubId: AddThis publisher id --> 
		<!-- Create AddThisIPub at addthis.com -->
		<socialMedia addThisPubId="" />
	</fundatie.Web.Modules.ProductDetail>