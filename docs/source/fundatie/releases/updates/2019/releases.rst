====
2019
====

.. toctree::
	:maxdepth: 1
	:glob:
	:reversed:
	:hidden:

	*

.. |update2019.014| raw:: html

	<strong>
		<a href="2019.014.html">Fundatie - update 2019.014</a>
	</strong>

| |update2019.014|
| *05-12-2019*
| Packages:

- Fundatie.Web.Core 4.3.5
- Fundatie.Modules.Payment.MSP 4.3.2
- Fundatie.Modules.Payment.MSP.Web 4.3.2
- Fundatie.Web.Modules.Contact 4.3.3
- Fundatie.Web.Modules.LookDetail 4.3.2
- Fundatie.Web.Modules.NewsDetail 4.3.1
- Fundatie.Web.Modules.ProductCompare 4.3.2
- Fundatie.Web.Modules.ProductDetail 4.3.2
- Fundatie.Web.Modules.ProductLister 4.3.4
- Fundatie.Web.Modules.Search 4.3.1

| Repositories:

- Fundatie.StarterKit tag: 4.3.6

Updated thirdparty library MultiSafepay to the latest version to support the newest payment methods.

Removed the old grunt implementation for generating styles and javascript files. Added the new
webpack implementation.

.. |update2019.013| raw:: html

	<strong>
		<a href="2019.013.html">Fundatie - update 2019.013</a>
	</strong>

| |update2019.013|
| *28-10-2019*
| Packages:

- Fundatie.Core.Logic 4.3.4

Re-added old function definitions in the interface for conflicting module errors.

.. |update2019.012| raw:: html

	<strong>
		<a href="2019.012.html">Fundatie - update 2019.012</a>
	</strong>

| |update2019.012|
| *21-10-2019*
| Packages:

- Fundatie.Core 4.3.2
- Fundatie.Core.Logic 4.3.3
- Fundatie.Web.Core 4.3.4
- Fundatie.Web.Modules.AccountLogout 4.3.1
- Fundatie.Web.Modules.AccountOrderDetail 4.3.1
- Fundatie.Web.Modules.AccountWishList 4.3.2
- Fundatie.Web.Modules.CheckoutBasket 4.3.2
- Fundatie.Web.Modules.CheckoutRelatedCombinations 4.3.2
- Fundatie.Web.Modules.ProductCompare 4.3.1
- Fundatie.Web.Modules.ProductLister 4.3.3

The ecManager platform is now able to filter products and combinations based on customers and
configured catalog settings. This is also added to Fundatie.

.. |update2019.011| raw:: html

	<strong>
		<a href="2019.011.html">Fundatie - update 2019.011</a>
	</strong>

| |update2019.011|
| *03-10-2019*
| Packages:

- Fundatie.Web.Modules.ErrorPagesHandler 4.3.1
- Fundatie.Web.Modules.SitemapHandler 4.3.1

Removed some hardcoded references to the cmsdata folder. This is now read from config.

.. |update2019.010| raw:: html

    <strong>
        <a href="2019.010.html">Fundatie - update 2019.010</a>
    </strong>

| |update2019.010|
| *11-09-2019*
| Packages:

- Fundatie.Core.Logic 4.3.2
- Fundatie.Web.Core 4.3.3
- Fundatie.Web.Modules.AccountLogin 4.3.1
- Fundatie.Web.Modules.CheckoutDelivery 4.3.6
- Fundatie.Web.Modules.CheckoutLoginChoice 4.3.2
- Fundatie.Web.Modules.Contact 4.3.2
- Fundatie.Web.Modules.LookDetail 4.3.1
- Fundatie.Web.Modules.ProductDetail 4.3.1
- Fundatie.Web.Modules.SetDetail 4.3.1
- Fundatie.Web.Modules.SetRelatedToProduct 4.3.1

Changed a few modules using a centralized LoginController. Fixed some minor issues.

.. |update2019.009| raw:: html

	<strong>
		<a href="2019.009.html">Fundatie - update 2019.009</a>
	</strong>

| |update2019.009|
| *11-07-2019*
| Packages:

- Fundatie.Modules.Payment.Buckaroo 4.3.2
- Fundatie.Modules.Payment.Buckaroo.Web 4.3.1
- Fundatie.Modules.Payment.MSP 4.3.1
- Fundatie.Modules.Payment.MSP.Web 4.3.1

Added customer information to PayPal payments to reduce fraud by reclaiming money through PayPal.
This applies for the Payment Providers Buckaroo and Multisafepay.

.. |update2019.008| raw:: html

	<strong>
		<a href="2019.008.html">Fundatie - update 2019.008</a>
	</strong>

| |update2019.008|
| *20-06-2019*
| Packages:

- Fundatie.Web.Core 4.3.2
- Fundatie.Web.Modules.CheckoutBasket 4.3.1
- Fundatie.Web.Modules.CheckoutDelivery 4.3.5
- Fundatie.Web.Modules.CheckoutOverview 4.3.3
- Fundatie.Web.Modules.CheckoutPayment 4.3.1
- Fundatie.Web.Modules.CheckoutThankYou 4.3.2
- Fundatie.Web.Modules.CheckoutUserData 4.3.3
- Fundatie.Web.Modules.Contact 4.3.1
- Fundatie.Web.Modules.ProductLister 4.3.2

| Repositories:

- Fundatie.StarterKit tag: 4.3.5

Added NoCacheAttribute on controllers in the Checkout process so that the results won't be cached in
the browser. Compare checkboxes are now properly checked if a product is marked for comparison.

.. |update2019.007| raw:: html

	<strong>
		<a href="2019.007.html">Fundatie - update 2019.007</a>
	</strong>

| |update2019.007|
| *03-06-2019*
| Packages:

- Fundatie.Web.Modules.CheckoutDelivery 4.3.4
- Fundatie.Web.Modules.CheckoutMiniBasket 4.3.1

Updated the Checkout delivery module with a better delivery selection improvement. The selected
delivery method and options will be remembered.

.. |update2019.006| raw:: html

	<strong>
		<a href="2019.006.html">Fundatie - update 2019.006</a>
	</strong>

| |update2019.006|
| *09-05-2019*
| Packages:

- Fundatie.Web.Modules.AccountCreateAccount 4.3.2
- Fundatie.Web.Modules.AccountAddressBook 4.3.1
- Fundatie.Web.Modules.CheckoutDelivery 4.3.3
- Fundatie.Web.Modules.CheckoutOverview 4.3.2
- Fundatie.Web.Modules.CheckoutLoginChoice 4.3.1

| Repositories:

- Fundatie.StarterKit tag: 4.3.4

Fixed some styling in Checkout, Checkout LoginChoice and Account Addresbook Modules. Fixed a couple
javascript bugs in AccountAddressBook and StarterKit modules.

.. |update2019.005| raw:: html

	<strong>
		<a href="2019.005.html">Fundatie - update 2019.005</a>
	</strong>

| |update2019.005|
| *17-04-2019*
| Packages:

- Fundatie.Web.Core 4.3.1
- Fundatie.Web.Modules.Account 4.3.0
- Fundatie.Web.Modules.AccountCreateAccount 4.3.1
- Fundatie.Web.Modules.AccountUserData 4.3.1
- Fundatie.Web.Modules.CheckoutDelivery 4.3.2
- Fundatie.Web.Modules.CheckoutUserData 4.3.2
- Fundatie.Web.Modules.VacancyDetail 4.3.2
- Fundatie.Web.Modules.VacancyOverview 4.3.2

Added new module Account for unified LoginUnique check. Due to the GeoBlock law in the EU we added a
check in CheckoutDelivery module for delivery to configured countries.

.. |update2019.003| raw:: html

	<strong>
		<a href="2019.003.html">Fundatie - update 2019.003</a>
	</strong>

| |update2019.003|
| *07-03-2019*
| Packages:

- Fundatie.Core 4.3.1
- Fundatie.Core.Logic 4.3.1
- Fundatie.Modules.Payment.Buckaroo 4.3.1
- Fundatie.Web.Modules.AccountWishList 4.3.1
- Fundatie.Web.Modules.ProductLister 4.3.1
- Fundatie.Web.Modules.VacancyDetail 4.3.1
- Fundatie.Web.Modules.VacancyOverview 4.3.1

| Repositories:

- Fundatie.StarterKit tag: 4.3.2

Added EntityState for Vacancy. Added scroll to first error implemented in Fundatie.

.. |update2019.001| raw:: html

	<strong>
		<a href="2019.001.html">Fundatie - update 2019.001</a>
	</strong>

| |update2019.001|
| *23-01-2019*
| Packages:

- Fundatie.Resources 4.3.1
- Fundatie.Web.Modules.CheckoutAddressBook 4.3.1
- Fundatie.Web.Modules.CheckoutDelivery 4.3.1
- Fundatie.Web.Modules.CheckoutOverview 4.3.1
- Fundatie.Web.Modules.CheckoutThankYou 4.3.1
- Fundatie.Web.Modules.CheckoutUserData 4.3.1

Added configuration for delivery locations. Added and improved translations resources for checkout.