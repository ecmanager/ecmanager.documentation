==========================
Fundatie - update 2019.013
==========================

Bugfixes
--------

| **ECM-6266**
| Packages:

- Fundatie.Core.Logic 4.3.4

Re-added old function definitions in the interface for conflicting module errors. Due to changes in 
the ProductLogic interface some Fundatie Web modules aren't working anymore. Fixed this issue by
restoring the old function definitions and add the new definitions as seperate functions.