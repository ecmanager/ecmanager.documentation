==========================
Fundatie - update 2019.007
==========================

Bugfixes
--------

| **ECM-6235**
| Packages:

- Fundatie.Web.Modules.CheckoutDelivery 4.3.4
- Fundatie.Web.Modules.CheckoutMiniBasket 4.3.1

Updated loader spinner in the minibasket. This works now correctly.

Updated the Checkout delivery module with a better delivery selection improvement. The selected
delivery method and options will be remembered.

.. important ::

    Some logic from the DeliverOrder function has been replaced in a new function
    ProcessTransporterOptions. Check your implementation for overrides on this particular fix.

File changes
------------

.. csv-table ::
    :header: "Filename", "Status", "Kind"

    "checkoutdelivery.js", "Changed", "javascript file"
    "checkoutminibasket.js", "Changed", "javascript file"
    "CheckoutDelivery/_DeliveryOptionHeaderPartial.cshtml", "Changed", "razorview file"
    "CheckoutDelivery/_TransporterDeliveryOptionPartial.cshtml", "Changed", "razorview file"
    "_CheckoutDeliveryPartial.cshtml", "Changed", "razorview file"