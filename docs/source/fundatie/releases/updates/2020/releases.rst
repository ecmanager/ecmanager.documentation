====
2020
====

.. toctree::
	:maxdepth: 1
	:glob:
	:reversed:
	:hidden:

	*

.. |update2020.019| raw:: html

	<strong>
		<a href="2020.019.html">Fundatie - update 2020.019</a>
	</strong>

| |update2020.019|
| *17-11-2020*
| Packages:

- Fundatie.Web.Modules.SitemapHandler 4.3.3

Fixed the bug that only the first 100 items were added (repeatedly) to the XML.


.. |update2020.018| raw:: html

	<strong>
		<a href="2020.018.html">Fundatie - update 2020.018</a>
	</strong>

| |update2020.018|
| *21-10-2020*
| Packages:

- Fundatie.Modules.Payment.MSP 4.3.6
- Fundatie.Web.Core 4.3.10
- Fundatie.Web.Modules.AccountCreateAccount 4.3.3
- Fundatie.Web.Modules.AccountUserData 4.3.2
- Fundatie.Web.Modules.CheckoutUserData 4.3.4

Updated the default emailaddress validation in Fundatie modules. The given canonical url is now
correctly set if present by the navigation item. MSP enum statusses translation added, so you can
change status messages to your own translations.

.. |update2020.017| raw:: html

	<strong>
		<a href="2020.017.html">Fundatie - update 2020.017</a>
	</strong>

| |update2020.017|
| *06-10-2020*
| Packages:

- Fundatie.Core.Logic 4.3.6
- Fundatie.Modules.Contact 4.3.5

Contact block forms are now configurable through Selection Codes.

.. |update2020.016| raw:: html

	<strong>
		<a href="2020.016.html">Fundatie - update 2020.016</a>
	</strong>

| |update2020.016|
| *09-09-2020*
| Packages:

- Fundatie.Core 4.3.3
- Fundatie.Web.Core 4.3.9
- Fundatie.Web.Modules.Carousel 4.3.1
- Fundatie.Web.Modules.CarouselWithText 4.3.1
- Fundatie.Web.Modules.CustomerService 4.3.1
- Fundatie.Web.Modules.Entrances 4.3.1
- Fundatie.Web.Modules.FreeContent 4.3.1
- Fundatie.Web.Modules.GeneralNavigation 4.3.1
- Fundatie.Web.Modules.HTML 4.3.1
- Fundatie.Web.Modules.Information 4.3.1
- Fundatie.Web.Modules.LocationDetail 4.3.1
- Fundatie.Web.Modules.LocationOverview 4.3.2
- Fundatie.Web.Modules.LookDetail 4.3.3
- Fundatie.Web.Modules.LooksRelatedToCombination 4.3.1
- Fundatie.Web.Modules.LooksRelatedToProduct 4.3.1
- Fundatie.Web.Modules.MainMenu 4.3.1
- Fundatie.Web.Modules.Payment 4.3.1
- Fundatie.Web.Modules.ProductDetail 4.3.4
- Fundatie.Web.Modules.ProductLister 4.3.7
- Fundatie.Web.Modules.ProductSale 4.3.1
- Fundatie.Web.Modules.RelatedProducts 4.3.2
- Fundatie.Web.Modules.SetDetail 4.3.2
- Fundatie.Web.Modules.SetRelatedToProduct 4.3.2
- Fundatie.Web.Modules.ShopNavigation 4.3.1
- Fundatie.Web.Modules.ShopNavigationMobile 4.3.1
- Fundatie.Web.Modules.Title 4.3.1
- Fundatie.Web.Modules.USP 4.3.1
- Fundatie.Web.Modules.VacancyDetail 4.3.3
- Fundatie.Web.Modules.VacancyOverview 4.3.3
- Fundatie.Web.Modules.Video 4.3.1
- Fundatie.Web.Modules.Visual 4.3.1
- Fundatie.Web.Modules.VisualFullWidth 4.3.1
- Fundatie.Web.Modules.VisualWithText 4.3.1

| Repositories:

- Fundatie.Starterkit tag: 4.3.9

Implemented a new feature into Fundatie: Output caching.

.. |update2020.015| raw:: html

	<strong>
		<a href="2020.015.html">Fundatie - update 2020.015</a>
	</strong>

| |update2020.015|
| *03-09-2020*
| Packages:

- Fundatie.Resources 4.3.2
- Fundatie.Web.Core 4.3.8
- Fundatie.Web.Modules.CheckoutBasket 4.3.3
- Fundatie.Web.Modules.CheckoutOverview 4.3.4
- Fundatie.Web.Modules.SearchAutocomplete 4.3.1

| Repositories:

- Fundatie.Starterkit tag: 4.3.8

Removed HttpGet attribute from the index action of the SchemaOrg controller. Trimmed the searchterm
on leading and trailing spaces. Updated Checkout blocks buttons with spinners. Updated 
VisualFullWidth block to support larger screens.

.. |update2020.014| raw:: html

	<strong>
		<a href="2020.014.html">Fundatie - update 2020.014</a>
	</strong>

| |update2020.014|
| *14-08-2020*
| Packages:

- Fundatie.Core.Logic 4.3.5
- Fundatie.Modules.Payment.MSP 4.3.5
- Fundatie.Modules.Payment.MSP.Web 4.3.5
- Fundatie.Web.Core 4.3.7
- Fundatie.Web.Modules.BreadCrumb 4.3.1
- Fundatie.Web.Modules.ProductDetail 4.3.3
- Fundatie.Web.Modules.RelatedProducts 4.3.1

Added the option to send the email address of the customer with the payment to MSP. Updated some
Fundatie Block modules to improve use of cachekeys.

.. |update2020.012| raw:: html

	<strong>
		<a href="2020.012.html">Fundatie - update 2020.012</a>
	</strong>

| |update2020.012|
| *26-06-2020*
| Packages:

- Fundatie.Modules.Payment.MSP 4.3.4

Added additional configuration property for currency in the MSP payment provider.

.. |update2020.010| raw:: html

	<strong>
		<a href="2020.010.html">Fundatie - update 2020.010</a>
	</strong>

| |update2020.010|
| *20-05-2020*
| Packages:

- Fundatie.Modules.Mail.Templates 4.3.2

Added vertical alignment to the base template for older browsers and e-mail clients.

.. |update2020.006| raw:: html

	<strong>
		<a href="2020.006.html">Fundatie - update 2020.006</a>
	</strong>

| |update2020.006|
| *09-04-2020*
| Packages:

- Fundatie.Modules.Payment.Buckaroo.Web 4.3.3
- Fundatie.Modules.Payment.MSP.Web 4.3.4
- Fundatie.Web.Modules.SitemapHandler 4.3.2

Added parse IP address logic for better compatibility with ipv6 addresses and ports for the payment 
providers MSP and Buckaroo. Enabled fallback label data for Sitemap generators.

.. |update2020.005| raw:: html

	<strong>
		<a href="2020.005.html">Fundatie - update 2020.005</a>
	</strong>

| |update2020.005|
| *18-03-2020*
| Packages:

- Fundatie.Web.Modules.ProductLister 4.3.6

Updated the lister module to use the new way to get optional meta data like prices and filter
counts.

.. |update2020.003| raw:: html

	<strong>
		<a href="2020.003.html">Fundatie - update 2020.003</a>
	</strong>

| |update2020.003|
| *25-02-2020*
| Packages:

- Fundatie.Web.Modules.Contact 4.3.4
- Fundatie.Web.Modules.LocationOverview 4.3.1
- Fundatie.Web.Modules.ProductLister 4.3.5

| Repositories:

- Fundatie.StarterKit tag: 4.3.7

Added some webpack optimizations. Fixed some bugs in modules and views.

.. |update2020.002| raw:: html

	<strong>
		<a href="2020.002.html">Fundatie - update 2020.002</a>
	</strong>

| |update2020.002|
| *06-02-2020*
| Packages:

- Fundatie.Modules.Mail.Core 4.3.1
- Fundatie.Modules.Mail.SMTP 4.3.1
- Fundatie.Modules.Mail.Templates 4.3.1
- Fundatie.Modules.Payment.Buckaroo 4.3.3
- Fundatie.Modules.Payment.Buckaroo.Web 4.3.2
- Fundatie.Modules.Payment.MSP 4.3.3
- Fundatie.Modules.Payment.MSP.Web 4.3.3
- Fundatie.Web.Core 4.3.6

Added new payment methods to MSP and Buckaroo. Fixed some bugs with converters in the mail logic.