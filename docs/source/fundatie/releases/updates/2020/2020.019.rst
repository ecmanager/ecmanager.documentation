==========================
Fundatie - update 2020.019
==========================

Bugfixes
--------

| **ECM-6620**
| Packages:

- Fundatie.Web.Modules.SitemapHandler 4.3.3

Fixed the bug that only the first 100 items were added (repeatedly) to the XML.
