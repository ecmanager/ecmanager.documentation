========
Releases
========

New features and fixes are continuously added as updates to Fundatie.
This is a listing of Fundatie updates, including patches to earlier product versions.
Refer to the documentation below for more information about Fundatie releases.

.. toctree::
	:maxdepth: 1
	:glob:
	:reversed:

	older/releases
	updates/2019/releases
	updates/2020/releases