#######
Webpack
#######

Introduction
============
This page gives you a quick understanding of how webpack is used in your implementation and how to
get you started. We are still using the CastleCSS Framework as basis for our HTML/SCSS and certain
JavaScript. You can find the documentation at `www.castlecss.com <http://www.castlecss.com>`_.

Prerequisites
=============

* `Node.js <https://nodejs.org/en/download/>`__ (v8.15.0 and up)
* npm (v6.5.0 and up)

Setting up webpack
==================
To use webpack in your implementation you need to copy the ``Static`` folder with contents into
your project. You can find this folder in the starter-kit repository.

First thing to do is configuring the websitelabels of your implementation. You can change this in
the ``package.json`` file which resides in the webpack folder.

.. code-block :: json

	{
	 "scripts" : {
	  "client-server": "webpack --config config/webpack.config.js --mode=development --labels=label-a,label-b,default",
	  "client-server-prod": "webpack --config config/webpack.config.js --mode=production --labels=label-a, label-b,default",
	 }
	}

After configuring this run the following commands from command-line.

.. code-block :: batch

	npm install
	npm start

Generated files
===============
Running webpack will generate the necessary files which includes the CSS and JavaScript. It also
generates layout razorviews files to include in your layout of your implementation.

Generated file locations:

* Static\\dist
* Static\\webpack\\src\\javascript\\generated
* Views\\Shared\\Layouts\\generated