#########
Front-end
#########

.. toctree::
    :maxdepth: 2
    :reversed:
    :hidden:

    grunt/index
    webpack/index

In the `2019.014 <../releases/updates/2019/2019.014.html>`__ release we changed the way how the
front-end is configured and generated. We recommend to use webpack in your implementation.

.. |front-end-webpack| raw:: html

	<strong>Webpack</strong>

|front-end-webpack|

To use the new and modern webpack in your implementation, you can read more on this `page <webpack/index.html>`__.

.. |front-end-grunt| raw:: html

	<strong>Grunt (deprecated)</strong>

|front-end-grunt|

To use grunt in your implementation, you can read more on this `page <grunt/index.html>`__.