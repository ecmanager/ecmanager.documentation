#####
Grunt
#####

Introduction
============
This page gives you a quick understanding of how Grunt is used in the projects and how to get you started. 
We are using the CastleCSS Framework as basis for our HTML/SCSS and certain JavaScript. You can find
the documentation at `www.castlecss.com <http://www.castlecss.com>`_.

Prerequisites
=============

* `Node.js <https://nodejs.org/en/download/>`__

Setting up Grunt
================
To use grunt in your project you need the following package:

* A globally installed grunt-cli package (Grunt command line interface) for executing Grunt commands.
* A grunt package installed in the project to build the project tasks tree and provide coding assistance while editing the Gruntfile.js.
 
Install the Grunt Command Line Interface:

.. code-block:: batch

	$ npm install -g grunt-cli

Install Grunt in a project:

Switch to the project root folder and type the following command at the command line prompt:

.. code-block:: batch

	$ npm install grunt 

Once you installed grunt, the next time you want to run grunt you can just use the run.bat in your 
projectfolder.

To check if grunt is installed correctly, you can run this command: 

.. code-block:: batch

	$ grunt --version

Included in the project
=======================
A few files are predefined in Fundatie:

* Package.json
* Gruntfile.js

The package.json file
=====================
This JSON file enables us to track and install all of our development dependencies.
Running 'npm install' in the same folder as a package.json file will install the correct version of 
each dependency listed in this file. 

In Fundatie we make use of sass and postCss, therefor you need a few plugins like:

* grunt-sass: to compile sass to css.
* grunt-postcss: to make it posible to autoprefix the css (autoprefixer) and to compile the css (cssnano)

The Gruntfile.js
================
This file is used to configure or define tasks and load Grunt plugins.

The Gruntfile consists of the following parts:

* Path variables: Here you define your paths for your project css and js
* Library: Imports external functions from the file Libraries.js
* Define main tasks: Compile sass to css, autoprefix, concatenate javascript and watch for changed files
* Register tasks: Every time Grunt is run, you specify one or more tasks to run, which tells Grunt what you'd like it to do

Gruntfile main tasks
====================
A short explanation of the tasks that are predefined in Fundatie.

run
---
With this task you run all the tasks: Compile Sass; Autoprefix Css; Concatenate Javascript; Watch 
changed files.

.. code-block:: batch

	grunt.registerTask('run',['sass','postcss','concat','watch']);

In your projectfolder you can execute run.bat to all these tasks.

create_css
----------
Compile the sass, add prefixers and set rem to pixels.

.. code-block:: batch

	grunt.registerTask('create_css',['sass','postcss']);

In your projectfolder you can execute create_css.bat to all these tasks. 

create_javascript
-----------------
Concatenates your javascript.