##################
Schema.org JSON-LD
##################

This page gives you a description of Schema.org JSON-LD and how it's implemented in Fundatie.
Additionally it explains how you can extend existing Schema.org JSON-LD and create a whole new 
entity in JSON-LD. 

What's Schema.org JSON-LD
-------------------------
Schema.org is a way to tell which information exists on a page in a way that's easier to process for
search engines like Google. JSON-LD is a format to structure data in JSON. Read the 
following pages for more information:

* `Schema.org <https://schema.org>`_
* `JSON-LD <https://json-ld.org/>`_

Fundatie and Schema.org JSON-LD
-------------------------------
In the following part we will referer to Schema.org when we are talking about Schema.org JSON-LD.
The following concepts are used in the implementation:

* SchemaOrgBuilder
* SchemaOrg data structures
* SchemaOrgMapper/Parameters

SchemaOrgBuilder
****************
The SchemaOrgBuilder is an object which collects POCOs, serializes them and returns a JSON
string. The builder is injected using Autofac, registered as InstancePerRequest. This
instance scope allows us to "store" data in the builder and retrieving the data later in that
request.

Every Fundatie module (block) can add the ``ISchemaOrgBuilder`` as constructor parameter and use the
``Append`` method to add data to the Schema.org output.

SchemaOrg data structures
*************************
The data structure are serialized using JSON.NET, this means that the structure in the entities
needs to match the desired JSON structure. The data structures needs to extend from 
``SchemaOrgThing`` so they can be added to the SchemaOrgBuilder.

SchemaOrgMapper/Parameters
**************************
The data retrieved from the services is not in the right format to be directly converted to JSON.
The data needs to be mapped to the right format. To do this we use the ``SchemaOrgMappers``. 
Mappers are injected using constructor injection.

The method `Map` will be called with a typed parameter with the data needed for the mapping. The
result can be added to the ``SchemaOrgBuilder``.

The Fundatie modules (blocks) fill the parameters with the data, call the mapper and append the data
to the builder.

Implement Schema.org JSON-LD
----------------------------
When you want to create you own Schema.org structure create your own entities, parameters and
mappers and append the data to the SchemaOrgBuilder.

Below a sample implementation without the use of a separated mapper and parameters:

.. container:: toggle

	.. container:: header

		**Show/Hide**

        .. code-block:: C#

            public class SampleEntity : SchemaOrgThing
            {
                public override string Type => "http://schema.org/SampleString";
                public string SampleString { get; set; }
            }

            public class SampleController : Controller
            {
                private readonly ISchemaOrgBuilder _schemaOrgBuilder;

                public SampleController(ISchemaOrgBuilder schemaOrgBuilder)
                {
                    _schemaOrgBuilder = schemaOrgBuilder;
                }
                public ActionResult Index()
                {
                    var sample = new SampleEntity
                    {
                        SampleString = "SomeData"
                    };

                    _schemaOrgBuilder.Append(sample);

                    return View();
                }
            }


Extend Schema.org JSON-LD
-------------------------
It is easy to add/modify the data in Schema.org.
An important part are the mappers you can easily create your own mapper or extend an existing one
and let Autofac do the heavy work. To use more data in your mapper extend the parameters with the
data. Edit your mappers to use the new data and parameters.

It's important to know how SchemaOrg is implemented before you can extend it. Below a sample
(incomplete) implementation for events to show you the structure.

.. container:: toggle

	.. container:: header

		**Show/Hide**

        .. code-block:: C#

            //Data structures
            public class SchemaOrgEvent : SchemaOrgThing
            {
                public override string Type => "http://schema.org/Event";

                [JsonProperty("location")]
                public SchemaOrgPlace Location { get; set; }

                public class SchemaOrgPlace : SchemaOrgThing
                {
                    public override string Type => "http://schema.org/Place";

                    [JsonProperty("Address")]
                    public SchemaOrgPostalAddress Address { get; set; }
                }

                [JsonProperty("startDate")]
                public DateTime? StartDate { get; set; }

                [JsonProperty("endDate")]
                public DateTime? EndDate { get; set; }
            }

            //Parameters
            public class SchemaOrgEventParameters
            {
                public Location Location { get; set; }
                public string LanguageCode { get; set; }
                public Event Event { get; set; }
            }

            //Mapper
            public sealed class DefaultSchemaOrgEventMapper : SchemaOrgEventMapper<SchemaOrgEventParameters, SchemaOrgEvent>
            {
            }
            
            public class SchemaOrgEventMapper<TParams, TModel> : SchemaOrgMapper<TParams, TModel>, ISchemaOrgEventMapper
                where TParams : SchemaOrgEventParameters
                where TModel : SchemaOrgEvent, new()
            {
                
                public SchemaOrgEvent Map(SchemaOrgEventParameters parameters)
                {
                    var actualParameters = GetTypedParameters(parameters);
                    return MapTyped(actualParameters);
                }
                
                protected virtual TModel MapTyped(TParams parameters)
                {
                    var model = new TModel();

                    if (parameters.Event == null)
                    {
                        return null;
                    }      
                    
                    //Remove code to simplify the example.
                    
                    var @event = parameters.Event;
                    var eventText = @event.Texts.FirstOrDefault(x => x.LanguageCode == parameters.LanguageCode);
                    if (eventText == null || string.IsNullOrWhiteSpace(eventText.Title))
                    {
                        return null;
                    }
                    
                    model.Name = eventText.Title;
                    model.Description = eventText.Description;
                    model.StartDate = @event.StartDate;
                    model.EndDate = @event.EndDate;
                    model.Image = @event.Images?.FirstOrDefault()?.Url?.AbsoluteUrl;

                    return model;
                }
            }

            //Autofac registration
            builder.RegisterType<SchemaOrgBuilder>().As<ISchemaOrgBuilder>().InstancePerRequest();
            builder.RegisterType<DefaultSchemaOrgEventMapper>().As<ISchemaOrgEventMapper>();

Custom implementation of the events to add a sample string.

.. container:: toggle

	.. container:: header

		**Show/Hide**

        .. code-block:: C#

            //Data structure
            public class CustomSchemaOrgEvent : SchemaOrgEvent
            {
                public string SampleString { get; set; }
            }
            
            //Parameters
            public class CustomSchemaOrgEventParameters : SchemaOrgEventParameters
            {
                public string SampleString { get; set; }
            }
            
            //Mapper
            public class CustomSchemaOrgEventMapper : SchemaOrgEventMapper<CustomSchemaOrgEventParameters, CustomSchemaOrgEvent>
            {
                protected override CustomSchemaOrgEvent MapTyped(CustomSchemaOrgEventParameters parameters)
                {
                    var model = base.MapTyped(parameters);
                    model.SampleString = parameters.SampleString;
                    return model;
                }
            }
            
            //BlockController
            public class EventDetailController : Controller
            {
                private readonly ISchemaOrgBuilder _schemaOrgBuilder;
                private readonly ISchemaOrgEventMapper _schemaOrgEventMapper;

                public EventDetailController(ISchemaOrgBuilder schemaOrgBuilder, ISchemaOrgEventMapper schemaOrgEventMapper)
                {
                    _schemaOrgBuilder = schemaOrgBuilder;
                    _schemaOrgEventMapper = schemaOrgEventMapper;
                }

                public ActionResult Index()
                {
                    var customParameter = new CustomSchemaOrgEventParameters();
                    customParameter.SampleString = "test";
                    //Fill parameters
                    var schemaOrgEvent = _schemaOrgEventMapper.Map(customParameter);
                    _schemaOrgBuilder.Append(schemaOrgEvent);
                    return View();
                }
            }

            //Autofac registration
            builder.RegisterType<CustomSchemaOrgEventMapper>().As<ISchemaOrgEventMapper>();

.. note:: Make sure to modify the Autofac registration to match the new mappers and parameters.







