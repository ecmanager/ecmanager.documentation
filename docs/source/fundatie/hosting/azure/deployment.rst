##########
Deployment
##########

Introduction
============
This page describes how you can deploy your applications and tools to Azure and the scripts we offer
to help you in this process.

ARM template
============
To help you setup your Azure resources quickly, you can use the ARM template that is provided in 
the ``Fundatie.StarterKit`` repository. Running the PowerShell script will prompt you for the 
desired pricing tiers for each component and then deploy all the resources to the specified resource
group. You can read more about ARM template deployment 
`here <https://docs.microsoft.com/en-us/azure/azure-resource-manager/resource-group-template-deploy>`__.

Web Apps deployment
===================
There are multiple ways to deploy your applications to Azure, like FTP, Git or cloud sync. From all
the different options we chose to use Web Deploy (msdeploy) because of its stability and because we 
use our own build server to deploy, so we don't want to deploy straight from source control.

Web Deploy is a command line tool available on most systems. Because it is not very easy to use,
we created a `Cake script <https://cakebuild.net/>`__ to make it easier to deploy. You can use our
script, but feel free to choose your own tool if you have another preference.

.. container:: toggle

	.. container:: header

		**Show/Hide Cake script**

	.. code-block:: none
		:linenos:

		#addin "Cake.MsDeploy"

		var relativeWorkingDirectory = Directory("./");
		var absoluteWorkingDirectory = MakeAbsolute(relativeWorkingDirectory).ToString();
		
		Task("CreateDeploymentPackage")
		    .Does(() =>
		{
		    var relativeSourceDirectory = Directory(Argument<string>("sourceDirectory"));
		    var absoluteSourceDirectory = MakeAbsolute(relativeSourceDirectory).ToString();
		
		    var settings = new MsDeploySettings
		    {
		        Verb = Operation.Sync,
		        Source = new ContentPathProvider
		        {
		            Direction = Direction.source,
		            Path = absoluteSourceDirectory
		        },
		        Destination = new PackageProvider
		        {
		            Direction = Direction.dest,
		            Path = absoluteWorkingDirectory + "/package.zip"
		        }
		    };
		
		    MsDeploy(settings);
		});
		
		Task("DeployPackage")
		    .IsDependentOn("CreateDeploymentPackage")
		    .Does(() =>
		{
		    var targetDirectory = Argument("targetDirectory", @"D:\home\site\wwwroot");
		    var site = Argument<string>("site");
		    var password = Argument<string>("password");
		
		    var settings = new MsDeploySettings
		    {
		        Verb = Operation.Sync,
		        Source = new PackageProvider
		        {
		            Direction = Direction.source,
		            Path = absoluteWorkingDirectory + "/package.zip"
		        },
		        Destination = new ContentPathProvider
		        {
		            Direction = Direction.dest,
		            Path = targetDirectory,
		            ComputerName = string.Format("https://{0}.scm.azurewebsites.net:443/msdeploy.axd?site={0}", site),
		            Username = "$" + site,
		            Password = password,
		            AuthenticationType = AuthenticationScheme.Basic
		        }
		    };
		
		    MsDeploy(settings);
		});
		
		RunTarget("CreateDeploymentPackage");

To use this script, name the file `build.cake` and download the Cake bootstrapper. 
The site parameter is the name of the Web App and the password can be found as described
`here <https://docs.microsoft.com/en-us/azure/app-service/app-service-deployment-credentials>`__.
Example usage of the script:

.. code-block:: PowerShell

	.\build.ps1 -ScriptArgs '-sourceDirectory="_Package/MyApplication" -site="myapplication" -password="password"'

The package will be deployed to the ``D:\home\site\wwwroot`` path.

WebJobs deployment
==================
WebJobs are deployed the exact same way as Web Apps, because they live inside a Web App. The only 
difference is that they are deployed to a different path. Instead of ``d:\home\site\wwwroot``, the 
path should be either:

* ``d:\home\site\wwwroot\app_data\jobs\continuous\{name}`` for a continuous WebJob
* ``d:\home\site\wwwroot\app_data\jobs\triggered\{name}`` for a triggered WebJob

To read more about WebJobs and their settings, click `here <https://github.com/projectkudu/kudu/wiki/WebJobs>`__.