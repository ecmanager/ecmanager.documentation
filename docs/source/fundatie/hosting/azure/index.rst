###############
Microsoft Azure
###############

.. toctree::
   :maxdepth: 1
   :titlesonly:
   :glob:

   overview
   architecture
   deployment