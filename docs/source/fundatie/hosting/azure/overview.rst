########
Overview
########

This section describes the basic information you need to get started when you want to host ecManager
and Fundatie in a Microsoft Azure environment. Of course Azure is a big and fast-growing platform
so we try to describe only the basic needs.

An overview and details for each component in the landscape can be found the 
`Architecture <architecture.html>`_ page. This is a good starting point to get
started and read about the differences between hosting on-premises and on Azure.

When you're ready to start deploying your applications to Azure, read the `Deployment <deployment.html>`_
page for our suggestions on how to do just that.