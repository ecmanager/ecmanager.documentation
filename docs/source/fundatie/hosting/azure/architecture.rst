############
Architecture
############

.. note:: In this documentation we assume you have at least basic knowledge of the services offered by Azure.

Introduction
============
This page describes the application landscape of ecManager and Fundatie when using 
Platform-as-a-Service services offered by Microsoft Azure. Running ecManager and Fundatie in a cloud 
environment requires a different application landscape than an on-premises environment. The main
difference is that each component is completely isolated from the others. This means that storage 
and caching has to be distributed. The next chapters will elaborate on each component individually.

The following diagram shows an overview of all the required resources that are part of the Azure
architecture:

.. figure:: img/total.png
	:alt: Resources overview
	:figwidth: 100%
	:align: left

App Service plans
=================
In Azure, there are multple ways to configure App Service plans and their Web Apps. We recommend to
create an App Service plan for each Web App, so they can each be scaled horizontally and vertically
without impacting the others. It is possible to run multiple Web Apps in the same App Service plan
because this might be cheaper, but this is at your own risk.

Web App (webshop)
=================

.. figure:: img/webshop.png
	:alt: Webshop
	:figwidth: 100%
	:align: left

The webshop which your customers will visit will be running inside a Web App. Web Apps have the
capability to scale quickly and have load balancing managed automatically. This is a major advantage
for the webshop, because you can use the scaling to handle peaks in concurrent users and save money
by scaling down when the peak has ended. We recommend to always use at least two instances of your
Web App to ensure high availability.

This Web App uses the SQL Database to store and retrieve data. Retrieved data will be cached both 
in-memory and distributed in the Redis Cache. You can `configure <../../../ecManager/services/caching/quick-guide.html#configuration>`__
which cache each resource should be stored in. It is important that you consider for each resource
which cache it should be stored in. Please keep the purging, latency and memory usage of a 
distributed cache in mind when making these decisions.

Sessions should be stored in Redis using the ``RedisSessionStateProvider`` from the 
`Microsoft.Web.RedisSessionStateProvider <https://www.nuget.org/packages/Microsoft.Web.RedisSessionStateProvider/>`__
NuGet package. Because the sessions are distributed, there is no need to use the ARR Affinity cookie.
This should provide better load balancing. The cookie can be disabled in the Web App application
settings.

Web App (CMS)
=============

.. figure:: img/cms.png
	:alt: CMS
	:figwidth: 100%
	:align: left

The ecManager CMS also runs in a Web App, but the usage is different. Because the user load of the 
CMS is almost guaranteed to be much lower than the webshop, there is no need for it to scale. 

The CMS should not cache any data, because the data retrieved should always be up-to-date. A big 
advantage of a distributed cache like Redis is the possiblity to purge data for all applications.
ecManager provides a `setting <../../../ecManager/services/caching/quick-guide.html#configuration>`__ 
called ``purgeOnly`` to allow the CMS to only purge data from the cache, and not consume it. For 
example: If you add content to your homepage and you save the navigation item, the previously cached
navigation item will be purged and the webshop will retrieve the updated version from the database.

The CMS is able to clear the cache totality or a single item in the cache. The Redis connectionstring
must have a property ``allowAdmin`` to enable this functionality. How to enable this, click 
`here <../../../ecManager/modules/cache-redis/quick-guide.html#configuring-additional-options>`__.

Uploading content in the CMS will result in the CMS uploading files to BlobStorage. 

Web App (ImageResizer)
======================

.. figure:: img/imageresizer.png
	:alt: ImageResizer
	:figwidth: 100%
	:align: left

Not all resizes are created immediately when a CMS user uploads images. The ImageResizer is used
to resize images as they are requested for certain dimensions using query string parameters for 
height and width. This is handled by a seperate Web App to lower the load of the webshop and to 
support CDN scenarios. You can read more about this in the CDN section.

.. warning:: This Web App MUST run in 64-bit mode. You can enable this in the Application Settings.

SQL Database
============

.. figure:: img/sqldatabase.png
	:alt: SQL Database
	:figwidth: 100%
	:align: left

The database is hosted in SQL Azure. There is no difference between the on-premises and cloud 
hosting of the database, other than the SQL Azure implementation limitations. When choosing a 
pricing tier, keep `these <https://docs.microsoft.com/en-us/azure/sql-database/sql-database-resource-limits>`__ 
resource limits in mind. All Web Apps use the SQL Database.

Azure manages the the `high availability and scalability 
<https://docs.microsoft.com/en-us/azure/sql-database/sql-database-high-availability>`__ of the SQL 
database. Because any SQL query on a SQL Azure database can randomly fail, ecManager has
a built-in retry mechanism so you don't have to worry about this.

Redis Cache
===========

.. figure:: img/rediscache.png
	:alt: Redis Cache
	:figwidth: 100%
	:align: left

A Redis Cache resource is used for distributed caching and session storage. A Redis Cache instance 
can have multiple databases. The sessions and cache MUST be stored in seperate databases. This
can be configured in the connection strings. Storing them in the same database will result in 
strange behavior, e.g. clearing the cache would also clear all stored sessions.

As mentioned in previous chapters, Redis offers several pros/cons compared to an in-memory-cache.

Pros:

* The ability to purge cache items distributedly
* Each instance always provides the same data from the cache
* Efficient memory usage

Cons:

* Performance overhead from latency and serialization
* Resource costs

To read more about caching guidelines, click `here <https://docs.microsoft.com/en-us/azure/architecture/best-practices/caching>`__,
or to read more about the ``ecManager.Modules.Cache.Redis`` module, click 
`here <../../../ecManager/modules/cache-redis/overview.html>`__.

Blob Storage
============

.. figure:: img/blobstorage.png
	:alt: Blob Storage
	:figwidth: 100%
	:align: left

In Azure, ecManager uses `Blob Storage <https://docs.microsoft.com/en-us/azure/storage/blobs/storage-blobs-introduction>`__
for storing static content like media. Blob Storage is accessed by all Web Apps in the application
landscape. Currently, ecManager only uses the Blob containers feature of Blob Storage. The 
``CmsData`` folder should be stored in a Blob container, with the exception of deployment-time 
configuration. It is important to realize that Blob Storage is accessed by its REST API, which adds
overhead in latency. 

ecManager supports multiple storage providers, and you can `determine <../../../ecManager/services/storage/quick-guide.html#configuring-storage-providers>`__
which paths should be retrieved from which provider, similar to the caching system.

.. note:: When migrating to Blob Storage, keep in mind that all paths and file names should be lowercase.

To read more about the ``ecManager.Modules.StorageProvider.AzureBlobStorage`` module, click 
`here <../../../ecManager/modules/azure%20blob%20storage%20provider/overview.html>`__.

CDN
===

.. figure:: img/cdn.png
	:alt: CDN
	:figwidth: 100%
	:align: left

A CDN should be used to lower the load on Web Apps and improve the serving time of static content.
Fundatie consumes two kinds of content: static and dynamically resized images. When a page is 
requested with images that have not yet been requested before, images are resized on the fly.

The CDN needs two endpoints. The first endpoint has Blob Storage as origin and simply retrieves and
caches directly from Blob Storage. The second endpoint has the ImageResizer Web App as origin.
This endpoint caches the content based on each unique URL, including query string parameters.

.. warning ::

    In the Caching settings of each endpoint, the "Query string caching behavior" setting should be set to "Cache every unique URL".

Uploading images in the CMS results in images with the same name. If the image was previously 
cached, it will not be updated in the website until the cache expires. To prevent this issue,
ecManager supports cache busting for content. When a user uploads an image, the timestamp is saved
along with the new image. When the webshop displays the image, it adds a query string parameter with
the timestamp. Because the URL is new to the CDN, it will not retrieve the old image.

Search
======

.. figure:: img/search.png
	:alt: Search
	:figwidth: 100%
	:align: left

The Lucene.NET search integration stores indices on disk, which is not usable in Web Apps. Because
of this reason, we developed an integration with the Azure Search service. You can find the 
documentation for the integration `here <../../../ecManager/modules/search%20azure/overview.html>`__.

The webshop performs search operations on the Azure Search service. A WebJob is responsible for 
indexing ecManager entities.

WebJobs
=======
Most projects which use ecManager have at least a few tools running running on the side as scheduled 
tasks, e.g. updating the entity state or generation of feeds. These are console applications
which can run inside an Azure Web App as a webjob. Just make sure to configure them to use the
right modules for storage.

Application Insights (optional)
===============================
Application Insights (AI) is the analytics and monitoring tool provided by Azure. AI is a seperate 
resource from your Web Apps. It is easy to configure your Web Apps to integrate with AI. You can 
read more about that `here <https://docs.microsoft.com/en-us/azure/application-insights/app-insights-configuration-with-applicationinsights-config>`__.