###########
Quick guide
###########

Introduction
============
This page gives you a quick guide to start a new project based on the sample project "Fundatie". To
do this, the Fundatie.StarterKit repository will be used. This is a repository with the bare minimum
of code required to get started with Fundatie. At the end you will have a working project without 
any optional modules installed.

If at any point you run into an issue, please check `Troubleshooting`_.

Prerequisites
=============
* `.NET Framework 4.7.2 <https://dotnet.microsoft.com/download/dotnet-framework/net472>`__ 
* `Visual Studio 2017 or higher <https://visualstudio.microsoft.com/vs/>`__
* IIS
* `IIS URL Rewrite 2.0 <https://www.iis.net/downloads/microsoft/url-rewrite>`__
* SQL Server Express (64-bit) or Azure SQL
* `SQL Server Management Studio 17.9.x <https://docs.microsoft.com/en-us/sql/ssms/sql-server-management-studio-ssms?view=sql-server-2017>`__
* `Git <https://git-scm.com/>`__
* `Node.js and npm <https://nodejs.org/en/download/>`__
* Access to the ``Fundatie.StarterKit`` repository
* Access to the ecManager MyGet feed

Clone the Fundatie.StarterKit repository
========================================
The first step is to clone the ``Fundatie.StarterKit`` using Git.

.. code-block:: batch

	git clone https://bitbucket.org/ecmanager/fundatie.starterkit.git .

.. note:: Clone your project in a path without spaces to prevent build errors.
	
Install the ecManager certificate authority (deprecated)
========================================================
To run PowerShell scripts with the default settings (execution policy all signed) all scripts are signed.
PowerShell checks the signing with certificates.

Please install the following certificates:
* install the root certificate ecmanagerCA.crt in the "Trusted Root Certification Authorities"
* install the code signing certificate in the "Trusted Publishers"

Both certificates are downloadable from the StarterKit downloads section:
https://bitbucket.org/ecmanager/fundatie.starterkit/downloads/ecManagerPowerShellCertificates.zip

.. note:: Certificates are expired. For running PowerShell scripts set the execution policy to unrestricted.

Configure the VSIX feed
=======================

.. figure:: ../../_static/img/fundatie/quick-guide/vsix.png
	:alt: VSIX feed details MyGet
	:align: right
	:scale: 90%

	VSIX feed details MyGet

In order to successfully use Fundatie, you need to add a VSIX feed to Visual Studio which will be 
used to install tools later. To do this, go to ``Tools -> Options -> Environment -> Extensions and Updates`` 
within Visual Studio. Click "Add" and enter a name for the ecManager extension feed and the feed URL
which can be retrieved from MyGet. This URL should be the pre-authenticated URL. To learn more about 
VSIX feeds in MyGet read the `Walkthrough - Working with your Vsix feed <http://docs.myget.org/docs/walkthrough/getting-started-with-vsix>`_.

Installing the ResourceGenerator
================================

.. figure:: ../../_static/img/fundatie/quick-guide/extensions_and_updates.png
	:alt: VSIX feed details MyGet
	:align: right

	Extensions and Updates

Go to ``Tools -> Extensions and Updates...`` and click the "Online" tab and filter on the name that
you gave the VSIX feed in the previous chapter. You should see the ``ecManager Resource Generator`` 
appear, this is what you need to install.

This tool allows you to use a custom ResourceManager. Resources can then be defined with the 
following naming convention:

.. code-block:: batch

	<module>.<label>.<language>.resx

If you configure a default label the label can be omitted from the resource name.

Add the ecManager NuGet feed
============================

.. figure:: ../../_static/img/fundatie/quick-guide/nuget.png
	:alt: NuGet feed details MyGet
	:align: right
	:scale: 90%

	NuGet feed details MyGet

The ecManager NuGet feed URL can be retrieved from MyGet in the same way as the VSIX feed. The
NuGet pre-authenticated feed should be added in two places. First add the URL to the 
``nuget.config`` file in the ecManager directory as the value of the ecManager package source.

Lastly replace the feed URL in ``paket.dependencies`` file as source, it holds a placeholder. You 
can use the existing nuget.org source as an example.

In the .paket directory, execute the paket.bootstrapper.exe so paket.exe will be downloaded. Execute
the following command from a command prompt in the root of the repository:

.. code-block:: batch

	.paket\paket.exe install --redirects

This will generate a paket.lock file. For more information about how paket works, visit their
`documentation <https://fsprojects.github.io/Paket/>`__.

Create a database
=================

.. figure:: ../../_static/img/fundatie/quick-guide/dacpac.png
	:alt: Deploy the DACPAC
	:align: right

	Deploy the DACPAC

To use Fundatie, you need a database. The ``Setup`` directory contains a .dacpac file which you 
should use to create your database. This DACPAC should be deployed to a SQL server instance.

The directory also contains ``Setup.sql``. This script has some variables which need to be given 
values. Those variables are declared at the top of the script. The script contains some example 
values in comments.

Some of the variables are 'tokens' for users. These should be unique and complex because they will 
be used to authenticate with the ecManager framework.

One of the required values is the hashed password for the administrator account. Use the 
``ecManager password.exe`` application to generate the password hash. Use the resulting value in 
your SQL script and remember the salt that you used. 

Remember which values are used in this script, because some of them will be used for
configuration later. The configuration ``authenticationSalt`` needs to be filled with this value. 

After deploying the .dacpac to SQL server, execute the script. 

.. note:: You can read more about DACPACs on `MSDN <https://msdn.microsoft.com/en-us/library/ee210546.aspx>`__.

Rename the project
==================

.. figure:: ../../_static/img/fundatie/quick-guide/rename.gif
	:alt: RenameProject.ps1
	:align: right
	:scale: 80%

	RenameProject.ps1

When you clone the ``Fundatie.StarterKit`` repository, the solution and namespaces are called 
``StarterKit``. This is most likely not the name you want for your project.

The StarterKit repository includes a script called ``RenameProject.ps1``. You can run this script
and enter the new name. All file names, namespaces and more will be renamed. The new name should not
contain spaces. 

This script requires you to be able to execute unsigned PowerShell scripts. If you receive an error,
you can set the execution policy like this:

.. code-block:: batch

	Set-ExecutionPolicy RemoteSigned

.. tip:: 
	If you receive an error about path names that are too long, make sure to delete the
	``HTML\Reference\_sources\default\_grunt\node_modules`` directory.

Configure your web project
==========================
The web.config of the web project located in  ``Web\<ProjectName>.Web`` contains a lot of 
configuration. Some of this has been pre-configured for you, but some of the values are placeholders.
The placeholders can be recognized by their value ``<PLACEHOLDER>``. If you search for these you 
should be able to easily find them.

All the configuration is already present in the StarterKit and only needs to be
filled. To learn more about the different configuration options a more detailed description can be
found in the links below. 

* ecManagerConfig (see `Configuring ecManager <../../ecManager/introduction/configuring-ecManager/index.html>`__)
* ecManager.Modules.HttpModules.HttpRedirectModule (see `HTTP redirect module configuration <../../ecManager/modules/seo%20http%20redirects/quick-guide.html#configuration>`__)
* fundatie.Core (see `Fundatie configuration <../configuration/configuration.html>`__)
* connectionStrings

The web.config also contains the ``system.web/sessionState`` element. Fundatie should use SQL Server 
session state, but for testing purposes you can remove the element so sessions are stored in process.

Configure IIS
=============
Some configuration of IIS is required to get your project working. In IIS, go to 
``Management -> Configuration Editor`` and choose the section ``system.webServer/httpErrors``.
Unlock the ``allowAbsolutePathsWhenDelegated`` attribute and set its value to true. Also unlock the 
``defaultPath`` attribute.

.. figure:: ../../_static/img/fundatie/quick-guide/iis_configuration.png
	:alt: IIS configuration
	:align: center

	IIS configuration

Add the project to IIS
======================
Add a new website to IIS. The physical path, ``Web\<ProjectName>.Web``, should point to the web 
project. Make sure to add a HTTPS binding and use certificate. 

Add a virtual directory to the new IIS website. The alias should be ``CmsData`` and the physical 
path should be the full path to ``Data\CmsData``.

Add the CDN to IIS (Optional)
=============================
If you configured your web project to use a CDN, you should also add the CDN to IIS. The steps for
the CDN are the same as the previous chapter, but for the CDN you should add an extra virtual 
directory with alias ``Content`` and the absolute physical path that points to 
``Web\<ProjectName>.Web\Content``.

Configure the ecManager CMS
===========================
The ecManager CMS project can be found in the ecManager directory. To get the CMS up and running, 
you should fill the following missing ``web.config`` configurations: 

* ecManagerConfig (should match the web project config)
* connectionStrings

The mandatory configurations can again be recognized by the ``<PLACEHOLDER>`` value.

There are other configs (e.g. cmsConfig) that should also be configured, but those are not mandatory
to get the CMS up and running.

Build the ecManager CMS
=======================
The ecManager CMS project must be build to run. Run the following commands in the root of CMS 
folder before the build:

* npm install

Add the ecManager CMS to IIS
============================
Repeat the steps you did earlier for the other websites. ``ecManager\Cms`` is the path that should 
be added. The ``CmsData`` virtual directory is also needed.

Configure the EntityStateApplication
====================================
ecManager comes with a concept called entity state. Entity state allows you to define rules to
determine if a entity is online or offline. The entity state application calculates the entity state
and saves it to the database. Read `EntityState application <../../tools/EntityState%20Application/overview.html>`_
to learn how to configure the entity state application.

.. note:: Once your starter kit is finished setting up, all your entities will be offline until entity state is determined.

Verification
============
After building all solutions, you should now check if all websites are working correctly. For the 
Fundatie project you should see an empty page without any content. The CMS should display a login
page.

.. tip:: 
	If you see an error "Failed to start monitoring file changes", make sure you create a directory
	with the name of your label in the ``<ProjectName>.Web\Content\Styles`` directory.

Next steps
==========
Now that you have a working project based on Fundatie, you can start installing modules or creating
your own. You can also use the CMS to add content for your site.

Troubleshooting
===============
Missing styles directory for label
----------------------------------
Error description:

	Failed to start monitoring file changes.

Solution:

	Create a directory with the name of your label in the ``<ProjectName>.Web\Content\Styles`` 
	directory.

Label name does not match
-------------------------
Error description:

	Could not find any resources appropriate for the specified culture or the neutral culture. Make 
	sure "Fundatie.Resources.Header.Standaard.resources" was correctly embedded or linked into 
	assembly "Fundatie.Resources" at compile time, or that all the satellite assemblies required are
	loadable and fully signed.

Solution:

	Make sure the value of @DefaultLabelDescription from the SQL script matches the 
	defaultWebsiteLabelName attribute in the Fundatie.Core configsection. These values are 
	case-sensitive.
