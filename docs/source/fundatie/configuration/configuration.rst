#############
Configuration
#############

Introduction
============
This page describes which configuration is required for Fundatie.

Fundatie.Core
=============
This configuration will determine settings required for the core of Fundatie. The following block
shows an example of this configuration:

.. code-block:: xml

	<configSections>
	  <section name="fundatie.Core" type="Fundatie.Core.Configuration.FundatieConfigurationSection, Fundatie.Core" />
	</configSections>

	<fundatie.Core 
	  applicationName="MyApplication" 
	  defaultWebsiteLabelId="1" 
	  fallbackWebsiteLabelIds="1" 
	  defaultWebsiteLabelName="Default" 
	  defaultWebsiteLanguage="nl" 
	  defaultWebsitePriceListId="1" 
	  defaultWebsiteShippingCountryId="1" 
	  useFamilyRollUp="false"	  
	  environment="Development"
	  useAbsoluteUrls="true">
	  <ecManager user="Website" token="abcdef" />
	  <price vatIncluded="true" />
	</fundatie.Core>

=============================== ====================================================================================================================
Element/Attribute               Description                                                                                                         
=============================== ====================================================================================================================
applicationName                 The name of the application you are currently running.                                                              
defaultWebsiteLabelId           The id of the website label that will be used if it can't be determined by the context of a request.                
fallbackWebsiteLabelIds         The ids of the website labels that will be used for fallback, seperated by comma.                                   
defaultWebsiteLabelName         The name of the default website label.                                                                              
defaultWebsiteLanguage          The default language that will be used if it can't be determined by the context of a request. (lowercase, e.g. 'nl')
defaultWebsitePriceListId       The id of the price list that will be used if if it can't be determined by the context of a request.                
defaultWebsiteShippingCountryId The id of the shipping country that will be used if it can't be determined by the context of a request..            
useFamilyRollUp                 Indicates whether `familyRollUp <../../ecManager/services/family/index.html>`_ should be used or not. (true or false)                                                      
environment                     The name of the environment to be used, e.g. 'Development'.                                                         
useAbsoluteUrls                 Determines if absolute URLs should be used for images.                                                              
ecManager.user                  The username of the user that will be used to authenticate ecManager service calls.                                 
ecManager.token                 The token of the user that will be used to authenticate ecManager service calls.                                    
price.vatIncluded               Indicates whether prices should be used with or without VAT. (true or false)                                        
=============================== ====================================================================================================================

.. tip:: 
	To learn more about concepts like website labels and price lists you can read 
	the `concepts in short page <../../ecManager/introduction/concepts-in-short.html>`__.

If you would like to access any of these settings in your own code, you can do this as follows:

.. code-block:: C#
	:linenos:

	var configProvider = new FundatieConfigurationProvider();
	var config = configProvider.GetConfigSettings();
	var applicationName = config.ApplicationName;

Preferably you should use dependency injection to retrieve an instance of 
``IFundatieConfigurationProvider`` and use that instance to retrieve the configuration values.
This will provide more loose coupling and improved testability.

WebsiteSettings
===============
To allow a user to configure some parts of Fundatie at runtime, we create a navigation item which 
will not be shown to the customers. Fundatie accesses the navigation item with SelectionCode *WSS*. 
This navigation item contains blocks with global settings for the website.The following blocks can
be configured:

* MailTemplate (Install Fundatie.Modules.Mail.Templates)
* SiteParameters (Installed in StarterKit by default)
* SiteFooter (Installed in StarterKit by default)