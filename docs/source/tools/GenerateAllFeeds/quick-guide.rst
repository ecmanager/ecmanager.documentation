###########
Quick guide
###########

Introduction
============
This tool will update all feeds with new data, this tool can be scheduled so feeds will stay up-to-date. 


Configuration
=============
The tool reads the UserName and Token used to perform the update from the configuration. This 
configuration is additional to the `ecManager configuration <../../ecManager/introduction/configuring-ecManager/configuring-the-framework.html>`_:


.. code-block:: XML
	:linenos:

	<appSettings>
		<add key="FeedGeneration.UserName" value="" />
		<add key="FeedGeneration.Token" value="" />
	</appSettings>
    

The GenerateAllFeeds can be run as scheduled task to keep the feeds updated.
	
Permissions
===========
The GenerateAllFeeds requires the following permissions to function:

+-------------+------------+
|   Entity    | Permission |
+=============+============+
| Feed        | Read/Write |
+-------------+------------+


We recommend to create a specific user and role for the tool with only the necessary permissions.
