########
Overview
########

This section contains information about a tool which generates feeds. 
To read more about feeds see: `Feeds <../../ecManager/services/feed/index.html>`_.

Visit the `Quick guide <quick-guide.html>`_ page to read more on how to get GenerateAllFeeds 
application started.