###########
Quick guide
###########

Introduction
============
ecManager.DbUp can be used to perform SQL migrations when updating to a newer version of 
ecManager. This quick guide will guide you through the process of using the tool.

Prerequisites
=============
#) A connection string for the database which requires migrations.
#) Read-access to the ecManager.SqlMigration repository.

Configuration
=============
The configuration of the tool can be set in the ecManager.DbUp.exe.config file. The 
app.format.config can be used as a template. The correct connection string should be used for the
database that needs migrations, and an appSetting called 'filePath' should point to the 
ecManager.SqlMigration repository containing the SQL scripts that will be executed.

There is also an optional appSetting called 'timeout' which is the time in minutes after which the
tool will time out. This defaults to 30 minutes. If a timeout occurs, the entire migration process
is rolled back.

The following configuration can be used as an example:

.. code-block:: XML
	:linenos:

	<connectionStrings>
	  <add name="Main.ConnectionString" connectionString="ConnectionString" providerName="System.Data.SqlClient" />
	</connectionStrings>
	<appSettings>
	  <add key="filePath" value="C:\ecManager.SqlMigration"/>
	  <add key="timeout" value="30"/>
	</appSettings>

Usage
=====
The tool can be used with the following arguments:

.. code-block:: XML

	ecManager.DbUp.exe <connectionString> <filePath> <timeout>

The arguments are only necessary if you have not configured them as described in the previous 
chapter. Providing the arguments will override any configuration.

.. tip:: Always make a back-up of your database before using this tool.

Applied migrations
==================
When using the tool, a new table is added to the database called dbo.SchemaVersions. This table 
contains all the scripts that have been executed. This way, the tool knows which scripts still need
to be executed.
