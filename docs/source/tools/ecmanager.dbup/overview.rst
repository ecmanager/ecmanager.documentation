########
Overview
########

When updating the ecManager framework, occassionally a SQL migration might be required. A tool called 
ecManager.DbUp can be used to perform those migrations. Visit the `Quick guide <quick-guide.html>`_ 
page to read more about how to use the tool.