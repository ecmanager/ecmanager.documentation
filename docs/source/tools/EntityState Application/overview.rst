########
Overview
########

Some entities in ecManager contains EntityState. This section contains information about a tool which updates entity state for multiple entities. 
To read more about entity state see: `Customize Entity State <../../ecManager/how-to/customize-entity-state/index.html>`_.

Visit the `Quick guide <quick-guide.html>`_ page to read more on how to get Entity State application started.