###########
Quick guide
###########

Introduction
============
This tool will update all entity states for the stateful entities. 


Configuration
=============
The tool reads the Labels and Pricelists used to perform the update from the configuration:


.. code-block:: XML
	:linenos:

	<configuration>
	  <configSections>
	    <section name="ecManager.Tools.EntityStateApplication" type="ecManager.Tools.EntityStateApplication.Configuration.EntityStateApplicationConfigurationSection, ecManager.Tools.EntityStateApplication"/>
	    // Other config sections
	  </configSections>
	
	  <ecManager.Tools.EntityStateApplication>
	    <Identity username="MyEntityStateUsername" token="TheToken" />
	    <Labels>
	      <Label Identifier="1"/>
	      <Label Identifier="2"/>
	    </Labels>
	    <PriceLists>
	      <PriceList Identifier="1"/>
	      <PriceList Identifier="2"/>
	    </PriceLists>
		<EntityTypes>
			<EntityType Enabled="True" Name="NavigationItem" Interval="60"/>
			<EntityType Enabled="True" Name="Product" Interval="60"/>
			<EntityType Enabled="True" Name="ProductGroup" Interval="60"/>
			<EntityType Enabled="True" Name="Combination" Interval="60"/>
			<EntityType Enabled="True" Name="MetaTag" Interval="60"/>
			<EntityType Enabled="True" Name="NewsItem" Interval="60"/>
			<EntityType Enabled="True" Name="Promotion" Interval="60"/>
			<EntityType Enabled="True" Name="Voucher" Interval="60"/>
			<EntityType Enabled="True" Name="Block" Interval="60"/>
			<EntityType Enabled="True" Name="Vacancy" Interval="60"/>
		</EntityTypes>
	  </ecManager.Tools.EntityStateApplication>
	</configuration>
    
The EntityTypes part of the configuration allows you to update only specific entity types, for example: only the Products and NavigationItems. 

The Entity state for entities is updated when the entity is saved, by scheduling the Entity State application you can force the entity state to update. The scheduling helps you to prevent that entities which should be offline to be online and vice versa. 

The interval option in the configuration will help you to limit the amount of entities updated by setting a time interval in minutes. The entity state will only be updated if the entity should be online/offline in the last x minutes.

**For example:**

+-------------+------------------------+
|   Entity    | Product 1              |
+=============+========================+
| DataActive  | 2016-05-19 16:00       |
+-------------+------------------------+
| DateInActive| 2016-05-19 19:00       |
+-------------+------------------------+

+------------------+-------------------------------+
|   Update date    | Status                        |
+==================+===============================+
| 2016-05-19 15:55 | Not updated                   |
+------------------+-------------------------------+
| 2016-05-19 16:03 | Updated: In range of interval |
+------------------+-------------------------------+


The EntityStateApplication can be run as scheduled task to keep the state updated.
We recommend to set the refresh interval to the same time (or lower to prevent "forgetting" entities) as the scheduled task runs so all entities are updated.

.. note:: To update the entity state of all entities set the Interval to 0.	
	
Permissions
===========
The Entity State Application requires the following permissions to function:

+-------------+------------+
|   Entity    | Permission |
+=============+============+
| Navigation  | Read       |
+-------------+------------+
| Product     | Read       |
+-------------+------------+
| Block       | Read       |
+-------------+------------+
| Combination | Read       |
+-------------+------------+
| MetaTag     | Read       |
+-------------+------------+
| News item   | Read       |
+-------------+------------+
| Promotion   | Read       |
+-------------+------------+
| Voucher     | Read       |
+-------------+------------+
| BlockType   | Read       |
+-------------+------------+
| EntityState | Read/Write |
+-------------+------------+
| Caching     | Read       |
+-------------+------------+
| ProductGroup| Read       |
+-------------+------------+
| Label       | Read       |
+-------------+------------+
| Vacancy     | Read       |
+-------------+------------+

We recommend to create a specific user and role for the tool with only the necessary permissions.
