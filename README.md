# ecManager.Documentation #

[![Documentation Status](https://readthedocs.com/projects/ecmanager-ecmanagerdocumentation/badge/?version=latest)](http://public.readthedocs.com/docs/ecmanager-ecmanagerdocumentation/en/latest/?badge=latest)

# Compiling the documentation locally #
The documentation is compiled using [Sphinx](http://www.sphinx-doc.org). If you already have Sphinx installed you should be ready to go. If this is not the case, please follow the steps below.

## Setting up your environment ##
1. The first step is confirming you have Python 2.7.x installed, and a that is accessible from the command line.


      *python --version*

      This should output something like:

      *Python 2.7.11*

      Please note that Python versions 3.0 and higher are not compatible with Sphinx.

1. Install the following packages using PIP

      *pip install sphinx*

      *pip install sphinx_rtd_theme*

      These commands should execute without errors.

1. Run the following batch file:
 
      ..\repository_root\docs\Build.bat

      This should take less than a minute. When the batch exists immediatly an error occured. Run the commands manually and diagnose the problem.
